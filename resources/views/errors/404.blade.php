@extends('layout.master', ['pageTitle' => 'Page Not Found'])
@section('content')
<div class="columns-container">
	<div id="columns" class="container">
		
		<div class="row">
			<div id="center_column" class="center_column col-xs-12 col-sm-12">
				<div class="pagenotfound">
					<h1>This page is not available</h1>
					<p>
						We're sorry, but the Web address you've entered is no longer available.
					</p>
					<h3>To find a product, please type its name in the field below.</h3>
					<form action="/search" method="post" class="std">
						<fieldset>
							<div>
								<label for="search_query">Search our product catalog:</label>
								<input id="search_query" name="search_query" type="text" class="form-control grey">
								<button type="submit" name="Submit" value="OK" class="btn btn-default button button-small"><span>Ok</span></button>
							</div>
						</fieldset>
					</form>
					<div class="buttons"><a class="btn btn-default button button-medium" href="/" title="Home"><span><i class="icon-chevron-left left"></i>Home page</span></a></div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection