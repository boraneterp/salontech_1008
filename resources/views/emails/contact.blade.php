@extends('beautymail::templates.minty')

@section('content')
<style>
table[hlitebg='edit'] {
    border-bottom: 1px solid #eee;
}
</style>
    @include('beautymail::templates.minty.contentStart')
        <tr>
            <td class="paragraph">
                Name: {{ $inputs['name'] }}<br />
                Email Address: {{ $inputs['email'] }}<br />
                Inquiry Type: {{ $inputs['type'] }}<br /><br />
            	{!! $inputs['message'] !!}
            </td>
        </tr>
        <tr>
            <td width="100%" height="10"></td>
        </tr>
    @include('beautymail::templates.minty.contentEnd')

@stop