@extends('beautymail::templates.minty')

@section('content')
<style>
.logo {
    width: 120px;
}
table[hlitebg='edit'] {
    border-bottom: 1px solid #eee;
}

.order__total {
    padding: 5px;
    font-size: 12px;
}
.order__total__row { margin: 5px 0; }
.order__total__row .total--amount{ 
    font-weight: bold;
    font-size: 14px; 
}
.logo {
    width: 120px;
}
.row {
    margin-right: -15px;
    margin-left: -15px;
}
.col-sm-6 {
    width: 50%;
    float: left;
    position: relative;
    min-height: 1px;
}

.col-sm-4 {
    width: 33.33333333%;
    float: left;
    position: relative;
    min-height: 1px;
}

.col-sm-8 {
    width: 66.66666667%;
    float: left;
    position: relative;
    min-height: 1px;
}

.title-1 {
    font-size: 16px;
    text-align: center;
}

h4 {
    margin-bottom: 10px;
}

.text-light-black {
    color: #666;
    font-size: 12px;
}

.text-light-black span {
    margin-left: 10px;
}

.text-right {
    text-align: right;
}

.table  {
    width: 100%;
}

.table thead {
    background: #eee;
}

.table td {
    text-align: center;
    font-size: 12px;
    border: 1px solid #ddd;
    color: #666;
    font-size: 12px;
}

.col-xs-10 {
    width: 90%;
    float: left;
    text-align: right;
}

.col-xs-2 {
    width: 10%;
    float: left;
    text-align: right;
}
.item-options {
    margin: 0;
    padding-left: 15px;
    color: #666;
}

.item-options__li {
    margin: 5px 0;
    font-size: 10px;
    color: #666;
}

.item-options__li__title {
    font-weight: bold;
    margin-right: 5px;
    color: #666;
}

.item-options__li__option {
    margin-right: 5px;
    color: #666;
}
</style>
    @include('beautymail::templates.minty.contentStart')
        <tr>
            <td class="title">
                Dear {{$order->shipingAddress()->first_name.' '.$order->shipingAddress()->last_name}}
            </td>
        </tr>
        <tr>
            <td width="100%" height="10"></td>
        </tr>
        <tr>
            <td class="paragraph">
                Thank you for your order with Salon Tech. Check your order status <a href="https://www.salontech.com">here</a>
            </td>
        </tr>
        <tr>
            <td class="paragraph">
                <div class="row billing-details ">
                    <div class="col-sm-6 ">
                        <div class="order-info bg-white text-center clearfix mb-30">
                            <h4 class="title-1 text-center">Order Info</h4>
                            <div class="text-light-black">
                                <div class="row">
                                    <label class="col-sm-4 text-right">Name</label>
                                    <div class="col-sm-8 text-left">
                                        <span>{{$order->shipingAddress()->first_name." ".$order->shipingAddress()->last_name}}
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="text-light-black">
                                <div class="row">
                                    <label class="col-sm-4 text-right">Phone</label>
                                    <div class="col-sm-8 text-left">
                                        <span>{{$order->shipingAddress()->phone_number}}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="text-light-black">
                                <div class="row">
                                    <label class="col-sm-4 text-right">Email</label>
                                    <div class="col-sm-8 text-left">
                                        <span>{{$order->user->email}}</span>
                                    </div>
                                </div>
                            </div>
                            <div class="text-light-black">
                                <div class="row">
                                    <label class="col-sm-4 text-right">Address</label>
                                    <div class="col-sm-8 text-left">
                                        <span>{!! $order->shipingAddress()->fullAddress() !!}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 ">
                        <div class="order-info bg-white text-center clearfix mb-30">
                            <h4 class="title-1 text-center">Payment Info</h4>
                            <div class="text-light-black">
                                <div class="row">
                                    <label class="col-sm-4 text-right">Order #</label>
                                    <div class="col-sm-8 text-left"><span>{{$order->id}}</span></div>
                                </div>
                                @if($order->payment->payment_type === 'card')
                                <div class="row">
                                    <label class="col-sm-4 text-right">Authorization #</label>
                                    <div class="col-sm-8 text-left">{{$order->payment->transaction_id}}</div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-4 text-right">Card Number</label>
                                    <div class="col-sm-8 text-left"><span>************{{substr($order->payment->card_number, -4)}}</span></div>
                                </div>
                                @else
                                <div class="row">
                                    <label class="col-sm-4 text-right">Payment Type</label>
                                    <div class="col-sm-8 text-left">Paypal</div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-4 text-right">Paypal ID</label>
                                    <div class="col-sm-8 text-left">{{$order->payment->transaction_id}}</div>
                                </div>
                                @endif
                                <div class="row">
                                    <label class="col-sm-4 text-right">Order Time</label>
                                    <div class="col-sm-8 text-left"><span>{{$order->created_at}}</span></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td class="paragraph">
                <div class="shop-cart-table check-out-wrap">
                    <h4 class="title-1 title-border text-uppercase mb-10">Order Items</h4>
                    <div class=" table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Image</th>
                                    <th>Product Name</th>
                                    <th>Qty</th>
                                    <th>Price</th>
                                    <th>Total Price</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($order->products as $index => $product)
                                <tr>
                                    <td class="text-center">
                                        <a href="https://www.salontech.com/detail{{$product->detail->url()}}"><img src="{{ $product->detail->image }}" width="50" height="50" /></a>
                                    </td>
                                    <td class="text-center">
                                        <strong>
                                            <a href="https://www.salontech.com/detail{{$product->detail->url()}}">{!! $product->detail->product_name !!}</a>
                                        </strong>
                                    </td>
                                    <td class="text-center">{{ $product->quantity }}</td>
                                    <td class="text-center">${{ sprintf('%01.2f', $product->price)}}</td>
                                    <td class="text-center">${{ sprintf('%01.2f', $product->total_price)}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </td>
            </tr>
            <tr>
                <td width="100%" height="25"></td>
            </tr>
            <tr>
                <td class="paragraph">
               <section class="order__total">
                    <div class="row order__total__row">
                        <label>Sub Total: </label> <span> ${{ sprintf('%01.2f', $order->sub_total)}}</span>
                    </div>
                    @if(!empty($order->coupon_code))
                    <div class="row order__total__row">
                        <label>Promotion: </label><span style="color:green;">-${{ sprintf('%01.2f', $order->promotion)}}</span>
                    </div>
                    @endif
                    <div class="row order__total__row">
                       <label>SHIPPING &amp; HANDLING: </label><span> ${{ sprintf('%01.2f', $order->shipping_price)}}</span>
                    </div>
                    <div class="row order__total__row">
                        <label>Tax: </label><span> ${{ sprintf('%01.2f', $order->tax)}}</span>
                    </div>
                    <div class="row order__total__row">
                        <label class="total--amount">Total Amount: </label><span class="total--amount"> ${{ sprintf('%01.2f', $order->charged)}}</span>
                    </div>
                </section>
            </td>
        </tr>
        <tr>
            <td width="100%" height="25"></td>
        </tr>
        <tr>
            <td class="paragraph">
                <p>If you have any questions, please contact us at <a href="mailto:salontech@salontech.com">salontech@salontech.com</a>.</p>
                <p>Thank you</p>

                <p>Sincerely,</p>

                <p>Salon Tech</p>
            </td>
        </tr>
        <tr>
            <td width="100%" height="25"></td>
        </tr>
    @include('beautymail::templates.minty.contentEnd')

@stop