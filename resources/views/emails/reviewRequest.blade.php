@extends('beautymail::templates.minty')

@section('content')
<style>
.logo {
    width: 120px;
}
table[hlitebg='edit'] {
    border-bottom: 1px solid #eee;
}

.order__total {
    padding: 5px;
    font-size: 12px;
}
.order__total__row { margin: 5px 0; }
.order__total__row .total--amount{ 
    font-weight: bold;
    font-size: 14px; 
}
.logo {
    width: 120px;
}
.row {
    margin-right: -15px;
    margin-left: -15px;
}
.col-sm-6 {
    width: 50%;
    float: left;
    position: relative;
    min-height: 1px;
}

.col-sm-4 {
    width: 33.33333333%;
    float: left;
    position: relative;
    min-height: 1px;
}

.col-sm-8 {
    width: 66.66666667%;
    float: left;
    position: relative;
    min-height: 1px;
}

.title-1 {
    font-size: 16px;
    text-align: center;
}

h4 {
    margin-bottom: 10px;
}

.text-light-black {
    color: #666;
    font-size: 12px;
}

.text-light-black span {
    margin-left: 10px;
}

.text-right {
    text-align: right;
}

.table  {
    width: 100%;
}

.table thead {
    background: #eee;
}

.table td {
    text-align: center;
    font-size: 12px;
    border: 1px solid #ddd;
    color: #666;
    font-size: 12px;
}

.col-xs-10 {
    width: 90%;
    float: left;
    text-align: right;
}

.col-xs-2 {
    width: 10%;
    float: left;
    text-align: right;
}
.item-options {
    margin: 0;
    padding-left: 15px;
    color: #666;
}

.item-options__li {
    margin: 5px 0;
    font-size: 10px;
    color: #666;
}

.item-options__li__title {
    font-weight: bold;
    margin-right: 5px;
    color: #666;
}

.item-options__li__option {
    margin-right: 5px;
    color: #666;
}
</style>
    @include('beautymail::templates.minty.contentStart')
        <tr>
            <td class="title">
                Dear {{$order->shipingAddress()->first_name.' '.$order->shipingAddress()->last_name}}
            </td>
        </tr>
        <tr>
            <td width="100%" height="10"></td>
        </tr>

        <tr>
            <td class="paragraph">
                <div class="row billing-details ">
                    <div class="col-sm-12 ">
                        <div class="order-info bg-white text-center clearfix mb-30">
                            <div class="text-light-black">
                                <div class="row">
                                    <label class="col-sm-12 text-center">Your opinion matters to us</label>
                                    <div class="col-sm-12 text-center">
                                        <span>Join the conversation and tell us what you think about the product you recently purchased.
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td class="paragraph">
                <div class="shop-cart-table check-out-wrap">
                    <h4 class="title-1 title-border text-uppercase mb-10">Order Items</h4>
                    <div class=" table-responsive">
                        <table class="table">
                            <tbody>
                                @foreach($order->products as $index => $product)
                                <tr>
                                    <td class="text-center">
                                        <a href="https://www.salontech.com/detail{{$product->detail->url()}}"><img src="{{ $product->detail->image }}" width="50" height="50" /></a>
                                    </td>
                                    <td class="text-center">
                                        <strong>
                                            <a href="https://www.salontech.com/detail{{$product->detail->url()}}">{!! $product->detail->product_name !!}</a>
                                        </strong>
                                    </td>
                                    <td class="text-center"><a href="https://www.salontech.com/detail{{$product->detail->url()}}#reviewForm" target="_blank"><button>Leave a reveiw</button></a></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </td>
            </tr>
            <tr>
                <td width="100%" height="25"></td>
            </tr>
        <tr>
            <td width="100%" height="25"></td>
        </tr>
        <tr>
            <td class="paragraph">
                <p>If you have any questions, please contact us at <a href="mailto:salontech@salontech.com">salontech@salontech.com</a>.</p>
                <p>Thank you</p>

                <p>Sincerely,</p>

                <p>Salon Tech</p>
            </td>
        </tr>
        <tr>
            <td width="100%" height="25"></td>
        </tr>
    @include('beautymail::templates.minty.contentEnd')

@stop