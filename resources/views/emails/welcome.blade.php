@extends('beautymail::templates.minty', ['css' => '.logo {width:200px;}'])

@section('content')
<style>
.logo {
    width: 120px;
}
table[hlitebg='edit'] {
    border-bottom: 1px solid #eee;
}
</style>
    @include('beautymail::templates.minty.contentStart')
        <tr>
            <td class="title">
                Dear {{$user->first_name.' '.$user->last_name}}
            </td>
        </tr>
        <tr>
            <td width="100%" height="10"></td>
        </tr>
        <tr>
            <td class="paragraph">
                Thank you for registering with <a href="http://www.salontech.com">www.salontech.com</a>.
            </td>
        </tr>
        <tr>
            <td width="100%" height="25"></td>
        </tr>
        <tr>
            <td class="paragraph">
                <p>If you have any questions, please contact us at <a href="mailto:salontech@salontech.com">salontech@salontech.com</a>.</p>
                <p>Thank you</p>

                <p>Sincerely,</p>

                <p>Salon Tech</p>
            </td>
        </tr>
        <tr>
            <td width="100%" height="25"></td>
        </tr>
    @include('beautymail::templates.minty.contentEnd')

@stop