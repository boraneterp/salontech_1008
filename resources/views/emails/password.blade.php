@extends('beautymail::templates.minty', ['css' => '.logo {width:200px;}'])

@section('content')
<style>
table[hlitebg='edit'] {
    border-bottom: 1px solid #eee;
}
</style>
    @include('beautymail::templates.minty.contentStart')
        <tr>
            <td class="title">
                Dear {{$user->first_name.' '.$user->last_name}}
            </td>
        </tr>
        <tr>
            <td width="100%" height="10"></td>
        </tr>
        <tr>
            <td class="paragraph">
                You recently requested password reset. If you'd like to reset your password, please use the link below: 
            </td>
        </tr>
        <tr>
            <td width="100%" height="10"></td>
        </tr>
        <tr>
            <td>
                @include('beautymail::templates.minty.button', ['text' => 'Reset Password', 'link' => getenv('APP_URL').'/reset_password/'.$user->password_token])
            </td>
        </tr>
        <tr>
            <td width="100%" height="10"></td>
        </tr>
        <tr>
            <td class="paragraph">
                <p>If you have any questions, please contact us at <a href="mailto:salontech@salontech.com">salontech@salontech.com</a>.</p>
                <p>Thank you</p>

                <p>Sincerely,</p>

                <p>Salon Tech</p>
            </td>
        </tr>
        <tr>
            <td width="100%" height="25"></td>
        </tr>
    @include('beautymail::templates.minty.contentEnd')

@stop