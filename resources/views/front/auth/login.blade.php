@extends('layout.master', ['pageTitle' => 'Login'])
@section('content')
<div class="labbreadcrumb">
	<div class="container">
		<div class="breadcrumb-i">
			<div class="breadcrumb clearfix">
				<a class="home" href="/" title="Return to Home">Home</a>
				<span class="navigation-pipe">&gt;</span>
				Login
			</div>
		</div>
	</div>
</div>
<div class="columns-container">
	<div class="container">
		<form action="/login" method="post" id="loginForm">
			{!! csrf_field() !!}
			{!! Form::hidden('redirectUrl', app('request')->input('redirectUrl'))!!}
			<div class="row">
				<div class="col-sm-6 col-sm-offset-3">
					<h1 class="page-heading">Login</h1>
					@foreach ($errors->all() as $error)
					<div class="alert alert-danger fade in" >{{$error}}</div>
					@endforeach
					<div class="form-group">
						<label >Email Address</label>
						{!! Form::email('email', '', ['class' => 'form-control required', 'placeholder' => 'Email Address']) !!}
					</div>
					<div class="form-group">
						<label >Password</label><a href="#" class="label-hint" data-toggle="modal" data-target="#forgot_password" >Forgot Password?</a>
						{!! Form::password('password', ['class' => 'form-control required', 'placeholder' => 'Password', 'minlength' => '8']) !!}
					</div>
					<div class="form-group">
						Don't have account? &nbsp;&nbsp;<a href="/register" >Register</a>
					</div>
					<div class="form-group">
						<a href="/" class="button button-default btn-sm pull-right" >Back</a>
						<button type="submit" class="button btn-submit btn-sm pull-right login-btn" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Login">Login</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
<div class="modal fade" id="forgot_password" tabindex="-1" role="dialog" aria-labelledby="forgot_password" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content modal-popup">
			<a href="#" class="close-link pull-right" data-dismiss="modal" aria-label="Close">
				<i class="fa fa-times"></i>
			</a>
			<form action="/forgot_password" method="post" class="popup-form" id="passwordForm">
				{!! csrf_field() !!}
				<h4>Please enter your email address.</h4>
				<div class="alert alert-danger fade in" style="display:none;">
					Sorry!!! The email that you provided is not match our record.
				</div>
				<div class="alert alert-success fade in" style="display:none;">
					A password reset link has been sent to your email address.
				</div>
				<div class="form-group">
					<label >Email Address</label>
					<input type="email" name="email" class="form-control required" placeholder="Email Address">
				</div>
				<div class="form-group">
					<button type="submit" class="button btn-submit btn-sm btn-full" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Sending Email">Submit</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection