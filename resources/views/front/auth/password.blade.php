@extends('layout.master', ['pageTitle' => 'Reset Password'])
@section('content')
<div class="labbreadcrumb">
	<div class="container">
		<div class="breadcrumb-i">
			<div class="breadcrumb clearfix">
				<a class="home" href="/" title="Return to Home">Home</a>
				<span class="navigation-pipe">&gt;</span>
				Reset Password
			</div>
		</div>
	</div>
</div>
<div class="columns-container">
	<div class="container">
		<form action="/set_password" method="post" class="popup-form" id="resetForm">
			{!! csrf_field() !!}
			{!! Form::hidden('passwordToken', $passwordToken)!!}
			<div class="row">
				<div class="col-sm-6 col-sm-offset-3">
					<h1 class="page-heading">Reset Password</h1>
					@foreach ($errors->all() as $error)
					<div class="alert alert-danger fade in" >{{$error}}</div>
					@endforeach
					<div class="form-group">
						<label >Password</label>
						{!! Form::password('password', ['class' => 'form-control required', 'placeholder' => 'Password', 'minlength' => '8']) !!}
					</div>
					<div class="form-group">
						<label >Confirm Password</label>
						{!! Form::password('confirmation', ['class' => 'form-control required', 'placeholder' => 'Confirm Password', 'minlength' => '8']) !!}
					</div>
					<div class="form-group">
						<a href="/" class="button button-default btn-sm pull-right" >Cancel</a>
						<button type="submit" class="button btn-submit btn-sm pull-right login-btn" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Save">Save</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
@endsection