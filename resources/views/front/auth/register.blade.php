@extends('layout.master', ['pageTitle' => 'Register'])
@section('content')
<div class="labbreadcrumb">
	<div class="container">
		<div class="breadcrumb-i">
			<div class="breadcrumb clearfix">
				<a class="home" href="/" title="Return to Home">Home</a>
				<span class="navigation-pipe">&gt;</span>
				Register
			</div>
		</div>
	</div>
</div>
<div class="columns-container">
	<div class="container">
		<h1 class="page-heading">Register</h1>
		<form action="/register" method="post" id="registerForm">
			{!! Form::open(array('route' => 'google.post-recaptcha-validation','method'=>'POST','files'=>true,'id'=>'myform')) !!}
			{!! csrf_field() !!}
			<div class="alert alert-danger fade in" style="display:none;">
				Sorry!!! We can't register with information you provided, please try again.
			</div>
			<fieldset>
				<legend>Basic Information</legend>
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
							<label >Account Type</label>
							<div class="checkbox-list">
								<div class="checkbox">
									<label><div class="radio-wrapper checked">{!! Form::radio('type', 'personal', ['class' => 'form-control']) !!} </div> Personal</label>
								</div>
								<div class="checkbox">
									<label><div class="radio-wrapper ">{!! Form::radio('type', 'professional', ['class' => 'form-control']) !!} </div> Professional</label>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-4">
						<div class="form-group">
							<label >First Name</label>
							{!! Form::text('first_name', '', ['class' => 'form-control required', 'placeholder' => 'First Name']) !!}
						</div>
					</div>
					<div class="col-sm-4">
						<div class="form-group">
							<label >Last Name</label>
							{!! Form::text('last_name', '', ['class' => 'form-control required', 'placeholder' => 'Last Name']) !!}
						</div>
					</div>
					<div class="col-sm-4">
						<div class="form-group">
							<label >Email Address</label>
							{!! Form::email('email', '', ['class' => 'form-control required', 'placeholder' => 'Email Address']) !!}
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
							<label >Password</label>
							{!! Form::password('password', ['class' => 'form-control required', 'placeholder' => 'Password', 'minlength' => '8']) !!}
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
							<label >Confirm password</label>
							{!! Form::password('confirmation', ['class' => 'form-control', 'placeholder' => 'Confirm password']) !!}
						</div>
					</div>
				</div>
			</fieldset>
			<fieldset class="business-detail">
				<legend>Business Information</legend>
				<div class="row">
					<div class="col-sm-4">
						<div class="form-group">
							<label >Company Name</label>
							{!! Form::text('company', '', ['class' => 'form-control required', 'placeholder' => 'Company Name']) !!}
						</div>
					</div>
					<div class="col-sm-4">
						<div class="form-group">
							<label >Cosmetology Id</label>
							{!! Form::text('cosmetology_id', '', ['class' => 'form-control required', 'placeholder' => 'Cosmetology Id']) !!}
						</div>
					</div>
					<div class="col-sm-4">
						<div class="form-group">
							<label >Phone Number</label>
							{!! Form::text('phone_number', '', ['class' => 'form-control phone-number required', 'placeholder' => 'Phone Number']) !!}
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-8">
						<div class="form-group">
							<label >Address 1</label>
							{!! Form::text('address_1', '', ['class' => 'form-control required', 'placeholder' => 'Address 1']) !!}
						</div>
					</div>
					<div class="col-sm-4">
						<div class="form-group">
							<label >Address 2</label>
							{!! Form::text('address_2', '', ['class' => 'form-control', 'placeholder' => 'Address 2']) !!}
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-4">
						<div class="form-group">
							<label >City</label>
							{!! Form::text('city', '', ['class' => 'form-control required', 'placeholder' => 'City']) !!}
						</div>
					</div>
					<div class="col-sm-4">
						<div class="form-group">
							<label >State</label>
							{!! Form::select('state', ['' => 'Choose State'] + Util::getStates()->all(), '', ['class' => 'required form-control']) !!}
						</div>
					</div>
					<div class="col-sm-4">
						<div class="form-group">
							<label >Zip Code</label>
							{!! Form::text('zip', '', ['class' => 'form-control required', 'placeholder' => 'Zip Code']) !!}
						</div>
					</div>
				</div>
			</fieldset>
			<div class="form-group">
				<p>By clicking Register, you agree to our <a href="/terms-and-conditions" target="_blank" class="register__model--link">Terms & Conditions</a>.</p>
			</div>
			<div class="form-group text-right captcha">
				{!! app('captcha')->display() !!}
				{!! $errors->first('g-recaptcha-response', '<p class="alert alert-danger">:message</p>') !!}
				<button type="submit" class="button btn-submit btn-sm" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Register">Register</button>
				<a href="/" class="button button-default btn-sm" >Back</a>
			</div>
			{!! Form::close() !!}
		</form>
	</div>
</div>
@endsection