@extends('layout.master', ['pageTitle' => strip_tags($product->product_name)])
@section('content')
<div class="labbreadcrumb">
    <div class="container">
        <div class="breadcrumb-i">
            <!-- Breadcrumb -->
            <div class="breadcrumb clearfix">
                <a class="home" href="/" title="Return to Home">Home</a>
                <span class="navigation-pipe">&gt;</span>
                <span class="navigation_page">
                    <span itemscope itemtype="http://data-vocabulary.org/Breadcrumb">
                        <a itemprop="url" href="/{{$product->category->seo_url}}" title="Women" ><span itemprop="title">{!! $product->category->name !!}</span></a>
                    </span>
                    <span class="navigation-pipe">></span>
                    {!! $product->product_name !!}
                </span>
            </div>
        </div>
    </div>
</div>
<div class="columns-container">
    <div class="container">
        <div itemscope itemtype="https://schema.org/Product">
            <meta itemprop="url" content="{{$product->url()}}">
            <div class="primary_block row">
                <div class="col-sm-2 col-md-1 ">
                    <ul class="thumbnail-container">
                        @foreach($product->images as $index => $image)
                        <li>
                            <a href="{{$image->image_src}}" data-type="I" class="{{$index === 0? 'active':''}}" title="{{strip_tags($product->product_name)}}">
                                <picture>
                                    <source media="(max-width: 768px)" srcset="{{$image->image_src}}">
                                    <img class="img-responsive" src="{{Util::getImageWithSuffix($image->image_src, 'xs')}}" alt="{{strip_tags($product->product_name)}}" title="{{strip_tags($product->product_name)}}" itemprop="image" />
                                </picture>
                                
                            </a>
                        </li>
                        @endforeach
                        @if(!empty($product->video))
                        <li>
                            <div class="video-container">
                                 <video controls  preload="auto">
                                    <source src="{{$product->video}}" type="video/mp4" />
                                </video>
                            </div>
                            <a href="#" data-type="V" class="video" >
                                <img src ="/assets/img/video-thumb.jpg">
                                <!--<i class="far fa-play-circle" aria-hidden="true"></i>-->
                            </a>
                        </li>
                        @endif
                    </ul>
                </div>
                <div class="col-sm-6">
                    @if(!empty($product->video))
                    <div class="video-container main-video clearfix">
                        <video width="100%" loop="true"  >
                            <source src="{{$product->video}}" type="video/mp4">
                        </video>
                    </div>
                    @endif
                    <div class="main-image-container clearfix hidden-xs">
                        <a href="{{$product->image}}">
                        <img itemprop="image" src="{{$product->image}}" title="{{strip_tags($product->product_name)}}" alt="{{strip_tags($product->product_name)}}" /></a>
                    </div>
                </div>
                <div class="col-sm-4 col-md-5">
                    <h1 class="product-detail-name" itemprop="name">{!! $product->product_name !!}</h1>
                    @include('front.includes.review')
                    @if($product->professional_only == 'N' || (Auth::check() && Auth::user()->type == 'business'))
                        @if($product->stock > 0)
                        <span id="availability_value" class="label label-success">In stock</span>
                        @else
                       <span id="availability_value" class="label label-success">Out of stock</span>
                        @endif
                        @if($product->stock < 10)
                        <p class="warning_inline" id="last_quantities" >Warning: Low Inventory!</p>
                        @endif
                    @endif
                    <div class="rte align_justify" itemprop="description">
                        {!! $product->description !!}
                    </div>
                    @if($product->professional_only == 'N' || (Auth::check() && Auth::user()->type == 'business'))
                    <div class="content_prices clearfix">
                        <p class="our_price_display">
                            <span itemprop="price" class="price ">${{ sprintf('%01.2f', $product->currentPrice())}}</span>
                            @if($product->currentPrice() != $product->msrp)
                            <span class="old-price product-price">
                                ${{ sprintf('%01.2f', $product->msrp) }}
                            </span>
                            @endif
                        </p>
                    </div>
                    @endif
                    @if($product->professional_only == 'Y' && (!Auth::check() || Auth::user()->type != 'business'))
                    <div style="font-weight: bold" class="professional-label">For Professional Only</div>
                    @endif
                    @if($product->stock > 0 && ($product->professional_only == 'N' || ($product->professional_only == 'Y' && Auth::check() && Auth::user()->type == 'business')))
                    <div class="box-info-product">
                        <label for="quantity_wanted">Quanty:</label>
                        <div class="qty-ii">
                            <a href="#" class="btn btn-default " data-dir="minus">
                                <span><i class="fa fa-minus"></i></span>
                            </a>
                            <input  name="qty" class="quantity" value="1"  />
                            <a href="#" class="btn btn-default " data-dir="plus">
                                <span><i class="fa fa-plus"></i></span>
                            </a>
                        </div>
                    </div>
                    <div class="clearfix ">
                        <button type="button" class="button btn-cart single-cart" data-product_id="{{$product->id}}"><span><i class="icon icon_bag_alt"></i> Add to cart</span>
                        </button>
                    </div>
                    @endif
                </div>
            </div>
        </div>
        <section class="lablistproducts grid ">
            <p class="title_block">
                <span>Related Products</span>
            </p>
            <div class="block products_block row">
                <div class="block_content ">
                    <div class="pos-Accessories slick-container">
                        @foreach($product->recommendation() as $p)
                        <div  class="item-inner">
                            @include('front.includes.product', ['product' => $p])
                        </div>
                        @endforeach
                    </div>
                </div>
                <div class="lab_boxnp">
                    <a class="prev prevac"><i class="icon icon-angle-left"></i></a>
                    <a class="next nextac"><i class="icon icon-angle-right"></i></a>
                </div>
            </div>
        </section>
        <section class="review-container">
            <p class="title_block">
                <span>Product Reviews</span>
            </p>
            @if($product->review_count > 0)
            <div class="review-wrapper">
                <h3>There are {{$product->review_count}} reviews about this product</h3>
                @foreach($product->activeReviews() as $review)
                <div class="review-container">
                    <h5>{{$review->user->first_name.' '.$review->user->last_name}}</h5>
                    <small> - {{date('F j, Y', strtotime($review->created_at))}} -</small>
                    <span class="review-rating">
                        <i class="fa fa-star voted"></i>
                        <i class="{{$review->rating >= 2? 'fa fa-star': ($review->rating == 1? 'fa fa-star-o': ' fa fa-star-half-o')}} {{$review->rating > 1? 'voted':''}}"></i>
                        <i class="{{$review->rating >= 3? 'fa fa-star': ($review->rating <= 2? 'fa fa-star-o': ' fa fa-star-half-o')}} {{$review->rating > 2? 'voted':''}}"></i>
                        <i class="{{$review->rating >= 4? 'fa fa-star': ($review->rating <= 3? 'fa fa-star-o': ' fa fa-star-half-o')}} {{$review->rating > 3? 'voted':''}}"></i>
                        <i class="{{$review->rating == 5? 'fa fa-star': ($review->rating <= 4? 'fa fa-star-o': ' fa fa-star-half-o')}} {{$review->rating > 4? 'voted':''}}"></i>
                    </span>
                    <p>{{$review->content}}</p>
                </div>
                @endforeach
            </div>
            @endif
            <div class="post-review-container">
                @if(Auth::check())
                <h4>Leave a comment</h4>
                @else
                <h5 style="margin-top: 50px;">Please <a href="/login">login</a> to leave a comment</h5>
                @endif
                @if(Auth::check())
                <form id="reviewForm" action="/products/review" method="post" >
                    <input type="hidden" name="product_id" value="{{$product->id}}">
                    <div class="alert alert-success fade in review--alert" >
                        Thanks for posting review here, we'll post on the site after review.
                    </div>
                    <select id="rating" name="rating">
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4" selected="selected">4</option>
                        <option value="5">5</option>
                    </select>
                    <div class="review__box">
                        <textarea name="review" class="form-control autosize required" placeholder="Leave a review" ></textarea>
                    </div>
                     <button type="submit" class="button btn-submit" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Submit">Leave a review</button>
                </form>
                @endif
            </div>
        </section>
    </div>
</div>
@endsection