@extends('layout.master', ['pageTitle' => strip_tags($category->name)])
@section('content')
<div class="labbreadcrumb">
	<div class="container">
		<div class="breadcrumb-i">
			<div class="breadcrumb clearfix">
				<a class="home" href="/" title="Return to Home">Home</a>
				<span class="navigation-pipe">&gt;</span>
				<span class="navigation_page">
					{!! $category->name !!}
					@if($sub_category["id"] !== '0')
						/ {!! $sub_category["name"] !!}
			 		@endif
				</span>
			</div>
		</div>
	</div>
</div>
@if($sub_category["id"] == '0')
<div>
	{!! $category->banner_content !!}
</div>
@endif
<form method="get" id="filterForm">
	<div class="columns-container">
		<div class="container">
			<h1 class="page-heading">
			
			{!! $category->name !!}
			 @if($sub_category["id"] !== '0')
				- <span class="text-primary">{!! $sub_category["name"] !!}</span>
			 @endif

			</h1>
			<div class="row">
				<div class="col-sm-3">
					<div class="filter">
						<section class="filter__group">
							<div class="filter__group__label">Category</div>
							<ul>
								@foreach(Util::getCategories() as $cat)
								<li >
									@if($category->id === $cat->id)
									
									<span>{!! $cat->name !!}</span>

										@if($cat->id == '48')

										<ul>
												@foreach($cat->subcategory as $index => $subcat)
												  
												   @if($sub_category["id"] === $subcat->id)
												   <li><span>{!! $subcat->name !!}</span></li>
												   @else
												   <li><a href="/{{$cat->seo_url}}/{{$subcat->seo_url}}"> {{$subcat->name}}</a></li>
												   @endif
													
												@endforeach
											</ul>
										
										@endif


									@else
										@if($cat->id == '48')
											<a href="/{{$cat->seo_url}}/strip-lash">{!! $cat->name !!}</a>
										@else
											<a href="/{{$cat->seo_url}}">{!! $cat->name !!}</a>
										@endif

									
									@endif
								</li>
								@endforeach
								{{--}}@foreach(App\Models\Category::with('subcategory')
                                ->where('parent_id',0)->get() as $cat)
									@if($cat->subcategory->count()>0)
										<li>
											<a href="/{{$cat->seo_url}}"><h4>{{$cat->name}}</h3></a>
											@foreach($cat->subcategory as $index => $subcat)
												<ul>
													<a href="/{{$cat->seo_url}}/{{$subcat->seo_url}}">
														{{$subcat->name}}</a>
													@foreach($subcat->children as $index => $subsubcat)
														<ul>
															<li><a href="/{{$cat->seo_url}}/{{$subcat->seo_url}}/{{$subsubcat->seo_url}}">
																	--{{$subsubcat->name}}</a></li>
														</ul>
													@endforeach
												</ul>
											@endforeach
										</li>
									@else
										<li>
											<a href="/{{$cat->seo_url}}/{{$cat->name}}">
												<span>{{$cat->name}}</span></a>
										</li>
									@endif
								@endforeach --}}

							</ul>
						</section>
					</div>
				</div>
				<div class="col-sm-9">
					@include('front.includes.toolbar', ['count' => sizeof($products), 'position' => ''])
					<ul class="product_list lablistproducts list row">
						@foreach($products as $product)
						<li class="col-xs-6 col-sm-12 item">
							<div class="product-container" itemscope itemtype="https://schema.org/Product">
								<div class="product-container__item row" >
									<div class="left-block col-sm-4">
										<div class="product-image-container">
											<a class= "product_image" href="/detail{{$product->url()}}" title="{{strip_tags($product->product_name)}}">
												@foreach($product->images as $index => $image)
												@if($index > 1)
												@break;
												@endif
												<img class="img-responsive img{{$index+1}}" src="{{Util::getImageWithSuffix($image->image_src, 'md')}}" alt="{{strip_tags($product->product_name)}}" />
												@endforeach
											</a>
										</div>
									</div>
									<div class="right-block col-sm-8">
										<h5 itemprop="name">

									
										<a class="product-name" href="/detail{{$product->url()}}" title="{{strip_tags($product->product_name)}}" itemprop="url" >
										{!! $product->product_name !!}
										</a>

										
										
										
										</h5>
										@include('front.includes.review')
										<p class="product-desc" itemprop="description">
											{!! str_limit(strip_tags($product->description), 300) !!}
										</p>
										@if($product->professional_only == 'Y' && (!Auth::check() || Auth::user()->type != 'business'))
					                    <div style="font-weight: bold" class="professional-label-list">For Professional Only</div>
					                    @endif
					                    @if($product->stock > 0 && ($product->professional_only == 'N' || ($product->professional_only == 'Y' && Auth::check() && Auth::user()->type == 'business')))
										<div class="row">
											<div class="col-xs-8">
												<div class="content_price">
													<span itemprop="price" class="price product-price">${{ sprintf('%01.2f', $product->currentPrice())}}</span>
													@if($product->currentPrice() != $product->msrp)
													<span class="old-price product-price">
														${{ sprintf('%01.2f', $product->msrp) }}
													</span>
													@endif
												</div>
											</div>
											<div class="col-xs-4">
												<div class="lab-cart text-right">
													<a class="button btn-cart add-to-cart" href="#"
														data-product_id="{{$product->id}}"
														title="Add to cart" >
														<i class="icon icon_bag_alt"></i>
														<span>Add to cart</span>
													</a>
												</div>
											</div>
										</div>
										@endif
									</div>
								</div>
							</div>
						</li>
						@endforeach
					</ul>
					{{$products->links()}}
					@include('front.includes.toolbar', ['count' => sizeof($products), 'position' => 'bottom'])
				</div>
			</div>
		</div>
	</div>
</form>
@endsection