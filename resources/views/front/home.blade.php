@extends('layout.master')
@section('content')

<section class="banner-videos" style="background-image: url({{$homeSetting->mobile_main_image}});">

  <div class="video-wrap" width="100%" height="100%">

    <video autoplay muted>
        
       <source src="{{$homeSetting->main_video}}" type="video/mp4" >
    </video>
</div>
 <div class="video-overlays">

    <img src="{{$homeSetting->mobile_main_image}}" width="100%" height="100%">
 </div>
</section>
<section class="feature-product">
    <img src="{{$homeSetting->featured_image_1}}"  />
    <div class="feature-product__content">
        <h2 class="feature-product__heading">{{$homeSetting->title_1}}</h2>
        <a class="feature-product__button" href="{{$homeSetting->link_1}}">SHOP NOW <i class="fa fa-chevron-right"></i></a>
    </div>
</section>
<section class="feature-product">
    <img src="{{$homeSetting->featured_image_2}}"  />
    <div class="feature-product__content">
        <h2 class="feature-product__heading">{{$homeSetting->title_2}}</h2>
        <a class="feature-product__button" href="{{$homeSetting->link_2}}">SHOP NOW <i class="fa fa-chevron-right"></i></a>
    </div>
</section>
<div class="container">
    <div class="row">
        <div class="lab_static">
            <div class="col-lg col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="img"><a href="{{$homeSetting->about_us_link}}" title=""> <img alt="About Us" src="{{$homeSetting->about_us_image}}"> </a></div>
            </div>
            <div class="col-lg col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="img"><a href="{{$homeSetting->review_youtube_link}}" class="popup-youtube"> <img alt="Review" src="{{$homeSetting->review_image}}"> </a></div>
            </div>
        </div>
    </div>
</div>
@include('front.includes.instagram')
@if(is_array($videos))
<div class="social-container">
    <div class="container">
        <h3><a href="https://www.youtube.com/user/salontechny/videos" target="_blank">FOLLOW US ON YOUTUBE</a></h3>
        <ul class="row">
            @foreach($videos as $video)
            <li class="col-xs-6">
                <div class="videoWrapper">
                    <iframe width="100%" src="https://www.youtube.com/embed/{{$video->id->videoId}}" frameborder="0" allowfullscreen></iframe>
                </div>
            </li>
            @endforeach
        </ul>
    </div>
</div>
@endif


<div class="look-book-container">
    <div class="container">
        <p class='title_block'>
            <span>Look Book</span>
        </p>
        <div class="row">
            @foreach($lookbooks as $lookbook)
            <div class="col-sm-4">
                 <div class="image_blog ">
                    <a href="/lookbooks/{{$lookbook->seo_url}}">
                        <img alt="{{$lookbook->name}}" class="feat_img_small" src="{{Util::getImageWithSuffix($lookbook->image_src, 'sm')}}">
                    </a>
                </div>
                <h4><a href="/lookbooks/{{$lookbook->seo_url}}">{{$lookbook->name}}</a></h4>
            </div>
            @endforeach
        </div>
    </div>
</div>
<div class="popup-content animated zoomIn" id="newsletter-popup">
    <div class="overlay-bg-popup"></div>
    <div class="popup-content-wrapper">
        <a class="close-popup" href="#"><i class="fa fa-times"></i></a>
        <div class="popup-container">
            <div class="row">
                <div class="col-md-6">
                    <img src="/assets/img/newsletter.jpg" class="img-responsive" />
                </div>
                <div class="col-md-6">
                    <div class="popup-content-text">
                        <h3>SIGN UP FOR EMAIL</h3>
                        <p>Don't miss your chance to receive members only benefits! Be the first to know about our exclusive product news and deals!</p>
                    </div>
                    <div class="newletter-form">
                        <form id="subscriptionForm" class="mc4wp-form" action="/subscribe" method="post" >
                            <div class="mc4wp-form-fields">
                                <div class="alert alert-success fade in" style="display: none;">You've successfully subscribed salontech.com.</div>
                                <div class="form-group">
                                    <input type="email"  class="form-control required" name="email_newsletter" placeholder="Enter Your Email"  />
                                 </div>
                                <div class="form-group">
                                    <button type="submit" class="button btn-submit" data-loading-text="<i class='fa fa-spinner fa-spin'></i> SIGN UP">SIGN UP</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <label class="not-again"><input type="checkbox" value="1" name="not-again" />Do not show this pop-up again</label>
    </div>
</div>
@endsection