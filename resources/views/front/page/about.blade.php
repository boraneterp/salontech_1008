@extends('layout.master', ['pageTitle' => 'About Us'])
@section('content')
<div class="labbreadcrumb">
	<div class="container">
		<div class="breadcrumb-i">
			<div class="breadcrumb clearfix">
				<a class="home" href="/" title="Return to Home">Home</a>
				<span class="navigation-pipe">&gt;</span>
				<span class="navigation_page">About Us</span>
			</div>
		</div>
	</div>
</div>
<div class="columns-container paragraph">
	<div class="container">
		<h1 class="page-heading">About Us</h1>
		<div class="blockPosition">
			<div class="lab_static">
				<img alt="images" alt="About Us" src="/assets/img/about-us.jpg" />
			</div>
		</div>
		<p>Salontech Provides innovative tools which are tailor made to your styling needs. We constantly strive to give passionate stylists the opportunity to perform at their best, with the best. With a strong belief of “going back to the basics”, Salontech® is striving to master the basics, with a hint of something new in every tool we make. Seeking ways to improve your daily ritual, we focus on the ergonomic precision with the perfect blend of performance that equals to your stunning results.</p>
	</div>
</div>
@endsection