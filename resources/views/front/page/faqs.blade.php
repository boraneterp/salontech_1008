@extends('layout.master', ['pageTitle' => 'FAQs'])
@section('content')
<div class="labbreadcrumb">
	<div class="container">
		<div class="breadcrumb-i">
			<div class="breadcrumb clearfix">
				<a class="home" href="/" title="Return to Home">Home</a>
				<span class="navigation-pipe">&gt;</span>
				<span class="navigation_page">FAQs</span>
			</div>
		</div>
	</div>
</div>
<div class="columns-container paragraph">
	<div class="container">
		<h1 class="page-heading">FAQs</h1>
		<h2>FAQs | TOOLS</h2>
		<ul class="faq">
			<li>
				<div class="faq__question">Q. What is the difference between the Silicone Iron and the Titanium Iron?</div>
				<div class="faq__answer">A. The main difference is the plate of the irons. The Silicone 450 Iron has ceramic plates with patented Three Silicone Bars Technology inserted into the plates. The Titanium 450 Iron has high-grade Titanium plates that create a perfect smooth surface and even heat distribution for ease of use.</div>
			</li>
			<li>
				<div class="faq__question">Q. What is the purpose of the Silicone Bars?</div>
				<div class="faq__answer">A. The Silicone Bars protect the hair as it passes through the iron by lifting the hair off the plate, preventing it from being pressed too hard and avoiding serious damage to the cuticle. They also help to retain the moisture and essential fatty acids that are normally burned off during flat ironing, leaving the hair soft and shiny.</div>
			</li>
			<li>
				<div class="faq__question">Q. What is the difference between the Titanium 450 and The Titanium PRO 450?</div>
				<div class="faq__answer">A. The main difference is the position of the plates. The Titanium 450 has plates that are fixed in position, whereas the Titanium PRO 450 has floating plates that are totally sealed to the body of the unit. This eliminates the possibility of by-products given off from finishing products and Keratin getting inside the unit. Both are EXCELLENT irons and are designed to meet your preferences.</div>
			</li>
			<li>
				<div class="faq__question">Q. Which flat iron should I use?</div>
				<div class="faq__answer">A. We recommend various irons depending on your hair type. For hair that is medium-to-coarse texture and very wavy (both natural and colored), we recommend either of the Titanium Irons. For hair that has been heavily chemically processed or isvery fine, we recommend the Silicone 450 Flat Iron.</div>
			</li>
			<li>
				<div class="faq__question">Q. What Iron is best for Keratin Treatments?</div>
				<div class="faq__answer">A. We highly recommend the use of the Titanium 450 or the Titanium PRO 450. These will give you the perfect finish you are looking for. You may also use the Silicone 450 should the hair be very fine or over processed due to heat, color or a relaxer.</div>
			</li>
			<li>
				<div class="faq__question">Q. What is Micro-Fuse Technology?</div>
				<div class="faq__answer">A. SalonTech<sup>&reg;</sup> has developed a new fuse system that is designed to help eliminate many of the problems related to high heat Flat Irons that create issues for stylists when the units stop working or performing. This will improve durability and performance of your Flat Iron.</div>
			</li>
			<li>
				<div class="faq__question">Q. What is the difference between a Turbo Dryer and a Feather Light Dryer?</div>
				<div class="faq__answer">A. Turbo dryer has an A/C motor which is usually a little heavier, but offers the benefit of greater air movement, often called a “Power Dryer”. Used by most stylist to remove the first 70 to 80% of moisture after shampooing. 
				<br />
				The Feather Light dryer usually has a D/C motor and is quite a bit lighter, but slightly less powered. The Feather Light air is often much “hotter” and is a great dryer for removing the last 20 to 30% of moisture and giving you the ability to be much more creative in your finished style.</div>
			</li>
			<li>
				<div class="faq__question">Q. The Turbo 3300 has an EMF shield. What is EMF?</div>
				<div class="faq__answer">A. EMF stands for Electronic Magnetic Field. This is what is produced by most electrical motors while running. It has been determined that a standard Blow Dryer has a very high EMF reading. There is some concern in the professional community about the effects of EMF. SalonTech<sup>&reg;</sup> has taken an aggressive position concerning EMF. SalonTech<sup>&reg;</sup> introduced EMF Shield Technology to reduce the EMF emission from the 3300 dryer, thereby reducing the amount of EMF exposure to the stylist and client by over 85% without losing efficiency of the dryer from loss of heat or power. We are doing this because we care about you!</div>
			</li>
			<li>
				<div class="faq__question">Q. What does Tourmaline Ionic Technology mean?</div>
				<div class="faq__answer">A. The technological components generate Infrared Rays and Anion, which reduces drying time and leaves the hair much healthier and more manageable.</div>
			</li>
		</ul>
		<h2>FAQs | KERATIN</h2>
		<ul class="faq">
			<li>
				<div class="faq__question">Q. Is the Progressive Keratin Shampoo and Conditioner for only hair that has had SALONTECH<sup>&reg;</sup> Keratin Treatment systems done?</div>
				<div class="faq__answer">A. No. The SalonTech<sup>&reg;</sup> formulations are safe to be used after a keratin service, no matter what brand of keratin was used for the service.</div>
			</li>
			<li>
				<div class="faq__question">Q. Is the shampoo and conditioner only for people who have had a keratin treatment?</div>
				<div class="faq__answer">A. No. It is a great shampoo for any hair type. It is also recommended to be used by customers who flat iron their hair on a daily basis. It will replenish the hair with nutrients and keratin that is normally burned off when flat ironing the hair frequently.</div>
			</li>
			<li>
				<div class="faq__question">Q. Are the shampoo and conditioner color safe? </div>
				<div class="faq__answer">A. The shampoo and conditioner are color safe due to their gentle cleansing properties. The specially-designed formulations contain no harsh chemicals that can affect color retention. Both formulations work to moisturize the hair that is often found to be dry with color processed hair.</div>
			</li>
			<li>
				<div class="faq__question">Q. How often can I shampoo my hair with the SalonTech<sup>&reg;</sup> products?</div>
				<div class="faq__answer">A. It is a fabulous shampoo that can be used daily. They have protective properties to help prevent breakage and split ends while adding volume, strength and shine.</div>
			</li>
			<li>
				<div class="faq__question">Q. How much shampoo and conditioner should I use?</div>
				<div class="faq__answer">A. We highly recommend no more than a Silver Dollar-sized amount in the palm of your hand.</div>
			</li>
			<li>
				<div class="faq__question">Q. Can I leave the conditioner in my hair and not rinse out?</div>
				<div class="faq__answer">A. The Progressive Keratin Conditioner is not a leave-in conditioner. It is important when applying the conditioner that you comb the conditioner through the hair with a wide tooth comb to assure even conditioning from roots to ends. The SalonTech<sup>&reg;</sup> “All around comb” is a great tool for this purpose. To maximize performance, let the conditioner sit in the hair for two to three minutes and then rinse out with luke-warm water.</div>
			</li>
		</ul>
	</div>
</div>
@endsection