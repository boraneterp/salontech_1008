@extends('layout.master', ['pageTitle' => 'Showcase'])
@section('content')
<div class="labbreadcrumb">
	<div class="container">
		<div class="breadcrumb-i">
			<div class="breadcrumb clearfix">
				<a class="home" href="/" title="Return to Home">Home</a>
				<span class="navigation-pipe">&gt;</span>
				<span class="navigation_page">Showcase</span>
			</div>
		</div>
	</div>
</div>
<div class="columns-container paragraph">
	<div class="container">
		<h1 class="page-heading">Showcase</h1>
		<div class="blockPosition">
			<div class="lab_static">
				<div class="img"><a target="_blank" href="{{starts_with($showcase->link, 'http://')? $showcase->link: "http://".$showcase->link}}" title=""> <img alt="images" alt="Showcase Main" src="{{$showcase->main_image}}" /> </a></div>
			</div>
		</div>
		<p>{{$showcase->description}}</p>
		<div class="showcase-container">
			<div class="container">
				<p class="title_block">
					<span>Other Showcases</span>
				</p>
				<div class="row">
					@foreach($showcase->items as $item)
					<div class="col-sm-3">
						<div class="image_blog ">
							<a href="{{$item->linkUrl()}}" target="_blank"><img alt="{{$item->name}}" class="feat_img_small" src="{{$item->image}}"></a>
						</div>
						<h4><a href="{{$item->linkUrl()}}" target="_blank">{{$item->name}}</a></h4>
					</div>
					@endforeach
				</div>
			</div>
		</div>
	</div>
</div>
@endsection