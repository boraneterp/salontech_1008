@extends('layout.master', ['pageTitle' => 'Warranty'])
@section('content')
<div class="labbreadcrumb">
	<div class="container">
		<div class="breadcrumb-i">
			<div class="breadcrumb clearfix">
				<a class="home" href="/" title="Return to Home">Home</a>
				<span class="navigation-pipe">&gt;</span>
				<span class="navigation_page">Warranty</span>
			</div>
		</div>
	</div>
</div>
<div class="columns-container">
	<div class="container">
		<h1 class="page-heading">Salontech<sup>&reg;</sup> Product Warranty Registration</h1>
		<p>If you experience problems with an item, and your product is still under warranty, complete our Warranty Replacement Form using the instructions below. Please note that we do charge a small fee to cover the replacement of your item. This fee also includes shipment of the new item back to you.</p>
		<form action="/warranty" method="post" id="warrantyForm">
			{!! csrf_field() !!}
			<div class="alert alert-danger fade in" style="display:none;">
				Sorry!!! We can't register with information you provided, please try again.
			</div>
			<div class="alert alert-success fade in" style="display:none;">
				We've received your request, thanks for contacting Salontech.
			</div>
			<fieldset>
				<legend>Contact Information</legend>
				<div class="row">
					<div class="col-sm-4">
						<div class="form-group">
							<label >First Name</label>
							{!! Form::text('first_name', '', ['class' => 'form-control required', 'placeholder' => 'First Name']) !!}
						</div>
					</div>
					<div class="col-sm-4">
						<div class="form-group">
							<label >Last Name</label>
							{!! Form::text('last_name', '', ['class' => 'form-control required', 'placeholder' => 'Last Name']) !!}
						</div>
					</div>
					<div class="col-sm-4">
						<div class="form-group">
							<label >Email Address</label>
							{!! Form::email('email', '', ['class' => 'form-control required', 'placeholder' => 'Email Address']) !!}
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-4">
						<div class="form-group">
							<label >Salon Name (if Applicable)</label>
							{!! Form::text('salon_name', '', ['class' => 'form-control', 'placeholder' => 'Salon Name']) !!}
						</div>
					</div>
					<div class="col-sm-4">
						<div class="form-group">
							<label >Phone Number</label>
							{!! Form::text('phone_number', '', ['class' => 'form-control required phone-number', 'placeholder' => 'Phone Number']) !!}
						</div>
					</div>
				</div>
			</fieldset>
			<fieldset>
				<legend>Address Information</legend> 
				<div class="row">
					<div class="col-sm-8">
						<div class="form-group">
							<label >Address 1</label>
							{!! Form::text('address_1', '', ['class' => 'form-control required', 'placeholder' => 'Address 1']) !!}
						</div>
					</div>
					<div class="col-sm-4">
						<div class="form-group">
							<label >Address 2</label>
							{!! Form::text('address_2', '', ['class' => 'form-control', 'placeholder' => 'Address 2']) !!}
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-4">
						<div class="form-group">
							<label >City</label>
							{!! Form::text('city', '', ['class' => 'form-control required', 'placeholder' => 'City']) !!}
						</div>
					</div>
					<div class="col-sm-4">
						<div class="form-group">
							<label >State</label>
							{!! Form::select('state', ['' => 'Choose State'] + Util::getStates()->all(), '', ['class' => 'required form-control']) !!}
						</div>
					</div>
					<div class="col-sm-4">
						<div class="form-group">
							<label >Zip Code</label>
							{!! Form::text('zip', '', ['class' => 'form-control required', 'placeholder' => 'Zip Code']) !!}
						</div>
					</div>
				</div>
			</fieldset>
			<fieldset>
				<legend>Defect Information</legend>
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
							<label >Product Category</label>
							{!! Form::select('category_id', ['' => 'Choose Category'] + Util::getCategoryList()->all(), '', ['class' => 'required form-control']) !!}
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
							<label >Defected Product</label>
							{!! Form::select('product_id', ['' => 'Choose Product'], '', ['class' => 'required form-control']) !!}
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
							<label >Proof of Purchase [ image | pdf | doc ]</label>
							{!! Form::file('proof_of_purchase', ['class' => 'form-control required']) !!}
						</div>
					</div>
					<div class="col-sm-6">
						<div class="form-group">
							<label >Date Code (if Applicable)</label>
							{!! Form::text('date_code', '', ['class' => 'form-control', 'placeholder' => 'Date Code']) !!}
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<div class="form-group">
							<label >Reason for Return</label>
							{!! Form::textarea('reason', '', ['class' => 'form-control required', 'placeholder' => 'Reason for Return']) !!}
						</div>
					</div>
				</div>
			</fieldset>
			<p>
				Note: A copy of your original receipt must be attached to this form. Any item sent to Salontech<sup>&reg;</sup>  without receipt of purchase will be returned to sender unless you have written authorization from a Salontech® representative.  Please send hair tools only (i.e. irons, dryers)! Do not send styling product, styling bags and boxes, they will not be returned. **We replace tools with “Like Kind” tools, limited edition stylers or dryers will be replaced with any available limited edition styler/dryer we have in stock.
			</p>
			<div class="checkbox-list agreement">
				<div class="checkbox">
					<label><div class="checkbox-wrapper ">{!! Form::checkbox('agree', '', ['class' => 'form-control']) !!} </div> I confirm that all the information provided above is correct</label>
				</div>
				<div class="agree-error" style="display: none">Please confirm all information is correct.</div>
			</div>
			<p>
				The warranty only covers manufacturer’s defect, and does not cover damages due to misuse or negligence by the user. Salontech or its affiliates will only accept defective returns of products purchased from authorized Salontech<sup>&reg;</sup> distributors. Products purchased from third party vendors such as Amazon or Ebay are not warrantied, and will not be covered for any manufacturer's defects. The warranty period is valid for one (1) year from the purchase date of the receipt.
			</p>
			<div class="form-group text-right">
				<button type="submit" class="button btn-submit btn-sm" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Submit">Submit</button>
				<a href="/" class="button button-default btn-sm" >Cancel</a>
			</div>
		</form>
	</div>
</div>
@endsection