@extends('layout.master', ['pageTitle' => 'Salon Locator'])
@section('content')
<div class="labbreadcrumb">
	<div class="container">
		<div class="breadcrumb-i">
			<div class="breadcrumb clearfix">
				<a class="home" href="/" title="Return to Home">Home</a>
				<span class="navigation-pipe">&gt;</span>
				<span class="navigation_page">Salon Locator</span>
			</div>
		</div>
	</div>
</div>
<div class="columns-container ">
	<div class="container">
		<h1 class="page-heading">Salon Locator</h1>
		<div class="row" >
			<div class="col-sm-4">
				<form method="GET" action="/store-finder" >
					<div class="store-filter form-group">
						<i class="fa fa-search"></i>
						<input name="zip" class="store-filter-input" value="{{$zip}}" placeholder="Enter Zip Code"  />
					</div>
				</form>
				<ul class="store-list">
					@foreach($stores as $store)
					<li data-name="{{$store->name}}" data-lat="{{$store->lat}}" data-lng="{{$store->lng}}" data-distance="{{$store->distance}}" >
						<div class="store-label {{$store->distributor == 'Y'? 'distributor':''}}">{{$store->name}} 
						@if($store->distributor == 'Y')
						(Distributor)
						@endif
						</div>
						<address>{!! $store->full_address !!}</address>
						<div class="store-distance">{{$store->distance}} miles away</div>
						<div class="store-phone">{{$store->phone_number}}</div>
						<i class="fa fa-chevron-right"></i>
					</li>
					@endforeach
				</ul>
			</div>
			<div class="col-sm-8">
				<div id="store-map" data-lat="{{$lat}}" data-lng="{{$lng}}"></div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('extra_js')
 <script src="//maps.google.com/maps/api/js?key={{getenv('GOOGLE_API_KEY')}}" type="text/javascript"></script>
@endsection