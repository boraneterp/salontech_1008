@extends('layout.master', ['pageTitle' => 'Return Policy'])
@section('content')
<div class="labbreadcrumb">
	<div class="container">
		<div class="breadcrumb-i">
			<div class="breadcrumb clearfix">
				<a class="home" href="/" title="Return to Home">Home</a>
				<span class="navigation-pipe">&gt;</span>
				<span class="navigation_page">Return Policy</span>
			</div>
		</div>
	</div>
</div>
<div class="columns-container paragraph">
	<div class="container">
		<h1 class="page-heading">Return Policy</h1>
		<p>If you have purchased a product from SALONTECH.COM and for any reason you are not satisfied with your order, we will gladly refund your purchase within 30 days of the purchase date.</p>
		<h2>How to Return</h2>
		<ol type="1">
			<li><a href="/warranty">Complete the warranty form online</a>.</li>
			<li>Include in the package a copy of your original receipt that came with the order.</li>
			<li>If possible, re-use your original shipping box and packing materials to secure the item(s).</li>
			<li>Address your package to: 
				<div class="address-container">
					<div class="address-label">AST Systems LLC/SalonTech<sup>&reg;</sup><br />Customer Returns Department </div>
					<address>25 Harbor Park Drive<br/>Port Washington, NY 11050</address>
				</div>
			</li>
			<li>We recommend shipping your package through a traceable shipping method such as FedEx or UPS which offer Delivery Confirmation.</li>
		</ol>
		<p>We will email you within 5 business days of receiving your package to confirm your return. Once your return is received, a refund will be issued within 1 billing cycle.</p>
		<p>Unfortunately, we cannot refund orders that are: </p>
		<ol type="1">
			<li>Received later than 30 days from purchase.</li>
			<li>Purchased from any other location, vendor, or website other than SalonTech.com.</li>
		</ol>

		<h2>Warranty Replacement Policy</h2>
		<p>If you experience problems with an item, and your product is still under warranty, complete our Warranty Replacement Form online (link). Please refer to the owner’s manual for warranty information regarding the item that you’ve purchased.  There will be a small fee to cover the replacement of your item. Fee details are listed later on in this information.  This fee also includes shipment of the new item back to you.</p>
		<p>Please note: Warranty periods are calculated from your original date of purchase; receiving a replacement item does not extend your warranty period. The Warranty Period varies based on the items you purchased. Please refer to the owner’s manual for the length of the Warranty Period. Warranty only covers manufacturer’s defects, and does not cover damages due to misuse or neglect. </p>
		<p>If we no longer carry the item you'd like to replace, our Customer Service Team will be happy to help you find a comparable alternative.</p>

		<h2>How to Make a Submission for Warranty Replacement</h2>
		<ol type="1">
			<li><a href="/warranty">Complete the online warranty form online</a>.</li>
			<li>Complete the Warranty Replacement form COMPLETELY.  Remember to attach the proof of purchase. </li>
			<li>You will receive a Return Authorization (RA) # via email.</li>
			<li>Send your warranty item, and payment for shipping and handling fee to: 
				<div class="address-container">
					<div class="address-label">AST Systems LLC/SalonTech<sup>&reg;</sup><br />Customer Returns Department </div>
					<address>25 Harbor Park Drive<br/>Prot Washington, NY 11050</address>
				</div>
			</li>
			<li>Make sure to include the RA#.</li>
			<li>Warranty Replacement Fees are as follows:
				<div class="row">
					<div class="col-sm-6">
						<table class="table">
							<thead>
								<tr>
									<td>Location</td>
									<td>Warranty Replacement Fee</td>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>Continental US</td>
									<td>$12.00</td>
								</tr>
								<tr>
									<td>Alaska, Hawaii, Puerto Rico</td>
									<td>$20.00</td>
								</tr>
								<tr>
									<td>Canada</td>
									<td>$30.00</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</li>
		</ol>
		<p>Our Warranty Replacement fee covers shipping and handling for your replacement item. Canadian and other international customers may be subject to additional duties and taxes according to the laws of your country.</p>
		<ul>
			<li>We recommend shipping your package through a traceable shipping method such as FedEx or UPS which both offer Delivery Confirmation.</li>
			<li>A valid copy of your Proof of Purchase must be submitted to SalonTech<sup>&reg;</sup>.</li>
		</ul>

		<h2>Damaged Shipment</h2>
		<p>If your shipment from SalonTech.com is damaged in transit, please keep the original shipping carton and follow these instructions to replace your order.</p>
		<ul>
			<li>If the package is visibly damaged upon receipt, please notify the UPS driver, and file a damage report. </li>
			<li>For orders purchased directly from salontech.com, e-mail salontech@salontech.com and let us know what happened; don't forget to include your order number and contact information.</li>
		</ul>
	</div>
</div>
@endsection