@extends('layout.master', ['pageTitle' => 'Shipping Policy'])
@section('content')
<div class="labbreadcrumb">
	<div class="container">
		<div class="breadcrumb-i">
			<div class="breadcrumb clearfix">
				<a class="home" href="/" title="Return to Home">Home</a>
				<span class="navigation-pipe">&gt;</span>
				<span class="navigation_page">Shipping Policy</span>
			</div>
		</div>
	</div>
</div>
<div class="columns-container paragraph">
	<div class="container">
		<h1 class="page-heading">Shipping Policy</h1>
		<p>Products usually ship within 24 to 48 hours after completion of the order. Once shipped, expect delivery to take two to five business days if being delivered within the continental United States.</p>
		<br />
		<p>We use UPS Ground for standard shipping. For certain orders, USPS may be used. Please keep in mind that our shipping partners may be closed on weekends and national holidays. Business days do not include Saturday, Sunday or national holidays.</p>
	</div>
</div>
@endsection