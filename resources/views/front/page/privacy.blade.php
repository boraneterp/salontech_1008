@extends('layout.master', ['pageTitle' => 'Privacy Policy'])
@section('content')
<div class="labbreadcrumb">
	<div class="container">
		<div class="breadcrumb-i">
			<div class="breadcrumb clearfix">
				<a class="home" href="/" title="Return to Home">Home</a>
				<span class="navigation-pipe">&gt;</span>
				<span class="navigation_page">Privacy Policy</span>
			</div>
		</div>
	</div>
</div>
<div class="columns-container paragraph">
	<div class="container">
		<h1 class="page-heading">Privacy Policy</h1>
		<h2>Purpose</h2>
		<p> This is the web site of AST Systems, LLC. AST Systems, LLC. has adopted this Privacy Statement in order to inform you of its policies with respect to information collected from this website. Your use of this website constitutes your acceptance of this Privacy Statement and your consent to the practices it describes.</p>

		<h2>Personally Identifiable Information</h2>
		<p> Personally Identifiable Information is any information that concerns you individually and would permit someone to contact you, for example, your name, address, telephone/fax number, social security number, email address or any information you submitted to SalonTech®.com that identifies you individually. For each visitor to our Web page, our Web server does not automatically recognize information regarding the domain or e-mail address. SalonTech®.com will not collect any personally identifiable information about you unless you provide it. Therefore, if you do not want SalonTech®.com to obtain any personally identifiable information about you, do not submit it. </p>
		<p>You can visit and browse the any AST Systems, LLC. website without revealing personally identifiable information about yourself. You may also choose to disclose personally identifiable information about yourself, which may be maintained as described below. SalonTech®.com may collect personally identifiable information about you from its website by methods such as the following:</p>
		<ul>
			<li>Mailing List/Registration Forms - If you are offered the opportunity to enter a promotion, to become a registered user of the AST Systems, LLC. website, or to opt-in to receive AST Systems, LLC. information through another site, you must apply by filling out the registration form on the site. This form requires certain personally identifiable information that may include, without limitation, your name, email address, postal address, telephone number, and/or product usage.</li>
			<li>Transactions and Activity - If you become a registered user or if you conduct transactions through the AST Systems, LLC. website, SalonTech®.com collects information about the transactions you engage in while on the website and your other activity on the site. This information may include, without limitation, areas of the website that you visit, transaction type, content that you view, download or submit, transaction amount, payment, shipping and billing information as well as the nature, quantity and price of the goods or services you exchange and the individuals or entities with whom you communicate or transact business.</li>
			<li>Email and other voluntary communications - You may also choose to communicate with SalonTech®.com through email, via our website, by telephone, in writing, or though other means. We collect the information in these communications, and such information may be personally identifiable.</li>
		</ul>
		<p>If you do not want to receive e-mail from us in the future, please let us know by sending us e-mail at <a href="mailto:salontech@salontech.com">salontech@salontech.com</a></p>

		<h2>Information Use</h2>
		<p>SalonTech®.com may use the personally identifiable information collected through its website primarily for such purposes as:</p>
		<ul>
			<li>Helping to establish and verify the identity of users;</li>
			<li>Opening, maintaining, administering and servicing users' accounts or memberships;</li>
			<li>Processing, servicing or enforcing transactions and sending related communications;</li>
			<li>Providing services and support to users;</li>
			<li>Improving the website, including tailoring it to users' preferences;</li>
			<li>Providing users with product or service updates, promotional notices and offers, and other information about AST Systems, LLC. and its affiliates;</li>
			<li>Responding to your questions inquiries, comments and instructions;</li>
			<li>Maintaining the security and integrity of its systems.</li>
		</ul>
		<p>SalonTech<sup>&reg;</sup>.com uses the anonymous browsing information collected automatically by its servers primarily to help it administer and improve its website. SalonTech®.com may also use aggregated anonymous information to provide information about its website to advertisers, potential business partners and other unaffiliated entities. Again, this information is not personally identifiable.</p>
		<p>If you supply us with your postal address on-line you may receive periodic mailings from us with information on new products and services or upcoming events. If you do not wish to receive such mailings, please let us know by calling us at the number provided above.</p>
		<p>Persons who supply us with their telephone numbers on-line may receive telephone contact from us with information regarding new products and services or upcoming events. If you do not wish to receive such telephone calls, please let us know by sending us e-mail at the above address.</p>

		<h2>"Cookies" and Advertisers</h2>
		<p>We use cookies to record user-specific information on what pages users access or visit.</p>
		<p>A "cookie" is a small piece of data that can be sent by a web server to your computer, which then may be stored by your browser on your computer's hard drive. Cookies allow SalonTech®.com to recognize your computer while you are on its website and help customize your online experience and make it more convenient for you. Cookies are also useful in allowing more efficient log-in for users, tracking transaction histories and preserving information between sessions. The information collected from cookies may also be used to improve the functionality of the website.</p>
		<p>Most web browser applications (such as Microsoft Internet Explorer and Netscape Navigator) have features that can notify you when you receive a cookie or prevent cookies from being sent. If you disable cookies, however, you may not be able to use certain personalized functions of this website.</p>
		<p>We do not partner with or have special relationships with any ad server companies.</p>

		<h2>External Links</h2>
		<p>SalonTech<sup>&reg;</sup>.com website may contain links to other websites. Please be aware that SalonTech®.com is not responsible for the privacy practices or the content of other websites. Websites that are accessible by hyperlinks from the SalonTech®.com website may use cookies. SalonTech®.com encourages you to read the privacy statements provided by other websites before you provide personally identifiable information to them.</p>

		<h2>Security</h2>
		<p>SalonTech<sup>&reg;</sup>.com has the appropriate security measures in place in our physical facilities to protect against the loss, misuse or alteration of information that we have collected from you at our site. We maintain physical, electronic, and procedural safeguards to help guard personally identifiable information. If transactions are offered on the site, transaction information is transmitted to and from SalonTech®.com in encrypted form using industry-standard Secure Sockets Layer (SSL) connections to help protect such information from interception. SalonTech®.com restricts authorized access to your personal information to those persons who have a legitimate purpose to know that information to provide products or services to you and those persons you have authorized to have access to such information. Please be aware, however, that any email or other transmission you send through the internet cannot be completely protected against unauthorized interception.</p>

		<h2>Data Quality/Access</h2>
		<p>SalonTech<sup>&reg;</sup>.com allows you to change, update, or delete the information you may provide in your optional mailing list form. If you would like to change, update, or delete your personal information, please SalonTech®@kissusa.com. Upon request we provide site visitors with access to a description of information that we maintain about them. Consumers can access this information by e-mailing us at the above address.</p>

		<h2>Contacting Us</h2>
		<p>If you have questions about this privacy statement, the information practices of SalonTech®.com or your dealings with SalonTech<sup>&reg;</sup>.com, please contact:</p>
		<div class="address-container">
			<div class="address-label">AST Systems LLC<br />Customer Service</div>
			<address>25 Harbor Park Drive<br/>Prot Washington, NY 11050</address>
		</div>
	</div>
</div>
@endsection