@extends('layout.master', ['pageTitle' => 'Contact Us'])
@section('content')
<div class="labbreadcrumb">
	<div class="container">
		<div class="breadcrumb-i">
			<div class="breadcrumb clearfix">
				<a class="home" href="/" title="Return to Home">Home</a>
				<span class="navigation-pipe">&gt;</span>
				<span class="navigation_page">Contact Us</span>
			</div>
		</div>
	</div>
</div>
<div class="columns-container">
	<div class="container">
		<h1 class="page-heading">Contact Us</h1>
		<form action="/contact-us" method="post" id="contactForm">
			{!! csrf_field() !!}
			<div class="row">
				<div class="col-sm-8 col-sm-offset-2">
					@foreach ($errors->all() as $error)
	                    <!-- <div class="alert alert-danger fade in" >Your input is not valid, please don't enter any special characters.</div> -->
	                    <div class="alert alert-danger fade in" >{{$error}}</div>
	                @endforeach
	                @if(Session::has('message'))
	                    <div class="alert alert-success fade in">{!! session('message') !!}</div>
	                @endif
					<div class="form-group">
						<label >Name</label>
						{!! Form::text('name', '', ['class' => 'form-control required', 'placeholder' => 'First Name']) !!}
					</div>
					<div class="form-group">
						<label >Email Address</label>
						{!! Form::email('email', '', ['class' => 'form-control required', 'placeholder' => 'Email Address']) !!}
					</div>
					<div class="form-group">
						<label >Inquiry type</label>
						{!! Form::select('type', ['' => 'Inquiry type', 'Product inquiry' => 'Product inquiry', 'Warranty' => 'Warranty', 'Order Status' => 'Order Status', 'Other' => 'Other'], '', ['class' => 'required form-control']) !!}
					</div>
					<div class="form-group">
						<label >Message</label>
						{!! Form::textarea('message', '', ['class' => 'form-control required', 'placeholder' => 'Message']) !!}
					</div>
					<div class="form-group text-right">
						<button type="submit" class="button btn-submit btn-sm" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Submit">Submit</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
@endsection