<div class="popup-content animated zoomIn" id="instagram-popup">
    <div class="overlay-bg-popup"></div>
    <div class="popup-content-wrapper">
        <a class="close-popup" href="#"><i class="fa fa-times"></i></a>
        <div class="popup-container">
            <div class="row">
                <div class="col-md-8 ">
                    <div class="image-container">
                        <div class="social-layer">
                            <i class="fa fa-heart"></i><span class="likes"></span>
                            <i class="fa fa-comment"></i><span class="comments"></span>
                        </div>
                        <img src="" class="img-responsive" />
                        <div class="caption"></div>
                    </div>
                    <div class="video-container">
                        <iframe src=""  width="640" height="640"></iframe>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="text-container">
                        <div class="author hidden-sm">
                            <img src="" width="50" />
                            <span></span>
                        </div>
                        <h3>SHOP THIS LOOK</h3>
                        <div class="product">
                            <a class="product__link" href=""><img class="product__image" src="" /></a>
                            <div class="product-name"></div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>