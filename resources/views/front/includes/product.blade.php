<div class="item">
	<div class="left-block">
		<div class="product-image-container">
			<a class="product_image" href="/detail{{$product->url()}}" title="{{strip_tags($product->product_name)}}">
				<img class="img-responsive" src="{{Util::getImageWithSuffix($product->image, 'md')}}" alt="{{$product->product_name}}" />
			</a>
		</div>
	</div>
	<div class="right-block">
		<h5 class="product-name" data-mh="title">
			<a href="/detail{{$product->url()}}" title="{{strip_tags($product->product_name)}}">{!! $product->product_name !!}</a>
		</h5>
		@include('front.includes.review')
		<div class="row" >
			<div class="col-xs-8">
				<div class="content_price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
					<span itemprop="price" class="price product-price">${{ sprintf('%01.2f', $product->currentPrice())}}</span>
					@if($product->currentPrice() != $product->msrp)
					<span class="old-price product-price">
						${{ sprintf('%01.2f', $product->msrp) }}
					</span>
					@endif
				</div>
			</div>
			@if($product->stock > 0)
			<div class="col-xs-4">
				<div class="lab-cart text-right">
					<a class="add-to-cart " href="#" data-product_id="{{$product->id}}" title="Add to cart" >
						<i class="icon icon_bag_alt"></i>
					</a>
				</div>
			</div>
			@endif
		</div>
	</div>
</div>