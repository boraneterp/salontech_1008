<div class="toolbar {{$position}}">
	<div class="row">
		<div class="col-xs-6">
			<span class="toolbar__info">{{$count}} items</span>
		</div>
		<div class="col-xs-6 text-right">
			{!! Form::select('sort', ['' => 'Sort By', 'best-selling' => 'Best Selling', 'created-descending' => 'Newest', 'created-ascending' => 'Oldest', 'price-descending' => 'Price: High to Low', 'price-ascending' => 'Price: Low to High'], app('request')->input('sort'), ['class' => 'toolbar__sort-control']) !!}
			<a href="#" title="Grid" data-style="grid" class="toolbar__view-control ">
				<i class="fa fa-th"></i>
			</a>
			<a href="#" title="List" data-style="list" class="toolbar__view-control active">
				<i class="fa fa-list"></i>
			</a>
		</div>
	</div>
</div>