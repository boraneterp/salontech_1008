@if(!empty($instagram))
<div class="social-container instagram">
    <div class="container">
        <h3><a href="https://www.instagram.com/salontechstyle/" target="_blank">FOLLOW US ON INSTAGRAM (#SALONTECHSTYLE)</a></h3>
        <ul class="row">
        @for($i = 0; $i < (sizeof($instagram['media']['nodes']) > 8? 8: sizeof($instagram['media']['nodes'])); $i++)
            <li class="col-xs-6 col-sm-3">
                <a href="#" class="instagram__link" data-likes="{{$instagram['media']['nodes'][$i]['likes']['count']}}" data-comments="{{$instagram['media']['nodes'][$i]['comments']['count']}}" data-text="{{array_key_exists('caption', $instagram['media']['nodes'][$i])? $instagram['media']['nodes'][$i]['caption']: ''}}" data-profile_image="{{$instagram['profile_pic_url']}}" data-full_name="{{$instagram['full_name']}}" data-product_name="{{$instagram['media']['nodes'][$i]['product']['name']}}" data-product_image="{{$instagram['media']['nodes'][$i]['product']['image']}}" data-product_url="{{$instagram['media']['nodes'][$i]['product']['url']}}" data-video="{{$instagram['media']['nodes'][$i]['is_video']}}" data-code="{{$instagram['media']['nodes'][$i]['code']}}" ><img src="{{$instagram['media']['nodes'][$i]['thumbnail_src']}}" alt="{{array_key_exists('caption', $instagram['media']['nodes'][$i])? $instagram['media']['nodes'][$i]['caption']: ''}}" /></a>
            </li>
        @endfor
        </ul>
    </div>
</div>
@endif