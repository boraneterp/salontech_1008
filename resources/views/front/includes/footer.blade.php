<!-- Footer -->
<div class="footer-container">
	<footer >
		<div class="topFooter">
			<div class="container">
				<div class="row">
					<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
						<!-- <div class="footer-static row-fluid"> -->
						
						<!-- MODULE Block contact infos -->
						<section id="block_contact_infos" class="footer-block">
							<h4>Contact Information</h4>
							<div class="toggle-footer">
								<!-- <p>
									<i class="fa fa-home"></i>  <span>42 Puffin street
										12345 Puffinville
									France</span>
								</p> -->
								<p>
									<i class="fa fa-phone"></i>
									<span>1-877-278(AST)-9030</span>
								</p>
								<p>
									<i class="far fa-envelope"></i>
									<span><a href="#" >salontech@salontech.com</a></span>
								</p>
								<div class="payment"><a href="#"><i class="fab fa-cc-mastercard"> </i></a> <a href="#"><i class="fab fa-cc-visa"> </i></a> <a href="#"><i class="fab fa-cc-paypal"> </i></a> <a href="#"><i class="fab fa-cc-discover"> </i></a> <a href="#"><i class="fa fa-credit-card"> </i></a> <a href="#"><i class="fab fa-cc-amex"> </i></a></div>
							</div>
						</section>
					</div>
					<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
						<section class="blockcategories_footer footer-block">
							<h4>Categories</h4>
							<div class="category_footer toggle-footer">
								<div class="list">
									<ul class="tree dhtml">
										@foreach(Util::getCategories() as $category) 
										<li >
											<a href="/{{$category->seo_url}}">
												<i class="fas fa-circle" ></i>
												{!! $category->name !!}
											</a>
										</li>
										@endforeach
											<li class="item">
												<a href="https://salontechblog.com" title="Salontech Blog"><i class="fas fa-circle" ></i>Salontech Blog</a>
											</li>
									</ul>
								</div>
							</div>
						</section>
					</div>
					<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
						<section class="footer-block " id="block_various_links_footer">
							<h4>Information</h4>
							<ul class="toggle-footer">
								<li class="item">
									<a href="/about-us" title="About Us"><i class="fas fa-circle" ></i>About Us</a>
								</li>
								<li class="item">
									<a href="/shipping-policy" title="Shipping Policy"><i class="fas fa-circle" ></i>Shipping Policy</a>
								</li>
								<li class="item">
									<a href="/return-policy" title="Return Policy"><i class="fas fa-circle" ></i>Return Policy</a>
								</li>
								<li class="terms-and-conditions">
									<a href="/terms-and-conditions" title="Terms & Conditions"><i class="fas fa-circle" ></i>Terms & Conditions</a>
								</li>
								<li class="item">
									<a href="/privacy-policy" title="Privacy Policy"><i class="fas fa-circle" ></i>Privacy Policy</a>
								</li>
								<li class="item">
									<a href="/faqs" title="FAQs"><i class="fas fa-circle" ></i>FAQs</a>
								</li>
								<li class="item">
									<a href="/store-finder" title="Salon Locator"><i class="fas fa-circle" ></i>Salon Locator</a>
								</li>
								<li class="item">
									<a href="/contact-us" title="Contact Us"><i class="fas fa-circle" ></i>Contact Us</a>
								</li>
							</ul>
						</section>
					</div>
					<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
						<section class="footer-block">
							<h4>My account</h4>
							<div class="block_content toggle-footer">
								<ul class="bullet">
									<li>
										@if (!Auth::check())
										<a href="/login?redirectUrl={{getenv('APP_URL')}}/account/orders" title="My orders" rel="nofollow"><i class="fas fa-circle" ></i>My orders</a>
										@else
										<a href="/account/orders" title="My orders" rel="nofollow"><i class="fas fa-circle" ></i>My orders</a>
										@endif
									</li>
									<li>
										<a href="/warranty" title="Warranty" rel="nofollow"><i class="fas fa-circle" ></i>Warranty</a>
									</li>
									<li>
										@if (!Auth::check())
										<a href="/login?redirectUrl={{getenv('APP_URL')}}/account/address" title="My orders" rel="nofollow"><i class="fas fa-circle" ></i>My Addresses</a>
										@else
										<a href="/account/address" title="My orders" rel="nofollow"><i class="fas fa-circle" ></i>My Addresses</a>
										@endif
									</li>
									<li>
										@if (!Auth::check())
										<a href="/login?redirectUrl={{getenv('APP_URL')}}/account" title="My orders" rel="nofollow"><i class="fas fa-circle" ></i>My Info</a>
										@else
										<a href="/account" title="My orders" rel="nofollow"><i class="fas fa-circle" ></i>My Info</a>
										@endif
									</li>
									<li>
										@if (!Auth::check())
										<a href="/login?redirectUrl={{Request::url()}}" title="Log in to your account"><i class="fas fa-circle" ></i>Sign in</a>
										@else
										<a href="/logout" title="Logout"><i class="fas fa-circle" ></i>Sign Out</a>
										@endif
									</li>
								</ul>
							</div>
						</section>
					</div>
				</div>
			</div>
		</div>
		<div class="bottomFooter">
			<div class="container">
				<div class="row">
					<!-- <div class="footer-static row-fluid"> -->
                    <div class="copyright col-lg-6 col-md-6 col-sm-6  col-xs-12 ">© {{date('Y')}} <a href="#">SalonTech. </a>All Rights Reserved. Powered by <a href="http://boranet.net">Boranet</a></div>
					
					<div id="social_block" class="top-block pull-right">
						<!-- <h4>Follow us</h4> -->
						<ul class="media-body">
							<li class="facebook pull-left">
								<a class="_blank" title="facebook" href="https://www.facebook.com/SalonTechStyle/" target="_blank">
									<i class="fab fa-facebook-f"></i>
								</a>
							</li>
							<li class="twitter pull-left">
								<a class="_blank" title="Youtube" href="https://www.youtube.com/channel/UCWTrRkJhyNT57Luya2vBOeA" target="_blank">
									<i class="fab fa-youtube"></i>
								</a>
							</li>
							<li class="rss pull-left">
								<a class="_blank" title="rss" href="https://instagram.com/salontechstyle/" target="_blank">
									<i class="fab fa-instagram"></i>
								</a>
							</li>
						</ul>
						
					</div>
				</div>
			</div>
		</div>
		
		<!-- <div class="labpopupnewsletter" style="width:970px;height:540px;background-image: url(http://labbluesky.com/prestashop/lab_eren/layout4/modules/labpopupnewsletter/img/popupbg_4.jpg?002310
			);">
			<div id="newsletter_block_popup" class="block">
				<div class="block_content">
					<form action="http://labbluesky.com/prestashop/lab_eren/layout4/en/" method="post">
						<div class="popup_title"><h2>Get Our Email Letter</h2></div>                            <div class="popup_text">Subscribe to the Eren mailing list to receive updates on new arrivals, special offers and other discount information.</div>                            <div class="send-response"></div>
						<input class="inputNew" id="newsletter-input-popup" type="text" name="email" value=""  placeholder="Enter your mail" autocomplete="off"/>
						
						<div class="send-reqest button_unique main_color_hover">Subscribe</div>
					</form>
				</div>
				<div class="newsletter_block_popup-bottom">
					<input id="newsletter_popup_dont_show_again" type="checkbox">
					<label for="newsletter_popup_dont_show_again">Don't show this popup again</label>
				</div>
			</div>
		</div> -->
	</footer>
	
</div>