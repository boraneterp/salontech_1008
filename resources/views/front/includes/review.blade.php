<div class="rating" data-mh="review">
	@if($product->review_rate > 0)
    <i class="fa fa-star voted"></i>
    <i class="{{$product->review_rate >= 2? 'fa fa-star': ($product->review_rate == 1? 'fa fa-star-o': ' fa fa-star-half-o')}} {{$product->review_rate > 1? 'voted':''}}"></i>
    <i class="{{$product->review_rate >= 3? 'fa fa-star': ($product->review_rate <= 2? 'fa fa-star-o': ' fa fa-star-half-o')}} {{$product->review_rate > 2? 'voted':''}}"></i>
    <i class="{{$product->review_rate >= 4? 'fa fa-star': ($product->review_rate <= 3? 'fa fa-star-o': ' fa fa-star-half-o')}} {{$product->review_rate > 3? 'voted':''}}"></i>
    <i class="{{$product->review_rate == 5? 'fa fa-star': ($product->review_rate <= 4? 'fa fa-star-o': ' fa fa-star-half-o')}} {{$product->review_rate > 4? 'voted':''}}"></i>
    <span>{{sprintf('%01.1f', $product->review_rate)}}</span>
	@endif
</div>