<div class="header-container">
	<header >
		<div class="nav">
			<div class="container-fluid">
				<div class="user-information pull-right">
					<ul>
						@if (!Auth::check() || Auth::user()->status != '1')
						<li class="pull-left">
							@if(Request::url() == getenv('APP_URL'))
							<a href="/login" title="Log in to your account">Sign in</a>
							@else
							<a href="/login?redirectUrl={{Request::url()}}" title="Log in to your account">Sign in</a>
							@endif
						</li>
						<li class="labLogin pull-left"><a href="/register" >Register</a></li>
						@else
						<li class="pull-left">
							<a class="link-myaccount" href="/account" title="My Account">My Account</a>
						</li>
						<li class="labLogin pull-left"><a href="/logout" title="Logout">Sign Out</a></li>
						@endif
					</ul>
				</div>
			</div>
		</div>
		<div class="labHeader">
			<div class="container-fluid">
				<div id="header_logo" >
					<a href="/" title="Eren">
						<img class="logo img-responsive" src="/assets/img/logo.png" alt="Eren" width="109" height="25"/>
					</a>
				</div>
				<div class="header-right pull-right">
					<a href="#" class="mobile-menu">
						<i class="fa fa-bars" aria-hidden="true"></i>
					</a>
					<div id="mini-cart" class="pull-right">
						<div class="shopping_cart ">
							<a href="/checkout/cart" title="View my shopping cart" rel="nofollow">
								<i class="icon icon_bag_alt"></i>
							</a>
						</div>
					</div>
					<div class="labSearch pull-right">
						<div id="search_block_top" class="clearfix">
							<div class="current">
								<i class="fa fa-search"></i>
							</div>
							<div class="toogle_content" style="display:none;">
								<form id="searchbox" method="get" action="/search" >
									<input class="search_query form-control" type="text" id="search_query_top" name="keyword" placeholder="Search" value="" />
									<button type="submit" class="btn btn-default button">
									<i class="fa fa-search"></i>
									</button>
								</form>
							</div>
						</div>
					</div>
				</div>
				
				<div class="labmegamenu container">
					<div class="lab-menu-horizontal">
						<ul class="menu-content ">
							@foreach(Util::getCategories(true) as $category) 
							<li class="level-1  parent">
								<a href="/{{$category->seo_url}}">
									<span>{!! $category->name !!}</span>
									<i class="fa fa-angle-down"></i>
								</a>
								<span class="icon-drop-mobile"></span>
								<div class="lab-sub-menu menu-dropdown col-xs-12 col-md-12 lab-sub-right">
									<div class="lab-menu-row row ">
									 @if($category->id == '48')
									 @foreach($category->subcategory as $index => $cat)
										<div class="lab-menu-col col-xs-6 col-sm-2">
											<ul class="ul-column">
												<li class="menu-item">
													<div class="html-block">
														<div class="img">
															<a href="/{{$category->seo_url}}/{{$cat->seo_url}}">
															@if($cat->id == 1)
																<img src="https://salontech.s3.amazonaws.com/product/beyond-naturale-01/GrDbMY0cBe.jpg" alt="salontech-ienvyprofessional-beyond-naturale-01" />
																@elseif ($cat->id == 2)
																<img src="https://salontech.s3.amazonaws.com/product/ultra-black-knot-free-short/4QIDmWSOEz.jpg" alt="salontech-ienvyprofessional-Ultra-Black-Knot-Free-Short-70PC" />
																@elseif($cat->id == 3)
																<img src="https://salontech.s3.amazonaws.com/product/super-strong-hold-eyelash-adhesive-cl/G9VuxstzVy.jpg" alt="salontech-ienvyprofessional-super-strong-hold-eyelash-adhesive-cl" />
																@elseif($cat->id == 4)
																<img src="https://salontech.s3.amazonaws.com/product/ienvy-precision-lash-applicator/EJfxebw6Vg.jpg" alt="salontech-ienvyprofessional-precision-lash-applicator" />
															@endif
															</a>
														</div>
														<h4><a href="/{{$category->seo_url}}/{{$cat->seo_url}}">{{$cat->name}}</a>
														</h4>
													</div>
												</li>
											</ul>
										</div>
										@endforeach
										@else

										@foreach($category->products as $index => $product)
					
										<div class="lab-menu-col col-xs-6 col-sm-2">
											<ul class="ul-column">
												<li class="menu-item">
													<div class="html-block">
														<div class="img">
															<a href="/detail/{{$category->seo_url}}/{{$product->seo_url}}">
																<img src="{{Util::getImageWithSuffix($product->image, 'sm')}}" alt="{!! $product->product_name !!}" />
															</a>
														</div>
														<h4>{!! $product->product_name !!}</h4>
													</div>
												</li>
											</ul>
										</div>

										

										
										
										
										@endforeach
										@endif
									</div>
								</div>
							</li>
							@endforeach
						</ul>
					</div>
				</div>
			</div>
		</div>
	</header>
</div>
<section class="header-banner">
	<div class="notification">
		<section class="notification__content">
			<span class="notification__content__title">{{Util::getHeaderMessage()['header_label']}}</span>
			<span class="notification__content__subtitle">{{Util::getHeaderMessage()['header_message']}}</span>
		</section>
	</div>
</section>