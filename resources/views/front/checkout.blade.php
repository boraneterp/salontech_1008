@extends('layout.master', ['pageTitle' => 'Checkout'])
@section('content')
<div id="checkout"></div>
{!! Form::hidden('shipping', Util::getShipping())!!}
@endsection
@section('extra_js')
<script src="//www.paypalobjects.com/api/checkout.js"></script>
@endsection