@extends('layout.master', ['pageTitle' => $lookbook->name])
@section('content')
<div>
	<img src="{{$lookbook->image_src}}" class="img-responsive" alt="{{$lookbook->name}}" style="width:100%">
</div>
<div class="labbreadcrumb">
	<div class="container">
		<div class="breadcrumb-i">
			<div class="breadcrumb clearfix">
				<a class="home" href="/" title="Return to Home">Home</a>
				<span class="navigation-pipe">&gt;</span>
				<span class="navigation_page">Look Book</span>
			</div>
		</div>
	</div>
</div>
<div class="columns-container ">

	<div class="container">
		<h1 class="page-heading">{{$lookbook->name}}</h1>
		<div class="look-social">
			<a href="/" class="button button-default btn-sm btn-share">Share</a>
			<div class="look-social__list">
				<a href="#"><i class="fa fa-facebook"></i></a>
				<a href="#"><i class="fa fa-twitter"></i></a>
				<a href="#"><i class="fa fa-google"></i></a>
				<a href="#"><i class="fa fa-instagram"></i></a>
			</div>
		</div>
		@foreach($lookbook->steps as $index => $step)
		<div class="look-step">
			<div class="row">
				<div class="col-md-7">
					<img src="{{$step->image}}" class="img-responsive" alt="{{$lookbook->name}}">
				</div>
				<div class="col-md-5">
					<h2 class="look-step__title">{{$step->step_name}}</h2>
					<p>{{$step->description}}</p>
					@if($step->product_id > 0)
					<div class="look-step__product">
						<div class="row">
							<div class="col-xs-8">
								<h3>{!! $step->product->product_name !!}</h3>
								<button type="button" class="button btn-cart add-to-cart" data-product_id="{{$step->product->id}}"><span><i class="icon icon_bag_alt"></i> Add to cart</span>
		                        </button>
							</div>
							<div class="col-xs-4">
								<img src="{{$step->product->image}}">
							</div>
						</div>
					</div>
					@endif
				</div>
			</div>
		</div>
		@endforeach
	</div>

	<div class="look-book-container">
	    <div class="container">
	        <p class='title_block'>
	            <span>Other Looks</span>
	        </p>
	        <div class="row">
	        	@foreach($others as $lookbook)
	            <div class="col-sm-4">
	                 <div class="image_blog ">
	                    <a href="/lookbooks/{{$lookbook->seo_url}}"><img alt="{{$lookbook->name}}" class="feat_img_small" src="{{Util::getImageWithSuffix($lookbook->image_src, 'sm')}}"></a>
	                </div>
	                <h4><a href="/lookbooks/{{$lookbook->seo_url}}">{{$lookbook->name}}</a></h4>
	            </div>
	            @endforeach
	        </div>
	    </div>
	</div>
	@include('front.includes.instagram')
</div>
@endsection
@section('extra_js')
<script type="text/javascript">
	$(function() {
		$(".btn-share").on("click", function(e) {
			e.preventDefault();
			if($(".look-social__list").is(":visible")) {
				$(".look-social__list").slideUp();
			} else {
				$(".look-social__list").slideDown();
			}
		});
	});
</script>
@endsection