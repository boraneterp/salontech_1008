<!doctype html>
<html>
    <head itemscope="itemscope" itemtype="http://schema.org/WebPage">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta property="og:type" content="website" />
        <meta name="description" itemprop="description" property="og:description" content="SalonTech Provides innovative tools which are tailor-made to your hair styling needs. We continuously strive to give passionate stylists the opportunity to perform at their best, with Salontech Kreatin shampoo,Flat Irons, Curling Irons, Hair products, Dryers,and Camellia. Discount code: GREATHAIR. Free Shipping!">
        <meta name="keywords" property="og:keywords" itemprop="keywords" content="salontech, salon tech, hair product wholesale, hair product online, flat iron, curling iron, hair oil, hair essence, camellia, keratin shampoo, spinstyle, hair accessories, featherlight, hair blowout, badhair day, hair product vendor, professional hair product, hair salon, hair product nyc, hair product new york, hair accessories new york" />
        <meta property="og:title" content="Salontech">
        <meta name="author" content="Jason">
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <meta name="google-site-verification" content="j-f97pJXwpJ6VqaXYz6-rwHJIKngoCf6wYTiVDF82AA" />
        @yield('meta')
        <title>{{!empty($pageTitle)? $pageTitle: 'Best Hair Tools&Hair Products'}} | SalonTech</title>
        <link rel="shortcut icon" href="/assets/img/favicon.png" type="image/x-icon">
        <!-- Google web fonts -->
        <link href='//fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
        <link href='//fonts.googleapis.com/css?family=Lato:300,400' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
        <link href="/assets/css/style.css" rel="stylesheet">
        <script src="https://www.google.com/recaptcha/api.js"></script>
    </head>
    <body class="{{isset($body_class)?$body_class:''}}" >
        @if(getenv('APP_ENV') === 'prod')
         
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-124275058-3"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'UA-124275058-3');
</script>


        @endif
        <div class="wrapper" >
        	@include('front.includes.header')
            <div class="content-wrapper">
        	   @yield('content')
            </div>
           	@include('front.includes.footer')
        </div>
        <a href= "#" class="mypresta_scrollup hidden-phone">
        	<span><i class="fa fa-angle-double-up"></i></span>
        </a>
        {!! Form::hidden('cart', Util::getCart())!!}
        @include('front.includes.modal')
        <script src="/assets/js/vendor.js"></script>  
        @yield('extra_js')
        <script src="/assets/js/app.js"></script>
    </body>
</html>