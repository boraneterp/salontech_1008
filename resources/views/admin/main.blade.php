<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		 <meta name="description" content="SALON TECH Professional Flat Irons &amp;amp; Hair Straigtheners. Power Blow Dryers. 1in &amp;amp; 1.5in Flat irons. Free Shipping!">
        <meta name="keywords" content="SALON TECH,Silicon 450 Flat Irons, Ceramic Tourmaline, Special Blow Dryers, Flat Irons, Blow Dryer,free shipping, Hair Brush, Hair Clips, Hair Combs, Flat Iron 1in, Flat Iron 1.5in, 2010 fashion week, Best Styling Tools, how to apply flat iron, David Dieguez of Blow, Quick Fix, NY CITY SALONS" />
		<meta name="author" content="Jason">
		<meta name="csrf-token" content="{{ csrf_token() }}" />
		<link rel="shortcut icon" href="/assets/img/favicon.png" type="image/x-icon">
		@yield('meta')
		<title>SALON TECH - Admin</title>
		<!-- Google web fonts -->
		<link href='//fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
		<link href='//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css' rel='stylesheet' type='text/css'>
		
		<!-- CSS -->
		<link href="/assets/css/admin.css" rel="stylesheet">
		
	</head>
	<body>
		<div id="root"></div>
		{!! Form::hidden('google_api_key', getenv('GOOGLE_API_KEY'))!!}
		<script src="/assets/js/vendor.js"></script>
		<script src="/assets/admin/js/app.js"></script>
	</body>
</html>