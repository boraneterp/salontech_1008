<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="K Chicago - 시카고 한인 커뮤니티 사이트">
		<meta name="author" content="Jason">
		<meta name="csrf-token" content="{{ csrf_token() }}" />
		<link rel="shortcut icon" href="/assets/img/favicon.png" type="image/x-icon">
		@yield('meta')
		<title>SALON TECH - Admin</title>
		<!-- Google web fonts -->
		<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Lato:300,400' rel='stylesheet' type='text/css'>
		
		<!-- CSS -->
		<link href="/assets/css/admin.css" rel="stylesheet">
	</head>
	<body>
		<section id="hero" class="login">
			<div class="container">
				<div class="row">
					<div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
						<div id="login">
							<div class="text-center">
								<a href="/"><img src="/assets/img/logo.png" alt="Image" data-retina="complete" class="img-responsive"></a>
							</div>
							<hr />
							<form action="{{getenv('ADMIN_PATH')}}/login" method="post" class="popup-form" id="loginForm">
								{!! csrf_field() !!}
								@foreach ($errors->all() as $error)
								<div class="alert alert-danger fade in" >{{$error}}</div>
								@endforeach
								<div class="form-group">
									<label>Username</label>
									<input type="text" name="username" class="form-control required" placeholder="User Name" value="{{ old("username") }}">
								</div>
								<div class="form-group">
									<label>Password</label>
									<input type="password" name="password" class="form-control  required" placeholder="Password" value="">
								</div>
								<button class="btn_full" type="submit" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Sign in">Sign in</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</section>
		<script src="/assets/js/vendor.js"></script>
		<script type="text/javascript">
			$(function() {
				$("#loginForm").validate({
					messages: {
						username: {
							required: "User Name is required"
						},
						password: {
							required: "Password is required"
						}
					},
					submitHandler: function(form) {
						$("#loginForm .btn_full").button('loading');
						return true;
					}
				});
			});
		</script>
	</body>
</html>