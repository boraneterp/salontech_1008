
const resetForm = () => {
	$("#resetForm").validate({
		rules: {
			confirmation: {
				equalTo: "#resetForm input[name='password']"
			}
		},
		messages: {
			password: {
				required: "Password is required.",
				minlength: "Password length should be at least minimum of 8 characters."
			},
			confirmation: {
				equalTo: "Password Confirmation is not match to password."
			}
		},
		submitHandler: function(form) {
			$("#resetForm .alert").hide();
			$("#resetForm .btn-submit").button('loading');
			return true;
		}
	});
}

export default () => {
	resetForm()
}