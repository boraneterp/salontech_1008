import React from 'react'
import PropTypes from 'prop-types'

const PaymentForm = ({card_info, handleChange}) => {
	let years = []
	const date = new Date()
	for(let i = 0; i < 10; i++) {
		years.push(date.getFullYear() + i)
	}

	let months = []
	for(let i = 1; i <= 12; i++) {
		let month = i + ""
		if(i < 10) {
			month = "0" + i
		}
		months.push(month)
	}

	return (
	    <div>
			<div className="row">
				<div className="col-sm-6">
					<div className="form-group ">
						<label className="control-label required">Card Holder Name</label>
						<input className="form-control required" placeholder="Card Holder Name" name="card_holder_name" type="text" value={card_info.card_holder_name} onChange={handleChange} />
					</div>
				</div>
				<div className="col-sm-6">
					<div className="form-group">
						<label className="control-label required">Card Number</label>
						<input className="form-control required" placeholder="Card Number" name="card_number" type="text" value={card_info.card_number} onChange={handleChange} />
					</div>
				</div>
			</div>
			<div className="row">
				<div className="col-md-4">
					<div className="form-group">
						<label className="control-label required">Expiration Year</label>
						<select className="required form-control" name="expire_year" value={card_info.expire_year} onChange={handleChange} >
							{years.map((year) =>
							<option key={year} value={year}>{year}</option>
							)}
						</select>
					</div>
				</div>
				<div className="col-md-4">
					<div className="form-group">
						<label className="control-label required">Expiration Month</label>
						<select className="required form-control" name="expire_month" value={card_info.expire_month} onChange={handleChange}>
							{months.map((month) =>
							<option key={month} value={month}>{month}</option>
							)}
						</select>
					</div>
				</div>
				<div className="col-md-4">
					<div className="form-group">
						<label className="control-label required">Security Code</label>
						<input className="form-control required" placeholder="CCV" minLength="3" name="ccv" type="text" value={card_info.ccv} onChange={handleChange} />
					</div>
				</div>
			</div>
		</div>	
	)
}

export default PaymentForm