import React from 'react'
import PropTypes from 'prop-types'

const SideCartItem = ({item}) => (
	<div className="checkout-cart__item" >
		<div className="checkout-cart__item__image">
			<img src={item.product.image} />
		</div>
		<div className="checkout-cart__item__detail">
			<h4 className="checkout-cart__item__detail__heading" dangerouslySetInnerHTML={{__html: item.product.product_name}} />
			<div className="option-row">
				<span className="option-row__label">Qty:</span>
				<span className="option-row__value">{item.quantity}</span>
			</div>
			<div className="checkout-cart__item__detail__price">
				{(item.product.current_price != item.product.msrp) &&
                    <span className="checkout-cart__item__detail__price__msrp">${parseFloat(item.product.msrp * item.quantity).toFixed(2)}</span>
                }
				<span>${parseFloat(item.product.current_price * item.quantity).toFixed(2)}</span>
			</div>
		</div>
	</div>
)

export default SideCartItem