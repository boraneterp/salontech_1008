import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router'

const BreadCrumb = ({ page }) => {
	let title = "Shopping Cart"
	if(page === 'shipping') {
		title = "Shipping"
	} else if(page === 'payment') {
		title = "Payment"
	} else if(page === 'review') {
		title = "Review"
	} else if(page === 'success') {
		title = "Order Success"
	}
	return (
		<div className="labbreadcrumb">
		    <div className="container">
		        <div className="breadcrumb-i">
		            <div className="breadcrumb clearfix">
		                <a className="home" href="/" title="Return to Home">Home</a>
		                <span className="navigation-pipe">&gt;</span>
		                {page != 'cart' &&
		                	<span>
		                		<Link to='/checkout/cart' >Shopping Cart</Link>
			                	<span className="navigation-pipe">&gt;</span>
		                	</span>
		                }
		                {title}
		            </div>
		        </div>
		    </div>
		</div>
	)
}

BreadCrumb.propTypes = {
  	page: PropTypes.string.isRequired
}

export default BreadCrumb