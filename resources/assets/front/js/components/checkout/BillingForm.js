import React, { Component } from 'react';;
import { connect } from 'react-redux';;
import { Field, FieldArray, reduxForm } from 'redux-form';;
import * as FormHelper from '../../../../utils/RenderFormField';
import { getStates, saveBilling, editBilling, cancelEditBilling } from '../../actions/CheckoutAction';

const validate = values => {
    const errors = {}

    if (!values.email_address) {
        errors.email_address = 'Email Address is required.';
    }

    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if(!re.test(String(values.email_address).toLowerCase())) {
    	 errors.email_address = 'Email Address is invalid.';
    }

    if (!values.phone_number) {
        errors.phone_number = 'Phone Number is required.';
    }
    
    if (!values.first_name) {
        errors.first_name = 'First Name is required.';
    }

    if (!values.last_name) {
        errors.last_name = 'Last Name is required.';
    }

    if (!values.address_1) {
        errors.address_1 = 'Address is required.';
    }

    if (!values.city) {
        errors.city = 'City is required.';
    }

    if (!values.state) {
        errors.state = 'State is required.';
    }

    if (!values.zip) {
        errors.zip = 'Zip Code is required.';
    }

    return errors
}

class BillingForm extends Component {

	componentWillMount() {
		const { getStates } = this.props;
		getStates();
	}

	mapStates(states) {
		return states.map(s => {
			return {
				id: s.code,
				value: s.name
			}
		});
	}

    render() {
        const { address, saveBilling, states, showEdit, editBilling, cancelEditBilling, handleSubmit, error, pristine, reset, submitting } = this.props;
        const dispalayLabel = showEdit? 'none': 'block';
        const dispalayEdit = !showEdit? 'none': 'block';
        return (
        	<form onSubmit={handleSubmit(saveBilling)}>
        		{error &&
	        		<div className="alert alert-danger" role="alert">
	        			<span>There is an error, please try again later.</span>
					</div>
		    	}
		    	<div className="checkout-content">
		    		<ul className="address-list clearfix" style={{display: dispalayLabel}}>
						<li>
							<div className="row">
								<div className="col-sm-6">
									<div className="address-label">{address.first_name + ' ' + address.last_name}</div>
									<div dangerouslySetInnerHTML={{__html: address.full_address}}></div>
								</div>
								<div className="col-sm-6 address-detail-actions">
									<button type="button" className="button button-default button-smaill button-inline" onClick={editBilling}>Edit</button>
								</div>
							</div>
						</li>
					</ul>
					<div style={{display: dispalayEdit}}>
						<div className="row">
							<div className="col-sm-6">
								<Field name="first_name" component={FormHelper.renderField} label="First Name" required type="text" />
							</div>
							<div className="col-sm-6">
								<Field name="last_name" component={FormHelper.renderField} label="Last Name" required type="text" />
							</div>
						</div>
						<div className="row">
							<div className="col-sm-8">
								<Field name="address_1" component={FormHelper.renderField} label="Address" required type="text" />
							</div>
							<div className="col-sm-4">
								<Field name="address_2" component={FormHelper.renderField} label="Apt, Suite, Floor" type="text" />
							</div>
						</div>
						<div className="row">
							<div className="col-md-4">
								<Field name="city" component={FormHelper.renderField} label="City" required type="text" />
							</div>
							<div className="col-md-4">
								<Field name="state" component={FormHelper.renderDropdown} addDefault options={this.mapStates(states)} label="States" required />
							</div>
							<div className="col-md-4">
								<Field name="zip" component={FormHelper.renderField} label="Zip Code" required type="text" />
							</div>
						</div>
						<div className="address-actions">
							<button type="submit" className="button btn-submit button-smaill" >{submitting && <i className='fa fa-spinner fa-spin'></i>} Save</button>
							<button type="button" className="button button-default button-smaill" onClick={cancelEditBilling}>Cancel</button>
						</div>
					</div>
				</div>
            </form>
        )
    }
}

const mapStateToProps = (state, ownProps) => ({
    initialValues: state.cart.payment.guest_address,
    address: state.cart.payment.guest_address,
    states: state.states.data, 
    showEdit: state.editBilling
})

const guestCheckoutConnect = reduxForm({
    form: 'guestBilling',
    enableReinitialize: true,
    validate
})(BillingForm)

export default connect(mapStateToProps, { getStates, saveBilling, editBilling, cancelEditBilling } )(guestCheckoutConnect);

