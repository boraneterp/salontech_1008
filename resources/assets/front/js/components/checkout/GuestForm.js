import React, { Component } from 'react';;
import { connect } from 'react-redux';;
import { Field, FieldArray, reduxForm } from 'redux-form';;
import * as FormHelper from '../../../../utils/RenderFormField';
import { getStates, saveShipping } from '../../actions/CheckoutAction';

const validate = values => {
    const errors = {}

    if (!values.email_address) {
        errors.email_address = 'Email Address is required.';
    }

    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if(!re.test(String(values.email_address).toLowerCase())) {
    	 errors.email_address = 'Email Address is invalid.';
    }

    if (!values.phone_number) {
        errors.phone_number = 'Phone Number is required.';
    }
    
    if (!values.first_name) {
        errors.first_name = 'First Name is required.';
    }

    if (!values.last_name) {
        errors.last_name = 'Last Name is required.';
    }

    if (!values.address_1) {
        errors.address_1 = 'Address is required.';
    }

    if (!values.city) {
        errors.city = 'City is required.';
    }

    if (!values.state) {
        errors.state = 'State is required.';
    }

    if (!values.zip) {
        errors.zip = 'Zip Code is required.';
    }

    return errors
}

class GuestForm extends Component {

	componentWillMount() {
		const { getStates } = this.props;
		getStates();
	}

	mapStates(states) {
		return states.map(s => {
			return {
				id: s.code,
				value: s.name
			}
		});
	}

    render() {
        const { saveShipping, states, handleSubmit, error, pristine, reset, submitting } = this.props;
        return (
        	<form onSubmit={handleSubmit(saveShipping)}>
        		{error &&
	        		<div className="alert alert-danger" role="alert">
	        			{error == '401' ?
	        				<span dangerouslySetInnerHTML={{__html: 'This email address is already registed, do you want to <a href="/login" >Login</a>?'}}></span>:
	        				<span>There is an error, please try again later.</span>
	        			}
					</div>
		    	}
		    	{!error &&
			    	<div style={{marginBottom: 5}}><a href="/login" >Login</a> to keep track of your orders, check out faster?</div>
			    }
		    	<div className="checkout-content">
					<div className="row">
						<div className="col-sm-6">
							<Field name="email_address" component={FormHelper.renderField} label="Email Address" required type="email" />
						</div>
						<div className="col-sm-6">
							<Field name="phone_number" component={FormHelper.renderField} normalize={FormHelper.normalizePhone} label="Phone Number" required type="text" />
						</div>
					</div>
					<div className="row">
						<div className="col-sm-6">
							<Field name="first_name" component={FormHelper.renderField} label="First Name" required type="text" />
						</div>
						<div className="col-sm-6">
							<Field name="last_name" component={FormHelper.renderField} label="Last Name" required type="text" />
						</div>
					</div>
					<div className="row">
						<div className="col-sm-8">
							<Field name="address_1" component={FormHelper.renderField} label="Address" required type="text" />
						</div>
						<div className="col-sm-4">
							<Field name="address_2" component={FormHelper.renderField} label="Apt, Suite, Floor" type="text" />
						</div>
					</div>
					<div className="row">
						<div className="col-md-4">
							<Field name="city" component={FormHelper.renderField} label="City" required type="text" />
						</div>
						<div className="col-md-4">
							<Field name="state" component={FormHelper.renderDropdown} addDefault options={this.mapStates(states)} label="States" required />
						</div>
						<div className="col-md-4">
							<Field name="zip" component={FormHelper.renderField} label="Zip Code" required type="text" />
						</div>
					</div>
					<p>Orders placed after 5pm EST begin processing the next business day.</p>
				</div>
				<div className="" >
					<span className="important">*</span>&nbsp;Required Fields
					<button type="submit" disabled={submitting} className="button btn-submit btn-sm pull-right" >{submitting && <i className='fa fa-spinner fa-spin'></i>} NEXT STEP</button>
				</div>
            </form>
        )
    }
}

const mapStateToProps = (state, ownProps) => ({
    initialValues: state.cart.shipping.guest_address,
    states: state.states.data
})

const guestCheckoutConnect = reduxForm({
    form: 'guestCheckout',
    enableReinitialize: true,
    validate
})(GuestForm)

export default connect(mapStateToProps, { getStates, saveShipping } )(guestCheckoutConnect);

