import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router'
import Help from './Help'

const CartSideBar = ({cart, shipping}) => {
    const loginUrl = '/login?redirectUrl=' + window.location.origin + '/checkout/shipping'
    return (
    	<div className="col-sm-3">
            <div className="summary">
                {!_.isEmpty(cart.content.items) &&
                <section className="summary__content">
                    <h4 className="summary__content__title">SUMMARY</h4>
                    <div className="summary-row">
                        <span className="summary__content__label">SUBTOTAL</span>
                        <span className="summary__content__value">${parseFloat(cart.content.sub_total).toFixed(2)}</span>
                    </div>
                    {cart.content.sub_total < parseFloat(shipping.free_shipping_amount) &&
                        <div className="summary-row">
                            <span className="summary__content__label">SHIPPING & HANDLING</span>
                            <span className="summary__content__value">${parseFloat(shipping.flat_fee).toFixed(2)}</span>
                        </div>
                    }
                    <div className="summary-row">
                        <span className="summary__content__label--big">Total</span>
                        <span className="summary__content__value--big">${parseFloat(cart.content.total).toFixed(2)}</span>
                    </div>
                    <div className="summary-row">
                        <Link to='/checkout/shipping' className="button btn-submit btn-full">Checkout</Link>
                    </div>
                </section>
                }
                <Help />
            </div>
        </div>
    )
}

export default CartSideBar