import React from 'react'
import PropTypes from 'prop-types'

const Help = () => (
    <section className="summary__help">
        <div className="summary__help__title">NEED HELP?</div>
        <ul >
            <li><a href="#">How is my order secure?</a></li>
            <li><a href="#">What payment methods can I use?</a></li>
            <li><a href="#">When will I get my order?</a></li>
        </ul>
        <a href="/faqs" className="summary__message__body">More Help</a>
    </section>
)

export default Help