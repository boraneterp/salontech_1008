import React, { Component } from 'react'
import PropTypes from 'prop-types'

class CartItem extends Component {

    constructor() {
        super()
        this.clickMinus = this.clickMinus.bind(this);
        this.clickPlus = this.clickPlus.bind(this);
    }

    clickMinus(e) {
        e.preventDefault();
        const { item } = this.props;
        if(item.quantity <= 1)
            return;
        this.props.onUpdate(item.quantity - 1)
    }

    clickPlus(e) {
        e.preventDefault();
        const { item } = this.props
        this.props.onUpdate(parseInt(item.quantity) + 1)
    }

    render() {
        const { item, onDelete, onUpdate } = this.props
        return (
        	<li className="cart__content__item">
                <div className="row" >
                    <div className="col-sm-3">
                        <a href={item.product.url}>
                            <img src={item.product.image} className="cart__content__image" />
                        </a>
                    </div>
                    <div className="col-sm-6">
                        <div className="cart__content__item__detail">
                            <a href={item.product.url} dangerouslySetInnerHTML={{__html: item.product.product_name}} />
                            <div className="cart__content__item__detail__option">
                                <div className="option-row">
                                    <span className="option-row__label">Qty:</span>
                                    <span className="option-row__value">{item.quantity} x ${parseFloat(item.product.current_price).toFixed(2)}</span>
                                    <button className="cart__content__item__detail__btn " onClick={this.clickMinus}>
                                        <span><i className="fa fa-minus"></i></span>
                                    </button>
                                    <button className="cart__content__item__detail__btn" onClick={this.clickPlus}>
                                        <span><i className="fa fa-plus"></i></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div className="cart__content__item__action">
                            <button className="button button-default btn-sm" onClick={(e) => {e.preventDefault(); onDelete()}}>Remove</button>
                        </div>
                    </div>
                    <div className="col-sm-3">
                        <div className="cart__content__item__price">
                            {(item.product.current_price != item.product.msrp) &&
                                <p className="cart__content__item__price__strikeout">${parseFloat(item.product.msrp * item.quantity).toFixed(2)}</p>
                            }
                            <p className="">${parseFloat(item.product.current_price * item.quantity).toFixed(2)}</p>
                        </div>
                    </div>
                </div>
            </li>
        )
    }
}

export default CartItem