import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router'
import SideCartItem from './SideCartItem'
import Help from './Help'

const CheckoutSideBar = ({cart, shipping}) => (
	<div className="col-sm-3">
		<h2 className="checkout-heading">SUMMARY</h2>
		<div className="checkout-summary-content">
			<div className="checkout-summary-row">
				<span className="checkout-summary-content__label">SUBTOTAL</span>
				<span className="checkout-summary-content__value">${parseFloat(cart.content.sub_total).toFixed(2)}</span>
			</div>
			{(cart.content.promotion_label != '' && cart.content.promotion > 0) &&
            <div className="checkout-summary-row">
				<span className="checkout-summary-content__label">{cart.content.promotion_label}</span>
				<span className="checkout-summary-content__value">-${cart.content.promotion}</span>
			</div>
			}
			{cart.content.sub_total < parseFloat(shipping.free_shipping_amount) &&
                <div className="checkout-summary-row">
                    <span className="checkout-summary-content__label">SHIPPING & HANDLING</span>
                    <span className="checkout-summary-content__value">${parseFloat(shipping.flat_fee).toFixed(2)}</span>
                </div>
            }
			<div className="checkout-summary-row">
				<span className="checkout-summary-content__label">TAX</span>
				<span className="checkout-summary-content__value">${parseFloat(cart.content.tax).toFixed(2)}</span>
			</div>
			<div className="checkout-summary-row">
				<span className="checkout-summary-content__label--big">Total</span>
				<span className="checkout-summary-content__value--big">${parseFloat(cart.content.total).toFixed(2)}</span>
			</div>
		</div>
		<h2 className="checkout-heading">IN YOUR CART<Link to='/checkout/cart' className="button button-default">Edit</Link></h2>
		<div className="checkout-summary-content">
			<div className="checkout-cart">
				<h3 className="checkout-cart__heading"></h3>
				{cart.content.items.map(item =>
                    <SideCartItem key = { item.id }
                      item = { item }/>
                )}
			</div>
		</div>
		<Help />
	</div>
)
export default CheckoutSideBar