import React from 'react'

const Loading = () => (
	<div className="page-loader">
		<i className="fa fa-circle-o-notch fa-spin fa-3x fa-fw" ></i>
	</div>
)

export default Loading