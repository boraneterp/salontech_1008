import React from 'react'
import PropTypes from 'prop-types'

const OrderProductItem = ({product}) => (
	<li className="cart__content__item">
        <div className="row" >
            <div className="col-sm-3">
                <a href={product.detail.url}>
                    <img src={product.detail.image} className="cart__content__image" />
                </a>
            </div>
            <div className="col-sm-6">
                <div className="cart__content__item__detail">

                    <a href={product.detail.url} dangerouslySetInnerHTML={{__html: product.detail.product_name}} />
                    <div className="cart__content__item__detail__option">
                        <div className="option-row">
                            <span className="option-row__label">Item #:</span>
                            <span className="option-row__value">{product.detail.item_no}</span>
                        </div>
                        <div className="option-row">
                            <span className="option-row__label">Qty:</span>
                            <span className="option-row__value">{product.quantity} x ${parseFloat(product.price).toFixed(2)}</span>
                        </div>
                    </div>
                </div>
            </div>
             <div className="col-sm-3">
                <div className="cart__content__item__price">
                    <p className="">${parseFloat(product.total_price).toFixed(2)}</p>
                </div>
            </div>
        </div>
    </li>
)

export default OrderProductItem