import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router'

const AccountSideBar = ({ page }) => (
	<aside className="col-sm-3">
		<ul className="account-nav">
			<li>
				{ new RegExp("^/account(/?)$").exec(page) !== null ?
		            <span className="active"><i className="fa fa-cog"></i>General information</span>
		            :
		            <Link to={`/account`} ><i className="fa fa-cog"></i>General information</Link>
		        }
			</li>
			<li>
				{ new RegExp("^/account/password(/?)$").exec(page) !== null ?
		            <span className="active"><i className="fa fa-lock"></i>Change Password</span>
		            :
		            <Link to={`/account/password`} ><i className="fa fa-lock"></i>Change Password</Link>
		        }
			</li>
			<li>
				{ page.startsWith("/account/orders") ?
		            <span className="active"><i className="fas fa-shopping-cart"></i>My Orders</span>
		            :
		            <Link to={`/account/orders`} className=""><i className="fas fa-shopping-cart"></i>My Orders</Link>
		        }
			</li>
			<li>
				{ new RegExp("^/account/address(/?)$").exec(page) !== null ?
		            <span className="active"><i className="far fa-address-card"></i>Addresses</span>
		            :
		            <Link to={`/account/address`} className=""><i className="far fa-address-card"></i>Addresses</Link>
		        }
			</li>
			<li>
				{ new RegExp("^/account/payment(/?)$").exec(page) !== null ?
		            <span className="active"><i className="fa fa-credit-card"></i>Payment Options</span>
		            :
		            <Link to={`/account/payment`} className=""><i className="fa fa-credit-card"></i>Payment Options</Link>
		        }
			</li>
		</ul>
	</aside>
)

export default AccountSideBar