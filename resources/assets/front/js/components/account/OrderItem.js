import React from 'react'
import { Link } from 'react-router'

const OrderItem = ({index, order}) => {
	const status = order.status === 'P' ? 'Pending' : order.status === 'I' ? 'Processing' : order.status === 'R' ? 'Refunded' : 'Completed';
	const statusClass = 'order-status ' + status.toLowerCase()
	return (
		<tr>
	        <td>{index}</td>
	        <td><b>${parseFloat(order.order_total).toFixed(2)}</b></td>
	        <td>{order.created_at}</td>
	        <td><span className={statusClass}>{status}</span></td>
	        <td><Link className="button button-default button-smaill" to={`/account/orders/${order.id}`} >View</Link></td>
	    </tr>
	)
}

export default OrderItem