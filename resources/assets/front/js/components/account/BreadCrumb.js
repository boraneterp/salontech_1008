import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router'

const BreadCrumb = ({ page }) => (
	<div className="labbreadcrumb">
	    <div className="container">
	        <div className="breadcrumb-i">
	            <div className="breadcrumb clearfix">
	                <a className="home" href="/" title="Return to Home">Home</a>
	                <span className="navigation-pipe">&gt;</span>
	                My Account
	            </div>
	        </div>
	    </div>
	</div>
)

BreadCrumb.propTypes = {
  	page: PropTypes.string.isRequired
}

export default BreadCrumb