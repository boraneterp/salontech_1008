import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import _ from 'lodash';
import { getStates } from '../../actions/CheckoutAction';
import { removeAddress } from '../../actions/AccountAction';
import AddressDetail from './AddressDetail';
import ShippingForm from './ShippingForm';

class AddressComponent extends Component {

  	constructor() {
    	super()
    	this.state = {
    		error: "",
			edit: true,
			activeAddress: {
				id: 0,
				first_name: '',
				last_name: '',
				email: '',
				phone_number: '',
				address_1: '',
				address_2: '',
				city: '',
				zip: ''
			}
    	}

    	this.cancelEdit = this.cancelEdit.bind(this);
    	this.chooseAddress = this.chooseAddress.bind(this);
    	this.handleChange = this.handleChange.bind(this);
    }

    componentWillMount() {
		const { getStates, used_address, addresses } = this.props

		getStates()

		for (let address of addresses) {
		  	if(address.id == used_address) {
		    	this.setState({
		    		edit: false,
		      		activeAddress: address
		    	});
		    	break
		  	}
		}

		if(typeof used_address === 'undefined') {
			this.setState({
	    		edit: false
	    	});
		}
	}

	componentDidMount() {

		const { edit } = this.state
		const { onSave } = this.props

		const self = this;

		$("#addressForm").validate({
		  	messages: {
			    first_name: "First Name is required.",
			    last_name: "Last Name is required.",
			    phone_number: {
		          	required:  "Phone Number is required.",
		            minlength: "Phone Number is invalid."
		        },
		        email_address: {
		          	required: "Email Address is required.",
		            email: "Email Address is invalid."
		        },
			    address_1: "Address is required.",
			    state: "State is required.",
			    city: "City is required.",
			    zip: "Zip Code is required."
		  	}, 
		  	submitHandler: function(form) {
		  		self.setState({
		          	error: ""
		        });
		    	$("#addressForm .btn-submit:enabled").button('loading');
		    	$("#addressForm").ajaxSubmit({
		      		dataType: 'json',
		      		error: function(e) {
		        		self.setState({
				          	error: e.responseJSON
				        });
		        		// $('html, body').animate({
		          //   		scrollTop: $(".cart-page-menu").offset().top - 90
		        		// }, 200);
		        		$(".btn-submit").button('reset');
		      		},
		      		success: function(data) {
		      			$(".btn-submit").button('reset')
		      			onSave(data)
		      			self.setState({
				          	edit: false
				        });
		        	}
		    	}); 
		    	return false;
		  	}
		});
  	}

  	cancelEdit() {
  		const { addresses, chooseable } = this.props
  		if(addresses.length === 0 && chooseable) {
  			return
  		}
  		this.setState({
		    edit: false
		});
  	}

  	chooseAddress(address) {
		this.setState({
			shipping_address: address.id
		});
	}

	editAddress(address) {
		if(_.isEmpty(address)) {
			address.id = 0
			address.address_label = ''
			address.first_name = ''
			address.last_name = ''
			address.email = this.state.activeAddress.email
			address.phone_number = ''
			address.address_1 = ''
			address.address_2 = ''
			address.city = ''
			address.zip = ''
		}

		const activeAddress = Object.assign({}, address)
		this.setState({
			edit: true,
			activeAddress
		});
	}

	handleChange(event) {
		const target = event.target;
		const value = target.value;
		const name = target.name;
		let activeAddress = this.state.activeAddress;
		activeAddress[name] = value
		this.setState({
		    activeAddress
		});
  	}

  	render() {
  		const { error, edit, activeAddress } = this.state
  		const { addresses, used_address, states, saveLabel, chooseable, onChoose, removeAddress } = this.props
  		return (
	  		<form id="addressForm" method="post" action="/account/address/save">
	  			<input name="address_id" type="hidden" defaultValue={activeAddress.id} />
				{(addresses.length > 0) &&
                  	<ul className="address-list clearfix">
                   	{addresses.map((address, index) =>
                    	<AddressDetail
							key={address.id}
							index={index + 1}
							address={address}
							used_address={used_address}
							chooseable={chooseable}
							onChoose={() => onChoose(address)}
							onEdit={() => this.editAddress(address)}
							onRemove={() => removeAddress(address.id)}
                    	/>
                    )}
                    <li className="new-address"><button type="button" className="button button-default button-smaill" onClick={() => this.editAddress({})}>New Address</button></li>
                  </ul>
                }
                {(addresses.length === 0 && !chooseable) &&
                  	<ul className="address-list clearfix">
                    	<li className="new-address"><button type="button" className="button button-default button-smaill" onClick={() => this.editAddress({})}>New Address</button></li>
                  	</ul>
                }
                {!_.isEmpty(error) &&
					<div className="alert alert-danger fade in " dangerouslySetInnerHTML={{__html: error}}></div>
				}
                {edit &&
					<ShippingForm 
						states={states} 
						activeAddress={activeAddress}
						saveLabel={saveLabel}
						cancelEdit={this.cancelEdit} 
						handleChange={this.handleChange} 
					/>
                }
			</form>
		)
  	}
}
const mapStateToProps = (state, ownProps) => ({
    states: state.states.data
})

export default connect(
  	mapStateToProps, {
  	getStates, removeAddress
})(AddressComponent)