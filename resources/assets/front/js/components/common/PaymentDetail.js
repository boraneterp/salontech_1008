import React from 'react'
import PropTypes from 'prop-types'

const PaymentDetail = ({ index, payment, used_payment, chooseable, onChoose, onEdit, onRemove }) => { 
	return (
		<li >
			<div className="row">
				<div className="col-sm-6">
					<div className="payment-label">{payment.card_holder_name}</div>
					<div>{payment.card_label}</div>
				</div>
				<div className="col-sm-6 payment-detail-actions">
				{chooseable && 
					<span>
						{(used_payment == payment.id)? 
							<span className="active"><i className="fa fa-check"></i></span>:
							<button type="button" className="button button-default button-smaill" onClick={onChoose}>Use This payment</button>
						}
					</span>
				}
				<button type="button" className="button button-default button-smaill button-inline" onClick={onEdit}>Edit</button>
				{!chooseable && 
					<button type="button" className="button button-default button-smaill button-inline button-danger" onClick={onRemove}>Remove</button>
				}
				</div>
			</div>
		</li>
	)
}

PaymentDetail.propTypes = {
	index: PropTypes.number.isRequired,
  	payment: PropTypes.object.isRequired
}

export default PaymentDetail