import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import _ from 'lodash'
import { removePayment } from '../../actions/AccountAction'
import PaymentDetail from './PaymentDetail'
import PaymentForm from './PaymentForm'

class PaymentComponent extends Component {

  	constructor() {
    	super()
    	this.state = {
    		error: "",
			edit: true,
			activePayment: {
				id: 0,
				card_label: '',
				card_holder_name: '',
				card_number: '',
				expire_year: '',
				expire_month: '',
				ccv: ''
			}
    	}

    	this.cancelEdit = this.cancelEdit.bind(this);
    	this.choosePayment = this.choosePayment.bind(this);
    	this.handleChange = this.handleChange.bind(this);
    }

    componentWillMount() {
		const { used_payment, payments } = this.props

		if(typeof used_payment === 'undefined') {
			this.setState({
	    		edit: false
	    	});
	    	return;
		}

		for (let payment of payments) {
		  	if(payment.id == used_payment) {
		    	this.setState({
		    		edit: false,
		      		activePayment: payment
		    	});
		    	break
		  	}
		}
		
	}

	componentDidMount() {

		const { edit } = this.state
		const { onSave } = this.props

		const self = this;

		$("#paymentForm").validate({
		  	messages: {
	        	card_holder_name: "Card Holder Name is required.",
	        	card_number: {
					required: "Card Number is required.",
					minlength: "Card Number is invalid."
	        	}, 
	        	ccv: {
					required: "CCV is required.",
					minlength: "CCV is invalid."
				}, 
				billing_zip: {
					required: "Billing Zipcode is required.",
					minlength: "Zipcode is invalid."
				}
	      	}, 
		  	submitHandler: function(form) {
		  		self.setState({
		          	error: ""
		        });
		    	$("#paymentForm .btn-submit:enabled").button('loading');
		    	$("#paymentForm").ajaxSubmit({
		      		dataType: 'json',
		      		error: function(e) {
		        		self.setState({
				          	error: e.responseJSON
				        });
		        		// $('html, body').animate({
		          //   		scrollTop: $(".cart-page-menu").offset().top - 90
		        		// }, 200);
		        		$(".btn-submit").button('reset');
		      		},
		      		success: function(data) {
		      			$(".btn-submit").button('reset')
		      			onSave(data)
		      			self.setState({
				          	edit: false
				        });
		        	}
		    	}); 
		    	return false;
		  	}
		});
  	}

  	cancelEdit() {
  		const { payments, chooseable } = this.props
  		if(payments.length === 0 && chooseable) {
  			return
  		}
  		this.setState({
		    edit: false,
		    error: ''
		});
  	}

  	choosePayment(payment) {
		this.setState({
			payment: payment.id
		});
	}

	editpayment(payment) {
		if(_.isEmpty(payment)) {
			payment.id = 0
			payment.card_label = ''
			payment.card_holder_name = '',
			payment.card_number = '',
			payment.expire_year = '',
			payment.expire_month = '',
			payment.ccv = ''
		}

		const activePayment = Object.assign({}, payment)
		this.setState({
			edit: true,
			activePayment
		});
	}

	handleChange(event) {
		const target = event.target;
		const value = target.value;
		const name = target.name;

		if(name === 'card_holder_name') {
			const reg = /^[a-z ,.'-]*$/i
			if(!reg.test(value) || value.length > 40)
				return
		}

		if(name === 'card_number') {
			const reg = /^\d*$/
			if(!reg.test(value) || value.length > 20)
				return
		}

		if(name === 'ccv') {
			if(value.length > 5)
				return
		}
		
		let activePayment = this.state.activePayment;
		activePayment[name] = value
		this.setState({
		    activePayment
		});
  	}

  	render() {
  		const { error, edit, activePayment } = this.state
  		const { payments, used_payment, states, saveLabel, chooseable, onChoose, removePayment } = this.props
  		return (
	  		<form id="paymentForm" method="post" action="/account/payment/save">
	  			<input name="payment_id" type="hidden" defaultValue={activePayment.id} />
				{(payments.length > 0) &&
                  	<ul className="payment-list clearfix">
                   	{payments.map((payment, index) =>
                    	<PaymentDetail
							key={payment.id}
							index={index + 1}
							payment={payment}
							used_payment={used_payment}
							chooseable={chooseable}
							onChoose={() => onChoose(payment)}
							onEdit={() => this.editpayment(payment)}
							onRemove={() => removePayment(payment.id)}
                    	/>
                    )}
                    <li className="new-payment"><button type="button" className="button button-default button-smaill" onClick={() => this.editpayment({})}>New payment</button></li>
                  </ul>
                }
                {(payments.length === 0 && !chooseable) &&
                  	<ul className="payment-list clearfix">
                    	<li className="new-payment"><button type="button" className="button button-default button-smaill" onClick={() => this.editpayment({})}>New payment</button></li>
                  	</ul>
                }
                {edit &&
                	<div>
	                	{!_.isEmpty(error) &&
							<div className="alert alert-danger fade in " dangerouslySetInnerHTML={{__html: error}}></div>
						}
						<PaymentForm 
							states={states} 
							activePayment={activePayment}
							saveLabel={saveLabel}
							cancelEdit={this.cancelEdit} 
							handleChange={this.handleChange} 
						/>
					</div>
                }
			</form>
		)
  	}
}
const mapStateToProps = (state, ownProps) => ({
    states: state.states.data
})

export default connect(
  	mapStateToProps, {
  	removePayment
})(PaymentComponent)