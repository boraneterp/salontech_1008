import React from 'react'
import PropTypes from 'prop-types'

const AddressDetail = ({ index, address, used_address, chooseable, onChoose, onEdit, onRemove }) => { 
	return (
		<li >
			<div className="row">
				<div className="col-sm-6">
					<div className="address-label">{address.first_name + ' ' + address.last_name}</div>
					<div dangerouslySetInnerHTML={{__html: address.full_address}}></div>
				</div>
				<div className="col-sm-6 address-detail-actions">
				{chooseable && 
					<span>
						{(used_address == address.id)? 
							<span className="active"><i className="fa fa-check"></i></span>:
							<button type="button" className="button button-default button-smaill" onClick={onChoose}>Use This Address</button>
						}
					</span>
				}
				<button type="button" className="button button-default button-smaill button-inline" onClick={onEdit}>Edit</button>
				{!chooseable && 
					<button type="button" className="button button-default button-smaill button-inline button-danger" onClick={onRemove}>Remove</button>
				}
				</div>
			</div>
		</li>
	)
}

AddressDetail.propTypes = {
	index: PropTypes.number.isRequired,
  	address: PropTypes.object.isRequired
}

export default AddressDetail