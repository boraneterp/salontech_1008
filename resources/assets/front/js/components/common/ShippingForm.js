import React from 'react'
import PropTypes from 'prop-types'
import InputMask from 'react-input-mask'

const ShippingForm = ({states, activeAddress, saveLabel, handleChange, cancelEdit}) => (
    <div>
		<div className="row">
			<div className="col-sm-4">
				<div className="form-group ">
					<label className="control-label required">First Name</label>
					<input className="form-control required" placeholder="First Name" value={activeAddress.first_name} name="first_name" type="text" onChange={handleChange} />
				</div>
			</div>
			<div className="col-sm-4">
				<div className="form-group">
					<label className="control-label required">Last Name</label>
					<input className="form-control required" placeholder="Last Name" value={activeAddress.last_name} name="last_name" type="text" onChange={handleChange} />
				</div>
			</div>
			<div className="col-sm-4">
				<div className="form-group">
					<label className="control-label required">Phone Number</label>
					<InputMask className="form-control required" mask="(999) 999-9999" maskChar=" " name="phone_number" placeholder="Phone Number"  value={activeAddress.phone_number} onChange={handleChange} />
				</div>
			</div>
		</div>
		<div className="row">
			<div className="col-sm-8">
				<div className="form-group">
					<label className="control-label required">Address</label>
					<input className="form-control required" placeholder="Address" name="address_1" type="text" value={activeAddress.address_1} onChange={handleChange} />
				</div>
			</div>
			<div className="col-sm-4">
				<div className="form-group">
					<label>Apt, Suite, Floor</label>
					<input className="form-control" placeholder="Apt, Suite, Floor" name="address_2" type="text" value={activeAddress.address_2} onChange={handleChange} />
				</div>
			</div>
		</div>
		<div className="row">
			<div className="col-md-4">
				<div className="form-group">
					<label className="control-label required">City</label>
					<input className="form-control required" placeholder="City" name="city" type="text" value={activeAddress.city} onChange={handleChange} />
				</div>
			</div>
			<div className="col-md-4">
				<div className="form-group">
					<label className="control-label required">State</label>
					<select className="required form-control" name="state" defaultValue={activeAddress.state} onChange={handleChange}>
						<option value="">Select State</option>
						{states.map((state) =>
						<option key={state.code} value={state.code}>{state.name}</option>
						)}
					</select>
				</div>
			</div>
			<div className="col-md-4">
				<div className="form-group">
					<label className="control-label required">Zip code</label>
					<input className="form-control required" placeholder="Zip Code" name="zip" type="text" value={activeAddress.zip} onChange={handleChange} />
				</div>
			</div>
		</div>
		<div className="address-actions">
			<button type="submit" className="button btn-submit button-smaill" data-loading-text="<i class='fa fa-spinner fa-spin'></i> SAVE">{saveLabel? saveLabel: 'Save & Use This Address'}</button>
			<button type="button" className="button button-default button-smaill" onClick={cancelEdit}>Cancel</button>
		</div>
	</div>
				
)

export default ShippingForm