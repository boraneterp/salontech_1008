const initialState = {
  addresses: [],
  payments: [],
  orders: []
}

const users = (state = initialState, action) => {
  	switch (action.type) {
    	case "GET_USER":
    		return Object.assign({}, state, action.user)
    	default:
      		return state
  	}
}

export default users