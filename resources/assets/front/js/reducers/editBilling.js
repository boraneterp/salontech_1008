const editBilling = (state = false, action) => {
  	switch (action.type) {
    	case "EDIT_BILLING":
    		return true;
    	case "CANCEL_EDIT_BILLING":
    		return false;
    	default:
      		return state
  	}
}

export default editBilling;