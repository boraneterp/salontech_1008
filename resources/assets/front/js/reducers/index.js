import { routerReducer as routing } from 'react-router-redux';
import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import user from './user';
import cart from './cart';
import shipping from './shipping';
import states from './states';
import editBilling from './editBilling';

const rootReducer = combineReducers({
	user,
	cart,
	shipping,
	states,
	editBilling,
	form: formReducer,
  	routing
})

export default rootReducer
