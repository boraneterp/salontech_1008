const initialState = JSON.parse($("input[name='cart']").val())
const cart = (state = initialState, action) => {
  	switch (action.type) {
  		case "GET_CART":
	      return Object.assign({}, state, action.cart)
    	case "GET_SHIPPING_QUOTE":
	      return {
    			...state,
    			shipping: {
    				...state.shipping,
    				quotes: action.quotes
    			}
    		}
      case "SET_SHIPPING_ADDRESS":
        return {
          ...state,
          content: {
            ...state.content
          },
          shipping: {
            ...state.shipping,
            shipping_address: action.address.id
          }
        }
      case "SET_SHIPPING_METHOD":
        const total = parseFloat(state.content.sub_total) + parseFloat(state.content.tax) + parseFloat(action.shipping_price)
        return {
          ...state,
          content: {
            ...state.content,
            total
          },
          shipping: {
            ...state.shipping,
            shipping_method: action.shipping_method,
            shipping_price: action.shipping_price
          }
        }
	    case "ADD_PRODUCT":
	    	return Object.assign({}, state, action.cart)
	    case "HIDE_PRODUCT":
	      return {
	      	...state,
	      	layerProduct: {}
	    }
    	default:
      		return state
  	}
}

export default cart