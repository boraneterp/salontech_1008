const initialState = {
  data: []
}

const states = (state = initialState, action) => {
  	switch (action.type) {
    	case "GET_STATES":
    		return {
    			...state,
    			data: action.data
    		}
      
    	default:
      		return state
  	}
}

export default states