
const profileForm = () => {
	$("#edit-profile").on("click", function(event){
		event.preventDefault();
		$("#profileForm input[name='first_name']").val($(this).data("first_name"));
		$("#profileForm input[name='last_name']").val($(this).data("last_name"));
		$("#profile-modal").modal();
	});

	$("#profile-modal").on('shown.bs.modal', function (e) {
		$("#profileForm .alert").hide();
		$("#profileForm .btn_1").button('reset');
		$("#profileForm").validate({
			messages: {
				first_name: "First Name is required.",
				last_name: {
					required: "Last Name is required"
		        }
			}, 
			submitHandler: function(form) {
				$("#profileForm .alert").hide();
				$("#profileForm .btn_1").button('loading');
				$(form).ajaxSubmit({
					dataType: 'json',
					error: function(e) {
						$("#profileForm .alert").show();
						$("#profileForm .btn_1").button('reset');
					},
					success: function(data) {
						window.location.reload(true);
						
					}
				});
				return false;
			}
		});
	});
}

const passwordForm = () => {
	$("#change-password").on("click", function(event){
		event.preventDefault();
		$("#passwordForm input[type='password']").val("");
		$("#password-modal").modal();
	});

	$("#password-modal").on('shown.bs.modal', function (e) {
		$("#passwordForm .alert").hide();
		$("#passwordForm .btn_1").button('reset');
		$("#passwordForm").validate({
			rules: {
			    confirmation: {
					equalTo: "#passwordForm input[name='new_password']"
				}
			},
			messages: {
				current_password: {
					required: "Current Password is required.",
					minlength: "Password length should be at least minimum of 8 characters."
				},
				new_password: {
					required: "New Password is required.",
					minlength: "Password length should be at least minimum of 8 characters."
		        },
				confirmation: {
		            equalTo: "Password Confirmation is not match to password." 
		        }
			}, 
			submitHandler: function(form) {
				$("#passwordForm .alert").hide();
				$("#passwordForm .btn_1").button('loading');
				$(form).ajaxSubmit({
					dataType: 'json',
					error: function(e) {
						$("#passwordForm .alert").text(e.responseText).show();
						$("#passwordForm .btn_1").button('reset');
					},
					success: function(data) {
						$("#password-modal").modal('hide');
					}
				});
				return false;
			}
		});
	});
}

export default () => {
	profileForm();
	passwordForm();
}