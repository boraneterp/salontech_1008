
const categoryProduct = () => {
	$("select[name='category_id']").on("change", function(e) {
		$("select[name='product_id']").html("<option value=''>Choose Product</option>");
		if($(this).val() == '') {
			return;
		}
		$.getJSON( "/category/" + $(this).val() + "/products.json", function( products ) {
			$.each(products, function(i) {
				$("select[name='product_id']").append($("<option></option>")
			                    .attr("value", products[i].id)
			                    .text(products[i].product_name)); 
			})
		});
	});
}

const agreement = () => {
	$(".checkbox-list").on("click", "input", function(e) {
		if($(".checkbox-list .checkbox-wrapper").hasClass("checked")) {
			$(".checkbox-list .checkbox-wrapper").removeClass("checked");
			$(".agree-error").show();
		} else {
			$(this).parent().addClass("checked");
			$(".agree-error").hide();
		}
	});
}

const warrantyForm = () => {
	$("#warrantyForm").validate({
		messages: {
			first_name: "First Name is required.",
			last_name: "Last Name is required.",
			phone_number: "Phone Number is required.",
			email: {
	        	required: "Email Address is required",
	            email: "Email Address is invalid"
	        },
			address_1: "Address is required.",
			city: "City is required.",
			state: "State is required.",
			zip: "Zip Code is required.",
			category_id: "Category is required.",
			product_id: "Product is required.",
			proof_of_purchase: "Proof of Purchase is required.",
			reason: "Reason is required.",
			agree: "Please confirm all information is correct."
		}, 
        submitHandler: function(form) {
			$("label.error, .alert, .agree-error").hide();
			if(!$(".checkbox-list .checkbox-wrapper").hasClass("checked")) {
				$(".agree-error").show();
				return;
			}
			$("#warrantyForm .btn-submit").button('loading');
			$("#warrantyForm").ajaxSubmit({
				error: function(e) {
					$("#warrantyForm .alert-danger").show();
					$(".btn-submit").button('reset');
					$('html, body').animate({
				        scrollTop: $("body").offset().top - 90
				    }, 0);
				},
				success: function(message) {
					$("#warrantyForm")[0].reset();
					$(".btn-submit").button('reset');
					$("#warrantyForm .alert-success").show();
					$('html, body').animate({
				        scrollTop: $("body").offset().top - 90
				    }, 500);
				}
			}); 
			return false;
		}
	 });
}

export default () => {
	categoryProduct()
	agreement()
	warrantyForm()
}