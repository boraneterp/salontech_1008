import _ from 'lodash'

export const getCart = (cart, callback) => dispatch => {
	if(!_.isEmpty(cart)) {
		dispatch({
		  	type: "GET_CART",
		  	cart
		})
		if(_.isFunction(callback))
			callback()
	} else {
		$.get('/cart.json', newCart => {
			dispatch({
			  	type: "GET_CART",
			  	cart: newCart
			})
			if(_.isFunction(callback))
				callback()
		})
	}
	
}

export const addToCart = (product_id, quantity, dispatch, callback) => {

	$.post( "/shopping-cart/product/add", { id: product_id, quantity }).done( cart => {

		dispatch({
		  	type: "ADD_PRODUCT",
		  	cart
		})

		setTimeout(function(){ 
			dispatch({
			  	type: "HIDE_PRODUCT"
			})
		}, 3000)

		if(_.isFunction(callback))
			callback();
	});
	
}

export const hideLayer = (e) => (dispatch) => {
	e.preventDefault()
	dispatch({
	  	type: "HIDE_PRODUCT"
	})
}

export const addSingleCart = (data, dispatch, callback) => {
	$.post( "/shopping-cart/item/add", data).done( cart => {
		dispatch({
		  	type: "GET_CART",
		  	cart
		})
	});
}

export const updateProduct = (product_id, quantity) => (dispatch) => {
	$.post( "/shopping-cart/product/update", { id: product_id, quantity: quantity }).fail(e => {
		alert("You can't update this product!!");
	}).done(cart => {
		dispatch({
		  	type: "GET_CART",
		  	cart
		})
	});
}

export const deleteProduct = (product_id) => (dispatch) => {
	$.post( "/shopping-cart/product/remove", { id: product_id }).fail(e => {
		alert("You can't remove this product!!");
	}).done(cart => {
		dispatch({
		  	type: "GET_CART",
		  	cart
		})
	});
}

export const applyReward = (amount) => dispatch => {
	dispatch({
	  	type: "APPLY_REWARD",
	  	amount
	})
}

export const clearErrorMessage = () => ({
    type: "RESET_ERROR_MESSAGE"
})

export const setErrorMessage = (error) => ({
    type: "SET_ERROR_MESSAGE",
    error: error
})