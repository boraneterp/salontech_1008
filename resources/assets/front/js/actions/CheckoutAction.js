import _ from 'lodash';
import axios from 'axios';
import { browserHistory } from 'react-router';
import { SubmissionError } from 'redux-form';

export const getShippingQuotes = () => (dispatch, getState) => {

	const quotes = getState().cart.shipping.quotes
	const address_id =getState().cart.shipping.shipping_address
	if(parseInt(address_id) === 0 || typeof quotes != 'undefined') {
		return null
	}

	//requestQuotes(getState().cart.shipping.shipping_address, dispatch)
}

export const saveShipping = (address) => (dispatch, getState) => {
	return axios.request({
        method: 'POST',
        data: address,
        url: '/checkout/shipping'
    }).then((cart) => {
    	dispatch({
		  	type: "GET_CART",
		  	cart
		});
		browserHistory.push(`/checkout/payment`);
    }).catch((error) => {
    	throw new SubmissionError({
	        _error: error.response.status
	    })
    });
}

export const editBilling = () => (dispatch, getState) => {
	dispatch({
	  	type: "EDIT_BILLING"
	});
}

export const cancelEditBilling = () => (dispatch, getState) => {
	dispatch({
	  	type: "CANCEL_EDIT_BILLING"
	});
}

export const saveBilling = (address) => (dispatch, getState) => {
	return axios.request({
        method: 'POST',
        data: address,
        url: '/checkout/billing'
    }).then((cart) => {
    	dispatch({
		  	type: "GET_CART",
		  	cart
		});

		dispatch({
		  	type: "CANCEL_EDIT_BILLING"
		});
    }).catch((error) => {
    	throw new SubmissionError({
	        _error: error.response.status
	    })
    });
}

export const setShippingAddress = (address) => (dispatch, getState) => {
	dispatch({
	  	type: "SET_SHIPPING_ADDRESS",
	  	address
	})

	//requestQuotes(address.id, dispatch)
}

const requestQuotes = (address_id, dispatch) => {
	$.ajax({
		url: '/checkout/quotes.json',
		dataType: 'json',
		type: 'GET',
		data: {
			address_id
		},
		success: quotes => {
			dispatch({
			  	type: "GET_SHIPPING_QUOTE",
			  	quotes
			})
		}
	});
}

export const setShippingMethod = (shipping_method, shipping_price) => (dispatch, getState) => {
	dispatch({
	  	type: "SET_SHIPPING_METHOD",
	  	shipping_method,
	  	shipping_price
	})
}

export const getStates = () => (dispatch, getState) => {

	const data = getState().states.data
	if(data.length > 0) {
		return null
	}

	$.ajax({
		url: '/states.json',
		dataType: 'json',
		type: 'GET',
		success: data => {
			dispatch({
			  	type: "GET_STATES",
			  	data
			})
		}
	});
}

