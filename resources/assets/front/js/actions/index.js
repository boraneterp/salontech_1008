import _ from 'lodash'

export const getCart = (cart, callback) => dispatch => {
	if(!_.isEmpty(cart)) {
		dispatch({
		  	type: "GET_CART",
		  	cart
		})
		if(_.isFunction(callback))
			callback()
	} else {
		$.get('/cart.json', newCart => {
			dispatch({
			  	type: "GET_CART",
			  	cart: newCart
			})
			if(_.isFunction(callback))
				callback()
		})
	}
	
}

export const addToCart = (product_id, quantity, dispatch, callback) => {
	$.post( "/shopping-cart/item/add", { id: product_id, quantity }).done( cart => {
		dispatch({
		  	type: "GET_CART",
		  	cart
		})
		if(_.isFunction(callback))
			callback();
	});
}

export const addSingleCart = (data, dispatch, callback) => {
	$.post( "/shopping-cart/item/add", data).done( cart => {
		dispatch({
		  	type: "GET_CART",
		  	cart
		})
	});
}

export const updateItem = (item_id, quantity) => (dispatch) => {
	$.post( "/shopping-cart/item/update", { id: item_id, quantity: quantity }).error(e => {
		alert("You can't update this item!!");
	}).done(cart => {
		dispatch({
		  	type: "GET_CART",
		  	cart
		})
	});
}

export const deleteItem = (item_id) => (dispatch) => {
	$.post( "/shopping-cart/item/remove", { id: item_id }).error(e => {
		alert("You can't remove this item!!");
	}).done(cart => {
		dispatch({
		  	type: "GET_CART",
		  	cart
		})
	});
}

export const setService = (service) => (dispatch, getState) => {
	if(service === getState().entities.cart.service) {
		return;
	}
	$.post( "/change-service", { service }).done(cart => {
		dispatch({
		  	type: "GET_CART",
		  	cart
		})
	});
}

export const scheduleDelivery = (delivery_date, delivery_time, callback) => dispatch => {
	$.post( "/set-delivery", { delivery_date, delivery_time }).done( cart => {
		dispatch({
		  	type: "GET_CART",
		  	cart
		})
		if(_.isFunction(callback))
			callback();
	});
}

export const applyReward = (amount) => dispatch => {
	dispatch({
	  	type: "APPLY_REWARD",
	  	amount
	})
}

export const clearErrorMessage = () => ({
    type: "RESET_ERROR_MESSAGE"
})

export const setErrorMessage = (error) => ({
    type: "SET_ERROR_MESSAGE",
    error: error
})