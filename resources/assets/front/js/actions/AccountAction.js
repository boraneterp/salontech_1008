import _ from 'lodash'

export const getUser = (callback) => (dispatch, getState) => {

	if(getState().user.id > 0) {
		if(_.isFunction(callback))
			callback(getState().user)
		return null
	}

	$.get('/account/user.json', user => {
		dispatch({
		  	type: "GET_USER",
		  	user
		})
		if(_.isFunction(callback))
			callback(user)
	})
	
}

export const setUser = (user) => (dispatch, getState) => {
	dispatch({
	  	type: "GET_USER",
	  	user
	})
	
}

export const removeAddress = (address_id) => (dispatch) => {
	if(confirm("Are you sure to remove this address?")) {
		$.post( "/account/address/remove", { address_id: address_id }).fail(e => {
			alert("You can't remove this address!!");
		}).done(user => {
			dispatch({
			  	type: "GET_USER",
			  	user
			})
		});
	}
	
}

export const removePayment = (payment_id) => (dispatch) => {
	if(confirm("Are you sure to remove this payment?")) {
		$.post( "/account/payment/remove", { payment_id: payment_id }).fail(e => {
			alert("You can't remove this payment!!");
		}).done(user => {
			dispatch({
			  	type: "GET_USER",
			  	user
			})
		});
	}
	
}