
const accountType = () => {
	$(".checkbox-list").on("click", "input", function(e) {
		$(".checkbox-list .radio-wrapper").removeClass("checked");
		$(this).parent().addClass("checked");
		if($(this).val() === 'professional') {
			$(".business-detail").show()
		} else {
			$(".business-detail").hide()
		}
	});
	$(".checkbox-list input:first").trigger("click")
}

const registerForm = () => {
	$("#registerForm").validate({
		rules: {
		 	email: {
		        remote: {
		            url: "/check-email",
		            type: "post"
		        }           
		    },
		    confirmation: {
				equalTo: "#registerForm input[name='password']"
			}
		},
		messages: {
			company: "Company Name is required.",
			cosmetology_id: "Cosmetology Id is required.",
			first_name: "First Name is required.",
			last_name: "Last Name is required.",
			phone_number: "Phone Number is required.",
			email: {
	        	required: "Email Address is required",
	            email: "Email Address is invalid",
	            remote: "Email address is already exist"
	        },
			password: {
				required: "Password is required.",
				minlength: "Password length should be at least minimum of 8 characters."
	        },
			confirmation: {
	            equalTo: "Password Confirmation is not match to password." 
	        },
			address_1: "Address is required.",
			city: "City is required.",
			state: "State is required.",
			zip: "Zip Code is required."
		}, 
        submitHandler: function(form) {
			$("label.error, .alert").hide();
			$("#registerForm .btn-submit").button('loading');
			$("#registerForm").ajaxSubmit({
				error: function(e) {
					$("#registerForm .alert").show();
					$(".btn-submit").button('reset');
				},
				success: function(message) {
					window.location.href = "/account";
				}
			}); 
			return false;
		}
	 });
}

export default () => {
	accountType()
	registerForm()
}