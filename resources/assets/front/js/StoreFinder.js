
const finderMap = () => {
    const map = new google.maps.Map(document.getElementById('store-map'), {
      	zoom: 10,
      	center: new google.maps.LatLng($("#store-map").data('lat'), $("#store-map").data('lng')),
      	mapTypeControl: false,
      	streetViewControl: false,
      	mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    let infowindow = new google.maps.InfoWindow();

    $(".store-list li").each(function(i) {
    	let $store = $(this)
    	var contentString = 
    	`<div class="info-window">
    		<div class="store-name">${$store.data('name')}</div>
    		<div class="store-distance">${$store.data('distance')} miles away</div>
    		<address>${$store.find("address").html()}</address>
    		<div class="info-action">
    			<a target="_blank" href="https://maps.google.com/maps?daddr=${$store.find("address").text()}" class="button button-default btn-sm">GET DIRECTIONS</a>
    		</div>
    	</div>`

    	let marker = new google.maps.Marker({
	        position: new google.maps.LatLng($store.data('lat'), $store.data('lng')),
	        map,
        	animation: google.maps.Animation.DROP
	    });

	    google.maps.event.addListener(marker, 'click', (function(marker) {
	        return function() {
	          infowindow.setContent(contentString);
	          infowindow.open(map, marker);
	        }
	    })(marker));

	    $(this).on("click", function(e) {
	    	new google.maps.event.trigger( marker, 'click' );
	    });
    })

    
}

export default () => {
	finderMap()
}