import React, { Component } from 'react'
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import _ from 'lodash'
import CartLayer from './CartLayer'
import MiniCartList from './MiniCartList'
import * as CartActions from '../actions/CartAction'

class MiniCart extends Component {

	constructor() {
		super()
		this.state = {
	      show: false
	    }

	    this.handleToggle = this.handleToggle.bind(this);
	}

	componentDidMount() {
		const _this = this
	    $("body").on('click', (e) => { 
	    	if(!ReactDOM.findDOMNode(this).contains(e.target)) {
				_this.setState({
					show: false
				})
			}
		});
	}

	handleToggle(e) {
		e.preventDefault()
		this.setState({
    		show: true
    	})
	}

	render() {
		const { cart, actions } = this.props 
		return (
			<div>
				<div className="shopping_cart ">
					<a href="#" title="View my shopping cart" onClick={this.handleToggle}>
						<i className="icon icon_bag_alt"></i>
						{!_.isEmpty(cart.content.items) &&
							<span className="ajax_cart_quantity unvisible">{cart.content.quantity}</span>
						}
					</a>
					{this.state.show &&
						<div className="cart_block block exclusive animated fadeIn">
						    <div className="block_content">
						    	{!_.isEmpty(cart.content.items) ?
						        	<MiniCartList cart={cart.content} onDelete={actions.deleteProduct}/>:
						        	<div className="cart_block_list">
							        	<p className="cart_block_no_products" >
							                No products
							            </p>
						        	</div>
						        }
						    </div>
						</div>
					}
				</div>
				{(!_.isEmpty(cart.layerProduct)) &&
					<CartLayer cart={cart} hideLayer={actions.hideLayer} />
				}
			</div>
		)
	}
}

const mapStateToProps = (state, ownProps) => ({
  	cart: state.cart
})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(CartActions, dispatch)
})

export default connect(
  	mapStateToProps, 
	mapDispatchToProps
)(MiniCart)