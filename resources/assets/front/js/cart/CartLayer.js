import React from 'react'
import PropTypes from 'prop-types'

const CartLayer = ({ cart, hideLayer }) => (
	<div>
		<div className="layer-cart animated fadeInDown">
			<a className="close" href="#" onClick={hideLayer}><i className="fa fa-close"></i></a>
			<div className="container layer-container">
				<div className="row">
					<div className="layer-container__first col-xs-6">
						<div className="layer__title">
							<i className="fa fa-check"></i>ADDED TO CART
						</div>
						<div className="row">
							<div className="col-sm-8 col-sm-offset-2">
								<div className="row">
									<div className="col-xs-5">
										<img className="layer_cart_img img-responsive" src={cart.layerProduct.image} alt="Blouse"  />
									</div>
									<div className="col-xs-7 ">
										<div className="layer__product__name" dangerouslySetInnerHTML={{__html: cart.layerProduct.product_name}} />
										<div className="layer__product__row">
											<strong className="layer__product__label">Quantity</strong>
											<span>{cart.layerProduct.quantity}</span>
										</div>
										<div className="layer__product__row">
											<strong className="layer__product__label">Total</strong>
											<span>${parseFloat(cart.layerProduct.total).toFixed(2)}</span>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div className="col-xs-6">
						<div className="layer__title">
							<span>SUBTOTAL</span>
							<span className="layer__total">${cart.content.sub_total}</span>
						</div>
						<div className="row">
							<div className="col-sm-8 col-sm-offset-2">
								<p className="text-center">Spend $75.00 more and get free shiiping.</p>
								<div className="button-container">
									<a className="button button-gray button-small" href="/checkout/cart" >
										View Cart ({cart.content.quantity})
									</a>&nbsp;
									<a className="button btn-submit button-small" href="/checkout/shipping" >
										Checkout
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div className="layer_cart_overlay"></div>
	</div>
)

CartLayer.propTypes = {
  cart: PropTypes.object.isRequired, 
  hideLayer: PropTypes.func.isRequired
}

export default CartLayer