import React from 'react'
import PropTypes from 'prop-types'

const MiniCartItem = ({ item, onDelete }) => (
	<dt >
        <a className="cart-images" href={item.product.url} >
        	<img src={item.product.image} />
        </a>
        <div className="cart-info">
            <div className="product-name"><span className="quantity-formated"><span className="quantity">{item.quantity}</span>&nbsp;x&nbsp;</span>
            	<a href={item.product.url} className="cart_block_product_name" dangerouslySetInnerHTML={{__html: item.product.product_name}} />
            </div>
            <span className="price">${item.total_price}</span>
        </div>
        <span className="remove_link">
        	<a className="ajax_cart_block_remove_link" href="#" onClick={(e) => {e.preventDefault(); onDelete()}}> </a>
        </span>
    </dt>
)

MiniCartItem.propTypes = {
  item: PropTypes.shape({
    product: PropTypes.object.isRequired,
    quantity: PropTypes.number.isRequired
  }).isRequired,
  onDelete: PropTypes.func.isRequired
}

export default MiniCartItem