import React from 'react'
import PropTypes from 'prop-types'
import MiniCartItem from './MiniCartItem'

const MiniCartList = ({ cart, onDelete }) => (
	<div className="cart_block_list">
        <dl className="products">
        	{cart.items.map(item =>
				<MiniCartItem key = { item.id }
			          item = { item }
			          onDelete = {() => onDelete(item.id)} />
			)}
        </dl>
        <div className="cart-prices">
            <div className="cart-prices-line last-line">
                <span className="price cart_block_total ajax_block_cart_total">${cart.total}</span>
                <span>Total</span>
            </div>
        </div>
        <p className="cart-buttons">
            <a href="/checkout/cart" className="btn btn-submit button-small" title="Check out" rel="nofollow">
                <span>
                    Check out <i className="fa fa-chevron-right"></i>
                </span>
            </a>
        </p>
    </div>
)

MiniCartList.propTypes = {
  cart: PropTypes.object.isRequired,
  onDelete: PropTypes.func.isRequired
}

export default MiniCartList