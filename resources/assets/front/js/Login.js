
const loginForm = () => {
	$("#loginForm").validate({
		messages: {
			email: {
	        	required: "Email Address is required",
	            email: "Email Address is invalid"
	        },
			password: {
				required: "Password is required"
	        }
		}, 
        submitHandler: function(form) {
			$("label.error, .alert").hide();
			$("#loginForm .btn-submit").button('loading');
			return true;
		}
	});
}

const forgotPassword = () => {
	// $("#forgot_password").on("click", function(event){
	// 	event.preventDefault();
	// 	$("#passwordForm input[name='email']").val("");
	// 	$("label.error, .alert").hide();
	// 	$("#forgot-modal").modal();
	// });

	$("#passwordForm").validate({
		messages: {
			email: {
	        	required: "Email Address is required",
	            email: "Email Address is invalid"
	        }
		}, 
        submitHandler: function(form) {
			$("label.error, .alert").hide();
			$("#passwordForm .btn-submit").button('loading');
			$("#passwordForm").ajaxSubmit({
				dataType: 'json',
				error: function(e) {
					$("#passwordForm .alert.alert-danger").show();
					$("#passwordForm .btn-submit").button('reset');
				},
				success: function(data) {
					$("#passwordForm .alert.alert-success").show();
					$("#passwordForm .btn-submit").button('reset');
				}
			}); 
			return false;
		}
	});
}

export default () => {
	loginForm()
	forgotPassword()
}