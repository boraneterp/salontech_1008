import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { CSSTransitionGroup } from 'react-transition-group'
import Breadcrumb from '../../components/account/Breadcrumb'
import AccountSideBar from '../../components/account/AccountSideBar'
import Loading from '../../components/account/Loading'

const Root = ({ children, user, page }) => (
	<div>
		<Breadcrumb page={page} />
		<div className="columns-container">
		    <div className="container">
		        <div className="row">
		        	<AccountSideBar page={page} />
		        	<div className="col-sm-9">
		        		{!user.id ?
		        			<Loading />:
		        			<CSSTransitionGroup
						      transitionName="transaction"
						      transitionEnterTimeout={500}
						      transitionLeaveTimeout={300}
						      >{children}
						    </CSSTransitionGroup>
		        		}
				    </div>
				</div>
			</div>
		</div>
	</div>
)

Root.propTypes = {
	children: PropTypes.node,
  	page: PropTypes.string.isRequired
}

const mapStateToProps = (state, ownProps) => ({
  	user: state.user,
  	page: ownProps.location.pathname.toLowerCase().replace("/checkout/", "")
})

export default connect(
	mapStateToProps, 
	null
)(Root)