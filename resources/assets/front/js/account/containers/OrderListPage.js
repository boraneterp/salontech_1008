import React, { Component } from 'react'
import { connect } from 'react-redux'
import { getUser } from '../../actions/AccountAction'
import OrderItem from '../../components/account/OrderItem'

class OrderListPage extends Component {

	render() {
		const { user } = this.props
		return (
			<div className="">
			    <h3 className="accont-heading">My Orders</h3>
				<div className="table-responsive">
				    <table className="table">
				        <thead>
				            <tr>
				                <th>No</th>
				                <th>Order Total</th>
								<th>Date</th>
								<th>Status</th>
								<th>Detail</th>
				            </tr>
				        </thead>
				        <tbody>
				        	{user.orders.map((order, index) =>
		                    	<OrderItem
									key={order.id}
									index={index + 1}
									order={order}
		                    	/>
		                    )}
				        </tbody>
				    </table>
				</div>
			</div>
		)
	}
}

const mapStateToProps = (state, ownProps) => ({
    user: state.user
})

export default connect(
  	mapStateToProps, {
  	getUser
})(OrderListPage)
