import React, { Component } from 'react'
import { connect } from 'react-redux'
import _ from 'lodash'
import { getUser, setUser } from '../../actions/AccountAction'
import PaymentComponent from '../../components/common/PaymentComponent'

class PaymentPage extends Component {

	constructor() {
		super()

		this.onSave = this.onSave.bind(this)
	}

	onSave(payment) {
  		const { user, setUser } = this.props
  		let index = _.findIndex(user.payments, _.matchesProperty('id', payment.id))
  		if(index > 0) {
  			user.payments[index] = payment
  		} else {
  			user.payments.unshift(payment)
  		}
 		setUser(user)
  	}

	render() {
		const { user } = this.props
		const chooseable = false
		return (
			<div className="">
			    <h3 className="accont-heading">Payment Options</h3>
			     <PaymentComponent 
					payments={user.payments} 
					chooseable={chooseable}
					saveLabel={'Save'}
					onChoose={this.onChoosePayment}
					onSave={this.onSave}
				/>
			</div>
		)
	}
}

const mapStateToProps = (state, ownProps) => ({
    user: state.user
})

export default connect(
  	mapStateToProps, { getUser, setUser }
)(PaymentPage)
