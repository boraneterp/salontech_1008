import React, { Component } from 'react'
import { connect } from 'react-redux'
import { getUser } from '../../actions/AccountAction'
import OrderProductItem from '../../components/account/OrderProductItem'
import { Link } from 'react-router'

class OrderDetailPage extends Component {

	componentWillMount() {
		const { getUser } = this.props
		getUser()
	}

	componentDidMount() {

	}

	getCurrentOrder(orders) {
		const orderId = window.location.pathname.substring(16)
		if(orders.length === 0)
			return null
		let order = null
		for(let i = 0; i < orders.length; i++) {
			if(orders[i].id == orderId) {
				order = orders[i]
				break
			}
		}
		return order
	}

	getOrderAddress(order) {
		let address = {}
		if(order === null)
			return address
  		for(let i = 0; i < order.addresses.length; i++) {
  			if(order.addresses[i].type == "S") {
  				address.shipping = order.addresses[i]
  			} else if(order.addresses[i].type == "B") {
  				address.billing = order.addresses[i]
  			}
  		}
  		return address
	}

	getCardNumberMask(order) {
		if(order == null || order.payment.payment_type === 'paypal' 
			|| order.payment.card_number.length < 4)
			return ''
		const cardNumber = order.payment.card_number
		return cardNumber.substring(cardNumber.length - 4)
	}

	render() {
		const { user } = this.props
		const order = this.getCurrentOrder(user.orders)
		const address = this.getOrderAddress(order)
		let status = ''
		let statusClass = ''
		if(order != null) {
			status = order.status === 'P' ? 'Pending' : order.status === 'I' ? 'Processing' : order.status === 'R' ? 'Refunded' : 'Completed';
			statusClass = 'order-status ' + status.toLowerCase()
		}

		let cardNumberMask = this.getCardNumberMask(order)
		return (
			<div>
				{order != null &&
					<div className="">
					    <h3 className="accont-heading">Order Detail</h3>
					    <Link to="/account/orders" className="button button-default button-smaill account-back">Back</Link>
					    <div className="row">
					    	<div className="col-sm-8">
					    		<h2 className="checkout-heading">Ordered Products</h2>
					    		<div className="cart">
						    		<ul className="cart__content">
						    			{order.products.map(product =>
		                                    <OrderProductItem key = { product.id }
		                                      product = { product } />
		                                )}
						    		</ul>
					    		</div>
					    		<h2 className="checkout-heading">SUMMARY</h2>
					    		<div className="checkout-summary-content">
					    			<div className="checkout-summary-row">
					    				<span className="checkout-summary-content__label">SUBTOTAL</span>
					    				<span className="checkout-summary-content__value">${parseFloat(order.sub_total).toFixed(2)}</span>
					    			</div>
					    			{!_.isEmpty(order.coupon_code) &&
					    				<div className="checkout-summary-row green">
						    				<span className="checkout-summary-content__label">PROMOTION</span>
						    				<span className="checkout-summary-content__value">-${parseFloat(order.promotion).toFixed(2)}</span>
						    			</div>
									}
					    			<div className="checkout-summary-row">
					    				<span className="checkout-summary-content__label">SHIPPING &amp; HANDLING</span>
					    				<span className="checkout-summary-content__value">${parseFloat(order.shipping_price).toFixed(2)}</span>
					    			</div>
					    			<div className="checkout-summary-row">
					    				<span className="checkout-summary-content__label">TAX</span>
					    				<span className="checkout-summary-content__value">${parseFloat(order.tax).toFixed(2)}</span>
					    			</div>
					    			<div className="checkout-summary-row">
					    				<span className="checkout-summary-content__label--big">Total</span>
					    				<span className="checkout-summary-content__value--big">${parseFloat(order.order_total).toFixed(2)}</span>
					    			</div>
					    		</div>
							</div>
							<div className="col-sm-4">
								<h2 className="checkout-heading ">ORDER DETAIL</h2>
								<div className="checkout-content">
									<div><span className="billing-label">Order Id:</span> <span>#{order.id}</span></div>
				                	<div><span className="billing-label">Order Status:</span> <span className={statusClass}>{status}</span></div>
				                	<div><span className="billing-label">Shipping:</span> <span >{order.shipping_method}</span></div>
				                </div>
								<h2 className="checkout-heading ">SHIPPING</h2>
								<div className="checkout-content">
				                	<div className="address-label">{address.shipping.first_name + ' ' + address.shipping.last_name}</div>
			                    	<address dangerouslySetInnerHTML={{__html: address.shipping.full_address}}></address>
				                </div>
				                <h2 className="checkout-heading ">PAYMENT</h2>
				                <div className="checkout-content">
				                	<h3 className="checkout-container__heading">Payment Method: {order.payment.payment_type === 'paypal'? 'Paypal':'Credit or Debit Card'}</h3>
				                	<div className="billing-detail">
				                		{order.payment.payment_type === 'paypal' ?
					                    	<div>
					                    		<div><span className="billing-label">Transaction Id:</span> <span>{order.payment.transaction_id}</span></div>
						                    	<div><span className="billing-label">Paypal ID:</span> <span>{order.payment.payer_email}</span></div>
											</div>:
											<div>
												<div><span className="billing-label">Transaction Id:</span> <span>{order.payment.transaction_id}</span></div>
												<div><span className="billing-label">Card Hoder Name:</span> <span>{order.payment.card_holder_name}</span></div>
						                    	<div><span className="billing-label">Card Number:</span> <span>************{cardNumberMask}</span></div>
						                    	<div><span className="billing-label">Exipre Date:</span> <span>{order.payment.expire_month}/{order.payment.expire_year}</span></div>
						                    	<div><span className="billing-label">Security Code:</span> <span>{order.payment.ccv}</span></div>
											</div>
										}
				                	</div>
				                	{order.payment.payment_type === 'card' &&
				                    	<div className="billing-address">
						                    <h3 className="checkout-container__heading">Billing Address:</h3>
						                    <div className="address-label">{address.billing.first_name + ' ' + address.billing.last_name}</div>
						                    <address dangerouslySetInnerHTML={{__html: address.billing.full_address}}></address>
				                    	</div>
				                    }
				                </div>
							</div>
						</div>
					</div>
				}
			</div>
		)
	}
}

const mapStateToProps = (state, ownProps) => ({
    user: state.user
})

export default connect(
  	mapStateToProps, {
  	getUser
})(OrderDetailPage)
