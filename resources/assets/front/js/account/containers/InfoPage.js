import React, { Component } from 'react'
import { Link } from 'react-router'
import { connect } from 'react-redux'
import { getUser, setUser } from '../../actions/AccountAction'
import Loading from '../../components/account/Loading'

class InfoPage extends Component {
	constructor() {
	    super();
	    this.state = {
	    	profile: {
	    		first_name: '',
		    	last_name: '',
		    	email: ''
	    	},
	    	error: "",
	    	success: ""
	    }

	    this.handleChange = this.handleChange.bind(this);
	}

	componentWillMount() {
		const { user } = this.props
		const profile = {
			first_name: user.first_name,
	    	last_name: user.last_name,
	    	email: user.email
		}

		this.setState({
			profile
        });
	}

	componentDidMount() {
		const self = this
		const { setUser } = this.props
		$("#profileForm").validate({
			messages: {
				first_name: "First Name is required.",
				last_name: {
					required: "Last Name is required"
		        },
		        email: {
					required: "Email Address is required"
		        }
			}, 
			submitHandler: function(form) {
				self.setState({
					error: "",
					success: ""
		        });
				$("#profileForm .btn-submit").button('loading');
				$(form).ajaxSubmit({
					dataType: 'json',
					error: function(e) {
						self.setState({
							error: e.responseJSON
				        });
						$("#profileForm .btn-submit").button('reset');
					},
					success: function(user) {
				        setUser(user)
				        const profile = {
							first_name: user.first_name,
					    	last_name: user.last_name,
					    	email: user.email
						}
						self.setState({
							profile,
							success: "Your profile has been updated."
				        });
				        $("#profileForm .btn-submit").button('reset');
						
					}
				});
				return false;
			}
		});
	}

	handleChange(event) {
		const target = event.target;
		const value = target.value;
		const name = target.name;
		let profile = this.state.profile;
		profile[name] = value
		this.setState({
		    profile
		});
	}

	render() {
		const { user } = this.props
		const { profile, error, success } = this.state
		return (
			<div className="">
				<h3 className="accont-heading">Account Settings</h3>
				<form id="profileForm" method="post" action="/account/profile/save" >
					{!_.isEmpty(error) &&
						<div className="alert alert-danger fade in" >{error}</div>
					}
					{!_.isEmpty(success) &&
						<div className="alert alert-info fade in" >{success}</div>
					}
					<div className="row">
						<div className="col-sm-8">
							<div className="form-group ">
								<label className="control-label required">First Name</label>
								<input className="form-control required" placeholder="First Name" value={profile.first_name} name="first_name" type="text" onChange={this.handleChange} />
							</div>
							<div className="form-group">
								<label className="control-label required">Last Name</label>
								<input className="form-control required" placeholder="Last Name" value={profile.last_name} name="last_name" type="text" onChange={this.handleChange} />
							</div>
							<div className="form-group">
								<label className="control-label required">Email Address</label>
								<input className="form-control required" placeholder="Email Address" value={profile.email} name="email" type="email" onChange={this.handleChange} />
							</div>
							<div className="" >
			                  	<button className="button btn-submit btn-sm pull-right" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Save">Save</button>
			                </div>
						</div>
					</div>
                </form>
			</div>
		)
	}
}

const mapStateToProps = (state, ownProps) => ({
    user: state.user
})

export default connect(
  	mapStateToProps, {
  	getUser, setUser
})(InfoPage)
