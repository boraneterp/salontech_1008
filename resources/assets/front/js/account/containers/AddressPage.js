import React, { Component } from 'react'
import { connect } from 'react-redux'
import { getUser, setUser } from '../../actions/AccountAction'
import AddressComponent from '../../components/common/AddressComponent'

class AddressPage extends Component {

	constructor() {
		super()

		this.onSave = this.onSave.bind(this)
	}

	componentWillMount() {
		const { getUser } = this.props
		const self = this
		getUser()
	}

	onSave(address) {
  		const { user, setUser } = this.props
  		let newAddr = true
  		for(let i = 0; i < user.addresses.length; i++) {
  			if(user.addresses[i].id == address.id) {
  				user.addresses[i] = address
  				newAddr = false
  				break
  			}
  		}

  		if(newAddr) {
  			user.addresses.unshift(address)
  		}

 		setUser(user)
  	}

	render() {
		const { user } = this.props
		const chooseable = false
		return (
			<div className="">
			    <h3 className="accont-heading">My Addresses</h3>
			    <AddressComponent 
					addresses={user.addresses} 
					chooseable={chooseable}
					saveLabel={'Save'}
					onChoose={this.onChooseAddress}
					onSave={this.onSave}
				/>
			</div>
		)
	}
}

const mapStateToProps = (state, ownProps) => ({
    user: state.user
})

export default connect(
  	mapStateToProps, { getUser, setUser }
)(AddressPage)