import React, { Component } from 'react'
import { connect } from 'react-redux'

class PasswordPage extends Component {

	constructor() {
		super()
		this.state = {
	    	error: "",
	    	success: ""
	    }
	}

	componentDidMount() {
		const self = this
		$("#passwordForm").validate({
			rules: {
			    confirmation: {
					equalTo: "#passwordForm input[name='new_password']"
				}
			},
			messages: {
				current_password: {
					required: "Current Password is required.",
					minlength: "Password length should be at least minimum of 8 characters."
				},
				new_password: {
					required: "New Password is required.",
					minlength: "Password length should be at least minimum of 8 characters."
		        },
				confirmation: {
		            equalTo: "Password Confirmation is not match to password." 
		        }
			}, 
			submitHandler: function(form) {
				self.setState({
					error: "",
					success: ""
		        });
				$("#passwordForm .btn-submit").button('loading');
				$(form).ajaxSubmit({
					dataType: 'json',
					error: function(e) {
						self.setState({
							error: e.responseJSON
				        });
						$("#passwordForm .btn-submit").button('reset');
					},
					success: function(data) {
						self.setState({
							success: "Your password has been changed."
				        });
				        $("#passwordForm input").val('');
						$("#passwordForm .btn-submit").button('reset');
					}
				});
				return false;
			}
		});
	}

	render() {
		const { error, success } = this.state
		return (
			<div className="">
			    <h3 className="accont-heading">Change Password</h3>
			    <form id="passwordForm" method="post" action="/account/password/change" >
				    <div className="row">
						<div className="col-sm-8">
							{!_.isEmpty(error) &&
								<div className="alert alert-danger fade in" >{error}</div>
							}
							{!_.isEmpty(success) &&
								<div className="alert alert-info fade in" >{success}</div>
							}
							<div className="form-group ">
								<label className="control-label required">Current Password</label>
								<input className="form-control required" placeholder="Current Password" minLength="8" defaultValue="" name="current_password" type="password" />
							</div>
							<div className="form-group">
								<label className="control-label required">New Password</label>
								<input className="form-control required" placeholder="New Password" name="new_password" minLength="8" type="password" defaultValue=""  />
							</div>
							<div className="form-group">
								<label>Confirm password</label>
								<input className="form-control" placeholder="Confirm password" name="confirmation" minLength="8" type="password" defaultValue="" />
							</div>
							<div className="" >
			                  	<button className="button btn-submit btn-sm pull-right" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Save">Save</button>
			                </div>
						</div>
					</div>
				</form>
			</div>
		)
	}
}

const mapStateToProps = (state, ownProps) => ({
    user: state.user
})

export default connect(
  	mapStateToProps, null
)(PasswordPage)
