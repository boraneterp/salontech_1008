import React from 'react';
import ReactDOM from 'react-dom';
import { browserHistory } from 'react-router';
import store from '../store';
import { Provider } from 'react-redux';
import { syncHistoryWithStore } from 'react-router-redux';
import { Router, Route, IndexRoute } from 'react-router';
import Root from './containers/Root';
import InfoPage from './containers/InfoPage';
import PasswordPage from './containers/PasswordPage';
import OrderListPage from './containers/OrderListPage';
import OrderDetailPage from './containers/OrderDetailPage';
import AddressPage from './containers/AddressPage';
import PaymentPage from './containers/PaymentPage';

const history = syncHistoryWithStore(browserHistory, store);

if(window.location.pathname.startsWith("/account")) {
	$.get('/account/user.json', user => {
		store.dispatch({
		  	type: "GET_USER",
		  	user
		})
	});

}

export default () => {
	ReactDOM.render(
		<Provider store={store}>
	    	<Router history={history}>
		    	<Route path="/account" component={Root}>
		    		<IndexRoute component={InfoPage} />
		    		<Route path="password"  component={PasswordPage}  />
				  	<Route path="orders"  component={OrderListPage}  />
				  	<Route path="orders/:id" component={OrderDetailPage}  />
				  	<Route path="address"  component={AddressPage}  />
				  	<Route path="payment"  component={PaymentPage}  />
				</Route>
	      	</Router>
	  	</Provider>,
			document.getElementById('account')
	)
}