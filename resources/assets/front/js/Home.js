import Global from './Global'

const headerVideo = () => {

    $(".header-video").on("click", "a.button_intro", function(e) {
    	e.preventDefault();
    	const offset = $("header").hasClass("labfixed")? -70: -140;
    	$.scrollTo('.feature-product', 500, {
    		offset: offset
    	});
    })
}

const popupYoutube = () => {
	$('.popup-youtube').magnificPopup({
		disableOn: 700,
		type: 'iframe',
		mainClass: 'mfp-fade',
		removalDelay: 160,
		preloader: false,
		fixedContentPos: false
	});
}


const blogCarousel = () => {
	$(".sdsblog-box-content").slick({
	  	infinite: false,
	  	slidesToShow: 3,
	  	slidesToScroll: 1,
	  	arrows: true,
	  	prevArrow: '<a class="prev labnewblogprev"><i class="fa fa-chevron-left"></i></a>',
	  	nextArrow: '<a class="next labnewblognext"><i class="fa fa-chevron-right"></i></a>',
	  	responsive: [
		    {
		      	breakpoint: 768,
		      	settings: {
			        slidesToShow: 2
			    }
			}
		]
	});
}

const subscriptionPopup = () => {
	if(Cookies.get('not-again') != 'yes') {
		setTimeout(function() {
			$("#newsletter-popup").show();
		}, 30000);
	}
}

const subscriptionForm = () => {
	$(document).on("click", "input[name='not-again']", noShowAgain);

	$("#subscriptionForm").validate({
		messages: {
			email_newsletter: {
	        	required: "Email Address is required",
	            email: "Email Address is invalid"
	        }
		}, 
        submitHandler: function(form) {
			$("label.error, .alert").hide();
			$("#subscriptionForm .btn-submit").button('loading');
			$("#subscriptionForm").ajaxSubmit({
				dataType: 'json',
				error: function(e) {
					$("#subscriptionForm .alert.alert-danger").show();
					$("#subscriptionForm .btn-submit").button('reset');
				},
				success: function(type) {
					$("#subscriptionForm .alert.alert-success").show();
					$("#subscriptionForm .btn-submit").button('reset');
					setTimeout(noShowAgain, 800);
				}
			}); 
			return false;
		}
	});
}

const noShowAgain = () => {
	Cookies.set('no-again', 'yes', { expires: 1 });
	Global.closePopup();
}

export default () => {
	headerVideo()
	popupYoutube()
	//blogCarousel()
	if($(window).width() >= 768) { 
		subscriptionPopup()
		subscriptionForm()
	}
	
}