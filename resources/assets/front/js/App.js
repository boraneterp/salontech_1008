import "babel-polyfill";
import axios from 'axios';
import Global from './Global';
import initHome from './Home';
import initProductFinder from './ProductFinder';
import initProductDetail from './ProductDetail';
import initLogin  from './Login';
import initRegister  from './Register';
import initAccount  from './account/index';
import initCheckout  from './checkout/index';
import initWarranty  from './Warranty';
import initContact  from './Contact';
import initStoreFinder  from './StoreFinder';
import initResetPassword  from './ResetPassword';

class App {

	start() {
		
		Global.init();

		const $CONTAINER = $("body");

		if($CONTAINER.hasClass("homepage")) {
			initHome();
		} else if($CONTAINER.hasClass("product-finder")) {
			initProductFinder();
		} else if($CONTAINER.hasClass("product-detail")) {
			initProductDetail();
		} else if($CONTAINER.hasClass("checkout")) {
			initCheckout();
		} else if($CONTAINER.hasClass("login")) {
			initLogin();
		} else if($CONTAINER.hasClass("register")) {
			initRegister();
		} else if($CONTAINER.hasClass("account")) {
			initAccount();
		} else if($CONTAINER.hasClass("warranty")) {
			initWarranty();
		} else if($CONTAINER.hasClass("contact")) {
			initContact();
		} else if($CONTAINER.hasClass("store-finder")) {
			initStoreFinder();
		} else if($CONTAINER.hasClass("reset-password")) {
			initResetPassword();
		}
	}
}

$(function() {
	axios.defaults.headers.common['X-CSRF-Token'] = $( 'meta[name="csrf-token"]' ).attr( 'content' );
	axios.interceptors.request.use(function (config) {
	    return config;
	}, function (error) {
		return Promise.reject(error);
	});

	axios.interceptors.response.use(function (response) {
		return response.data;
	}, function (error) {
		return Promise.reject(error);
	});

	$.ajaxSetup( {
        beforeSend: function ( xhr, req ) {
        	xhr.setRequestHeader( 'X-CSRF-Token', $( 'meta[name="csrf-token"]' ).attr( 'content' ) );
        }
    });
	new App().start();
});