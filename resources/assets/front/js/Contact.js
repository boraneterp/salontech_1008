
const contactForm = () => {
	$("#contactForm").validate({
		messages: {
			name: "Name is required.",
			email: {
	        	required: "Email Address is required",
	            email: "Email Address is invalid"
	        },
			type: "Inquery Type is required.",
			message: "Message is required."
		}, 
        submitHandler: function(form) {
			$("label.error, .alert").hide();
			$("#contactForm .btn-submit").button('loading');
			return true;
		}
	 });
}

export default () => {
	contactForm()
}