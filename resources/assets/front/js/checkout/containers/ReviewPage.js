import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import _ from 'lodash'
import { browserHistory } from 'react-router'
import { Link } from 'react-router'
import { getCart } from '../../actions/CartAction'
import CheckoutSideBar from '../../components/checkout/CheckoutSideBar'

class ReviewPage extends Component {

  	constructor() {
  		super()
  		this.state = {
	    	error: ""
	    }
  		this.onCheckout = this.onCheckout.bind(this);
  	}

  	componentDidMount() {
  		const self = this
  		const { getCart } = this.props
	    $("#checkoutForm").validate({
	        submitHandler: function(form) {
				$("#checkoutForm .btn-submit").button('loading');
				$("#checkoutForm").ajaxSubmit({
					dataType: 'json',
					error: function(e) {
					  	self.setState({
							error: e.responseText
				        });
					  	$("#checkoutForm .btn-submit").button('reset');
					},
					success: function(cart) {
					  	$("#checkoutForm .btn-submit").button('reset')
					  	getCart(cart, function() {
					  		browserHistory.push(`/checkout/success`)
					  	})
					}
				}); 
				return false;
	      	}
	    });
	}

  	onCheckout() {
  		const { cart } = this.props

  		if(_.isEmpty(cart.items) || cart.sub_total < cart.minimum) {
			$('html, body').animate({
		        scrollTop: $(".cart-page-menu").offset().top - 90
		    }, 200);
			return;
		}
  		browserHistory.push(`/checkout/shipping`)
  	}

  	getAddressDetail(cart) {
  		let address = {}
  		for(let i = 0; i < cart.user.addresses.length; i++) {
  			if(cart.shipping.shipping_address == cart.user.addresses[i].id) {
  				address.shipping = cart.user.addresses[i]
  			} 

  			if(cart.payment.billing_address == cart.user.addresses[i].id) {
  				address.billing = cart.user.addresses[i]
  			}
  		}
  		return address
  	}

  	render() {
	    const { cart, shipping } = this.props;
	    if(cart.content.items.length === 0) {
	    	return (<div></div>);
	    }
	    const { error } = this.state;
	    const address = this.getAddressDetail(cart);
	    const payment = _.find(cart.user.payments, _.matchesProperty('id', parseInt(cart.payment.payment_id)));
	    return (
	    	<div className="columns-container animated fadeIn">
			    <div className="container">
			        <div className="row">
			            <div className="col-sm-9">
			                <h2 className="checkout-heading ">
			                	<i className="fa fa-check"></i> 1. SHIPPING
			                	<Link to='/checkout/shipping' className="button button-default">Edit</Link>
			                </h2>
			                <div className="checkout-content">
			                	<h3 className="checkout-container__heading">Shipping Address:</h3>
			                	<div className="address-label">{address.shipping.first_name + ' ' + address.shipping.last_name}</div>
			                    <address dangerouslySetInnerHTML={{__html: address.shipping.full_address}}>
			                    </address>
			                </div>
			                <h2 className="checkout-heading ">
			                	<i className="fa fa-check"></i> 2. PAYMENT
			                	<Link to='/checkout/payment' className="button button-default">Edit</Link>
			                </h2>
			                <div className="checkout-content">
			                	<h3 className="checkout-container__heading">Payment Method: {cart.payment.type === 'paypal'? 'Paypal':'Credit or Debit Card'}</h3>
			                    <div className="billing-detail">
			                    	{cart.payment.type === 'paypal' ?
				                    	<div>
					                    	<span>Paypal ID:</span> <strong>{cart.payment.paypal_payer}</strong>
										</div>:
										<div>
											<div><span>Card Hoder Name:</span> <span>{payment.card_holder_name}</span></div>
					                    	<div><span>Card Number:</span> <span>{payment.card_label}</span></div>
					                    	<div><span>Exipre Date:</span> <span>{payment.expire_month}/{payment.expire_year}</span></div>
					                    	<div><span>Security Code:</span> <span>{payment.ccv}</span></div>
										</div>
									}
			                    </div>
			                    {cart.payment.type === 'card' &&
			                    	<div className="billing-address">
					                    <h3 className="checkout-container__heading">Billing Address:</h3>
					                    <div className="address-label">{address.billing.first_name + ' ' + address.billing.last_name}</div>
					                    <address dangerouslySetInnerHTML={{__html: address.billing.full_address}}></address>
			                    	</div>
			                    }
			                </div>
			                <h2 className="checkout-heading active">3. REVIEW AND SUBMIT YOUR ORDER</h2>
			                <div className="checkout-container">
			                	{!_.isEmpty(error) &&
									<div className="alert alert-danger fade in " dangerouslySetInnerHTML={{__html: error}}></div>
								}
			                    <p>By clicking the ’Place Order’ button, you confirm that you have read, understood, and accept our Terms and Conditions, Returns Policy, and Privacy Policy.</p>
			                    <form id="checkoutForm" action="/checkout/confirm" method="post">
			                    	<button type="submit" className="button btn-submit btn-sm pull-right" onClick={this.submitForm} data-loading-text="<i class='fa fa-spinner fa-spin'></i> Submit">Place Order</button>
			                    </form>
			                    <div className="clearfix"></div>
			                </div>
			            </div>
			            <CheckoutSideBar cart={cart} shipping={shipping} />
			        </div>
			    </div>
			</div>
	    )
	}
}

const mapStateToProps = (state, ownProps) => ({
    cart: state.cart,
    shipping: state.shipping
})

export default connect(
  	mapStateToProps, 
  	{ getCart }
)(ReviewPage)