import React, { Component } from 'react'

export default class SuccessPage extends Component {
	componentDidMount() {
	    $('html, body').animate({
	        scrollTop: $("body").offset().top - 90
	    }, 500);
	}

	render() {
		return (
			<div className="columns-container animated fadeIn">
			    <div id="columns" className="container">
			        
			        <div className="row">
			            <div id="center_column" className="center_column col-xs-12 col-sm-12">
			                <div className="pagenotfound">
			                    <h1>Order Complete</h1>
			                    <p>
			                        We've successfully received your order.
			                    </p>
			                    <h3>To check your order status, please go to <a href="/account/orders">My Orders</a>.</h3>
			                    <h3>To continue shopping, please go to <a href="/">Home</a>.</h3>
			                </div>
			            </div>
			        </div>
			    </div>
			</div>
		)
	}
}
