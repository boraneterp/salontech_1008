import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { browserHistory } from 'react-router';
import { Link } from 'react-router';
import _ from 'lodash';
import { getCart } from '../../actions/CartAction';
import { setShippingAddress, setShippingMethod } from '../../actions/CheckoutAction';
import AddressComponent from '../../components/common/AddressComponent';
import GuestForm from '../../components/checkout/GuestForm';
import CheckoutSideBar from '../../components/checkout/CheckoutSideBar';

class ShippingPage extends Component {

  	constructor() {
    	super();
    	this.changeShippingMethod = this.changeShippingMethod.bind(this)
    	this.onSave = this.onSave.bind(this)
    	this.submitForm = this.submitForm.bind(this)
  	}

	componentWillReceiveProps(nextProps) {
		const { cart } = nextProps
		if(_.isEmpty(cart.content.items)) {
		    browserHistory.push(`/checkout/cart`)
		}
	}

	componentWillMount() {
		const { cart } = this.props
		if(_.isEmpty(cart.content.items)) {
		    browserHistory.push(`/checkout/cart`)
		}
	}

  	componentDidMount() {
		$('html, body').animate({
		    scrollTop: $("body").offset().top - 90
		}, 500);

		const { getCart } = this.props

		const submitBtn = this.refs.submitBtn
		$("#shippingForm").validate({
			messages: {
				"shiiping_method": "Shipping Method is required."
			},
			submitHandler: function(form) {
				$(submitBtn).button('loading')
				$(form).ajaxSubmit({
		      		dataType: 'json',
		      		error: function(e) {
		        		$(submitBtn).button('reset');
		      		},
		      		success: function(cart) {
		      			$(submitBtn).button('reset')
		      			getCart(cart)
		      			browserHistory.push(`/checkout/payment`)
		        	}
		    	});
			}
		});
  	}

  	changeShippingMethod(e) {
  		const target = e.target
  		const { setShippingMethod } = this.props
  		setShippingMethod(target.value, target.options[target.options.selectedIndex].getAttribute('data-price'))
  	}

  	onSave(address) {
  		const { cart, getCart, setShippingAddress } = this.props
  		let newAddr = true
  		for(let i = 0; i < cart.user.addresses.length; i++) {
  			if(cart.user.addresses[i].id == address.id) {
  				cart.user.addresses[i] = address
  				newAddr = false
  				break
  			}
  		}

  		if(newAddr) {
  			cart.user.addresses.unshift(address)
  		}

  		// cart.shipping.shipping_address = address.id
  		// cart.shipping.shipping_method = ''
  		// cart.shipping.shipping_price = 

 		getCart(cart)
  		setShippingAddress(address)
  	}

  	submitForm(e) {
  		$(this.refs.shippingForm).submit()
  	}

  	render() {
	    const { cart, shipping, setShippingAddress } = this.props;
	    const chooseable = true;
	    let quotes = cart.shipping.quotes;
	    if(typeof quotes === 'undefined') {
	    	quotes = [];
	    }

	    const addresses = !cart.is_guest? cart.user.addresses: [];
    	return (
			<div className="columns-container animated fadeIn">
				<div className="container">
					<div className="row">
						<div className="col-sm-9">
							<h2 className="checkout-heading active">1. SHIPPING</h2>
							<div className="checkout-container">
								{cart.is_guest ?
									<GuestForm />:
									<div>
										<div className="checkout-content">
											<AddressComponent 
												addresses={addresses} 
												used_address={cart.shipping.shipping_address}
												chooseable={chooseable}
												onChoose={setShippingAddress}
												onSave={this.onSave}
											/>
											<form id="shippingForm" ref="shippingForm" method="post" action="/checkout/shipping">
												<input name="shipping_address" type="hidden" defaultValue={cart.shipping.shipping_address} />
											</form>
											<p>Orders placed after 5pm EST begin processing the next business day.</p>
                                            <p className="shipping-msg">Shipping charge of $15 applies only to U.S. orders, lower 48 states. For Alaska and Hawaii, please call 1-877-278-9030 (M-F 9 am – 6 pm EST) before placing order.</p>
										</div>
										<div className="" >
											<span className="important">*</span>&nbsp;Required Fields
											<button type="button" ref="submitBtn" disabled={(addresses.length === 0)?'disabled':''} className="button btn-submit btn-sm pull-right" onClick={this.submitForm} data-loading-text="<i class='fa fa-spinner fa-spin'></i> NEXT STEP">NEXT STEP</button>
										</div>
									</div>
								}
								
								<div className="clearfix"></div>
							</div>
							<h2 className="checkout-heading closed">
							<i className="fa fa-check"></i> 2. PAYMENT
							</h2>
							<h2 className="checkout-heading closed">3. ORDER PREVIEW
							{cart.payment.processed &&
							<Link to='/checkout/review' className="button button-default">Edit</Link>
							}
							</h2>
						</div>
						<CheckoutSideBar cart={cart} shipping={shipping} />
					</div>
				</div>
			</div>
    	)
  	}
}

const mapStateToProps = (state, ownProps) => ({
    cart: state.cart,
    shipping: state.shipping
})

export default connect(
  	mapStateToProps, {
  	getCart, setShippingAddress, setShippingMethod
})(ShippingPage)
