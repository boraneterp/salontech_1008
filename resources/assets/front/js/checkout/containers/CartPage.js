import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import _ from 'lodash'
import { browserHistory } from 'react-router'
import { Link } from 'react-router'
import * as CartActions from '../../actions/CartAction'
import CartItem from '../../components/checkout/CartItem'
import CartSideBar from '../../components/checkout/CartSideBar'

class CartPage extends Component {

  	constructor() {
  		super()
  		this.onCheckout = this.onCheckout.bind(this);
  	}

    componentDidMount() {
    }

  	onCheckout() {
  		const { cart } = this.props

  		if(_.isEmpty(cart.content.items) || cart.content.sub_total < cart.content.minimum) {
			$('html, body').animate({
		        scrollTop: $(".cart-page-menu").offset().top - 90
		    }, 200);
			return;
		}
  		browserHistory.push(`/checkout/shipping`)
  	}

  	render() {
	    const { cart, shipping, actions } = this.props
        const totalQty = _.isEmpty(cart.content.items)? 0: cart.content.quantity
	    return (
	    	<div className="columns-container">
            <div className="container">
                <div className="row">
                    <div className="col-sm-9">
                        <div className="cart">
                            <div className="alert alert-warning shipping-msg">Shipping charge of $15 applies only to U.S. orders, lower 48 states. <br/> For Alaska and Hawaii, please call 1-877-278-9030 (M-F 9 am – 6 pm EST) before placing order.</div>
                            <h2 className="checkout-heading">Your Cart ({totalQty})</h2>
                            <ul className="cart__content">
                            {!_.isEmpty(cart.content.items) ?
                                cart.content.items.map(item =>
                                    <CartItem key = { item.id }
                                      item = { item }
                                      onDelete = {() => actions.deleteProduct(item.id)}
                                      onUpdate = {(quantity) => actions.updateProduct(item.id, quantity)} />
                                )
                            :
                            <li className="cart__content__empty">
                                There are no items in your cart.
                            </li>
                            }
                            </ul>
                        </div>
                    </div>
                    <CartSideBar cart={cart} shipping={shipping} />
                </div>
            </div>
        </div>
	    )
	}
}

const mapStateToProps = (state, ownProps) => ({
    cart: state.cart,
    shipping: state.shipping
})

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(CartActions, dispatch)
})

export default connect(
  	mapStateToProps, 
  	mapDispatchToProps
)(CartPage)