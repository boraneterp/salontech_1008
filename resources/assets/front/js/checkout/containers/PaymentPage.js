import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { browserHistory } from 'react-router';
import { Link } from 'react-router';
import _ from 'lodash';
import { getCart } from '../../actions/CartAction';
import PaymentComponent from '../../components/common/PaymentComponent';
import AddressComponent from '../../components/common/AddressComponent';
import BillingForm from '../../components/checkout/BillingForm';
import CheckoutSideBar from '../../components/checkout/CheckoutSideBar';

class PaymentPage extends Component {

  	constructor() {
    	super();
    	const date = new Date()
    	const currentMonth = (date.getMonth() + 1) < 10? "0" + (date.getMonth() + 1): (date.getMonth() + 1)
	    this.state = {
	    	error: "",
	    	payment_type: 'card',
	    	billing_address: 0,
	    	payment_id: 0,
	    	promotion_error: ''
	    }

	    this.cancelCoupon = this.cancelCoupon.bind(this)
	    this.changePaymentType = this.changePaymentType.bind(this)
	    this.onChoosePayment = this.onChoosePayment.bind(this)
	    this.onChooseAddress = this.onChooseAddress.bind(this)
    	this.onSaveAddress = this.onSaveAddress.bind(this)
    	this.onSavePayment = this.onSavePayment.bind(this)
    	this.submitForm = this.submitForm.bind(this)
  	}

  	componentWillReceiveProps(nextProps) {
	    const { cart } = nextProps
	    if(_.isEmpty(cart.content.items)) {
	        browserHistory.push(`/checkout/cart`)
	    }
  	}

  	componentWillMount() {
	    const { cart } = this.props
	    if(_.isEmpty(cart.content.items)) {
	        browserHistory.push(`/checkout/cart`)
	    }

	    if(cart.payment.type) {
	    	this.setState({
				payment_type: cart.payment.type
	        });
	    }

	    this.setState({
	    	payment_id: cart.payment.payment_id,
			billing_address: cart.payment.billing_address
        });
  	}

  	componentDidMount() {

  		const btn = this.refs.submitBtn
  		const { getCart } = this.props
  		const self = this

	    $("#nextForm").validate({
	        submitHandler: function(form) {
	        	if(self.state.payment_type === 'paypal') {
	        		window.location.href = '/checkout/paypal'
	        		return
	        	}
				$(btn).button('loading');
				$("#nextForm").ajaxSubmit({
					dataType: 'json',
					error: function(e) {
					  	self.setState({
							error: e.responseText
				        });
					  	$(btn).button('reset');
					},
					success: function(cart) {
					  	$(btn).button('reset')
					  	getCart(cart, function() {
					  		browserHistory.push(`/checkout/review`)
					  	})
					}
				}); 
				return false;
	      	}
	    });

	    if($("#couponForm").length > 0) {
	    	$("#couponForm").validate({
	    		messages: {
	    			"promotion_code": "Promotion Code is required."
	    		},
		        submitHandler: function(form) {
		        	self.setState({
						promotion_error: ''
			        });
		        	$("#couponForm button").button('loading');
					$("#couponForm").ajaxSubmit({
						dataType: 'json',
						error: function(e) {
						  	self.setState({
								promotion_error: e.responseJSON
					        });
						  	$("#couponForm button").button('reset');
						},
						success: function(cart) {
						  	$("#couponForm button").button('reset');
						  	getCart(cart)
						}
					}); 
					return false;
		      	}
		    });
	    }
  	}

  	cancelCoupon(e) {
  		const { getCart } = this.props
  		$.post('/checkout/payment/coupon/cancel', cart => {
			getCart(cart)
		})
  	}

  	changePaymentType(e) {
  		const { payment_type } = this.state
  		const target = e.target
  		if(payment_type === target.value) {
  			return
  		}
  		this.setState({
  			error: '',
		    payment_type: target.value
		});
  	}

  	onChoosePayment(payment) {
  		this.setState({
			payment_id: payment.id
        });
  	}

  	onChooseAddress(address) {
  		this.setState({
			billing_address: address.id
        });
  	}

  	onSavePayment(payment) {
  		const { cart, getCart } = this.props
  		const index = _.findIndex(cart.user.payments, _.matchesProperty('id', payment.id))
  		if(index > 0) {
  			cart.user.payments[index] = payment
  		} else {
  			cart.user.payments.unshift(payment)
  		}

  		this.setState({
			payment_id: payment.id
        });

 		getCart(cart)
  	}

  	onSaveAddress(address) {
  		const { cart, getCart } = this.props
  		const index = _.findIndex(cart.user.addresses, _.matchesProperty('id', address.id))
  		if(index > 0) {
  			cart.user.addresses[index] = address
  		} else {
  			cart.user.addresses.unshift(address)
  		}

  		this.setState({
			billing_address: address.id
        });

 		getCart(cart)
  	}

  	submitForm(e) {
  		$(this.refs.nextForm).submit(); 
  	}

  	render() {
    	const { cart, shipping, editBilling, errorMessage } = this.props
    	const { error, payment_id, billing_address, payment_type, card_info, promotion_error } = this.state
    	const chooseable = true
    	return (
			<div className="columns-container animated fadeIn">
				<div className="container">
					<div className="row">
						<div className="col-sm-9">
							<h2 className="checkout-heading closed">
							<i className="fa fa-check"></i> 1. SHIPPING
							<Link to='/checkout/shipping' className="button button-default">Edit</Link>
							</h2>
							<h2 className="checkout-heading active">2. PAYMENT</h2>
							<div className="checkout-container">
								<h3 className="checkout-container__heading">Promotion Code:</h3>
								<div className="checkout-content">
									<div className={_.isEmpty(cart.payment.coupon)? 'row': 'row hide'} >
										<form method="post" action="/checkout/payment/coupon/apply" id="couponForm">
											<div className="col-sm-4 col-xs-6">
												<input type="text" className="form-control required" placeholder="Promotion Code" defaultValue="" name="promotion_code" />
												{promotion_error &&
													<label className="error">{promotion_error}</label>
												}
											</div>
											<div className="col-xs-8 col-xs-6">
												<button type="submit" className="button btn-submit button-smaill" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Apply">Apply</button>
											</div>
										</form>
									</div>
									<div className={!_.isEmpty(cart.payment.coupon)? 'row': 'row hide'} >
										<div className="col-sm-4 col-xs-6">{cart.payment.coupon}</div>
										<div className="col-sm-8 col-xs-6">
											<button type="submit" className="button btn-submit button-smaill" onClick={this.cancelCoupon}>Cancel</button>
										</div>
									</div>
								</div>
								<h3 className="checkout-container__heading">PAYMENT METHOD:</h3>
								<div className="form-group">
									<div className="checkbox-list">
										<div className="checkbox">
											<label><div className={payment_type == 'card'? "radio-wrapper checked": "radio-wrapper"}><input type="radio" name="payment_method" value="card" className="form-control" onClick={this.changePaymentType} /></div> Credit or Debit Card</label>
										</div>
										<div className="checkbox">
											<label><div className={payment_type == 'paypal'? "radio-wrapper checked": "radio-wrapper"}><input type="radio" name="payment_method" value="paypal" className="form-control" onClick={this.changePaymentType} /></div> Paypal</label>
										</div>
									</div>
								</div>
								{(payment_type == 'card') &&
									<div className="checkout-content">
										<PaymentComponent 
											payments={cart.user.payments} 
											used_payment={payment_id}
											chooseable={chooseable}
											onChoose={this.onChoosePayment}
											onSave={this.onSavePayment}
										/>
									</div>
								}
								{(payment_type == 'card') &&
									<div>
										<h3 className="checkout-container__heading">BILLING ADDRESS:</h3>
										{cart.is_guest ?
											<BillingForm />:
											<div className="checkout-content">
												<AddressComponent 
													addresses={cart.user.addresses} 
													used_address={billing_address}
													chooseable={chooseable}
													onChoose={this.onChooseAddress}
													onSave={this.onSaveAddress}
												/>
											</div>
										}
									</div>
								}
								<div className="" >
									<form id="nextForm" method="post" ref="nextForm" action="/checkout/payment">
										<input name="payment_id" type="hidden" defaultValue={payment_id} />
										<input name="billing_address" type="hidden" defaultValue={billing_address} />
									</form>
									<button type="submit" ref="submitBtn" disabled={((cart.user.payments.length === 0 || editBilling) && payment_type == 'card')?'disabled':''} className="button btn-submit btn-sm pull-right" onClick={this.submitForm} data-loading-text="<i class='fa fa-spinner fa-spin'></i> NEXT STEP">NEXT STEP</button>
								</div>
								<div className="clearfix"></div>
							</div>
							<h2 className="checkout-heading closed">3. ORDER PREVIEW
							{cart.payment.processed &&
							<Link to='/checkout/review' className="button button-default">Edit</Link>
							}
							</h2>
						</div>
						<CheckoutSideBar cart={cart} shipping={shipping}  />
					</div>
				</div>
			</div>
    	)
  	}
}

const mapStateToProps = (state, ownProps) => ({
    cart: state.cart,
    shipping: state.shipping,
    editBilling: state.editBilling
})

export default connect(
  	mapStateToProps, {
    	getCart
})(PaymentPage)
