import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { CSSTransitionGroup } from 'react-transition-group'
import Breadcrumb from '../../components/checkout/Breadcrumb'

const Root = ({ children, errorMessage, page }) => (
  <div>
    <Breadcrumb page={page} />
    <CSSTransitionGroup
      transitionName="slide"
      transitionAppear={true}
      transitionAppearTimeout={300}
      transitionEnterTimeout={300}
      transitionLeaveTimeout={300}>
      {children}
    </CSSTransitionGroup>
  </div>
	 
)

Root.propTypes = {
	children: PropTypes.node,
  	errorMessage: PropTypes.string,
  	page: PropTypes.string.isRequired
}

const mapStateToProps = (state, ownProps) => ({
  	errorMessage: state.errorMessage,
  	page: ownProps.location.pathname.toLowerCase().replace("/checkout/", "")
})

export default connect(
	mapStateToProps, 
	null
)(Root)