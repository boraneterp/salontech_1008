import React from 'react'
import ReactDOM from 'react-dom'
import { browserHistory } from 'react-router'
import store from '../store'
import { Provider } from 'react-redux'
import { syncHistoryWithStore } from 'react-router-redux'
import { Router, Route } from 'react-router'
import _ from 'lodash'
import Root from './containers/Root'
import CartPage from './containers/CartPage'
import ShippingPage from './containers/ShippingPage'
import PaymentPage from './containers/PaymentPage'
import ReviewPage from './containers/ReviewPage'
import SuccessPage from './containers/SuccessPage'

const history = syncHistoryWithStore(browserHistory, store)

const cartNotEmpty = (nextState, replace, callback) => {
	if(_.isEmpty(store.getState().entities.cart.items)
		|| store.getState().entities.cart.sub_total < store.getState().entities.cart.minimum) {
		replace(`/checkout/cart`)
	}

	callback()
}

const addressNotEmpty = (nextState, replace, callback) => {
	if(_.isEmpty(store.getState().entities.cart.items)
		|| store.getState().entities.cart.sub_total < store.getState().entities.cart.minimum) {
		replace(`/checkout/cart`)
	} else if(_.isEmpty(store.getState().entities.cart.user)) {
		replace(`/checkout/shipping`)
	}

	callback()
}

export default () => {
	ReactDOM.render(
		<Provider store={store}>
	    	<Router history={history}>
		    	<Route path="/" component={Root}>
		    		<Route path="/checkout/cart" component={CartPage} />
				  	<Route path="/checkout/shipping"  component={ShippingPage}  />
				  	<Route path="/checkout/payment" component={PaymentPage}  />
				  	<Route path="/checkout/review" component={ReviewPage}  />
				  	<Route path="/checkout/success" component={SuccessPage}  />
				</Route>
	      	</Router>
	  	</Provider>,
			document.getElementById('checkout')
	)
}