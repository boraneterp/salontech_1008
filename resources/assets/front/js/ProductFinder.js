
const viewControl = () => {
	$(".toolbar__view-control").on("click", function(e) {
		e.preventDefault();
		if($(this).hasClass("active")) {
			return;
		}
		$(".toolbar__view-control").removeClass("active");
		const style = $(this).data("style");
		$(".toolbar__view-control[data-style='" + style + "']").addClass("active");
		if($(this).data("style") == "grid") {
			$(".product_list").removeClass("list");
			$(".product_list").addClass("grid");
			$(".product_list li").removeClass("col-sm-12");
			$(".product_list li").addClass("col-sm-4");
			$(".product_list li .product-container__item").removeClass("row");
			$(".product_list li .left-block").removeClass("col-sm-4");
			$(".product_list li .right-block").removeClass("col-sm-8");
		} else {
			$(".product_list").removeClass("grid");
			$(".product_list").addClass("list");
			$(".product_list li").removeClass("col-sm-4");
			$(".product_list li").addClass("col-sm-12");
			$(".product_list li .product-container__item").addClass("row");
			$(".product_list li .left-block").addClass("col-sm-4");
			$(".product_list li .right-block").addClass("col-sm-8");
		}
	});

	if($(window).width() <= 768) { 
		$(".toolbar__view-control[data-style='grid']").trigger("click")
		$(".lablistproducts .item").matchHeight()
	}
}

const sortControl = () => {
	$(".toolbar__sort-control").on("change", function(e) {
		window.location.href = window.location.pathname + "?sort=" + $(this).val()
	});
}

export default () => {
	viewControl()
	sortControl()
}