import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import store from './store';
import MiniCart from './cart/MiniCart';
import { addToCart } from './actions/CartAction';

const scrollTop = () => {
	$(window).scroll(function () {
		if ($(this).scrollTop() > 100) {
			$('.mypresta_scrollup').addClass('open');
		} else {
			$('.mypresta_scrollup').removeClass('open');
		}
	});
	// scroll body to 0px on click
	$('.mypresta_scrollup').click(function () {
		$('body,html').animate({
			scrollTop: 0
		}, 800);
		return false;
	});
}

const stickyMenu = () => {
	$(window).scroll(function() {
		var _height =$(".labHeader").height();
		var scroll = $(window).scrollTop();
		if (scroll < _height) {
		 	$("header").removeClass("labfixed");
		}else{
		 	$("header").addClass("labfixed");
		}
	});
}

const headerMenu = () => {
	if($(window).width() > 768) { 
		$("header .lab-menu-row").slick({
		  	infinite: false,
		  	slidesToShow: 6,
		  	slidesToScroll: 6,
		  	arrows: true,
		  	prevArrow: '<a class="prev labnewblogprev"><i class="fa fa-chevron-left"></i></a>',
		  	nextArrow: '<a class="next labnewblognext"><i class="fa fa-chevron-right"></i></a>',
		  	responsive: [
			    {
			      	breakpoint: 768,
			      	settings: {
				        slidesToShow: 2
				    }
				}
			]
		});
	}
	
}

const mobileMenu = () => {
	$("header").on("click", ".mobile-menu", function(e) {
		e.preventDefault();
		if($(".lab-menu-horizontal").hasClass("show")) {
			$(".lab-menu-horizontal").removeClass("show");
		} else {
			$(".lab-menu-horizontal").addClass("show");
		}
	});

	$("header").on("click", ".icon-drop-mobile", function(e) {
		e.preventDefault();
		const $subMenu = $(this).parent().find(".lab-sub-menu");
		if($subMenu.is(":visible")) {
			$subMenu.slideUp();
			$(this).removeClass("opened");
		} else {
			$subMenu.slideDown();
			$(this).addClass("opened");
		}

	});
}

const search = () => {
	const elementClick = '#search_block_top .current';
	const elementSlide =  '.toogle_content';

	$(elementClick).on('click', function(e){
		e.stopPropagation();
		var subUl = $(this).next(elementSlide);
		if(subUl.is(':hidden'))
		{
			subUl.slideDown();
			$(this).addClass("active");
		}
		else
		{
			subUl.slideUp();
			$(this).removeClass("active");
		}
		$(elementClick).not(this).next(elementSlide).slideUp();
		$(elementClick).not(this).removeClass("active");
		e.preventDefault();
	});

	$(elementSlide).on('click', function(e){
		e.stopPropagation();
	});

	$(document).on('click', function(e){
		e.stopPropagation();
		var elementHide = $(elementClick).next(elementSlide);
		$(elementHide).slideUp();
		$(elementClick).removeClass('active');
	});
}

const popup = () => {
	$(".popup-content").on("click", ".close-popup", function(e) {
		e.preventDefault();
		closePopup()
	});
}

export const closePopup = () => {
	$(".popup-content").addClass("zoomOut");
	setTimeout(function() {
		$(".popup-content").hide()
		$(".popup-content").removeClass("zoomOut");
	}, 500);
}

const wow = () => {
	new WOW().init();
}

const footer = () => {
	$(".footer-block").on("click", "h4", function(e) {
		e.preventDefault();
		const $subMenu = $(this).parent().find(".toggle-footer");
		if($subMenu.is(":visible")) {
			$subMenu.slideUp();
			$(this).removeClass("active");
		} else {
			$subMenu.slideDown();
			$(this).addClass("active");
		}

	});
}

const matchHeight = () => {
	if($(".lablistproducts").length > 0) {
		$(".lablistproducts .item").matchHeight()
	}
}

const initModal = () => {
	$('.modal').on('show.bs.modal', centerModal);
	$('.modal-popup .close-link').click(function(event){
		event.preventDefault();
		$('.modal').modal('hide');
	});
	$(window).on("resize", function() {
		//$('.modal:visible').each(centerModal);
	});
}

const centerModal = () => {
	$("label.error, .alert").hide();
	$(".btn-submit").button('reset');
	$(this).css('display', 'block');
	var $dialog = $(this).find(".modal-dialog"),
		offset = ($(window).height() - $dialog.height()) / 2,
		bottomMargin = parseInt($dialog.css('marginBottom'), 10);
	if (offset < bottomMargin) offset = bottomMargin;
	$dialog.css("margin-top", offset);
}

const bindCart = () => {
	$(document).on("click", ".add-to-cart", function(e) {
		e.preventDefault()
		const product_id = $(this).data("product_id")
		const quantity = 1
		addToCart(product_id, quantity, store.dispatch)
	});
}

const mask = () => {
	if($(".phone-number").length > 0) {
		$(".phone-number").mask('(000) 000-0000');
	}
}

const instagram = () => {
	if($(".social-container.instagram").length == 0)
		return;

	$(".social-container.instagram ul").slick({
	  	infinite: false,
	  	slidesToShow: 4,
	  	slidesToScroll: 4,
	  	arrows: true,
	  	prevArrow: '<a class="prev labnewblogprev"><i class="fa fa-chevron-left"></i></a>',
	  	nextArrow: '<a class="next labnewblognext"><i class="fa fa-chevron-right"></i></a>',
	  	responsive: [
		    {
		      	breakpoint: 768,
		      	settings: {
			        slidesToShow: 2,
			        slidesToScroll: 2
			    }
			}
		]
	});

	$('.social-container.instagram').on("click", ".instagram__link", function(e) {
		e.preventDefault();
		const image = $(this).find("img").attr("src");
		const likes = $(this).data("likes");
		const comments = $(this).data("comments");
		const text = $(this).data("text");
		const profile_image = $(this).data("profile_image");
		const full_name = $(this).data("full_name");
		const product_url = $(this).data("product_url");
		const product_image = $(this).data("product_image");
		const product_name = $(this).data("product_name");
		const video = $(this).data("video");
		const code = $(this).data("code");
		$("#instagram-popup .image-container").hide();
		$("#instagram-popup .video-container").hide();
		if(video == "1") {
			$("#instagram-popup iframe").attr("src", "https://www.instagram.com/p/" + code + "/embed/");
			$("#instagram-popup .video-container").show();
		} else {
			$("#instagram-popup .image-container img").attr("src", image);
			$("#instagram-popup .image-container .likes").html(likes);
			$("#instagram-popup .image-container .comments").html(comments);
			$("#instagram-popup .image-container .caption").html(text);
			$("#instagram-popup .image-container").show();
		}
		
		$("#instagram-popup .text-container .author img").attr("src", profile_image);
		$("#instagram-popup .text-container .author span").html(full_name);
		$("#instagram-popup .text-container .product .product__link").attr("href", product_url);
		$("#instagram-popup .text-container .product .product__image").attr("src", product_image);
		$("#instagram-popup .text-container .product .product-name").html(product_name);
		$("#instagram-popup").show();
	});
}

export const init = () => {
	scrollTop()
	//loader();
	popup()
	stickyMenu()
	headerMenu()
	mobileMenu()
	search()
	//wow();
	mask()
	matchHeight()
	footer()
	initModal()
	bindCart()
	instagram()
	ReactDOM.render(
		<Provider store={store}>
	    	<MiniCart />
	  	</Provider>,
	  	document.getElementById('mini-cart')
	)
}

export default {
	init,
	closePopup
}