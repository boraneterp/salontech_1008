import store from './store'
import { addToCart } from './actions/CartAction'

const thumbnail = () => {
	// $("#thumbs_list_frame").slick({
	//   	infinite: false,
	//   	slidesToShow: 4,
	//   	slidesToScroll: 1,
	//   	arrows: true,
	//   	prevArrow: '<a class="prev labnewblogprev"><i class="fa fa-chevron-left"></i></a>',
	//   	nextArrow: '<a class="next labnewblognext"><i class="fa fa-chevron-right"></i></a>',
	//   	responsive: [
	// 	    {
	// 	      	breakpoint: 768,
	// 	      	settings: {
	// 		        slidesToShow: 2
	// 		    }
	// 		}
	// 	]
	// });

	if($(window).width() <= 768) { 
		$(".thumbnail-container").slick({
		  infinite: false,
		  slidesToShow: 1,
		  slidesToScroll: 1,
		  dots: true,
		  arrows: false
		});
	} else {
		$(".thumbnail-container").on("click", "a", function(e) {
			e.preventDefault();
			$(".thumbnail-container a").removeClass("active");
			$(this).addClass("active");
			if($(this).hasClass("video")) {
				$(".main-image-container").hide();
				$(".main-video").show();
				$(".main-video video").get(0).play();
			} else {
				if($(".main-video video").length > 0) {
					$(".main-video video").get(0).pause();
				}
				$(".main-video").hide();
				$(".main-image-container a").attr("href", $(this).attr("href"));
				$(".main-image-container img").attr("src", $(this).attr("href"));
				$(".main-image-container").show();
			}
		});

		$(".main-image-container a").magnificPopup({
			type: 'image',
			closeOnContentClick: true,
			closeBtnInside: false,
			fixedContentPos: true,
			mainClass: 'mfp-no-margins mfp-with-zoom', // class to remove default margin from left and right side
			image: {
				verticalFit: true
			},
			zoom: {
				enabled: true,
				duration: 300 // don't foget to change the duration also in CSS
			}
		});
	}

	
}

const qtyControl = () => {
	$(".box-info-product").on("click", ".btn", function(e) {
		e.preventDefault();
		const dir = $(this).data('dir');
		let qty = parseInt($(this).parent().find('input').val());
		if(dir == 'plus' && qty < 99) {
			qty++;
		} else if(dir == 'minus' && qty > 1) {
			qty--;
		} else {
			return
		}
		$(this).parent().find('input').val(qty);
	});
}

const singleCart = () => {

	$(".single-cart").on("click", function(e) {
		const product_id = $(this).data("product_id")
		const quantity = $(".quantity").val();
		addToCart(product_id, quantity, store.dispatch)
	});
	
}

const relatedCarousel = () => {
	$(".pos-Accessories").slick({
	  	infinite: false,
	  	slidesToShow: 4,
	  	slidesToScroll: 1,
	  	arrows: true,
	  	prevArrow: '<a class="prev labnewblogprev"><i class="fa fa-chevron-left"></i></a>',
	  	nextArrow: '<a class="next labnewblognext"><i class="fa fa-chevron-right"></i></a>',
	  	responsive: [
		    {
		      	breakpoint: 980,
		      	settings: {
			        slidesToShow: 3
			    }
			},
			{
				breakpoint: 768,
		      	settings: {
			        slidesToShow: 2
			    }
		    }
		]
	});
}

const reviewForm = () => {
	
	$('#rating').barrating({
		theme: 'fontawesome-stars',
		initialRating: 4
	});

	$("#reviewForm").validate({
		messages: {
			review: "Review is required."
		}, 
        submitHandler: function(form) {
			$(".review--alert").hide();
			$("#reviewForm .btn-submit").button('loading');
			$(form).ajaxSubmit({
				dataType: 'json',
				error: function(e) {
					$("#reviewForm .btn-submit").button('reset');
				},
				success: function(total) {
					$(".review--alert").show();
					$("#reviewForm textarea").val("");
					$("#reviewForm .btn-submit").button('reset');
				}
			});
			return false;
		}
	});
	
} 

export default () => {
	thumbnail()
	qtyControl()
	singleCart()
	relatedCarousel()
	reviewForm()
}