import axios from 'axios'
import * as types from '../constants/ActionTypes'

export const getWarranties = (force) => (dispatch, getState) => {

	axios.request({
        method: 'GET',
        params: getState().warranty.param,
        url: `/admin777/warranties.json`
    }).then((response) => {
    	dispatch({
		  	type: types.SET_WARRANTIES,
		  	data: response.data,
		  	page_count: response.page_count
		})
    });
}

export const onSearch = () => (dispatch, getState) => {
	dispatch({
		type: types.CHANGE_WARRANTY_PAGE,
		offset: 0
	})
}

export const changeWarrantyStatus = (id, status) => (dispatch, getState) => {
	axios.request({
        method: 'POST',
        data: { status },
        url: `/admin777/warranties/${id}/status`
    }).then((response) => {
    	dispatch({
		  	type: types.UPDATE_WARRANTY_STATUS,
		  	warranty: id,
		  	status: status
		})
    }).catch((error) => {
    	alert("You can't update this warranty!!")
    });
}