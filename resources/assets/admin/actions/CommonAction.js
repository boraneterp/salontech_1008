import axios from 'axios'
import * as types from '../constants/ActionTypes'

export const getStates = () => (dispatch, getState) => {

	// const states = getState().states
	// if(states.length > 0) {
	// 	return null
	// }

	axios.request({
        method: 'GET',
        url: '/states.json'
    }).then((response) => {
    	dispatch({
		  	type: types.GET_STATES,
		  	data: response
		})
    });
}

export const updateStateTax = (state, value) => (dispatch, getState) => {
	dispatch({
	  	type: types.CHANGE_STATE_TAX,
	  	state,
	  	value
	});
}

export const saveStateTax = (state) => (dispatch, getState) => {
	axios.post('/admin777/settings/tax/save', state).then((response) => {
    	dispatch({
		  	type: types.GET_STATES,
		  	data: response
		})
    }).catch((error) => {
    	alert("Invalid tax rate.");
    });
}

export const getDashboardData = () => (dispatch, getState) => {
	axios.request({
        method: 'GET',
        url: '/admin777/dashboard/data.json'
    }).then((response) => {
    	dispatch({
		  	type: types.SET_DASHBOARD_DATA,
		  	data: response
		})
    });
}

export const getAnimation = () => (dispatch, getState) => {
	const path = getState().path
	if(path.previoutPath === '' || path.currentPath === ''|| path.previoutPath === '/admin777') {
		return 'fadeIn'
	}

	if(path.currentPath.includes(path.previoutPath)) {
		return 'slideInRight'
	}

	if(path.previoutPath.includes(path.currentPath)) {
		return 'slideInLeft'
	}
}