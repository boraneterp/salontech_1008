import axios from 'axios';
import { SubmissionError } from 'redux-form';
import objectToFormData from '../../utils/objectToFormData';
import * as types from '../constants/ActionTypes';

export const getSettings = () => (dispatch, getState) => {

	const setting = getState().setting
	if(!_.isEmpty(setting.home)) {
		return null
	}

	$.ajax({
		url: '/admin777/settings/list.json',
		dataType: 'json',
		type: 'GET',
		success: settings => {
			dispatch({
			  	type: types.SET_SETTINGS,
			  	settings
			})
		}
	});
}

export const saveSettings = (settings) => (dispatch, getState) => {
	dispatch({
	  	type: types.SET_SETTING_MESSAGE,
	  	message: ''
	})
	const config = { headers: { 'Content-Type': 'multipart/form-data' } };
	return axios.post('/admin777/settings/save', objectToFormData(settings), config).then((response) => {

		dispatch({
		  	type: types.SET_SETTING_MESSAGE,
		  	message: 'Saved Successfully.'
		})

		dispatch({
		  	type: types.SET_SETTINGS,
		  	settings: response
		})
		
		$('html, body').animate({
	        scrollTop: $("body").offset().top - 90
	    }, 500);
		
    }).catch((error) => {
    	throw new SubmissionError({
	        _error: 'Fail'
	    })
    })
}

export const setSettings = (settings) => (dispatch, getState) => {

	dispatch({
	  	type: types.SET_SETTINGS,
	  	settings
	})
}

export const getCoupons = (p) => (dispatch, getState) => {

	const params = _.isEmpty(p)? getState().setting.coupon.params: p
	axios.request({
        method: 'GET',
        params,
        url: '/admin777/settings/coupons/list.json'
    }).then((response) => {
    	dispatch({
		  	type: types.GET_COUPONS,
		  	data: response.data,
		  	page_count: response.page_count
		})
    });
}

export const onCouponPagination = (offset) => (dispatch, getState) => {
	dispatch({
		type: types.CHANGE_COUPON_PAGE,
		offset
	})
}

export const setCurrentCoupon = (coupon) => (dispatch, getState) => {
	dispatch({
	  	type: types.SET_CURRENT_COUPON,
	  	coupon
	})
}

export const saveCoupon = (coupon) => (dispatch, getState) => {
	return axios.post('/admin777/settings/coupons/save', coupon).then((response) => {
    	dispatch({
		  	type: types.GET_COUPONS,
		  	data: response.data,
		  	page_count: response.page_count
		})
		
    }).catch((error) => {
    	throw new SubmissionError({
	        _error: 'Fail'
	    })
    })
}

export const hideCouponModal = (coupon) => (dispatch, getState) => {
	dispatch({
		type: types.HIDE_COUPON_MODAL,
		coupon
	})
}

export const deleteCoupon = (id) => (dispatch, getState) => {
	if(confirm("Are you sure remove this coupon?")) {
		return axios.post('/admin777/settings/coupons/delete', { id }).then((response) => {
	    	dispatch({
			  	type: types.GET_COUPONS,
			  	data: response.data,
			  	page_count: response.page_count
			})
			
	    }).catch((error) => {
	    	throw new SubmissionError({
		        _error: 'Fail'
		    })
	    })
	}
}

export const getStores = (force) => (dispatch, getState) => {

	const data = getState().setting.store.data
	if(!force && !_.isEmpty(data) && data.length > 0) {
		return null
	}

	axios.request({
        method: 'GET',
        params: getState().setting.store.params,
        url: '/admin777/settings/stores/list.json'
    }).then((response) => {
    	dispatch({
		  	type: types.GET_STORES,
		  	data: response.data,
		  	page_count: response.last_page
		})
    });
}

export const setStores = (stores) => (dispatch, getState) => {
	dispatch({
	  	type: types.SET_STORES,
	  	stores
	})
}

export const setCurrentStore = (store) => (dispatch, getState) => {
	dispatch({
	  	type: types.SET_CURRNET_STORE,
	  	currentStore: store
	})
}

export const deleteStore = (id) => (dispatch, getState) => {
	if(confirm("Are you sure remove this store?")) {
  		$.post( "/admin777/settings/stores/delete", { id }, ).fail(e => {
			alert("You can't remove this store!!")
		}).done( stores => {
			dispatch({
			  	type: types.SET_STORES,
			  	stores
			})
		});
	}
}

export const onSearch = (name) => (dispatch, getState) => {
	dispatch({
		type: types.CHANGE_STORE_PARAM,
		name,
		offset: 0
	})
}