import axios from 'axios'
import objectToFormData from '../../utils/objectToFormData'
import * as types from '../constants/ActionTypes'

export const getShowcases = (force) => (dispatch, getState) => {

	axios.request({
        method: 'GET',
        params: getState().showcase.param,
        url: `/admin777/showcases.json`
    }).then((response) => {
    	dispatch({
		  	type: types.SET_SHOWCASES,
		  	data: response
		})
    });
}

export const saveShowcases = (showcase) => (dispatch, getState) => {
	dispatch({
	  	type: types.SET_SHOWCASE_MESSAGE,
	  	message: ''
	})
	const config = { headers: { 'Content-Type': 'multipart/form-data' } };
	return axios.post('/admin777/showcases/save', objectToFormData(showcase), config).then((response) => {

		dispatch({
		  	type: types.SET_SHOWCASE_MESSAGE,
		  	message: 'Saved Successfully.'
		})

		dispatch({
		  	type: types.SET_SHOWCASES,
		  	data: response
		})
		
		$('html, body').animate({
	        scrollTop: $("body").offset().top - 90
	    }, 500);
		
    }).catch((error) => {
    	throw new SubmissionError({
	        _error: 'Fail'
	    })
    })
}
