import axios from 'axios'
import * as types from '../constants/ActionTypes'

export const getSubscriptions = (force) => (dispatch, getState) => {

	axios.request({
        method: 'GET',
        params: getState().subscription.param,
        url: `/admin777/subscriptions.json`
    }).then((response) => {
    	dispatch({
		  	type: types.GET_SUBSCRIPTIONS,
		  	data: response.data,
		  	page_count: response.page_count
		})
    });
}

export const onSearch = () => (dispatch, getState) => {
	dispatch({
		type: types.CHANGE_SUBSCRIPTION_PAGE,
		offset: 0
	})
}