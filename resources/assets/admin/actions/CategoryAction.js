import axios from 'axios';
import * as types from '../constants/ActionTypes';
import { browserHistory } from 'react-router';
import { SubmissionError } from 'redux-form';

export const getCategories = (force) => (dispatch, getState) => {

	let param = getState().category.param
	if(force) {
		param.name = ''
	}

	axios.request({
        method: 'GET',
        params: param,
        url: `/admin777/category/list.json`
    }).then((response) => {
    	dispatch({
		  	type: types.GET_CATEGORIES,
		  	data: response.data,
		  	page_count: response.page_count
		})
    });
}

export const getCategoriesWithProducts = () => (dispatch, getState) => {

	axios.request({
        method: 'GET',
        url: `/admin777/category/deepList.json`
    }).then((response) => {
    	dispatch({
		  	type: types.SET_CATEGORIES_WITH_PRODUCTS,
		  	data: response.data
		})
    });
}

export const setCategories = (categories) => (dispatch, getState) => {
	dispatch({
	  	type: types.SET_CATEGORIES,
	  	categories
	})
}

export const setCurrentCategory = (category) => (dispatch, getState) => {
	dispatch({
	  	type: types.SET_CURRNET_CATEGORY,
	  	currentCategory: category
	})
}

export const getCategoryDetail = (id) => (dispatch, getState) => {

	if(!id) {
		dispatch({
		  	type: types.SET_CURRNET_CATEGORY,
		  	currentCategory: {}
		})
		return;
	}

	axios.request({
        method: 'GET',
        url: `/admin777/category/detail/${id}.json`
    }).then((category) => {
    	dispatch({
		  	type: types.SET_CURRNET_CATEGORY,
		  	currentCategory: category
		})
    });
}


export const saveCategory = (category) => (dispatch, getState) => {
	const isNew = category.id? false: true
	dispatch({
	  	type: types.SET_CATEGORY_MESSAGE,
	  	message: ''
	});


	return axios.post('/admin777/category/save', category).then((response) => {

    	dispatch({
		  	type: types.SET_CURRNET_CATEGORY,
		  	currentCategory: response
		})

		dispatch({
		  	type: types.SET_CATEGORY_MESSAGE,
		  	message: 'Saved Successfully.'
		})

		if(isNew) {
			browserHistory.push(`/admin777/category/detail/${response.id}`)
		} 
		
		$('html, body').animate({
	        scrollTop: $("body").offset().top - 90
	    }, 500);
		
    }).catch((error) => {
    	throw new SubmissionError({
	        _error: 'Fail'
	    })
    })
}

export const deleteCategory = (id) => (dispatch, getState) => {
	if(confirm("Are you sure remove this category?")) {
		axios.request({
	        method: 'POST',
	        data: { id },
	        url: "/admin777/category/delete"
	    }).then((response) => {
	    	dispatch({
			  	type: types.GET_CATEGORIES,
			  	data: response.data,
			  	page_count: 1
			})
	    });
	}
}

export const onSearch = (name) => (dispatch, getState) => {
	dispatch({
		type: types.CHANGE_CATEGORY_PARAM,
		name,
		offset: 0
	})
}