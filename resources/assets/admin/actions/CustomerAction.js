import axios from 'axios'
import * as types from '../constants/ActionTypes'

export const getCustomers = (force) => (dispatch, getState) => {

	axios.request({
        method: 'GET',
        params: getState().customer.params,
        url: '/admin777/customers/list.json'
    }).then((response) => {
    	dispatch({
		  	type: types.GET_CUSTOMERS,
		  	data: response.data,
		  	page_count: response.page_count
		})
    });
}

export const getCustomerDetail = (id) => (dispatch, getState) => {

	const data = getState().customer.data
	if(!_.isEmpty(data) && data.length > 0) {
		for (let i = 0; i < data.length; i++) {
			if(data[i].id == id) {
				dispatch({
				  	type: types.SET_CURRNET_CUSTOMER,
				  	currentCustomer: data[i]
				})
				break;
			}
		}
		return;
	}

	axios.request({
        method: 'GET',
        url: `/admin777/customers/detail/${id}.json`
    }).then((customer) => {
    	dispatch({
		  	type: types.SET_CURRNET_CUSTOMER,
		  	currentCustomer: customer
		})
    });
}

export const changeCustomerType = (type) => (dispatch, getState) => {
	if(confirm("Are you sure?")) {
		const id = getState().customer.currentCustomer.id
		axios.request({
	        method: 'POST',
	        data: { id, type },
	        url: "/admin777/customers/change-type"
	    }).then((customer) => {
	    	dispatch({
			  	type: types.SET_CURRNET_CUSTOMER,
			  	currentCustomer: customer
			})
	    }).catch((error) => {
	    	alert("You can't update this customer!!")
	    });
	}
}

export const deleteCustomer = (id) => (dispatch, getState) => {
	if(confirm("Are you sure remove this customer?")) {
		axios.request({
	        method: 'POST',
	        data: { ...getState().customer.params, id },
	        url: "/admin777/customers/delete"
	    }).then((response) => {
	    	dispatch({
			  	type: types.GET_CUSTOMERS,
			  	data: response.data,
			  	page_count: response.page_count
			})
	    }).catch((error) => {
	    	alert("You can't remove this customer!!")
	    });
	}
}

export const onSearch = (email) => (dispatch, getState) => {
	dispatch({
		type: types.CHANGE_CUSTOMER_PARAM,
		params: {
			email,
			offset: 0
		}
	})
}

export const onPagination = (offset) => (dispatch, getState) => {
	dispatch({
		type: types.CHANGE_CUSTOMER_PARAM,
		params: {
			...getState().customer.params,
			offset
		}
	})
}