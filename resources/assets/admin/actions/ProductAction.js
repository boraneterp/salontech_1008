import axios from 'axios';
import { browserHistory } from 'react-router';
import { SubmissionError } from 'redux-form';
import objectToFormData from '../../utils/objectToFormData';
import * as types from '../constants/ActionTypes';

export const getProducts = (p) => (dispatch, getState) => {
	const params = _.isEmpty(p)? getState().product.params: p
	axios.request({
        method: 'GET',
        params,
        url: '/admin777/products/list.json'
    }).then((response) => {
    	dispatch({
		  	type: types.GET_PRODUCTS,
		  	data: response.data,
		  	page_count: response.page_count
		})
    });
}

export const searchProducts = (keyword) => {
	return axios.request({
        method: 'GET',
        params: {
        	keyword
        },
        url: '/admin777/products/search.json'
    });
}

export const getProductDetail = (id) => (dispatch, getState) => {

	if(!id) {
		dispatch({
		  	type: types.SET_CURRNET_PRODUCT,
		  	product: {}
		})
		return;
	}
	const data = getState().product.data
	if(!_.isEmpty(data) && data.length > 0) {
		for (let i = 0; i < data.length; i++) {
			if(data[i].id == id) {
				const currentProduct = Object.assign({}, data[i]);
				dispatch({
				  	type: types.SET_CURRNET_PRODUCT,
				  	product: currentProduct
				})
				break;
			}
		}
		return;
	}

	axios.request({
        method: 'GET',
        url: `/admin777/products/detail/${id}.json`
    }).then((product) => {
    	dispatch({
		  	type: types.SET_CURRNET_PRODUCT,
		  	product
		})
    });
}

export const setCurrentProduct = (product) => (dispatch, getState) => {
	dispatch({
	  	type: types.SET_CURRNET_PRODUCT,
	  	product
	})
}

export const updateProductList = (product) => (dispatch, getState) => {
	dispatch({
	  	type: types.UPDATE_PRODUCTS,
	  	product
	})
}

export const uploadImages = (images) => (dispatch, getState) => {
	dispatch({
	  	type: types.UPLOAD_PRODUCT_IMAGES,
	  	images
	})
}

export const uploadVideo = (video) => (dispatch, getState) => {
	dispatch({
	  	type: types.UPLOAD_PRODUCT_VIDEO,
	  	video
	})
	
}

export const removeImage = (image) => (dispatch, getState) => {
	dispatch({
	  	type: types.REMOVE_PRODUCT_IMAGE,
	  	image
	})
}

export const sortImage = (images) => (dispatch, getState) => {
	dispatch({
	  	type: types.MOVE_PRODUCT_IMAGE,
	  	images
	})
}

export const saveProduct = (product) => (dispatch, getState) => {
	const isNew = product.id? false: true
	dispatch({
	  	type: types.SET_PRODUCT_MESSAGE,
	  	message: ''
	})
	const config = { headers: { 'Content-Type': 'multipart/form-data' } };
	return axios.post('/admin777/products/save', objectToFormData(product), config).then((response) => {

    	dispatch({
		  	type: types.SET_CURRNET_PRODUCT,
		  	product: response
		})

		dispatch({
		  	type: types.SET_PRODUCT_MESSAGE,
		  	message: 'Saved Successfully.'
		})

		if(isNew) {
			browserHistory.push(`/admin777/products/detail/${response.id}`)
		} 
		
		$('html, body').animate({
	        scrollTop: $("body").offset().top - 90
	    }, 500);
		
    }).catch((error) => {
    	throw new SubmissionError({
	        _error: 'Fail'
	    })
    })
}

export const deleteProduct = (id) => (dispatch, getState) => {
	if(confirm("Are you sure remove this product?")) {
		axios.request({
	        method: 'POST',
	        data: { ...getState().product.params, id },
	        url: "/admin777/products/delete"
	    }).then((response) => {
	    	dispatch({
			  	type: types.DELETE_PRODUCT,
			  	id
			})
	    }).catch((error) => {
	    	alert("You can't remove this product!!")
	    });
	}
}

export const onSearch = (params) => (dispatch, getState) => {
	dispatch({
		type: types.CHANGE_PRODUCT_PARAM,
		params
	})
}

export const onPagination = (offset) => (dispatch, getState) => {
	dispatch({
		type: types.CHANGE_PRODUCT_PAGE,
		offset
	})
}

export const showReviewModal = (reviews) => (dispatch, getState) => {
	dispatch({
		type: types.SHOW_REVIEW_MODAL,
		reviews
	})
}

export const hideReviewModal = () => (dispatch, getState) => {
	dispatch({
		type: types.HIDE_REVIEW_MODAL
	})
}


