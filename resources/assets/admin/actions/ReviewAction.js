import axios from 'axios'
import _ from 'lodash'
import * as types from '../constants/ActionTypes'

export const getReviews = (p) => (dispatch, getState) => {
	const params = _.isEmpty(p)? getState().review.params: p
	return axios.request({
        method: 'GET',
        params,
        url: `/admin777/reviews/list.json`
    }).then((response) => {
    	dispatch({
		  	type: types.GET_REVIEWS,
		  	data: response.data,
		  	page_count: response.page_count
		})
    });
}

export const changeReviewStatus = (review) => (dispatch, getState) => {
	return axios.request({
        method: 'POST',
        data: review,
        url: `/admin777/reviews/update`
    }).then((response) => {
    	review.status = review.status == "1"? "0": "1"
    	dispatch({
		  	type: types.UPDATE_REVIEW,
		  	review
		})
    });
}

export const deleteReview = (id) => (dispatch, getState) => {
	if(confirm("Are you sure remove this review?")) {
		axios.request({
	        method: 'POST',
	        data: {id},
	        url: "/admin777/reviews/delete"
	    }).then((response) => {
	    	dispatch({
			  	type: types.DELETE_REVIEW,
			  	id
			})
	    }).catch((error) => {
	    	alert("You can't remove this review!!")
	    });
	}
}