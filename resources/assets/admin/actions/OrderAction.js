import axios from 'axios'
import * as types from '../constants/ActionTypes'

export const getOrders = (force) => (dispatch, getState) => {
	axios.request({
        method: 'GET',
        params: getState().order.params,
        url: '/admin777/orders/list.json'
    }).then((response) => {
    	dispatch({
		  	type: types.GET_ORDERS,
		  	data: response.data,
		  	page_count: response.page_count
		})
    });
}

export const getOrderDetail = (id) => (dispatch, getState) => {

	const data = getState().order.data
	if(!_.isEmpty(data) && data.length > 0) {
		for (let i = 0; i < data.length; i++) {
			if(data[i].id == id) {
				dispatch({
				  	type: types.SET_CURRNET_ORDER,
				  	currentOrder: data[i]
				})
				break;
			}
		}
		return;
	}

	axios.request({
        method: 'GET',
        url: `/admin777/orders/detail/${id}.json`
    }).then((order) => {
    	dispatch({
		  	type: types.SET_CURRNET_ORDER,
		  	currentOrder: order
		})
    });
}

export const changeParams = (params) => (dispatch, getState) => {
	dispatch({
		type: types.CHANGE_ORDER_PARAM,
		params
	})
}

export const onPagination = (offset) => (dispatch, getState) => {
	dispatch({
		type: types.CHANGE_ORDER_PAGE,
		offset
	})
}

export const changeOrderStatus = (status) => (dispatch, getState) => {
	let confirmed = true
	if(status == "R") {
		confirmed = confirm("Are you sure to refund this order?")
	}
	if(!confirmed)
		return;

	dispatch({
	  	type: types.START_PROCESSING
	})

	const id = getState().order.currentOrder.id

	axios.request({
        method: 'POST',
        data: { status },
        url: `/admin777/orders/${id}/status`
    }).then((response) => {
		if(getState().order.currentOrder.status == "P") {
			dispatch({
			  	type: types.UPDATE_ORDER_BADGE
			})
		}

    	dispatch({
		  	type: types.SET_CURRNET_ORDER,
		  	currentOrder: response
		})

		dispatch({
		  	type: types.DONE_PROCESSING
		})
    }).catch((error) => {
    	dispatch({
		  	type: types.DONE_PROCESSING
		})
    	alert("You can't update this order!!")
    });
}

