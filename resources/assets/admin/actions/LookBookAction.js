import axios from 'axios';
import _ from 'lodash';
import { browserHistory } from 'react-router';
import * as types from '../constants/ActionTypes';
import objectToFormData from '../../utils/objectToFormData';

export const getLookBooks = (force) => (dispatch, getState) => {

	axios.request({
        method: 'GET',
        params: getState().lookbook.params,
        url: '/admin777/lookbooks/list.json'
    }).then((response) => {
    	dispatch({
		  	type: types.GET_LOOKBOOKS,
		  	data: response.data,
		  	page_count: response.page_count
		})
    });
}

export const getLookBookDetail = (params) => (dispatch, getState) => {
	if(!_.isEmpty(params)) {
		axios.get(`/admin777/lookbooks/detail/${params.id}.json`).then((lookbook) => {
	    	dispatch({
			  	type: types.SET_CURRNET_LOOKBOOK,
			  	lookbook
			})
	    })
	} else {
		dispatch({
		  	type: types.SET_CURRNET_LOOKBOOK,
		  	lookbook: {
		  		steps: [
			        {
			            description: ''
			        }
			    ]
			}
		})
	}
}

export const setCurrentLookBook = (lookBook) => (dispatch, getState) => {
	dispatch({
	  	type: types.SET_CURRNET_LOOKBOOK,
	  	lookBook
	})
}

export const updateLookBookList = (lookBook) => (dispatch, getState) => {
	dispatch({
	  	type: types.UPDATE_LOOKBOOKS,
	  	lookBook
	})
}

export const saveLookBook = (lookBook) => (dispatch, getState) => {
	const isNew = lookBook.id? false: true
	dispatch({
	  	type: types.SET_LOOKBOOK_MESSAGE,
	  	message: ''
	})
	const config = { headers: { 'Content-Type': 'multipart/form-data' } };
	return axios.post('/admin777/lookbooks/save', objectToFormData(lookBook), config).then((response) => {

    	dispatch({
		  	type: types.SET_CURRNET_LOOKBOOK,
		  	lookbook: response
		})

		dispatch({
		  	type: types.SET_LOOKBOOK_MESSAGE,
		  	message: 'Saved Successfully.'
		})

		if(isNew) {
			browserHistory.push(`/admin777/lookbooks/detail/${response.id}`)
		} 
		
		$('html, body').animate({
	        scrollTop: $("body").offset().top - 90
	    }, 500);
		
    }).catch((error) => {
    	throw new SubmissionError({
	        _error: 'Fail'
	    })
    })
}


export const deleteLookBook = (id) => (dispatch, getState) => {
	if(confirm("Are you sure remove this lookbook?")) {
		axios.request({
	        method: 'POST',
	        data: { ...getState().lookbook.params, id },
	        url: "/admin777/lookbooks/delete"
	    }).then((response) => {
	    	dispatch({
			  	type: types.DELETE_LOOKBOOK,
			  	id
			})
	    }).catch((error) => {
	    	alert("You can't remove this lookbook!!")
	    });
	}
}

export const onSearch = (params) => (dispatch, getState) => {
	dispatch({
		type: types.CHANGE_LOOKBOOK_PARAM,
		params
	})
}

export const onPagination = (offset) => (dispatch, getState) => {
	dispatch({
		type: types.CHANGE_LOOKBOOK_PAGE,
		offset
	})
}

export const showReviewModal = (reviews) => (dispatch, getState) => {
	dispatch({
		type: types.SHOW_REVIEW_MODAL,
		reviews
	})
}

export const hideReviewModal = () => (dispatch, getState) => {
	dispatch({
		type: types.HIDE_REVIEW_MODAL
	})
}


