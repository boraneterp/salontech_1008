import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import ReactPaginate from 'react-paginate'
import _ from 'lodash'
import { getWarranties, onSearch, changeWarrantyStatus } from '../../actions/WarrantyAction'
import Loading from '../../components/Loading'
import WarrantyItem from '../../components/warranties/WarrantyItem'

class WarrantyPage extends Component {

	componentWillMount() {
		const { getWarranties } = this.props
	    getWarranties(false)
	}

	render() {
		const { warranty, changeWarrantyStatus } = this.props
		return (
			<div className="container main animated fadeIn">
				<h2>Warranty List</h2>
				<div className="content">
					<section className="top-border">
						<div className=" table-responsive">
							{!warranty.initialized ?
								<Loading />:
								<table className="table">
									<thead>
										<tr>
											<th>No</th>
											<th>Name</th>
											<th>Email Address</th>
											<th>Salon Name</th>
											<th>Phone Number</th>
											<th>Address</th>
											<th>Product</th>
											<th>Date Code</th>
											<th>Reason</th>
											<th>Date</th>
											<th>Proof of Purchase</th>
											<th>Status</th>
										</tr>
									</thead>
									{!_.isEmpty(warranty.data) ?
										<tbody>
											{warranty.data.map((warranty, index) =>
												<WarrantyItem
													key={warranty.id}
													index={index + 1}
													warranty={warranty}
													changeWarrantyStatus={changeWarrantyStatus}
												/>
									    	)}
										</tbody>:
										<tbody>
											<tr >
												<td colSpan="12">No Warranty Found!</td>
											</tr>
										</tbody>
									}
								</table>
							}
						</div>
					</section>
				</div>
			</div>
		)
	}
}

const mapStateToProps = (state, ownProps) => ({
  	warranty: state.warranty
})

export default connect(mapStateToProps, 
	{ getWarranties, onSearch, changeWarrantyStatus }
)(WarrantyPage)