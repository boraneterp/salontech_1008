import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Header from '../components/Header'
import Footer from '../components/Footer'

const Root = ({ children, page, badge }) => (
  	<div>
    	<Header page={page} badge={badge} />
		{children}
		<Footer />
  	</div>
)

Root.propTypes = {
	 children: PropTypes.node,
  	badge: PropTypes.object,
  	page: PropTypes.string.isRequired
}

const mapStateToProps = (state, ownProps) => ({
  	badge: state.badge,
  	page: ownProps.location.pathname.toLowerCase().replace("/admin777", "")
})

export default connect(
	mapStateToProps, 
	null
)(Root)