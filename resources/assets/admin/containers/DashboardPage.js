import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import _ from 'lodash'
import { getDashboardData } from '../actions/CommonAction'
import Loading from '../components/Loading'
import ProductItem from '../components/dashboard/ProductItem'

class DashboardPage extends Component {

	componentWillMount() {
		const { getDashboardData } = this.props
	    getDashboardData()
	}

	render() {
		const { dashboard } = this.props
		return (
			<div className="container main dashboard">
				<h2>Dashboard</h2>
				<div className="content">
					{!_.has(dashboard, 'lifetime_sales') ?
						<Loading />:
						<section className="top-border">
							<div className="row summary">
								<div className="col-sm-4 ">
									<h4>Lifetime Sales</h4>
									<div className="price">${parseFloat(dashboard.lifetime_sales).toFixed(2)}</div>
								</div>
								<div className="col-sm-4 ">
									<h4>This Month Sales</h4>
									<div className="price">${parseFloat(dashboard.month_sales).toFixed(2)}</div>
								</div>
								<div className="col-sm-4 ">
									<h4>Average Order</h4>
									<div className="price">${parseFloat(dashboard.average_order).toFixed(2)}</div>
								</div>
							</div>
							<hr />
							<div className="row product-report">
								<div className="col-sm-6">
									<h4>Best Selling Products</h4>
									<div className="table-responsive">
										<table className="table">
					                        <thead>
					                          	<tr>
					                          		<th>Product Image</th>
					                            	<th>Category</th>
					                            	<th>Product Name</th>
					                            	<th>Sold</th>
					                            	<th>Detail</th>
					                          	</tr>
					                        </thead>
					                        <tbody>
					                        	{dashboard.best_selling_products.map(product =>
					                        		<ProductItem
					                        			key={product.id}
					                        			product={product}
					                        			value={product.sold}
					                        		/>
					                        	)}
					                        </tbody>
					                    </table>
				                    </div>
								</div>
								<div className="col-sm-6">
									<h4>Most Viewed Products</h4>
									<div className="table-responsive">
										<table className="table">
					                        <thead>
					                          	<tr>
					                          		<th>Product Image</th>
					                            	<th>Category</th>
					                            	<th>Product Name</th>
					                            	<th>View</th>
					                            	<th>Detail</th>
					                          	</tr>
					                        </thead>
					                        <tbody>
					                        	{dashboard.most_viewed_products.map(product =>
					                        		<ProductItem
					                        			key={product.id}
					                        			product={product}
					                        			value={product.view}
					                        		/>
					                        	)}
					                        </tbody>
					                     </table>
				                     </div>
								</div>
							</div>
						</section>
					}
				</div>
			</div>
		)
	}
}
const mapStateToProps = (state, ownProps) => ({
  	dashboard: state.dashboard
})

export default connect(mapStateToProps, 
	{ getDashboardData }
)(DashboardPage)
