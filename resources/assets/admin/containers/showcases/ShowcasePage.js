import React, { Component } from 'react'
import PropTypes from 'prop-types'
import ShowcaseForm from '../../components/showcases/ShowcaseForm'

const ShowcasePage = () => (
	<div className="container main animated fadeIn">
		<h2>Showcase Management</h2>
		<div className="content">
			<section className="top-border">
				<ShowcaseForm />
			</section>
		</div>
	</div>
)

export default ShowcasePage