import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import ReactPaginate from 'react-paginate'
import _ from 'lodash'
import DatePicker from "react-bootstrap-date-picker"
import * as CommonActions from '../../actions/CommonAction'
import * as OrderActions from '../../actions/OrderAction'
import Loading from '../../components/Loading'
import OrderItem from '../../components/orders/OrderItem'

class OrderListPage extends Component {

	constructor() {
		super();
		this.changeStartDate = this.changeStartDate.bind(this);
		this.changeEndDate = this.changeEndDate.bind(this);
		this.changePrams = this.changePrams.bind(this);
		this.handleChange = this.handleChange.bind(this);
		this.handlePageClick = this.handlePageClick.bind(this);
	}

	componentWillMount() {
		const { actions } = this.props
	    actions.order.getOrders(false)
	}

	changeStartDate(value) {
		this.changePrams("start_date", value, this.props)
  	}

	changeEndDate(value) {
		this.changePrams("end_date", value, this.props)
  	}

  	handleChange(e) {
  		const target = e.target
  		this.changePrams(target.name, target.value, this.props)
  	}

  	changePrams(name, value, props) {
  		const { actions, order } = props
  		let params = Object.assign({}, order.params)
  		params[name] = value
  		actions.order.changeParams(params)
  	}

  	handlePageClick(data) {
    	const { actions, order } = this.props
    	let selected = data.selected;
    	let offset = Math.ceil(selected * 30);
    	actions.order.onPagination(offset)
    	actions.order.getOrders(true)
  	}

	render() {
		const { isFetching, order, actions } = this.props
		const animationClass = "animated " + actions.common.getAnimation()
		return (
			<div className={animationClass}>
				{isFetching &&
					<Loading />
				}
				<div className="container main">
					<h2>Order Management</h2>
					<div className="content">
						<section className="top-border">
							<div id="tools">
								<div className="row">
									<div className="col-sm-3">
										<DatePicker value={order.params.start_date} placeholder="Start Date" className="styled-text-filters" onChange={this.changeStartDate}  />
									</div>
									<div className="col-sm-3">
										<DatePicker value={order.params.end_date} placeholder="End Date" className="styled-text-filters" onChange={this.changeEndDate}  />
									</div>
									<div className="col-sm-2 ">
										<input name="coupon" value={order.params.coupon} placeholder="Coupon" className="styled-text-filters" onChange={this.handleChange} />
									</div>
									<div className="col-sm-2 ">
										<div className="styled-select-filters">
											<select name="status" value={order.params.status} onChange={this.handleChange}>
												<option value="" >Status</option>
												<option value="P" >Pending</option>
												<option value="I" >Processing</option>
												<option value="C" >Completed</option>
												<option value="R" >Refunded</option>
											</select>
										</div>
									</div>
									<div className="col-sm-2">
										<button className="btn_search pull-right" onClick={actions.order.getOrders} >Search</button>
									</div>
								</div>
							</div>
							<div className=" table-responsive">
								<table className="table">
									<thead>
										<tr>
											<th>Order No</th>
											<th>First Name</th>
											<th>Last Name</th>
											<th>Total Amount</th>
											<th>Charged</th>
											<th>Status</th>
											<th>Date</th>
											<th>View</th>
										</tr>
									</thead>
									{!_.isEmpty(order.data) ?
										<tbody>
										{order.data.map((order, index) =>
												<OrderItem
													key={order.id}
													index={index + 1}
													order={order}
												/>
									    	)
										}
										</tbody>:
										<tbody>
											{!isFetching &&
											<tr >
												<td colSpan="8">No Order Found!</td>
											</tr>
											}
										</tbody>
									}
								</table>
							</div>
							<div className="text-right">
								{order.pageCount > 1 &&
									<ReactPaginate 
										previousLabel={'Prev'}
										breakLabel={<a href="">...</a>}
										breakClassName={"break-me"}
										pageCount={order.pageCount}
										marginPagesDisplayed={2}
										pageRangeDisplayed={5}
										onPageChange={this.handlePageClick}
										containerClassName={"pagination"}
										activeClassName={"active"} />
								}
					        </div>
						</section>
					</div>
				</div>
			</div>
		)
	}
}

const mapStateToProps = (state, ownProps) => ({
	isFetching: state.isFetching,
  	order: state.order
})

const mapDispatchToProps = dispatch => ({
	actions: {
    	common: bindActionCreators(CommonActions, dispatch),
    	order: bindActionCreators(OrderActions, dispatch)
   	}
})

export default connect(
	mapStateToProps, 
	mapDispatchToProps
)(OrderListPage)