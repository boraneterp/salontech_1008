import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Link } from 'react-router'
import _ from 'lodash'
import Loading from '../../components/Loading'
import * as CommonActions from '../../actions/CommonAction'
import * as OrderActions from '../../actions/OrderAction'
import OrderProductItem from '../../components/orders/OrderProductItem'
import OrderStatusHistory from '../../components/orders/OrderStatusHistory'


class OrderDetailPage extends Component {

	constructor() {
		super()
	}

	componentWillMount() {
		const { order, actions } = this.props
	    const orderId = window.location.pathname.substring(24)
	    actions.order.getOrderDetail(orderId)
	}

	getCardNumberMask(order) {
		if(_.isEmpty(order.payment) || order.payment.type === 'paypal' 
			|| order.payment.card_number.length < 4)
			return ''
		const cardNumber = order.payment.card_number
		return cardNumber.substring(cardNumber.length - 4)
	}

	getOrderAddress(order) {
		let address = {}
		if(_.isEmpty(order.addresses))
			return address
  		for(let i = 0; i < order.addresses.length; i++) {
  			if(order.addresses[i].type == "S") {
  				address.shipping = order.addresses[i]
  			} else if(order.addresses[i].type == "B") {
  				address.billing = order.addresses[i]
  			}
  		}
  		return address
	}


	render() {
		const { order, actions } = this.props
		const currentOrder = order.currentOrder
		const address = this.getOrderAddress(currentOrder)
		let status = currentOrder.status === 'P'? 'Pending': currentOrder.status === 'I'? 'Processing': currentOrder.status === 'R'? 'Refunded': 'Completed'
		let statusClass = 'order-status ' + status.toLowerCase()
		let cardNumberMask = this.getCardNumberMask(currentOrder)
		const animationClass = "animated " + actions.common.getAnimation()
		return (
			<div className={animationClass}>
				<div className="container main">
					<h2>Order Detail</h2>
					{_.isEmpty(currentOrder) ?
						<Loading />:
						<div className="content">
							{order.processing &&
								<Loading />
							}
							<div className="action-container">
								{(currentOrder.status === 'P') &&
								<span>
								<button type="button" className="btn_2" onClick={() => actions.order.changeOrderStatus('I')}>Processing</button>
								</span>
								}
								{(currentOrder.status === 'I') &&
								<span>
								<button type="button" className="btn_2" onClick={() => actions.order.changeOrderStatus('C')}>Complete</button>
								<button type="button" className="btn_4" onClick={() => actions.order.changeOrderStatus('R')}>Refund</button>
								</span>
								}
								<Link to={`/admin777/orders`} className="btn_3">Back</Link>
							</div>
							<div className="row">
						    	<div className="col-sm-8">
						    		<div className="box_style_1 expose">
							    		<h3 className="inner">Ordered Products</h3>
										<table className="table">
				                        	<thead>
						                        <tr>
													<th>No</th>
													<th>Image</th>
													<th>Product Name</th>
													<th>Item No</th>
													<th>Quantity</th>
													<th>Price</th>
													<th>Total Price</th>
						                        </tr>
				                        	</thead>
					                        <tbody>
					                        	{currentOrder.products.map((product, index) =>
				                                    <OrderProductItem 
				                                    	key = { product.id }
				                                    	index = { index + 1 }
				                                      	product = { product } />
				                                )}
					                        </tbody>
				                      	</table>
			                      	</div>
			                      	<div className="box_style_1 expose">
										<h3 className="inner">Order Totals</h3>
										<table className="table table_summary">
											<tbody>
												<tr>
													<td className="text-right">SUBTOTAL</td>
													<td className="text-right">${parseFloat(currentOrder.sub_total).toFixed(2)}</td>
												</tr>
												{!_.isEmpty(currentOrder.coupon_code) &&
													<tr className="green">
														<td className="text-right">Coupon ({currentOrder.coupon_code})</td>
														<td className="text-right">-${parseFloat(currentOrder.promotion).toFixed(2)}</td>
													</tr>
												}
												<tr>
													<td className="text-right">SHIPPING &amp; HANDLING</td>
													<td className="text-right">${parseFloat(currentOrder.shipping_price).toFixed(2)}</td>
												</tr>
												<tr>
													<td className="text-right">TAX</td>
													<td className="text-right">${parseFloat(currentOrder.tax).toFixed(2)}</td>
												</tr>
												<tr className="total">
													<td className="text-right">Total</td>
													<td className="text-right">${parseFloat(currentOrder.order_total).toFixed(2)}</td>
												</tr>
											</tbody>
										</table>
									</div>
									<div className="box_style_1 expose">
										<h3 className="inner">Order History</h3>
										<table className="table">
				                        	<thead>
						                        <tr>
													<th>No</th>
													<th>Date</th>
													<th>Status</th>
													<th>Comments</th>
						                        </tr>
				                        	</thead>
					                        <tbody>
					                        	{currentOrder.status_histories.map((history, index) =>
				                                    <OrderStatusHistory
				                                    	key = { history.id }
				                                    	index = { index + 1 }
				                                      	history = { history } />
				                                )}
					                        </tbody>
				                      	</table>
									</div>
						    	</div>
						    	<div className="col-sm-4">
						    		<div className="default-title">
										<div className="box_style_1 expose">
											<h3 className="inner">Order Info</h3>
											<div><span className="billing-label">Order Id:</span> <span>#{currentOrder.id}</span></div>
					                		<div><span className="billing-label">Order Status:</span> <span className={statusClass}>{status}</span></div>
					                		<div><span className="billing-label">Shipping:</span> <span>{currentOrder.shipping_method}</span></div>
					                		<div><span className="billing-label">Date:</span> <span>{currentOrder.created_at}</span></div>
										</div>
									</div>
									<div className="box_style_1 expose">
										<h3 className="inner">Contact Info</h3>
										<div><span className="billing-label">Email Address:</span> <span>{currentOrder.user.email}</span></div>
										<div><span className="billing-label">Phone Number:</span> <span>{address.shipping.phone_number}</span></div>
									</div>
									<div className="box_style_1 expose">
										<h3 className="inner">Shipping Info</h3>
										<div className="address-label">{address.shipping.first_name + ' ' + address.shipping.last_name}</div>
				                    	<address dangerouslySetInnerHTML={{__html: address.shipping.full_address}}></address>
									</div>
									<div className="box_style_1 expose">
										<h3 className="inner">Payment Info</h3>
										<div className="billing-detail">
					                		{currentOrder.payment.payment_type === 'paypal' ?
						                    	<div>
						                    		<div><span className="billing-label">Transaction Id:</span> <span>{currentOrder.payment.transaction_id}</span></div>
							                    	<div><span className="billing-label">Paypal ID:</span> <span>{currentOrder.payment.payer_email}</span></div>
												</div>:
												<div>
													<div><span className="billing-label">Transaction Id:</span> <span>{currentOrder.payment.transaction_id}</span></div>
													<div><span className="billing-label">Card Hoder Name:</span> <span>{currentOrder.payment.card_holder_name}</span></div>
							                    	<div><span className="billing-label">Card Number:</span> <span>************{cardNumberMask}</span></div>
							                    	<div><span className="billing-label">Exipre Date:</span> <span>{currentOrder.payment.expire_month}/{currentOrder.payment.expire_year}</span></div>
							                    	<div><span className="billing-label">Security Code:</span> <span>{currentOrder.payment.ccv}</span></div>
												</div>
											}
				                		</div>
									</div>

									{currentOrder.payment.payment_type === 'card' &&
										<div className="box_style_1 expose">
											<h3 className="inner">Billing Address</h3>
											<div className="address-label">{address.billing.first_name + ' ' + address.billing.last_name}</div>
							                <address dangerouslySetInnerHTML={{__html: address.billing.full_address}}></address>
										</div>
									}
						    	</div>
						    </div>
						</div>
					}
				</div>
			</div>
		)
	}
}

const mapStateToProps = (state, ownProps) => ({
  	order: state.order
})

const mapDispatchToProps = dispatch => ({
	actions: {
    	common: bindActionCreators(CommonActions, dispatch),
    	order: bindActionCreators(OrderActions, dispatch)
   	}
})

export default connect(
	mapStateToProps, 
	mapDispatchToProps
)(OrderDetailPage)