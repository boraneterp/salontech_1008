import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import ReactPaginate from 'react-paginate'
import _ from 'lodash'
import Loading from '../../components/Loading'
import { getReviews, changeReviewStatus, deleteReview } from '../../actions/ReviewAction'
import ReviewSearchForm from '../../components/reviews/ReviewSearchForm'
import ReviewItem from '../../components/reviews/ReviewItem'

class ReviewPage extends Component {

	componentWillMount() {
		const { getReviews } = this.props
	    getReviews(false)
	}

	render() {
		const { review, isFetching, changeReviewStatus, deleteReview } = this.props
		return (
			<div className="container main animated fadeIn">
				<h2>Reviews</h2>
				<div className="content">
					<section className="top-border">
						{isFetching &&
							<Loading />
						}
						<ReviewSearchForm />
						<div className=" table-responsive">
							<table className="table">
								<thead>
									<tr>
										<th>Image</th>
										<th>Product Name</th>
										<th>User Name</th>
										<th>Rating</th>
										<th>Content</th>
										<th>Date</th>
										<th>Status</th>
										<th>Delete</th>
									</tr>
								</thead>
								{!_.isEmpty(review.data) ?
									<tbody>
									{review.data.map((review, index) =>
										<ReviewItem
											key={review.id}
											index={index + 1}
											review={review}
											changeReviewStatus={() => changeReviewStatus(review)}
											deleteReview={() => deleteReview(review.id)}
										/>)
							    	}
							    	</tbody>:
							    	<tbody>
										{!isFetching &&
										<tr >
											<td colSpan="8">No Review Found!</td>
										</tr>
										}
									</tbody>
								}
								
							</table>
						</div>
					</section>
				</div>
			</div>
		)
	}
}

const mapStateToProps = (state, ownProps) => ({
	isFetching: state.isFetching,
  	review: state.review
})

export default connect(mapStateToProps, 
	{ getReviews, changeReviewStatus, deleteReview }
)(ReviewPage)