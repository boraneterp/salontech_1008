import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Link } from 'react-router'
import _ from 'lodash'
import { getSettings, setSettings } from '../../actions/SettingAction'
import Nav from '../../components/settings/Nav'

class GeneralSettingPage extends Component {

	constructor() {
		super();
		this.state = {
			setting: {
				instagram_id: '',
				contact_email: '',
				tax_rate: '',
				header_label: '',
				header_message: '',
				free_shipping_amount: '',
				flat_fee: ''
			},
			error: '',
			success: false
		}

		this.handleChange = this.handleChange.bind(this);
	}

	componentWillMount() {
		const { generalSetting, getSettings } = this.props
		if(!_.isEmpty(generalSetting)) {
			this.setState( {
				setting: generalSetting
			})
			return
		}
		getSettings()
	}

	componentWillReceiveProps(nextProps) {
		const { generalSetting } = nextProps
		this.setState( {
			setting: generalSetting
		})
	}

	componentDidMount() {
		const self = this
		const { setSettings } = this.props
		$("#settingForm").validate({
  			messages: {
  				instagram_id: "Instagram Id is required.",
				contact_email: "Contact Email is required."
			},
			submitHandler: function(form) {
				self.setState({
					error: '',
					success: false
				})
				$("#settingForm .btn_1").button('loading');
				$(form).ajaxSubmit({
					dataType: 'json',
					error: function(e) {
						self.setState({
							error: 'Error, Please try again!'
						})
						$("#settingForm .btn_1").button('reset');
					},
					success: function(settings) {
						$("#settingForm .btn_1").button('reset')
						self.setState({
							success: true
						});

						setSettings(settings)
					}
				});
				return false;
			}
		})
	}

	handleChange(event) {
		const target = event.target
	    const value = target.value
	    const name = target.name
	    let setting = this.state.setting
	    setting[name] = value
	    this.setState({
	    	setting
	    })
	}

	render() {
		const { error, success, setting } = this.state
		return (
			<div className="container main animated fadeIn">
				<h2>Settings</h2>
				<div id="tabs" className="tabs">
					<Nav />
					<div className="content">
						<form method="POST" id="settingForm" action="/admin777/settings/save">
							<input name="group" type="hidden" value="general" />
							<section >
								{!_.isEmpty(error) &&
									<div className="alert alert-danger fade in">Error, Please try again!</div>
								}
								{(success) &&
									<div className="alert alert-success fade in">Saved Successfully</div>
								}
								<h3>Basic Info</h3>
								<div className="row">
									<div className="col-sm-6">
										<div className="form-group">
											<label>Instagram Id</label>
											<input type="text" className="form-control required" name="instagram_id" placeholder="Instagram Id" value={setting.instagram_id} onChange={this.handleChange} />
										</div>
									</div>
									<div className="col-sm-6">
										<div className="form-group">
											<label>Contact Email</label>
											<input type="text" className="form-control required" name="contact_email" placeholder="Contact Email" value={setting.contact_email} onChange={this.handleChange} />
										</div>
									</div>
								</div>
								<hr />
								<h3>Header Message</h3>
								<div className="row">
									<div className="col-sm-6">
										<div className="form-group">
											<label>Header Label</label>
											<input type="text" className="form-control required" name="header_label" placeholder="Header Label" value={setting.header_label} onChange={this.handleChange} />
										</div>
									</div>
									<div className="col-sm-6">
										<div className="form-group">
											<label>Header Message</label>
											<input type="text" className="form-control required" name="header_message" placeholder="Header Message" value={setting.header_message} onChange={this.handleChange} />
										</div>
									</div>
								</div>
								<h3>Shipping Settings</h3>
								<div className="row">
									<div className="col-sm-6">
										<div className="form-group">
											<label>Free Shipping Amount</label>
											<input type="text" className="form-control required" name="free_shipping_amount" placeholder="Free Shipping Amount" value={setting.free_shipping_amount} onChange={this.handleChange} />
										</div>
									</div>
									<div className="col-sm-6">
										<div className="form-group">
											<label>Flat Fee</label>
											<input type="text" className="form-control required" name="flat_fee" placeholder="Flat Fee" value={setting.flat_fee} onChange={this.handleChange} />
										</div>
									</div>
								</div>
								<div className="row">
									<div className="col-sm-12">
										<div className="form-group text-right">
											<button type="submit" className="btn_1 green" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Save" >Save</button>
										</div>
									</div>
								</div>
							</section>
						</form>
						<div id="preview">
							<div className="dz-preview dz-file-preview">
								<div className="dz-details">
									<img data-dz-thumbnail />
									<a className="remove" data-url="">x</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		)
	}
}

const mapStateToProps = (state, ownProps) => ({
  	generalSetting: state.setting.general
})

export default connect(mapStateToProps, 
	{ getSettings, setSettings }
)(GeneralSettingPage)