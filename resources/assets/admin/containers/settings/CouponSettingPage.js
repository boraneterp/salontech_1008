import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import ReactPaginate from 'react-paginate';
import { Modal } from 'react-bootstrap';
import _ from 'lodash';
import InputMask from 'react-input-mask';
import { getCoupons, onCouponPagination, deleteCoupon, setCurrentCoupon, hideCouponModal } from '../../actions/SettingAction';
import Nav from '../../components/settings/Nav';
import CouponForm from '../../components/settings/CouponForm';
import CouponItem from '../../components/settings/CouponItem';

class CouponSettingPage extends Component {

	componentWillMount() {
		const { getCoupons } = this.props
	    getCoupons()
	}

  	handlePageClick(data) {
    	const { onCouponPagination, getCoupons } = this.props
    	let selected = data.selected;
    	let offset = Math.ceil(selected * 30);
    	onCouponPagination(offset)
    	getCoupons()
  	}

	render() {
		const { isFetching, coupon, deleteCoupon, setCurrentCoupon, hideCouponModal } = this.props
		return (
			<div className="container main animated fadeIn">
				<h2>Settings</h2>
				<div id="tabs" className="tabs">
					<Nav />
					{isFetching ?
						<div className="page-loader">
							<i className="fa fa-circle-o-notch fa-spin fa-3x fa-fw" ></i>
						</div>:
						<div className="content ">
							<section>
								<div className="action-container">
									<button type="button" onClick={() => setCurrentCoupon({"type": "V"})} className="btn_1">Add Coupon</button>
								</div>
								<div className=" table-responsive">
									<table className="table">
										<thead>
											<tr>
												<th>No</th>
												<th>Coupon Code</th>
												<th>Type</th>
												<th>Value</th>
												<th>Expire Date</th>
												<th>Count</th>
												<th>Used Count</th>
												<th>Products</th>
												<th>Edit</th>
												<th>Delete</th>
											</tr>
										</thead>
										{!_.isEmpty(coupon.data) ?
											<tbody>
											{coupon.data.map((coupon, index) =>
												<CouponItem
													key={coupon.id}
													index={index + 1}
													coupon={coupon}
													onEdit={() => setCurrentCoupon(coupon)}
													onDelete={() => deleteCoupon(coupon.id)}
												/>
									    	)}
											</tbody>:
											<tbody>
												<tr >
													<td colSpan="10">No Coupon Found!</td>
												</tr>
											</tbody>
										}
									</table>
								</div>
								<div className="text-right">
									{coupon.pageCount > 1 &&
										<ReactPaginate 
											previousLabel={'Prev'}
											breakLabel={<a href="">...</a>}
											breakClassName={"break-me"}
											pageCount={coupon.pageCount}
											marginPagesDisplayed={2}
											pageRangeDisplayed={5}
											onPageChange={this.handlePageClick}
											containerClassName={"pagination"}
											activeClassName={"active"} />
									}
						        </div>
							</section>
						</div>
					}
				</div>
				<Modal
		          	show={coupon.modal.show}
		          	onHide={hideCouponModal}
		          	dialogClassName="store-modal animated flipInX"
		        >
		          	<Modal.Header closeButton>
		            	<Modal.Title >{coupon.modal.title}</Modal.Title>
		          	</Modal.Header>
		         	<Modal.Body>
		         		<CouponForm  />
		          	</Modal.Body>
		        </Modal>
			</div>
		  	
		)
	}
}

const mapStateToProps = (state, ownProps) => ({
	isFetching: state.isFetching,
  	coupon: state.setting.coupon
})

export default connect(mapStateToProps, 
	{ getCoupons, onCouponPagination, deleteCoupon, setCurrentCoupon, hideCouponModal }
)(CouponSettingPage)
