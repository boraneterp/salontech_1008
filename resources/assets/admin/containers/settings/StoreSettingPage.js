import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Link } from 'react-router'
import ReactPaginate from 'react-paginate'
import { Modal } from 'react-bootstrap'
import _ from 'lodash'
import InputMask from 'react-input-mask'
import { getStates } from '../../actions/CommonAction'
import { getStores, setStores, setCurrentStore, deleteStore, onSearch } from '../../actions/SettingAction'
import Nav from '../../components/settings/Nav'
import StoreItem from '../../components/settings/StoreItem'

class StoreSettingPage extends Component {

	constructor() {
		super()
		this.state = {
			error: '',
			modalTitle: 'Add Store',
			modalShow: false
		}

		this.storePopup = this.storePopup.bind(this);
		this.hideModal = this.hideModal.bind(this);
		this.validateForm = this.validateForm.bind(this);
	}

	componentWillMount() {
		const { getStates, getStores } = this.props
		getStates()
	    getStores(false)
	}

  	handleFilter(e) {
  		if(e.key === 'Enter') {
  			const { onSearch, getStores } = this.props
	  		let name = e.target.value;
	    	onSearch(name)
    		getStores(true)
  		}
  	}

  	storePopup(store) {
  		const { setCurrentStore } = this.props
  		setCurrentStore(store)
  		const modalTitle = !_.isEmpty(store) ? 'Edit Store': 'Add Store'
  		this.setState({
  			modalTitle, 
    		modalShow: true
    	})
  	}

  	validateForm() {
  		const { setStores, type } = this.props
  		const self = this;
		$("#storeForm .btn_1").button('reset');
  		$("#storeForm").validate({
  			messages: {
				name: "Store Name is required.",
				address_1: "Address is required.",
				city: "City is required.",
				state: "State is required.",
				zip: "Zip Code is required.",
			},
			submitHandler: function(form) {
				self.setState({
					error: ""
		        });
				$("#storeForm .btn_1").button('loading');
				const address = form.address_1.value
					+ (form.address_2.value != ""? (" " + form.address_2.value): "") 
					+ ", " + form.city.value + ", " + form.state.value + " " + form.zip.value 
				$.getJSON( "https://maps.googleapis.com/maps/api/geocode/json?address=" + address + "&key=" 
						+ $("input[name='google_api_key']").val()).always(response => {
					if(response.results.length === 0) {
						self.setState({
							error: "Please enter a valid address."
				        });
				        $("#storeForm .btn_1").button('reset')
						return
					}

					$("#storeForm input[name='lat']").val(response.results[0].geometry.location.lat)
					$("#storeForm input[name='lng']").val(response.results[0].geometry.location.lng)

					$(form).ajaxSubmit({
						dataType: 'json',
						error: function(e) {
							self.setState({
								error: "Can't save store, please try again later!!"
					        });
							$("#storeForm .btn_1").button('reset');
						},
						success: function(stores) {
							$("#storeForm .btn_1").button('reset')
							setStores(stores)
							self.setState({modalShow: false});
						}
					});
				    
				});
				return false;
			}
		});
  	}

  	hideModal() {
    	this.setState({modalShow: false});
  	}

	render() {
		const { states, store, deleteStore } = this.props
		const { error } = this.state
		return (
			<div className="container main animated fadeIn">
				<h2>Settings</h2>
				<div id="tabs" className="tabs">
					<Nav />
					{_.isEmpty(store.data)?
						<div className="page-loader">
							<i className="fa fa-circle-o-notch fa-spin fa-3x fa-fw" ></i>
						</div>:
						<div className="content ">
							<section>
								<div className="action-container">
									<button type="button" onClick={() => this.storePopup({})} className="btn_1">Add Store</button>
								</div>
								<div className=" table-responsive">
									<table className="table">
										<thead>
											<tr>
												<th>No</th>
												<th>Store Name</th>
												<th>Distributor</th>
												<th>Address</th>
												<th>Phone Number</th>
												<th>Edit</th>
												<th>Delete</th>
											</tr>
										</thead>
										<tbody>
											{!_.isEmpty(store.data) &&
												store.data.map((store, index) =>
													<StoreItem
														key={store.id}
														index={index + 1}
														store={store}
														onEdit={() => this.storePopup(store)}
														onDelete={() => deleteStore(store.id)}
													/>
										    	)
											}
										</tbody>
									</table>
								</div>
							</section>
						</div>
					}
				</div>
				<Modal
		          	show={this.state.modalShow}
		          	onHide={this.hideModal}
		          	onEntered={this.validateForm}
		          	dialogClassName="store-modal animated flipInX"
		        >
		        	<form method="POST" id="storeForm" action="/admin777/settings/stores/save">
			        	<input type="hidden" name="id" value={store.currentStore.id} />
			        	<input type="hidden" name="lat" defaultValue={store.currentStore.lat} />
			        	<input type="hidden" name="lng" defaultValue={store.currentStore.lng} />
			          	<Modal.Header closeButton>
			            	<Modal.Title >{this.state.modalTitle}</Modal.Title>
			          	</Modal.Header>
			         	<Modal.Body>
			         		{!_.isEmpty(error) &&
								<div className="alert alert-danger fade in " dangerouslySetInnerHTML={{__html: error}}></div>
							}
							<div className="row">
			                    <div className="col-sm-5">
			                      	<div className="form-group">
										<label>Store Name</label>
										<input type="text" name="name" className="form-control required" placeholder="Store Name" defaultValue={store.currentStore.name} />
									</div>
			                    </div>
			                    <div className="col-sm-3">
			                      	<div className="form-group">
										<label>Distributor</label>
										<div>
											<input type="checkbox" name="distributor" value="Y" defaultChecked={store.currentStore.distributor === 'Y'} />
										</div>
									</div>
			                    </div>
			                    <div className="col-sm-4">
			                      	<div className="form-group">
										<label>Phone Number</label>
										<InputMask type="text" name="phone_number" mask="(999) 999-9999" maskChar=" " className="form-control " placeholder="Phone Number" defaultValue={store.currentStore.phone_number} />
									</div>
			                    </div>
			                </div>
							<div className="row">
			                    <div className="col-sm-8">
			                      	<div className="form-group">
			                        	<label className="control-label required">Address 1</label>
			                        	<input className="form-control required" placeholder="Address" name="address_1" type="text" defaultValue={store.currentStore.address_1} />
			                      	</div>
			                    </div>
			                    <div className="col-sm-4">
			                      	<div className="form-group">
			                        	<label>Address 2</label>
			                            <input className="form-control" placeholder="Address 2" name="address_2" type="text" defaultValue={store.currentStore.address_2} />
			                      	</div>
			                    </div>
			                </div>
			                <div className="row">
			                    <div className="col-md-4">
			                      	<div className="form-group">
			                        	<label className="control-label required">City</label>
			                        	<input className="form-control required" placeholder="City" name="city" type="text" defaultValue={store.currentStore.city} />
			                      	</div>
			                    </div>
			                    <div className="col-md-4">
			                      	<div className="form-group">
			                        	<label className="control-label required">State</label>
			                        	<select className="required form-control" name="state" defaultValue={store.currentStore.state} onChange={this.handleChange}>
				                            <option value="">Select State</option>
				                            {states.map((state) =>
												<option key={state.code} value={state.code}>{state.name}</option>
									    	)}
				                        </select>
			                      	</div>
			                    </div>
			                    <div className="col-md-4">
			                      	<div className="form-group">
			                        	<label className="control-label required">Zip code</label>
			                        	<input className="form-control required" placeholder="Zip Code" name="zip" type="text" defaultValue={store.currentStore.zip} />
			                      	</div>
			                    </div>
			                </div>
			          	</Modal.Body>
			          	<Modal.Footer>
			          		<button type="submit" className="btn_1 green" data-loading-text="<i class='fa fa-spinner fa-spin'></i> Save" >Save</button>
			          	</Modal.Footer>
		          	</form>
		        </Modal>
			</div>
		  	
		)
	}
}

const mapStateToProps = (state, ownProps) => ({
	states: state.states,
  	store: state.setting.store
})

export default connect(mapStateToProps, 
	{ getStates, getStores, setStores, setCurrentStore, deleteStore, onSearch }
)(StoreSettingPage)
