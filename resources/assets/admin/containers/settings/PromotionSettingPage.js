import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Nav from '../../components/settings/Nav'
import PromotionSettingForm from '../../components/settings/PromotionSettingForm'

const HomeSettingPage = () => (
	<div className="container main animated fadeIn">
		<h2>Settings</h2>
		<div id="tabs" className="tabs">
			<Nav />
			<div className="content">
				<section >
					<PromotionSettingForm />
				</section>
			</div>
		</div>
	</div>
)

export default HomeSettingPage