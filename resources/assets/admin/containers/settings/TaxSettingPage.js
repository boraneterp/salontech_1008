import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import _ from 'lodash';
import { getStates, updateStateTax, saveStateTax } from '../../actions/CommonAction';
import Nav from '../../components/settings/Nav';
import StateItem from '../../components/settings/StateItem';

class TaxSettingPage extends Component {

	componentWillMount() {
		const { getStates } = this.props
	    getStates()
	}

	render() {
		const { states, updateStateTax, saveStateTax } = this.props
		return (
			<div className="container main animated fadeIn">
				<h2>Settings</h2>
				<div id="tabs" className="tabs">
					<Nav />
					{states.length === 0 ?
						<div className="page-loader">
							<i className="fa fa-circle-o-notch fa-spin fa-3x fa-fw" ></i>
						</div>:
						<div className="content ">
							<section>
								<div className=" table-responsive">
									<table className="table">
										<thead>
											<tr>
												<th>No</th>
												<th>State Code</th>
												<th>State Name</th>
												<th>Tax Rate</th>
												<th>Save</th>
											</tr>
										</thead>
										{!_.isEmpty(states) ?
											<tbody>
											{states.map((state, index) =>
												<StateItem
													key={state.code}
													index={index + 1}
													state={state}
													onUpdate={(value) => updateStateTax(state, value)}
													onSave={() => saveStateTax(state)}
												/>
									    	)}
											</tbody>:
											<tbody>
												<tr >
													<td colSpan="5">No Data Found!</td>
												</tr>
											</tbody>
										}
									</table>
								</div>
							</section>
						</div>
					}
				</div>
			</div>
		  	
		)
	}
}

const mapStateToProps = (state, ownProps) => ({
  	states: state.states
})

export default connect(mapStateToProps, 
	{ getStates, updateStateTax, saveStateTax }
)(TaxSettingPage)
