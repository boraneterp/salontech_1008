import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import ReactPaginate from 'react-paginate'
import { Link } from 'react-router'
import _ from 'lodash'
import Loading from '../../components/Loading'
import { getAnimation } from '../../actions/CommonAction'
import { getLookBooks, deleteLookBook, onSearch, onPagination } from '../../actions/LookBookAction'
import LookBookItem from '../../components/lookbooks/LookBookItem'

class LookBookListPage extends Component {

	componentWillMount() {
		const { getLookBooks } = this.props
	    getLookBooks()
	}

  	handleFilter(e) {
  		let submit = true
  		if(e.key && e.key !== 'Enter') {
  			submit = false
  		} 

  		if(submit) {
			const { lookbook, onSearch, getLookBooks } = this.props
			let params = lookbook.params
			params.offset = 0
			params[e.target.name] = e.target.value
			onSearch(params)
    		getLookBooks(true)
  		}
  	}

  	handlePageClick(data) {
    	const { onPagination, getLookBooks } = this.props
    	let selected = data.selected;
    	let offset = Math.ceil(selected * 30);
    	onPagination(offset)
    	getLookBooks(true)
  	}

	render() {
		const { isFetching, getAnimation, lookbook, deleteLookBook } = this.props
		const animationClass = "container main animated " + getAnimation()
		return (
			<div className={animationClass}>
				<h2>Look Book List</h2>
				{(lookbook.data.length === 0  && isFetching)?
					<Loading />:
					<div className="content">
						<div className="action-container">
							<Link to={`/admin777/lookbooks/add`} className="btn_1">Add Look Book</Link>
						</div>
						<section className="top-border">
							<div id="tools">
								<div className="row">
									<div className="col-sm-3 col-sm-offset-9">
										<input name='name' placeholder="Look Book Name" className="styled-text-filters" onKeyPress={this.handleFilter} />
									</div>
								</div>
							</div>
							<div className=" table-responsive">
								<table className="table">
									<thead>
										<tr>
											<th>No</th>
											<th>Image</th>
											<th>Look Book Name</th>
											<th>Edit</th>
											<th>Delete</th>
										</tr>
									</thead>
									{!_.isEmpty(lookbook.data) ?
										<tbody>
										{lookbook.data.map((lookbook, index) =>
											<LookBookItem
												key={lookbook.id}
												index={index + 1}
												lookbook={lookbook}
												onDelete={() => deleteLookBook(lookbook.id)}
											/>
								    	)}
										</tbody>:
										<tbody>
											<tr >
												<td colSpan="5">No Look Book Found!</td>
											</tr>
										</tbody>
									}
								</table>
							</div>
							<div className="text-right">
								{lookbook.pageCount > 1 &&
									<ReactPaginate 
										previousLabel={'Prev'}
										breakLabel={<a href="">...</a>}
										breakClassName={"break-me"}
										pageCount={lookbook.pageCount}
										marginPagesDisplayed={2}
										pageRangeDisplayed={5}
										onPageChange={this.handlePageClick}
										containerClassName={"pagination"}
										activeClassName={"active"} />
								}
					        </div>
						</section>
					</div>
				}
			</div>
		)
	}
}

const mapStateToProps = (state, ownProps) => ({
	isFetching: state.isFetching,
  	lookbook: state.lookbook
})

export default connect(mapStateToProps, 
	{ getAnimation, getLookBooks, deleteLookBook, onSearch, onPagination }
)(LookBookListPage)