import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Link } from 'react-router'
import LookBookForm from '../../components/lookbooks/LookBookForm'
import * as CommonActions from '../../actions/CommonAction'
import * as LookBookActions from '../../actions/LookBookAction'

class LookBookDetailPage extends Component {

	componentWillMount() {
		const { actions, params } = this.props
	    actions.lookbook.getLookBookDetail(params)
	}
	
	render() {
		const { lookbook, params, actions } = this.props
		const animationClass = "container main animated " + actions.common.getAnimation()
		const currentLookBook = lookbook.currentLookBook
		const isNew = _.isEmpty(params)
		return (
			<div className={animationClass}>
				<h2>Look Book Detail</h2>
				<div className="content ">
					<section className="top-border">
						<div className="action-container">
							<Link className="btn_3" to="/admin777/lookbooks">Back</Link>
						</div>
						{(lookbook.message) &&
						<div className="alert alert-success fade in">{lookbook.message}</div>
						}
						{(!_.isEmpty(currentLookBook.name) || isNew) ?
							<LookBookForm isNew={isNew}/>:
							<div className="content-loader">
								<i className="fa fa-circle-o-notch fa-spin fa-3x fa-fw" ></i>
							</div>
						}
	                </section>
				</div>
			</div>
		)
	}
}

const mapStateToProps = (state, ownProps) => ({
	params: ownProps.params,
  	lookbook: state.lookbook
})

const mapDispatchToProps = dispatch => ({
    actions: {
    	common: bindActionCreators(CommonActions, dispatch),
    	lookbook: bindActionCreators(LookBookActions, dispatch)
   	}
})

export default connect(
	mapStateToProps, 
	mapDispatchToProps
)(LookBookDetailPage)