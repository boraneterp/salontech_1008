import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Link } from 'react-router'
import Loading from '../../components/Loading'
import ProductForm from '../../components/products/ProductForm'
import * as CommonActions from '../../actions/CommonAction'
import * as ProductActions from '../../actions/ProductAction'

class ProductDetailPage extends Component {

	componentWillMount() {
		const { getProductDetail, params, actions } = this.props
		actions.product.getProductDetail(params.id)
	}
	
	render() {
		const { product, actions, params } = this.props
		const animationClass = "container main animated " + actions.common.getAnimation()
		return (
			<div className={animationClass}>
				<h2>Product Detail</h2>
				<div className="content">
					<section className="top-border">
						<div className="action-container">
							<Link to={`/admin777/products`} className="btn_3">Back</Link>
						</div>
						{(!params.id || product.id)?
							<ProductForm />:
							<Loading />
						}
					</section>
				</div>
			</div>
		)
	}
}

const mapStateToProps = (state, ownProps) => ({
  	params: ownProps.params,
  	product: state.product.currentProduct,
})

const mapDispatchToProps = dispatch => ({
    actions: {
    	common: bindActionCreators(CommonActions, dispatch),
    	product: bindActionCreators(ProductActions, dispatch)
   	}
})

export default connect(
	mapStateToProps, 
	mapDispatchToProps
)(ProductDetailPage)