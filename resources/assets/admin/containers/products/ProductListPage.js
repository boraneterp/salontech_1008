import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import ReactPaginate from 'react-paginate'
import { Link } from 'react-router'
import _ from 'lodash'
import ProductReviewContainer from './ProductReviewContainer'
import Loading from '../../components/Loading'
import { getAnimation } from '../../actions/CommonAction'
import { getProducts, deleteProduct, onPagination } from '../../actions/ProductAction'
import ProductSearchForm from '../../components/products/ProductSearchForm'
import ProductItem from '../../components/products/ProductItem'

class ProductListPage extends Component {

	constructor() {
		super();
		this.handlePageClick = this.handlePageClick.bind(this);
	}

	componentWillMount() {
		const { getProducts } = this.props
	    getProducts()
	}

  	handlePageClick(data) {
    	const { onPagination, getProducts, customer } = this.props
    	let selected = data.selected;
    	let offset = Math.ceil(selected * 30);
    	onPagination(offset)
    	getProducts(true)
  	}

	render() {
		const { isFetching, getAnimation, categories, product, showReviewModal, deleteProduct } = this.props
		const animationClass = "container main animated " + getAnimation()
		return (
			<div className={animationClass}>
				<h2>Product List</h2>
				<div className="content">
					<div className="action-container">
						<Link to={`/admin777/products/add`} className="btn_1">Add Product</Link>
					</div>
					<section className="top-border">
						{isFetching &&
							<Loading />
						}
						<ProductSearchForm />
						<div className=" table-responsive">
							<table className="table">
								<thead>
									<tr>
										<th>No</th>
										<th>Image</th>
										<th>Category</th>
										<th>Product Name</th>
										<th>Item No</th>
										<th>Suggested Retail Price</th>
										<th>Professional Price</th>
										<th>Edit</th>
										<th>Delete</th>
									</tr>
								</thead>
								{!_.isEmpty(product.data) ?
									<tbody>
									{product.data.map((product, index) =>
										<ProductItem
											key={product.id}
											index={index + 1}
											product={product}
											onReview={() => showReviewModal(product.reviews)}
											onDelete={() => deleteProduct(product.id)}
										/>
							    	)}
									</tbody>:
									<tbody>
										{!isFetching &&
										<tr >
											<td colSpan="9">No Product Found!</td>
										</tr>
										}
									</tbody>
								}
							</table>
						</div>
						<div className="text-right">
							{product.pageCount > 1 &&
								<ReactPaginate 
									previousLabel={'Prev'}
									breakLabel={<a href="">...</a>}
									breakClassName={"break-me"}
									pageCount={product.pageCount}
									marginPagesDisplayed={2}
									pageRangeDisplayed={5}
									onPageChange={this.handlePageClick}
									containerClassName={"pagination"}
									activeClassName={"active"} />
							}
				        </div>
					</section>
				</div>
			</div>
		)
	}
}

const mapStateToProps = (state, ownProps) => ({
	isFetching: state.isFetching,
  	product: state.product
})

export default connect(mapStateToProps, 
	{ getAnimation, getProducts, deleteProduct, onPagination }
)(ProductListPage)