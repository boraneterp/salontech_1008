import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Link } from 'react-router'
import { Modal } from 'react-bootstrap'
import { hideReviewModal } from '../../actions/ProductAction'
import ReviewItem from '../../components/products/ReviewItem'

class ProductReviewContainer extends Component {

	render() {
		const { modal, hideReviewModal } = this.props
		return (
			<Modal
	          	show={modal.show}
	          	onHide={hideReviewModal}
	          	dialogClassName="review-modal animated flipInX"
	        >
	          	<Modal.Header closeButton>
	            	<Modal.Title >Product Review</Modal.Title>
	          	</Modal.Header>
	         	<Modal.Body>
					<table className="table">
						<thead>
							<tr>
								<th>No</th>
								<th>User Name</th>
								<th>Rating</th>
								<th>Content</th>
								<th>Date</th>
								<th>Status</th>
								<th>Delete</th>
							</tr>
						</thead>
						{!_.isEmpty(modal.reviews) ?
							<tbody>
							{modal.reviews.map((review, index) =>
								<ReviewItem
									key={review.id}
									index={index + 1}
									review={review}
								/>
					    	)}
							</tbody>:
							<tbody>
								<tr >
									<td colSpan="8">No Review Found!</td>
								</tr>
							</tbody>
						}
					</table>
	          	</Modal.Body>
	        </Modal>
		)
	}
}

const mapStateToProps = (state, ownProps) => ({
  	modal: state.product.reviewModal
})

export default connect(mapStateToProps, 
	{ hideReviewModal}
)(ProductReviewContainer)
