import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import ReactPaginate from 'react-paginate'
import _ from 'lodash'
import { getSubscriptions, onSearch } from '../../actions/SubscriptionAction'
import SubscriptionItem from '../../components/subscriptions/SubscriptionItem'

class SubscriptionPage extends Component {

	componentWillMount() {
		const { getSubscriptions } = this.props
	    getSubscriptions(false)
	}

	render() {
		const { subscription } = this.props
		return (
			<div className="container main animated fadeIn">
				<h2>Subscriptions</h2>
				<div className="content">
					<section className="top-border">
						<div className=" table-responsive">
							<table className="table">
								<thead>
									<tr>
										<th>No</th>
										<th>Email Address</th>
										<th>Date</th>
									</tr>
								</thead>
								<tbody>
								{!_.isEmpty(subscription.data) &&
									subscription.data.map((subscription, index) =>
										<SubscriptionItem
											key={subscription.id}
											index={index + 1}
											subscription={subscription}
										/>
							    	)
								}
								</tbody>
							</table>
						</div>
					</section>
				</div>
			</div>
		)
	}
}

const mapStateToProps = (state, ownProps) => ({
  	subscription: state.subscription
})

export default connect(mapStateToProps, 
	{ getSubscriptions, onSearch }
)(SubscriptionPage)