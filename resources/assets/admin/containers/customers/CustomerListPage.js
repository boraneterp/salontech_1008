import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import ReactPaginate from 'react-paginate'
import _ from 'lodash'
import { getAnimation } from '../../actions/CommonAction'
import { getCustomers, deleteCustomer, onSearch, onPagination } from '../../actions/CustomerAction'
import CustomerItem from '../../components/customers/CustomerItem'

class CustomerListPage extends Component {

	constructor() {
		super();
		this.handleFilter = this.handleFilter.bind(this);
		this.handlePageClick = this.handlePageClick.bind(this);
	}

	componentWillMount() {
		const { getCustomers } = this.props
	    getCustomers(false)
	}

  	handleFilter(e) {
  		if(e.key === 'Enter') {
  			const { onSearch, getCustomers, customer } = this.props
	  		let email = e.target.value;
	    	onSearch(email)
    		getCustomers(true)
  		}
  	}

  	handlePageClick(data) {
    	const { onPagination, getCustomers, customer } = this.props
    	let selected = data.selected;
    	let offset = Math.ceil(selected * 30);
    	onPagination(offset)
    	getCustomers(true)
  	}

	render() {
		const { isFetching, getAnimation, customer, deleteCustomer } = this.props
		const animationClass = "container main animated " + getAnimation()
		return (
			<div className={animationClass}>
				<h2>Customer Management</h2>
				{customer.data.length === 0 && isFetching?
					<div className="page-loader">
						<i className="fa fa-circle-o-notch fa-spin fa-3x fa-fw" ></i>
					</div>:
					<div className="content">
						<section className="top-border">
							<div id="tools">
								<div className="row">
									<div className="col-sm-3 col-xs-6 col-sm-offset-9">
										<input name='email' placeholder="Email Address" className="styled-text-filters" onKeyPress={this.handleFilter} />
									</div>
								</div>
							</div>
							<div className=" table-responsive">
								<table className="table">
									<thead>
										<tr>
											<th>No</th>
											<th>First Name</th>
											<th>Last Name</th>
											<th>Type</th>
											<th>Email Address</th>
											<th>Signup Date</th>
											<th>Edit</th>
											<th>Delete</th>
										</tr>
									</thead>
									{!_.isEmpty(customer.data) ?
										<tbody>
										{customer.data.map((customer, index) =>
											<CustomerItem
												key={customer.id}
												index={index + 1}
												customer={customer}
												onDelete={() => deleteCustomer(customer.id)}
											/>
								    	)}
										</tbody>:
										<tbody>
											<tr >
												<td colSpan="8">No Customer Found!</td>
											</tr>
										</tbody>
									}
								</table>
							</div>
							<div className="text-right">
								{customer.pageCount > 1 &&
									<ReactPaginate 
										previousLabel={'Prev'}
										breakLabel={<a href="">...</a>}
										breakClassName={"break-me"}
										pageCount={customer.pageCount}
										marginPagesDisplayed={2}
										pageRangeDisplayed={5}
										onPageChange={this.handlePageClick}
										containerClassName={"pagination"}
										activeClassName={"active"} />
								}
					        </div>
						</section>
					</div>
				}
			</div>
		)
	}
}

const mapStateToProps = (state, ownProps) => ({
	isFetching: state.isFetching,
  	customer: state.customer
})

export default connect(mapStateToProps, 
	{ getAnimation, getCustomers, deleteCustomer, onSearch, onPagination }
)(CustomerListPage)