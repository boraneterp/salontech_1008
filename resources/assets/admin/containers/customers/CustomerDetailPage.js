import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Link } from 'react-router'
import _ from 'lodash'
import { getAnimation } from '../../actions/CommonAction'
import { getCustomerDetail, changeCustomerType } from '../../actions/CustomerAction'
import OrderItem from '../../components/customers/OrderItem'
import AddressItem from '../../components/customers/AddressItem'
import PaymentItem from '../../components/customers/PaymentItem'

class CustomerDetailPage extends Component {

	constructor() {
		super()
	}

	componentWillMount() {
		const { getCustomerDetail, params } = this.props
	    getCustomerDetail(params.id)
	}
	
	render() {
		const { getAnimation, customer, changeCustomerType } = this.props
		const currentCustomer = customer.currentCustomer
		const animationClass = "container main animated " + getAnimation()
		return (
			<div className={animationClass}>
				<h2>Customer Detail</h2>
				{_.isEmpty(currentCustomer)?
					<div className="page-loader">
						<i className="fa fa-circle-o-notch fa-spin fa-3x fa-fw" ></i>
					</div>:
					<div className="content">
						<section className="top-border">
							<div className="action-container">
								{(currentCustomer.type === 'pending') &&
								<button type="button" className="btn_2" onClick={() => changeCustomerType('business')}>Approve</button>
								}
								{(currentCustomer.type === 'business') &&
								<button type="button" className="btn_4" onClick={() => changeCustomerType('pending')}>Revoke Business Access</button>
								}
								<Link to={`/admin777/customers`} className="btn_3">Back</Link>
							</div>
							<div className="row">
								<div className="col-md-6 col-sm-6">
									<h4>Basic Info</h4>
									<ul className="profile_summary">
										<li>First name <span>{currentCustomer.first_name}</span></li>
										<li>Last name <span>{currentCustomer.last_name}</span></li>
										<li>Email Address <span>{currentCustomer.email}</span></li>
									</ul>
								</div>
								{currentCustomer.type != 'personal' &&
									<div className="col-md-6 col-sm-6">
										<h4>Profesional Info</h4>
										<ul className="profile_summary">
											<li>Company Name <span>{currentCustomer.company}</span></li>
											<li>Cosmetology Id <span>{currentCustomer.cosmetology_id}</span></li>
											<li>Phone Number <span>{currentCustomer.phone_number}</span></li>
											<li>Status 
												{(currentCustomer.type === 'business') ?
												<span className="customer-status business">Active</span>:
												<span className="customer-status pending">Pending</span>
												}
											</li>
										</ul>
									</div>
								}
							</div>
							<div className="divider"></div>
							<div className="section">
								<h4>Orders</h4>
								{!_.isEmpty(currentCustomer.orders) ?
									<table className="table">
										<thead>
											<tr>
												<th>Order No</th>
												<th>Total Amount</th>
												<th>Charged</th>
												<th>Status</th>
												<th>Date</th>
												<th>Detail</th>
											</tr>
										</thead>
										<tbody>
										{currentCustomer.orders.map((order, index) =>
											<OrderItem
												key={order.id}
												index={index + 1}
												order={order}
											/>
								    	)}
										</tbody>
									</table>:
									<div className="no-data">No Order Found!</div>
								}
							</div>
							<div className="section">
								<h4>Addresses</h4>
								{!_.isEmpty(currentCustomer.addresses) ?
									<table className="table">
										<thead>
											<tr>
												<th>No</th>
												<th>First Name</th>
												<th>Last Name</th>
												<th>Address</th>
												<th>Phone Number</th>
											</tr>
										</thead>
										<tbody>
											{currentCustomer.addresses.map((address, index) =>
												<AddressItem
													key={address.id}
													index={index + 1}
													address={address}
												/>
									    	)}
										</tbody>
									</table>:
									<div className="no-data">No Address Found!</div>
								}
							</div>
							<div className="section">
								<h4>Payments</h4>
									{!_.isEmpty(currentCustomer.payments) ?
									<table className="table">
										<thead>
											<tr>
												<th>No</th>
												<th>Card Holder Name</th>
												<th>Card Label</th>
											</tr>
										</thead>
										<tbody>
										{currentCustomer.payments.map((payment, index) =>
											<PaymentItem
												key={payment.id}
												index={index + 1}
												payment={payment}
											/>
								    	)}
										</tbody>
									</table>:
									<div className="no-data">No Payment Found!</div>
								}
							</div>
						</section>
					</div>
				}
			</div>
		)
	}
}

const mapStateToProps = (state, ownProps) => ({
	params: ownProps.params,
  	customer: state.customer
})

export default connect(mapStateToProps, 
	{ getAnimation, getCustomerDetail, changeCustomerType }
)(CustomerDetailPage)