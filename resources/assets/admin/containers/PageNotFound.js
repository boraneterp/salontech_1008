import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router'

class PageNotFound extends Component {
	render() {
		return (
			<div className="container main text-center animated fadeIn">
				<h2>Page Not Found</h2>
				<p>The page you are looking for can not be found!!!</p>
				Go to <Link to={`/admin777`} className="">Dashboard</Link>
			</div>
		)
	}
}

export default PageNotFound