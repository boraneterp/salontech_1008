import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Link } from 'react-router'
import Loading from '../../components/Loading'
import CategoryForm from '../../components/categories/CategoryForm'
import * as CommonActions from '../../actions/CommonAction'
import * as CategoryActions from '../../actions/CategoryAction'

class CategoryDetailPage extends Component {

	componentWillMount() {
		const { params, actions } = this.props
		actions.category.getCategoryDetail(params.id)
	}
	
	render() {
		const { category, actions, params } = this.props
		const animationClass = "container main animated " + actions.common.getAnimation()
		return (
			<div className={animationClass}>
				<h2>Category Detail</h2>
				<div className="content">
					<section className="top-border">
						<div className="action-container">
							<Link to={`/admin777/category`} className="btn_3">Back</Link>
						</div>
						{(!params.id || category.id)?
							<CategoryForm />:
							<Loading />
						}
					</section>
				</div>
			</div>
		)
	}
}

const mapStateToProps = (state, ownProps) => ({
  	params: ownProps.params,
  	category: state.category.currentCategory,
})

const mapDispatchToProps = dispatch => ({
    actions: {
    	common: bindActionCreators(CommonActions, dispatch),
    	category: bindActionCreators(CategoryActions, dispatch)
   	}
})

export default connect(
	mapStateToProps, 
	mapDispatchToProps
)(CategoryDetailPage)