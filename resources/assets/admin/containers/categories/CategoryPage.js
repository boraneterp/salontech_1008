import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import ReactPaginate from 'react-paginate';
import { Modal } from 'react-bootstrap';
import { Link } from 'react-router';
import _ from 'lodash';
import { getCategories, setCategories, setCurrentCategory, deleteCategory, onSearch } from '../../actions/CategoryAction';
import CategoryItem from '../../components/categories/CategoryItem';

class CategoryPage extends Component {

	constructor() {
		super()
		this.state = {
			modalTitle: 'Add Category',
			modalShow: false
		}
	}

	componentWillMount() {
		const { getCategories } = this.props
	    getCategories(false)
	}

  	handleFilter(e) {
  		if(e.key === 'Enter') {
  			const { onSearch, getCategories, category } = this.props
	  		let name = e.target.value;
	    	onSearch(name)
    		getCategories(true)
  		}
  	}

	render() {
		const { category, deleteCategory } = this.props
		return (
			<div className="container main animated fadeIn">
				<h2>Category Management</h2>
				<div className="content">
					<div className="action-container">
						<Link to={`/admin777/category/add`} className="btn_1">Add Category</Link>
					</div>
					<section className="top-border">
						<div id="tools">
							<div className="row">
								<div className="col-sm-3 col-xs-6 col-sm-offset-9">
									<input name='name' placeholder="Category Name" className="styled-text-filters" onKeyPress={this.handleFilter} />
								</div>
							</div>
						</div>
						<div className=" table-responsive">
							<table className="table">
								<thead>
									<tr>
										<th>No</th>
										<th>Category Name</th>
										<th>Edit</th>
										<th>Delete</th>
									</tr>
								</thead>
								<tbody>
								{!_.isEmpty(category.data) &&
									category.data.map((category, index) =>
										<CategoryItem
											key={category.id}
											index={index + 1}
											category={category}
											onEdit={() => this.categoryPopup(category.id, category.name)}
											onDelete={() => deleteCategory(category.id)}
										/>
							    	)
								}
								</tbody>
							</table>
						</div>
					</section>
				</div>
			</div>
		)
	}
}

const mapStateToProps = (state, ownProps) => ({
  	category: state.category
})

export default connect(mapStateToProps, 
	{ getCategories, setCategories, setCurrentCategory, deleteCategory, onSearch }
)(CategoryPage)