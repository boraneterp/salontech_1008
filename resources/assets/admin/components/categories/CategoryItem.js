import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router';

const CategoryItem = ({ index, category, onEdit, onDelete }) => (
	<tr>
		<td>{index}</td>
		<td dangerouslySetInnerHTML={{__html: category.name}} />
		<td><Link to={`/admin777/category/detail/${category.id}`} className="btn_2">Edit</Link></td>
		<td><button className="btn_4" onClick={onDelete}>Delete</button></td>
	</tr>
)

CategoryItem.propTypes = {
	index: PropTypes.number.isRequired,
  	category: PropTypes.object.isRequired,
  	onEdit: PropTypes.func.isRequired,
  	onDelete: PropTypes.func.isRequired
}

export default CategoryItem
