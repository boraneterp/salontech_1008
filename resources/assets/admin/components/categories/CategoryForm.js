import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Field, FieldArray, reduxForm } from 'redux-form';
import * as FormHelper from '../../../utils/RenderFormField';
import editorConfig from '../../constants/editorConfig';
import { saveCategory } from '../../actions/CategoryAction';

const validate = values => {
    const errors = {}
    
    if (!values.name) {
        errors.name = 'Name is required.'
    }

    if (!values.seo_url) {
        errors.seo_url = 'Url Key is required.'
    }

    if (!values.order) {
        errors.order = 'Sort Order is required.'
    }

    return errors
}

const categoryOptions = (categories) => {
	return categories.map(category => {
		category.value = category.name
		return category
	})
}

class CategoryForm extends Component {

    render() {
        const { saveCategory, isNew, handleSubmit, error, message, pristine, reset, submitting } = this.props;
        return (
        	<form onSubmit={handleSubmit(saveCategory)}>
        		{error &&
	        		<div className="alert alert-danger" role="alert">
					  	Please try again.
					</div>
		    	}
                {message &&
                    <div className="alert alert-success" role="alert">{message}</div>
                }
                <div className="row">
                    <div className="col-sm-4">
                        <Field name="name" component={FormHelper.renderField} label="Category Name" type="text" />
                    </div>
                    <div className="col-sm-4">
                        <Field name="seo_url" component={FormHelper.renderField} label="Url Key" type="text" />
                    </div>
                    <div className="col-sm-4">
                        <Field name="order" component={FormHelper.renderField} label="Sort Order" type="number" />
                    </div>
                </div>
                <Field name="banner_content" component={FormHelper.renderTinyMCE} label="Banner Content" config={ editorConfig } />
				<div className="row clearfix">
					<div className="col-sm-12">
						<div className="form-group" >
		                  	<button type="submit" disabled={submitting} className="btn_1 green pull-right" >{submitting && <i className='fa fa-spinner fa-spin'></i>} Save</button>
		                </div>
                	</div>
                </div>
            </form>
        )
    }
}

const mapStateToProps = (state, ownProps) => ({
    initialValues: state.category.currentCategory,
    message: state.category.message
})

const categoryConnect = reduxForm({
    form: 'category',
    enableReinitialize: true,
    validate
})(CategoryForm)

export default connect(mapStateToProps, { saveCategory } )(categoryConnect);
