import React from 'react'
import PropTypes from 'prop-types'

const SubscriptionItem = ({ index, subscription }) => (
	<tr>
		<td>{index}</td>
		<td>{subscription.email}</td>
		<td>{subscription.created_at}</td>
	</tr>
)

SubscriptionItem.propTypes = {
	index: PropTypes.number.isRequired,
  	subscription: PropTypes.object.isRequired
}

export default SubscriptionItem
