import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router'


const LabelWithBadge = ({ label, badge }) => (
	<span>
		{label}
		{badge > 0 &&
			<span className="badge">{badge}</span>
		}
	</span>
)

LabelWithBadge.propTypes = {
  	label: PropTypes.string.isRequired
}

export default LabelWithBadge