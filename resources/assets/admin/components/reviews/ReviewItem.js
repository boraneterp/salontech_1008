import React from 'react'
import PropTypes from 'prop-types'

const ReviewItem = ({ index, review, changeReviewStatus, deleteReview }) => (
	<tr>
		<td><img src={review.product.image} /></td>
		<td>{review.product.product_name}</td>
		<td>{review.user.first_name + ' ' + review.user.last_name}</td>
		<td>{review.rating}</td>
		<td>{review.content}</td>
		<td>{review.created_at}</td>
		<td>
			{(review.status == '0') &&
			<button type="button" className="btn_2" onClick={changeReviewStatus}>Approve</button>
			}
			{(review.status == '1') &&
			<button type="button" className="btn_4" onClick={changeReviewStatus}>Inactivate</button>
			}
		</td>
		<td><button className="btn_4" onClick={deleteReview} >Delete</button></td>
	</tr>
)

ReviewItem.propTypes = {
	index: PropTypes.number.isRequired,
  	review: PropTypes.object.isRequired
}

export default ReviewItem
