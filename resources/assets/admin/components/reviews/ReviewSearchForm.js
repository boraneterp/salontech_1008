import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import * as FormHelper from '../../../utils/RenderFormField'
import { getReviews } from '../../actions/ReviewAction'

const reviewStatus = () => {
	return [
		{id: "1", value: "Active"},
		{id: "0", value: "Pending"}
	]
}

class ReviewSearchForm extends Component {

    render() {
        const { initialValues, getReviews, handleSubmit, pristine, reset, submitting } = this.props;
        return (
        	<form onSubmit={handleSubmit(getReviews)}>
        		<div id="tools">
        			<Field name="status" component={FormHelper.renderInlineDropdown} className="styled-text-filters" label="Status" options={reviewStatus()}/>
					<button type="submit" disabled={submitting} className="btn_search" >{submitting && <i className='icon-spin3 animate-spin'></i>} Search</button>
				</div>
            </form>
        )
    }
}

const mapStateToProps = (state, ownProps) => ({
    initialValues: state.review.params
})

const reviewSearchConnect = reduxForm({
    form: 'reviewSearch'
})(ReviewSearchForm)

export default connect(mapStateToProps, { getReviews } )(reviewSearchConnect);