import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router'

const ProductItem = ({ product, value }) => (
	<tr>
		<td><img src={product.image} /></td>
		<td dangerouslySetInnerHTML={{__html: product.category_name}} />
		<td dangerouslySetInnerHTML={{__html: product.product_name}} />
		<td>{value}</td>
		<td><Link to={`/admin777/products/detail/${product.id}`} className="btn_2">Detail</Link></td>
	</tr>
)

export default ProductItem