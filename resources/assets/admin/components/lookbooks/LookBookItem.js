import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router'

const LookBookItem = ({ index, lookbook, onDelete }) => (
	<tr>
		<td>{index}</td>
		<td><img src={lookbook.image_src} /></td>
		<td>{lookbook.name}</td>
		<td><Link to={`/admin777/lookbooks/detail/${lookbook.id}`} className="btn_2">Edit</Link></td>
		<td><button className="btn_4" onClick={onDelete}>Delete</button></td>
	</tr>
)

LookBookItem.propTypes = {
	index: PropTypes.number.isRequired,
  	lookbook: PropTypes.object.isRequired,
  	onDelete: PropTypes.func.isRequired
}

export default LookBookItem
