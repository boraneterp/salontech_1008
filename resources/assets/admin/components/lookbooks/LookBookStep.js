import React from 'react';
import { Field, reduxForm } from 'redux-form';
import * as FormHelper from '../../../utils/RenderFormField'

const transformCategories = (categories) => {
	let renderCategories = []
	for(let i = 0; i < categories.length; i++) {
		renderCategories.push({id: categories[i]['id'], value: categories[i]['name']})
	}

	return renderCategories
}

const transformProducts = (products) => {
	let renderProducts = []
	for(let i = 0; i < products.length; i++) {
		renderProducts.push({id: products[i]['id'], value: products[i]['product_name']})
	}

	return renderProducts
}

const getProducts = (categories, fields, index) => {
	const selectedCategory = categories.filter(category => category.id == fields.get(index).category_id)
	return selectedCategory.length > 0? transformProducts(selectedCategory[0].products): []
}

const renderSteps = ({ fields, categories, meta: { error, submitFailed } }) =>
	<div>
		<label className="control-label required">Steps
			<a onClick={() => fields.push({})}><i className="fa fa-plus-square"></i></a>
      	</label>
	  	<ul className="multi-field">
		    {fields.map((step, index) =>
		    <li className="multi-field__li" key={index}>
		    	<a className="remove-field" onClick={() => fields.remove(index)}><i className="fa fa-window-close"></i></a>
		    	<h5 className="control-label">Step {index + 1}</h5>
		    	<div className="row ">
		    		<div className="col-sm-6">
				    	<div className="form-group image-container">
							<label className="control-label required">Image</label>
							<Field name={`${step}.image`} component={FormHelper.renderDropzoneInput} multiple={false} />
						</div>
					</div>
					<div className="col-sm-6">
						<Field name={`${step}.step_name`} component={FormHelper.renderField} label="Name" type="text" />
			    		<Field name={`${step}.description`} component={FormHelper.renderTextarea} label="Description"  />
			    		<Field name={`${step}.category_id`} component={FormHelper.renderDropdown} label="Category" addDefault={true}
							options={transformCategories(categories)}  />
						<Field name={`${step}.product_id`} component={FormHelper.renderDropdown} label="Product" addDefault={true}
							options={getProducts(categories, fields, index)}  />
			    	</div>
		    	</div>
		    </li>
		    )}
	  	</ul>
  	</div>

export default renderSteps