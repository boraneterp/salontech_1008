import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Field, FieldArray, reduxForm } from 'redux-form';
import * as FormHelper from '../../../utils/RenderFormField'
import renderSteps from './LookBookStep'
import { getCategoriesWithProducts } from '../../actions/CategoryAction'
import { saveLookBook } from '../../actions/LookBookAction'

const validate = values => {
    const errors = {}
    
    if (!values.name) {
        errors.name = 'Name is required.'
    }

    if (!values.image_src) {
        errors.image_src = 'Image is required.'
    }

    if (!values.steps || !values.steps.length) {
        errors.steps = { _error: 'Please enter at least one step.' }
    } else {
        const stepsArrayErrors = []
        values.steps.forEach((step, index) => {
            const stepErrors = {}
            if (!step || !step.description) {
                stepErrors.description = 'Description is required.'
                stepsArrayErrors[index] = stepErrors
            }

            if (!step || !step.image) {
                stepErrors.image = 'Image is required.'
                stepsArrayErrors[index] = stepErrors
            }

            return stepErrors
        })

        if(stepsArrayErrors.length) {
            errors.steps = stepsArrayErrors
        }
    }

    return errors
}

class LookBookForm extends Component {

    componentWillMount() {
        const { getCategoriesWithProducts } = this.props
        getCategoriesWithProducts()
    }

    render() {
        const { saveLookBook, categories, isNew, handleSubmit, error, pristine, reset, submitting } = this.props;
        return (
        	<form onSubmit={handleSubmit(saveLookBook)}>
        		{error &&
	        		<div className="alert alert-danger" role="alert">
					  	Code is invalid! Please try again.
					</div>
		    	}
                <div className="row">
                    <div className="col-sm-6">
                        <div className="form-group image-container">
                            <label className="control-label">Image</label>
                            <Field name="image_src" component={FormHelper.renderDropzoneInput} multiple={false} />
                        </div>
                    </div>
                    <div className="col-sm-6">
                        <Field name="name" component={FormHelper.renderField} label="Name" type="text" />
                    </div>
                </div>
				<FieldArray name="steps" categories={categories} component={renderSteps} />
				<div className="row">
					<div className="col-sm-12">
						<div className="form-group" >
		                  	<button type="submit" disabled={submitting} className="btn_1 green pull-right" >{submitting && <i className='fa fa-spinner fa-spin'></i>} Save</button>
		                </div>
                	</div>
                </div>
            </form>
        )
    }
}

const mapStateToProps = (state, ownProps) => ({
    initialValues: state.lookbook.currentLookBook,
    categories: state.category.dataWithProducts
})

const lookbookConnect = reduxForm({
    form: 'lookbook',
    enableReinitialize: true,
    validate
})(LookBookForm)

export default connect(mapStateToProps, { getCategoriesWithProducts, saveLookBook } )(lookbookConnect);
