import React from 'react';
import PropTypes from 'prop-types';

const StateItem = ({ index, state, onUpdate, onSave }) => (
	<tr>
		<td>{index}</td>
		<td>{state.code}</td>
		<td>{state.name}</td>
		<td>%<input className="form-control" value={state.tax_rate} onChange={(e) => {onUpdate(e.target.value)}} /></td>
		<td><button className="btn_2" onClick={onSave}>Save</button></td>
	</tr>
)

StateItem.propTypes = {
	index: PropTypes.number.isRequired,
  	state: PropTypes.object.isRequired,
  	onUpdate: PropTypes.func.isRequired,
  	onSave: PropTypes.func.isRequired,
}

export default StateItem;
