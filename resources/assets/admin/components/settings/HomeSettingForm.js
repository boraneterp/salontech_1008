import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Field, FieldArray, reduxForm } from 'redux-form';
import * as FormHelper from '../../../utils/RenderFormField'
import { getSettings, saveSettings } from '../../actions/SettingAction'

const validate = values => {
    const errors = {}

    if (!values.main_video) {
        errors.main_video = 'Home Video is required.'
    }

    if (values.items && values.items.length > 0) {
        const itemsArrayErrors = []
        values.items.forEach((item, index) => {
            const itemErrors = {}
            if (!item || !item.name) {
                itemErrors.name = 'Name is required.'
                itemsArrayErrors[index] = itemErrors
            }

            if (!item || !item.image) {
                itemErrors.image = 'Image is required.'
                itemsArrayErrors[index] = itemErrors
            }

            return itemErrors
        })

        if(itemsArrayErrors.length) {
            errors.items = itemsArrayErrors
        }
    } 

    return errors
}

class HomeSettingForm extends Component {

    componentWillMount() {
		const { getSettings } = this.props
		getSettings()
	}

    render() {
        const { saveSettings, message, handleSubmit, error, pristine, reset, submitting } = this.props;
        return (
        	<form onSubmit={handleSubmit(values => saveSettings({...values, group: "home"}))}>
        		{error &&
	        		<div className="alert alert-danger" role="alert">
					  	Opps! Please try again.
					</div>
		    	}
                {(message) &&
                <div className="alert alert-success fade in">{message}</div>
                }
                <div className="row">
                    <div className="col-sm-6">
                        <div className="form-group image-container showcase">
                            <label className="control-label">Home Video</label>
                            <Field name="main_video" component={FormHelper.renderDropzoneVideo}  />
                        </div>
                    </div>
                    <div className="col-sm-6">
                        <div className="form-group image-container showcase">
                            <label className="control-label">Mobile Main Image</label>
                            <Field name="mobile_main_image" component={FormHelper.renderDropzoneInput} multiple={false} />
                        </div>
                    </div>
                </div>
                <hr />
                <div className="row">
                    <div className="col-sm-6">
                        <div className="form-group image-container showcase">
                            <label className="control-label">Featured Image 1</label>
                            <Field name="featured_image_1" component={FormHelper.renderDropzoneInput} multiple={false} />
                        </div>
                    </div>
                    <div className="col-sm-6">
                    	<Field name="title_1" component={FormHelper.renderField} label="Title 1" type="text" />
                        <Field name="link_1" component={FormHelper.renderField} label="Shop Now Link 1" type="text" />
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm-6">
                        <div className="form-group image-container showcase">
                            <label className="control-label">Featured Image 2</label>
                            <Field name="featured_image_2" component={FormHelper.renderDropzoneInput} multiple={false} />
                        </div>
                    </div>
                    <div className="col-sm-6">
                    	<Field name="title_2" component={FormHelper.renderField} label="Title 2" type="text" />
                        <Field name="link_2" component={FormHelper.renderField} label="Shop Now Link 2" type="text" />
                    </div>
                </div>
                <hr />
                <div className="row">
                    <div className="col-sm-6">
                        <div className="form-group image-container showcase">
                            <label className="control-label">About Us Image</label>
                            <Field name="about_us_image" component={FormHelper.renderDropzoneInput} multiple={false} />
                        </div>
                    </div>
                    <div className="col-sm-6">
                    	<Field name="about_us_link" component={FormHelper.renderField} label="About Us Link" type="text" />
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm-6">
                        <div className="form-group image-container showcase">
                            <label className="control-label">Review Image</label>
                            <Field name="review_image" component={FormHelper.renderDropzoneInput} multiple={false} />
                        </div>
                    </div>
                    <div className="col-sm-6">
                    	<Field name="review_youtube_link" component={FormHelper.renderField} label="Review Youtube Link" type="text" />
                    </div>
                </div>
				<div className="row">
					<div className="col-sm-12">
						<div className="form-group" >
		                  	<button type="submit" disabled={submitting} className="btn_1 green pull-right" >{submitting && <i className='fa fa-spinner fa-spin'></i>} Save</button>
		                </div>
                	</div>
                </div>
            </form>
        )
    }
}

const mapStateToProps = (state, ownProps) => ({
    initialValues: state.setting.home,
    message: state.setting.message
})

const homeSettingConnect = reduxForm({
    form: 'homeSetting',
    enableReinitialize: true,
    validate
})(HomeSettingForm)

export default connect(mapStateToProps, { getSettings, saveSettings } )(homeSettingConnect);
