import React from 'react'
import PropTypes from 'prop-types'

const StoreItem = ({ index, store, onEdit, onDelete }) => (
	<tr>
		<td>{index}</td>
		<td>{store.name}</td>
		<td>{store.distributor === 'Y'? 'Yes': 'No'}</td>
		<td dangerouslySetInnerHTML={{__html: store.full_address}}></td>
		<td>{store.phone_number}</td>
		<td><button className="btn_2" onClick={onEdit}>Edit</button></td>
		<td><button className="btn_4" onClick={onDelete}>Delete</button></td>
	</tr>
)

StoreItem.propTypes = {
	index: PropTypes.number.isRequired,
  	store: PropTypes.object.isRequired,
  	onEdit: PropTypes.func.isRequired,
  	onDelete: PropTypes.func.isRequired
}

export default StoreItem
