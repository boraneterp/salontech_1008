import React, { PropTypes } from 'react'
import { Link } from 'react-router'

const Nav = () => {
	const page = location.pathname.split('/')[3]
	return (
		<nav>
			<ul>
				<li className={typeof page === 'undefined'? 'tab-current': ''}>
					<Link to={`/admin777/settings`} className="icon-settings"><span>General Settins</span></Link>
				</li>
				<li className={page === 'home'? 'tab-current': ''}>
					<Link to={`/admin777/settings/home`} className="icon-home"><span>Homepage Settings</span></Link>
				</li>
				<li className={page === 'promotion'? 'tab-current': ''}>
					<Link to={`/admin777/settings/promotion`} className="icon-gift"><span>Promotion Settings</span></Link>
				</li>
				<li className={(page === 'coupon')? 'tab-current': ''}>
					<Link to={`/admin777/settings/coupon`} className="icon-coupon"><span>Coupon Settings</span></Link>
				</li>
				<li className={(page === 'store')? 'tab-current': ''}>
					<Link to={`/admin777/settings/store`} className="icon-store"><span>Store Settings</span></Link>
				</li>
				<li className={(page === 'tax')? 'tab-current': ''}>
					<Link to={`/admin777/settings/tax`} className="icon-coupon"><span>Tax Settings</span></Link>
				</li>
			</ul>
		</nav>
	)
}

export default Nav