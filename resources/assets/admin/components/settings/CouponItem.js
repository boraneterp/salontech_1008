import React from 'react'
import PropTypes from 'prop-types'

const CouponItem = ({ index, coupon, onEdit, onDelete }) => (
	<tr>
		<td>{index}</td>
		<td>{coupon.coupon_code}</td>
		<td>{coupon.type === 'V'? 'Price': 'Percentage'}</td>
		<td>{coupon.value}</td>
		<td>{coupon.expire_date}</td>
		<td>{coupon.count}</td>
		<td>{coupon.use_count}</td>
		<td>{coupon.products.length > 0 ?
			<div>
			{coupon.products.map((product, index) => 
				<span key={index} className="coupon-tag" dangerouslySetInnerHTML={{__html: product.product_name}} />
			)}
			</div>:
			<span>All</span>
		}
		</td>
		<td><button className="btn_2" onClick={onEdit}>Edit</button></td>
		<td><button className="btn_4" onClick={onDelete}>Delete</button></td>
	</tr>
)

CouponItem.propTypes = {
	index: PropTypes.number.isRequired,
  	coupon: PropTypes.object.isRequired,
  	onEdit: PropTypes.func.isRequired,
  	onDelete: PropTypes.func.isRequired
}

export default CouponItem
