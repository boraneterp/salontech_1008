import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import * as FormHelper from '../../../utils/RenderFormField'
import { saveCoupon } from '../../actions/SettingAction'

const couponType = () => {
	return [
		{id: "V", value: "Price"},
		{id: "P", value: "Percentage"}
	]
}

const validate = values => {
    const errors = {}
    
    if (!values.code) {
        errors.code = 'Confirmation Code is invalid.'
    }

    return errors
}

class BannerForm extends Component {

    render() {
        const { initialValues, saveCoupon, handleSubmit, error, pristine, reset, submitting } = this.props;
        return (
        	<form onSubmit={handleSubmit(saveCoupon)}>
        		<Field name="coupon_code" component={FormHelper.renderField} label="Coupon Code" />
        		<Field name="type" component={FormHelper.renderDropdown} label="Type" options={couponType()} />
		    	<Field name="value" component={FormHelper.renderField} label="Value"  />
                <Field name="count" component={FormHelper.renderField} label="Count" type="number" />
		    	<Field name="expire_date" component={FormHelper.renderDatepicker} label="Expire Date"  />
                <Field name="products" component={FormHelper.renderMultiProducts} label="Products (Leave empty if appy to all)"  />
                <div className="row">
					<div className="col-sm-12">
						<div className="form-group" >
		                  	<button type="submit" disabled={submitting} className="btn_1 green pull-right" >{submitting && <i className='fa fa-spinner fa-spin'></i>} Save</button>
		                </div>
                	</div>
                </div>
            </form>
        )
    }
}

const mapStateToProps = (state, ownProps) => ({
    initialValues: state.setting.coupon.currentCoupon
})

const couponConnect = reduxForm({
    form: 'banner',
    enableReinitialize: true,
    validate
})(BannerForm)

export default connect(mapStateToProps, { saveCoupon } )(couponConnect);
