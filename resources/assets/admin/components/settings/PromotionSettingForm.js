import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Field, FieldArray, reduxForm } from 'redux-form';
import * as FormHelper from '../../../utils/RenderFormField'
import { getSettings, saveSettings } from '../../actions/SettingAction'

const validate = values => {
    const errors = {}

    if (!values.main_video) {
        errors.main_video = 'Home Video is required.'
    }

    return errors
}

class PromotionSettingForm extends Component {

    componentWillMount() {
		const { getSettings } = this.props
		getSettings()
	}

    render() {
        const { saveSettings, message, handleSubmit, error, pristine, reset, submitting } = this.props;
        return (
        	<form onSubmit={handleSubmit(values => saveSettings({...values, group: "promotion"}))}>
        		{error &&
	        		<div className="alert alert-danger" role="alert">
					  	Opps! Please try again.
					</div>
		    	}
                {(message) &&
                <div className="alert alert-success fade in">{message}</div>
                }
                <div className="row">
                    <div className="col-sm-4">
                        <Field name="promotion_start" component={FormHelper.renderDatepicker} label="Promotion Start" type="text" />
                    </div>
                    <div className="col-sm-4">
                        <Field name="promotion_end" component={FormHelper.renderDatepicker} label="Promotion End" type="text" />
                    </div>
                    <div className="col-sm-4">
                    	<Field name="value" component={FormHelper.renderField} label="Percentage" type="text" />
                    </div>
                </div>
				<div className="row">
					<div className="col-sm-12">
						<div className="form-group" >
		                  	<button type="submit" disabled={submitting} className="btn_1 green pull-right" >{submitting && <i className='fa fa-spinner fa-spin'></i>} Save</button>
		                </div>
                	</div>
                </div>
            </form>
        )
    }
}

const mapStateToProps = (state, ownProps) => ({
    initialValues: state.setting.promotion,
    message: state.setting.message
})

const promotionSettingConnect = reduxForm({
    form: 'promotionSetting',
    enableReinitialize: true,
    validate
})(PromotionSettingForm)

export default connect(mapStateToProps, { getSettings, saveSettings } )(promotionSettingConnect);
