import React from 'react'
import PropTypes from 'prop-types'

const WarrantyItem = ({ index, warranty, changeWarrantyStatus }) => {
	return (
		<tr>
			<td>{index}</td>
			<td>{warranty.first_name + ' ' + warranty.last_name}</td>
			<td>{warranty.email}</td>
			<td>{warranty.salon_name}</td>
			<td>{warranty.phone_number}</td>
			<td><div dangerouslySetInnerHTML={{__html: warranty.full_address}}></div></td>
			<td dangerouslySetInnerHTML={{__html: warranty.product.product_name}} />
			<td>{warranty.date_code}</td>
			<td>{warranty.reason}</td>
			<td>{warranty.created_at}</td>
			<td><a href={warranty.proof_of_purchase} className="btn_2" target="_blank">View</a></td>
			<td>
				{warranty.status == 'P'?
				<button type="button" className="btn_2" onClick={() => changeWarrantyStatus(warranty.id, 'C')}>Complete</button>:
				<span className="order-status completed">Completed</span>
				}
			</td>
		</tr>
	)
}

WarrantyItem.propTypes = {
	index: PropTypes.number.isRequired,
  	warranty: PropTypes.object.isRequired
}

export default WarrantyItem
