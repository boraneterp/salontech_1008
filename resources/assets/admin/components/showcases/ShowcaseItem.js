import React from 'react';
import { Field, reduxForm } from 'redux-form';
import * as FormHelper from '../../../utils/RenderFormField'

const renderItems = ({ fields, meta: { error, submitFailed } }) =>
	<div>
		<label className="control-label required">Showcases
			<a onClick={() => fields.push({})}><i className="fa fa-plus-square"></i></a>
      	</label>
	  	<ul className="multi-field">
		    {fields.map((showcase, index) =>
		    <li className="multi-field__li" key={index}>
		    	<a className="remove-field" onClick={() => fields.remove(index)}><i className="fa fa-window-close"></i></a>
		    	<div className="row ">
		    		<div className="col-sm-6">
				    	<div className="form-group image-container">
							<label className="control-label required">Image</label>
							<Field name={`${showcase}.image`} component={FormHelper.renderDropzoneInput} multiple={false} />
						</div>
					</div>
					<div className="col-sm-6">
						<Field name={`${showcase}.name`} component={FormHelper.renderField} label="Name" type="text" />
						<Field name={`${showcase}.link`} component={FormHelper.renderField} label="Link" type="text" />
			    	</div>
		    	</div>
		    </li>
		    )}
	  	</ul>
  	</div>

export default renderItems