import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Field, FieldArray, reduxForm } from 'redux-form';
import * as FormHelper from '../../../utils/RenderFormField'
import renderItems from './ShowcaseItem'
import { getShowcases, saveShowcases } from '../../actions/ShowcaseAction'

const validate = values => {
    const errors = {}

    if (!values.main_image) {
        errors.main_image = 'Image is required.'
    }

    if (values.items && values.items.length > 0) {
        const itemsArrayErrors = []
        values.items.forEach((item, index) => {
            const itemErrors = {}
            if (!item || !item.name) {
                itemErrors.name = 'Name is required.'
                itemsArrayErrors[index] = itemErrors
            }

            if (!item || !item.image) {
                itemErrors.image = 'Image is required.'
                itemsArrayErrors[index] = itemErrors
            }

            return itemErrors
        })

        if(itemsArrayErrors.length) {
            errors.items = itemsArrayErrors
        }
    } 

    return errors
}

class ShowcaseForm extends Component {

    componentWillMount() {
        const { getShowcases } = this.props
        getShowcases()
    }

    render() {
        const { saveShowcases, message, handleSubmit, error, pristine, reset, submitting } = this.props;
        return (
        	<form onSubmit={handleSubmit(saveShowcases)}>
        		{error &&
	        		<div className="alert alert-danger" role="alert">
					  	Opps! Please try again.
					</div>
		    	}
                {(message) &&
                <div className="alert alert-success fade in">{message}</div>
                }
                <div className="row">
                    <div className="col-sm-6">
                        <div className="form-group image-container showcase">
                            <label className="control-label">Image</label>
                            <Field name="main_image" component={FormHelper.renderDropzoneInput} multiple={false} />
                        </div>
                    </div>
                    <div className="col-sm-6">
                        <Field name="link" component={FormHelper.renderField} label="Link" type="text" />
                        <Field name="description" component={FormHelper.renderTextarea} label="Description"  />
                    </div>
                </div>
				<FieldArray name="items" component={renderItems} />
				<div className="row">
					<div className="col-sm-12">
						<div className="form-group" >
		                  	<button type="submit" disabled={submitting} className="btn_1 green pull-right" >{submitting && <i className='fa fa-spinner fa-spin'></i>} Save</button>
		                </div>
                	</div>
                </div>
            </form>
        )
    }
}

const mapStateToProps = (state, ownProps) => ({
    initialValues: state.showcase.data,
    message: state.showcase.message
})

const lookbookConnect = reduxForm({
    form: 'showcase',
    enableReinitialize: true,
    validate
})(ShowcaseForm)

export default connect(mapStateToProps, { getShowcases, saveShowcases } )(lookbookConnect);
