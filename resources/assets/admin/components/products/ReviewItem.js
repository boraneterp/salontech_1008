import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router'

const ReviewItem = ({ index, review, onApprove, onDelete }) => (
	<tr>
		<td>{index}</td>
		<td>{review.user.first_name + ' ' + review.user.last_name}</td>
		<td>{review.rating}</td>
		<td>{review.content}</td>
		<td>{review.created_at}</td>
		<td><button className="btn_2" onClick={onApprove}>Approve</button></td>
		<td><button className="btn_4" onClick={onDelete}>Delete</button></td>
	</tr>
)

ReviewItem.propTypes = {
	index: PropTypes.number.isRequired,
  	review: PropTypes.object.isRequired
}

export default ReviewItem
