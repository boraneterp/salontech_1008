import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router'

const ProductItem = ({ index, product, onReview, onDelete }) => (
	<tr>
		<td>{index}</td>
		<td><img src={product.image} /></td>
		<td>{product.category.name}</td>
		<td dangerouslySetInnerHTML={{__html: product.product_name}} />
		<td>{product.item_no}</td>
		<td>${parseFloat(product.msrp).toFixed(2)}</td>
		<td>${parseFloat(product.price).toFixed(2)}</td>
		<td><Link to={`/admin777/products/detail/${product.id}`} className="btn_2">Edit</Link></td>
		<td><button className="btn_4" onClick={onDelete}>Delete</button></td>
	</tr>
)

ProductItem.propTypes = {
	index: PropTypes.number.isRequired,
  	product: PropTypes.object.isRequired,
  	onReview: PropTypes.func.isRequired,
  	onDelete: PropTypes.func.isRequired
}

export default ProductItem
