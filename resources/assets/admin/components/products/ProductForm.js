import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Field, FieldArray, reduxForm } from 'redux-form';
import * as FormHelper from '../../../utils/RenderFormField'
import { getCategories } from '../../actions/CategoryAction'
import { saveProduct } from '../../actions/ProductAction'

const validate = values => {
    const errors = {}
    
    if (!values.name) {
        errors.name = 'Name is required.'
    }

    if (!values.item_no) {
        errors.item_no = 'Item No is required.'
    }

    if (!values.product_name) {
        errors.product_name = 'Product Name is required.'
    }

    if (!values.msrp) {
        errors.msrp = 'Suggested Retail Price is required.'
    }

    if (!values.price) {
        errors.price = 'Professional Price is required.'
    }

    if (!values.professional_only) {
        errors.professional_only = 'Professional Flag is required.'
    }

    if (!values.stock) {
        errors.stock = 'Stock is required.'
    }

    if (!values.order) {
        errors.order = 'Sort Order is required.'
    }

    return errors
}

const categoryOptions = (categories) => {
	return categories.map(category => {
		category.value = category.name
		return category
	})
}

class ProductForm extends Component {

    componentWillMount() {
        const { getCategories } = this.props
        getCategories(false)
    }

    render() {
        const { saveProduct, categories, isNew, handleSubmit, error, message, pristine, reset, submitting } = this.props;
        return (
        	<form onSubmit={handleSubmit(saveProduct)}>
        		{error &&
	        		<div className="alert alert-danger" role="alert">
					  	Please try again.
					</div>
		    	}
                {message &&
                    <div className="alert alert-success" role="alert">{message}</div>
                }
                <div className="row">
                    <div className="col-sm-3">
                        <Field name="category_id" component={FormHelper.renderDropdown} label="Category" options={categoryOptions(categories)} addDefault={true}/>
                    </div>
                    <div className="col-sm-3">
                        <Field name="item_no" component={FormHelper.renderField} label="Item No" type="text" />
                    </div>
                    <div className="col-sm-3">
                        <Field name="product_name" component={FormHelper.renderField} label="Product Name" type="text" />
                    </div>
                    <div className="col-sm-3">
                        <Field name="seo_url" component={FormHelper.renderField} label="Seo Url" type="text" />
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm-4">
                        <Field name="msrp" component={FormHelper.renderField} label="Suggested Retail Price" type="text" />
                    </div>
                    <div className="col-sm-4">
                        <Field name="price" component={FormHelper.renderField} label="Professional Price" type="text" />
                    </div>
                    <div className="col-sm-4">
                        <Field name="professional_only" component={FormHelper.renderDropdown} label="Professional Only" 
                            addDefault={true} options={[{id: 'Y', value: 'Yes'}, {id: 'N', value: 'No'}]} />
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm-4">
                        <Field name="sale" component={FormHelper.renderField} label="Sale (%)" type="number" />
                    </div>
                    <div className="col-sm-4">
                        <Field name="promotion_start" component={FormHelper.renderDatepicker} label="Promotion Start" type="text" />
                    </div>
                    <div className="col-sm-4">
                        <Field name="promotion_end" component={FormHelper.renderDatepicker} label="Promotion End" type="text" />
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm-4">
                        <Field name="hashtag" component={FormHelper.renderField} label="Hashtag" type="text" />
                    </div>
                    <div className="col-sm-4">
                        <Field name="stock" component={FormHelper.renderField} label="Stock" type="text" />
                    </div>
                    <div className="col-sm-4">
                        <Field name="order" component={FormHelper.renderField} label="Sort Order" type="text" />
                    </div>
                </div>
                <div className="form-group image-container">
                    <label className="control-label">Images</label>
                    <Field name="images" component={FormHelper.renderDropzoneInput} multiple={true} />
                </div>
                <div className="form-group image-container showcase">
                    <label className="control-label">Video</label>
                    <Field name="video" component={FormHelper.renderDropzoneVideo}  />
                </div>
                <Field name="description" component={FormHelper.renderTinyMCE} label="Description" />
				<div className="row clearfix">
					<div className="col-sm-12">
						<div className="form-group" >
		                  	<button type="submit" disabled={submitting} className="btn_1 green pull-right" >{submitting && <i className='fa fa-spinner fa-spin'></i>} Save</button>
		                </div>
                	</div>
                </div>
            </form>
        )
    }
}

const mapStateToProps = (state, ownProps) => ({
    initialValues: state.product.currentProduct,
    message: state.product.message,
    categories: state.category.data
})

const lookbookConnect = reduxForm({
    form: 'product',
    enableReinitialize: true,
    validate
})(ProductForm)

export default connect(mapStateToProps, { getCategories, saveProduct } )(lookbookConnect);
