import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import * as FormHelper from '../../../utils/RenderFormField'
import { getCategories } from '../../actions/CategoryAction'
import { getProducts } from '../../actions/ProductAction'

const categoryOptions = (categories) => {
	return categories.map(category => {
		category.value = category.name
		return category
	})
}

class ProductSearchForm extends Component {

	componentWillMount() {
		const { getCategories } = this.props
		getCategories(false)
	}

    render() {
        const { initialValues, categories, getProducts, handleSubmit, pristine, reset, submitting } = this.props;
        return (
        	<form onSubmit={handleSubmit(getProducts)}>
        		<div id="tools">
        			<Field name="category" component={FormHelper.renderInlineDropdown} className="styled-text-filters" label="Category" options={categoryOptions(categories)}/>
        			<Field name="name" component={FormHelper.renderInlineField} className="styled-text-filters" label="Product Name"/>
                    <Field name="item_no" component={FormHelper.renderInlineField} className="styled-text-filters" label="Item No"/>
					<button type="submit" disabled={submitting} className="btn_search" >{submitting && <i className='icon-spin3 animate-spin'></i>} Search</button>
				</div>
            </form>
        )
    }
}

const mapStateToProps = (state, ownProps) => ({
    initialValues: state.product.params,
    categories: state.category.data
})

const productSearchConnect = reduxForm({
    form: 'productSearch'
})(ProductSearchForm)

export default connect(mapStateToProps, { getCategories, getProducts } )(productSearchConnect);