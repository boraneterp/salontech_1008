import React from 'react'
import PropTypes from 'prop-types'

const AddressItem = ({ index, address }) => {
	return (
		<tr>
			<td>{index}</td>
			<td>{address.first_name}</td>
			<td>{address.last_name}</td>
			<td dangerouslySetInnerHTML={{__html: address.full_address}}></td>
			<td>{address.phone_number}</td>
		</tr>
	)
}

AddressItem.propTypes = {
	index: PropTypes.number.isRequired,
  	address: PropTypes.object.isRequired
}

export default AddressItem
