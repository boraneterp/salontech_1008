import React from 'react'
import PropTypes from 'prop-types'

const PaymentItem = ({ index, payment }) => {
	return (
		<tr>
			<td>{index}</td>
			<td>{payment.card_holder_name}</td>
			<td>{payment.card_label}</td>
		</tr>
	)
}

PaymentItem.propTypes = {
	index: PropTypes.number.isRequired,
  	payment: PropTypes.object.isRequired
}

export default PaymentItem
