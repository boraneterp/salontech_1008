import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router'

const CustomerItem = ({ index, customer, onDelete }) => {
	const label = customer.type.substring(0, 1).toUpperCase() + customer.type.substring(1)
	const className = "customer-status " + customer.type
	return (
		<tr>
			<td>{index}</td>
			<td>{customer.first_name}</td>
			<td>{customer.last_name}</td>
			<td><span className={className}>{label}</span></td>
			<td>{customer.email}</td>
			<td>{customer.created_at}</td>
			<td><Link to={`/admin777/customers/detail/${customer.id}`} className="btn_2">Detail</Link></td>
			<td><button className="btn_4" onClick={onDelete}>Delete</button></td>
		</tr>
	)
}

CustomerItem.propTypes = {
	index: PropTypes.number.isRequired,
  	customer: PropTypes.object.isRequired,
  	onDelete: PropTypes.func.isRequired
}

export default CustomerItem
