import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router'

const OrderItem = ({ index, order }) => {
	const status = order.status === 'P' ? 'Pending' : order.status === 'I' ? 'Processing' : order.status === 'R' ? 'Refunded' : 'Completed';
	return (
		<tr>
			<td>{order.id}</td>
			<td >${parseFloat(order.order_total).toFixed(2)}</td>
			<td ><strong>${parseFloat(order.charged).toFixed(2)}</strong></td>
			<td><span className={status.toLowerCase() + ' order-status'}>{status}</span></td>
			<td>{order.created_at}</td>
			<td><Link to={`/admin777/orders/detail/${order.id}`} className="btn_2">Detail</Link></td>
		</tr>
	)
}

OrderItem.propTypes = {
	index: PropTypes.number.isRequired,
  	order: PropTypes.object.isRequired
}

export default OrderItem
