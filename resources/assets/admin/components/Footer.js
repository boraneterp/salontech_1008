import React from 'react'

const Footer = ({ page }) => (
	<footer>
		<div className="container">
			<div className="row">
				<div className="col-md-12 ">
					<p className="text-center">© Salon Tech {new Date().getFullYear()}</p>
				</div>
			</div>
		</div>
	</footer>
)

export default Footer