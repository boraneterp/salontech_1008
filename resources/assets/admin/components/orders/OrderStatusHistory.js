import React from 'react'
import PropTypes from 'prop-types'

const OrderStatusHistory = ({ index, history }) => {
	const status = history.status === 'P' ? 'Pending' : history.status === 'I' ? 'Processing' : history.status === 'R' ? 'Refunded' : 'Completed';
	return (
		<tr>
			<td>{index}</td>
			<td>{history.created_at}</td>
			<td><span className={status.toLowerCase() + ' order-status'}>{status}</span></td>
			<td>{history.comments}</td>
		</tr>
	)
}

export default OrderStatusHistory