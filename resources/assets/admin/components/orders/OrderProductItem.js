import React from 'react'
import PropTypes from 'prop-types'

const OrderProductItem = ({ index, product }) => (
	<tr>
		<td>{index}</td>
		<td><img src={product.detail.image} alt={product.detail.product_name} width="100" /></td>
		<td dangerouslySetInnerHTML={{__html: product.detail.product_name}} />
		<td>{product.detail.item_no}</td>
		<td>{product.quantity}</td>
		<td>${parseFloat(product.price).toFixed(2)}</td>
		<td>${parseFloat(product.total_price).toFixed(2)}</td>
	</tr>
)

export default OrderProductItem