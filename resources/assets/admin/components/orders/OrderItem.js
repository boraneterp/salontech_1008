import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router'

const OrderItem = ({ index, order }) => {
	let first_name = ''
	let last_name = ''
	for(let i = 0; i < order.addresses.length; i++) {
		if(order.addresses[i].type == "S") {
			first_name = order.addresses[i].first_name
			last_name = order.addresses[i].last_name
			break
		} 
	}
	const status = order.status === 'P' ? 'Pending' : order.status === 'I' ? 'Processing' : order.status === 'R' ? 'Refunded' : 'Completed';
	return (
		<tr>
			<td>{order.id}</td>
			<td>{first_name}</td>
			<td>{last_name}</td>
			<td >${parseFloat(order.order_total).toFixed(2)}</td>
			<td ><strong>${parseFloat(order.charged).toFixed(2)}</strong></td>
			<td><span className={status.toLowerCase() + ' order-status'}>{status}</span></td>
			<td>{order.created_at}</td>
			<td><Link to={`/admin777/orders/detail/${order.id}`} className="btn_2">Detail</Link></td>
		</tr>
	)
}

OrderItem.propTypes = {
	index: PropTypes.number.isRequired,
  	order: PropTypes.object.isRequired
}

export default OrderItem
