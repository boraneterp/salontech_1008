import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router'
import LabelWithBadge from './LabelWithBadge'

const Header = ({ page, badge }) => (
	<header id="plain">
		<div className="container">
			<div className="row">
				<div className="col-xs-2">
					<div id="logo_home">
						<h1><a href="/" title="City tours travel template"></a></h1>
					</div>
				</div>
				<nav className="col-xs-10">
					<a className="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a>
					<div className="main-menu">
						<div id="header_menu">
							<img src="/assets/img/logo.png" width="160" height="34" alt="Salon Tech" data-retina="true" />
						</div>
						<a href="#" className="open_close" id="close_in"><i className="icon_set_1_icon-77"></i></a>
						<ul className="pull-right">
							<li className="submenu">
								<Link to={`/admin777`} className="show-submenu">Dashboard</Link>
							</li>
							<li className={page.startsWith('/orders')? 'submenu active': 'submenu'}>
								<a href={`/admin777/orders`} className="show-submenu">
									<LabelWithBadge label="Orders" badge={badge.order} />
								</a>
							</li>
							<li className={(page.startsWith('/category') || page.startsWith('/products'))? 'submenu active': 'submenu'}>
								<a href={`/admin777/products`} className="show-submenu">Catalog
									<i className="fa fa-chevron-down"></i>
								</a>
								<ul>
                                    <li><a href={`/admin777/category`} className="show-submenu">Manage Categories</a></li>
                                    <li><a href={`/admin777/products`} className="show-submenu">Manage Products</a></li>
                                </ul>
							</li>
							<li className={(page.startsWith('/customers') || page.startsWith('/subscriptions'))? 'submenu active': 'submenu'}>
								<a href={`/admin777/customers`} className="show-submenu">Customers
									<i className="fa fa-chevron-down"></i>
								</a>
								<ul>
                                    <li><a href={`/admin777/customers`} className="show-submenu">Manage Customers</a></li>
                                    <li><a href={`/admin777/subscriptions`} className="show-submenu">Subscriptions</a></li>
                                </ul>
							</li>
							<li className={(page.startsWith('/lookbooks') || page.startsWith('/showcases'))? 'submenu active': 'submenu'}>
								<Link to={`/admin777/lookbooks`} className="show-submenu">Pages
									<i className="fa fa-chevron-down"></i>
								</Link>
								<ul>
                                    <li><Link to={`/admin777/lookbooks`} className="show-submenu">Manage Look Books</Link></li>
                                    <li><Link to={`/admin777/showcases`} className="show-submenu">Manage Showcase</Link></li>
                                </ul>
							</li>
							<li className={page.startsWith('/reviews')? 'submenu active': 'submenu'}>
								<a href={`/admin777/reviews`} className="show-submenu">
									<LabelWithBadge label="Reviews" badge={badge.review} />
								</a>
							</li>
							<li className={page.startsWith('/warranties')? 'submenu active': 'submenu'}>
								<a href={`/admin777/warranties`} className="show-submenu">
									<LabelWithBadge label="Warranty" badge={badge.warranty} />
								</a>
							</li>
							<li className={page.startsWith('/settings')? 'submenu active': 'submenu'}>
								<Link href={`/admin777/settings`} className="show-submenu">Settings
									<i className="fa fa-chevron-down"></i>
								</Link>
								<ul>
                                    <li><Link to={`/admin777/settings`} className="show-submenu">General Settings</Link></li>
                                    <li><Link to={`/admin777/settings/home`} className="show-submenu">Homepage Settings</Link></li>
                                    <li><Link to={`/admin777/settings/promotion`} className="show-submenu">Promotion Settings</Link></li>
                                    <li><Link to={`/admin777/settings/coupon`} className="show-submenu">Coupon Settings</Link></li>
                                    <li><Link to={`/admin777/settings/store`} className="show-submenu">Store Settings</Link></li>
                                    <li><Link to={`/admin777/settings/tax`} className="show-submenu">Tax Settings</Link></li>
                                </ul>
							</li>
							<li><a href="/admin777/logout">Logout</a></li>
						</ul>
					</div>
				</nav>
			</div>
		</div>
	</header>
)

Header.propTypes = {
  	page: PropTypes.string.isRequired
}

export default Header