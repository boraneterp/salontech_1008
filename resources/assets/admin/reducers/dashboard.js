import _ from 'lodash'
import { combineReducers } from 'redux';
import * as types from '../constants/ActionTypes'

const dashboard = (state = {}, action) => {
    switch (action.type) {
        case types.SET_DASHBOARD_DATA:
            return action.data
        default:
            return state
    }
}

export default dashboard;

