import { combineReducers } from 'redux';
import * as types from '../constants/ActionTypes'

const intialParams = {
    email: '',
    offset: 0
}

const params = (state = intialParams, action) => {
    switch (action.type) {
        case types.CHANGE_CUSTOMER_PARAM:
            return action.params
        default:
            return state
    }
}

const currentCustomer = (state = {}, action) => {
    switch (action.type) {
        case types.SET_CURRNET_CUSTOMER:
            return action.currentCustomer
        default:
            return state
    }
}

const data = (state = [], action) => {
    switch (action.type) {
        case types.GET_CUSTOMERS:
            return action.data
        default:
            return state
    }
}

const pageCount = (state = 1, action) => {
    switch (action.type) {
        case types.GET_CUSTOMERS:
            return action.page_count
        default:
            return state
    }
}

export default combineReducers({
    params,
    currentCustomer,
    data,
    pageCount
});
