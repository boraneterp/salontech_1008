import _ from 'lodash'
import { combineReducers } from 'redux';
import * as types from '../constants/ActionTypes'
import coupon from './setting/coupon'
import store from './setting/store'

const message = (state = '', action) => {
    switch (action.type) {
        case types.SET_SETTING_MESSAGE:
            return action.message
        case "@@router/LOCATION_CHANGE":
            return ''
        default:
            return state
    }
}

const general = (state = {}, action) => {
    switch (action.type) {
        case types.SET_SETTINGS:
  			return Object.assign({}, state, action.settings.general)
        default:
            return state
    }
}

const home = (state = {}, action) => {
    switch (action.type) {
        case types.SET_SETTINGS:
  			return Object.assign({}, state, action.settings.home)
        default:
            return state
    }
}

const promotion = (state = {}, action) => {
    switch (action.type) {
        case types.SET_SETTINGS:
            return Object.assign({}, state, action.settings.promotion)
        default:
            return state
    }
}

export default combineReducers({
    message,
    general,
    home,
    coupon,
    store,
    promotion
});