import { combineReducers } from 'redux';
import * as types from '../constants/ActionTypes'

const message = (state = '', action) => {
    switch (action.type) {
        case types.SET_SHOWCASE_MESSAGE:
            return action.message
        default:
            return state
    }
}

const data = (state = {}, action) => {
    switch (action.type) {
        case types.SET_SHOWCASES:
            return action.data
        default:
            return state
    }
}

export default combineReducers({
    message,
    data
});