import { routerReducer as routing } from 'react-router-redux';
import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import * as types from '../constants/ActionTypes';
import badge from './badge';
import states from './states';
import dashboard from './dashboard';
import category from './category';
import product from './product';
import lookbook from './lookbook';
import showcase from './showcase';
import order from './order';
import subscription from './subscription';
import review from './review';
import warranty from './warranty';
import customer from './customer';
import setting from './setting';
import path from './path';

const isFetching = (state = false, action) => {
    switch (action.type) {
        case types.CREATE_REQUEST:
            return true;
        case types.REQUEST_SUCCESS:
        case types.REQUEST_FAILURE:
            return false;
        default:
            return state;
    }
};

const rootReducer = combineReducers({
	isFetching,
	badge,
	states,
	dashboard,
	category,
	product,
	lookbook,
	showcase,
	order,
	subscription,
	review,
	warranty,
	customer,
	setting,
	routing,
	path,
	form: formReducer,
})

export default rootReducer
