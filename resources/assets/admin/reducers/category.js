import { combineReducers } from 'redux';
import * as types from '../constants/ActionTypes'

const message = (state = '', action) => {
    switch (action.type) {
        case types.SET_CATEGORY_MESSAGE:
            return action.message
        case types.SET_CURRNET_CATEGORY:
            return ''
        default:
            return state
    }
}

const intialParams = {
    name: '',
    offset: 0
}

const params = (state = intialParams, action) => {
    switch (action.type) {
        case types.CHANGE_CATEGORY_PARAM:
            return action.params
        default:
            return state
    }
}

const currentCategory = (state = {}, action) => {
    switch (action.type) {
        case types.SET_CURRNET_CATEGORY:
            return action.currentCategory
        default:
            return state
    }
}

const data = (state = [], action) => {
    switch (action.type) {
        case types.SET_CATEGORIES:
        case types.GET_CATEGORIES:
            return action.data
        default:
            return state
    }
}

const dataWithProducts = (state = [], action) => {
    switch (action.type) {
        case types.SET_CATEGORIES_WITH_PRODUCTS:
            return action.data
        default:
            return state
    }
}

export default combineReducers({
    message,
    params,
    currentCategory,
    data,
    dataWithProducts
});