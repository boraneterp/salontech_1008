import { combineReducers } from 'redux';
import * as types from '../constants/ActionTypes'

const intialParams = {
    offset: 0
}

const params = (state = intialParams, action) => {
    switch (action.type) {
        case types.CHANGE_SUBSCRIPTION_PAGE:
            return action.params
        default:
            return state
    }
}

const data = (state = [], action) => {
    switch (action.type) {
        case types.GET_SUBSCRIPTIONS:
            return action.data
        default:
            return state
    }
}

const pageCount = (state = 1, action) => {
    switch (action.type) {
        case types.GET_SUBSCRIPTIONS:
            return action.page_count
        default:
            return state
    }
}

export default combineReducers({
    params,
    data,
    pageCount
});