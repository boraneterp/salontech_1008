import { combineReducers } from 'redux';
import * as types from '../../constants/ActionTypes'

const param = (state = {name: '', offset: 0}, action) => {
    switch (action.type) {
    	case types.CHANGE_STORE_PARAM:
	        return {
	          	name: action.name,
            	offset: action.offset
	        }
    	default:
      		return state
    }
}

const currentStore = (state = {}, action) => {
    switch (action.type) {
    	case types.SET_CURRNET_STORE:
	        return action.currentStore
    	default:
      		return state
    }
}

const data = (state = [], action) => {
    switch (action.type) {
        case types.GET_STORES:
    		return action.data
      	case types.SET_STORES:
	        return action.stores.data
        default:
            return state
    }
}

const pageCount = (state = 1, action) => {
    switch (action.type) {
        case types.GET_STORES:
            return action.page_count
        default:
            return state
    }
}

export default combineReducers({
	param,
	data,
	currentStore,
	pageCount
})