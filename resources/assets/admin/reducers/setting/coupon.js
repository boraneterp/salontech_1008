import { combineReducers } from 'redux';
import * as types from '../../constants/ActionTypes'

const intialParams = {
    type: '',
    offset: 0
}

const params = (state = intialParams, action) => {
    switch (action.type) {
        case types.CHANGE_COUPON_PAGE:
            return {
                ...state,
                offset: action.offset
            }
        default:
            return state
    }
}

const getCoupons = (data) => {
    for (let i = 0; i < data.length; i++) {
        for (let j = 0; j < data[i].products.length; j++) {
            data[i].products[j].label = data[i].products[j].product_name;
            data[i].products[j].value = data[i].products[j].id;
        }
    }
    return data;
}

const data = (state = [], action) => {
    switch (action.type) {
        case types.GET_COUPONS:
            return getCoupons(action.data);
        case types.DELETE_COUPON:
            const i = _.findIndex(state, (review) => review.id == action.id)
            const remainState = _.cloneDeep(state)
            remainState.splice(i, 1)
            return remainState
        default:
            return state
    }
}

const pageCount = (state = 1, action) => {
    switch (action.type) {
        case types.GET_COUPONS:
            return action.page_count
        default:
            return state
    }
}

const intialModalState = {
    title: '',
    show: false
}

const showModal = (coupon) => {
	const title = coupon.id? 'Edit Coupon': 'Add Coupon'
	return {
		title,
		show: true
	}
}

const modal = (state = intialModalState, action) => {
    switch (action.type) {
        case types.SET_CURRENT_COUPON:
            return showModal(action.coupon)
        case types.HIDE_COUPON_MODAL:
        case types.GET_COUPONS:
        	return intialModalState
        default:
            return state
    }
}

const currentCoupon = (state = {}, action) => {
    switch (action.type) {
        case types.SET_CURRENT_COUPON:
            return action.coupon
        default:
            return state
    }
}

export default combineReducers({
    params,
    data,
    pageCount,
    modal,
    currentCoupon
});