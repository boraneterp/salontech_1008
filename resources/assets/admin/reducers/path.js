const initialState = {
  previoutPath: '',
  currentPath: ''
}

const path = (state = initialState, action) => {
  	switch (action.type) {
    	case "@@router/LOCATION_CHANGE":
    		return {
    			previoutPath: state.currentPath,
    			currentPath: window.location.pathname
    		}
      
    	default:
      		return state
  	}
}

export default path