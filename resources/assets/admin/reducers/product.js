import _ from 'lodash'
import { combineReducers } from 'redux';
import * as types from '../constants/ActionTypes'

const message = (state = '', action) => {
    switch (action.type) {
        case types.SET_PRODUCT_MESSAGE:
            return action.message
        case types.SET_CURRNET_PRODUCT:
            return ''
        default:
            return state
    }
}

const intialParams = {
    category: '',
    name: '',
    offset: 0
}
const params = (state = intialParams, action) => {
    switch (action.type) {
        case types.CHANGE_PRODUCT_PARAM:
            return action.params
        case types.CHANGE_PRODUCT_PAGE:
            return {
                ...state,
                offset: action.offset
            }
        default:
            return state
    }
}

const currentProduct = (state = {}, action) => {
    switch (action.type) {
        case types.SET_CURRNET_PRODUCT:
            return action.product
        default:
            return state
    }
}

const data = (state = [], action) => {
    switch (action.type) {
        case types.GET_PRODUCTS:
            return action.data
        case types.DELETE_PRODUCT:
            return _.remove(state, product => {
                return product.id != action.id
            })
        case types.UPDATE_PRODUCTS:
            let data = state
            if(!_.isEmpty(data) && data.length > 0) {
                for (let i = 0; i < data.length; i++) {
                    if(data[i].id === action.product.id) {
                        data[i] = action.product
                    } else {
                        data[i] = data[i]
                    }
                }
            }
            return data
        default:
            return state
    }
}

const pageCount = (state = 1, action) => {
    switch (action.type) {
        case types.GET_PRODUCTS:
            return action.page_count
        default:
            return state
    }
}

const reviewModalState = {
    show: false,
    reviews: []
}
const reviewModal = (state = reviewModalState, action) => {
    switch (action.type) {
        case types.SHOW_REVIEW_MODAL:
            return {
                show: true,
                reviews: action.reviews
            }
        case types.HIDE_REVIEW_MODAL:
            return reviewModalState
        default:
            return state
    }
}

export default combineReducers({
    message,
    params,
    currentProduct,
    data,
    pageCount,
    reviewModal
});

