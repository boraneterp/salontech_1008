import * as types from '../constants/ActionTypes';

const states = (state = [], action) => {
  	switch (action.type) {
    	case types.GET_STATES:
    		return action.data
    	case types.CHANGE_STATE_TAX:
    		return state.map(s => {
    			if(s.code === action.state.code) {
    				s = action.state;
    				s.tax_rate = action.value;
    			}
    			return s;
    		});
    	default:
      		return state
  	}
}

export default states