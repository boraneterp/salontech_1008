import _ from 'lodash'
import { combineReducers } from 'redux';
import * as types from '../constants/ActionTypes'

const message = (state = '', action) => {
    switch (action.type) {
        case types.SET_LOOKBOOK_MESSAGE:
            return action.message
        case types.SET_CURRNET_LOOKBOOK:
            return ''
        default:
            return state
    }
}

const intialParams = {
    name: '',
    offset: 0
}
const params = (state = intialParams, action) => {
    switch (action.type) {
        case types.CHANGE_LOOKBOOK_PARAM:
            return action.params
        case types.CHANGE_LOOKBOOK_PAGE:
            return {
                ...state,
                offset: action.offset
            }
        default:
            return state
    }
}

const currentLookBook = (state = {}, action) => {
    switch (action.type) {
        case types.SET_CURRNET_LOOKBOOK:
            return action.lookbook
        default:
            return state
    }
}

const data = (state = [], action) => {
    switch (action.type) {
        case types.GET_LOOKBOOKS:
            return action.data
        case types.DELETE_LOOKBOOK:
            return _.remove(state, lookbook => {
                return lookbook.id != action.id
            })
        case types.UPDATE_LOOKBOOKS:
            let data = state
            if(!_.isEmpty(data) && data.length > 0) {
                for (let i = 0; i < data.length; i++) {
                    if(data[i].id === action.lookbook.id) {
                        data[i] = action.lookbook
                    } else {
                        data[i] = data[i]
                    }
                }
            }
            return data
        default:
            return state
    }
}

const pageCount = (state = 1, action) => {
    switch (action.type) {
        case types.GET_LOOKBOOKS:
            return action.page_count
        default:
            return state
    }
}

export default combineReducers({
    message,
    params,
    currentLookBook,
    data,
    pageCount
});

