import { combineReducers } from 'redux';
import _ from 'lodash'
import * as types from '../constants/ActionTypes'

const intialParams = {
    status: '0',
    offset: 0
}

const params = (state = intialParams, action) => {
    switch (action.type) {
        default:
            return state
    }
}

const data = (state = [], action) => {
    switch (action.type) {
        case types.GET_REVIEWS:
            return action.data
        case types.UPDATE_REVIEW:
            const index = _.findIndex(state, (review) => review.id == action.review.id)
            const newState = _.cloneDeep(state)
            newState[index] = action.review
            return newState
        case types.DELETE_REVIEW:
            const i = _.findIndex(state, (review) => review.id == action.id)
            const remainState = _.cloneDeep(state)
            remainState.splice(i, 1)
            return remainState
        default:
            return state
    }
}

const pageCount = (state = 1, action) => {
    switch (action.type) {
        case types.GET_REVIEWS:
            return action.page_count
        default:
            return state
    }
}

export default combineReducers({
    params,
    data,
    pageCount
});
