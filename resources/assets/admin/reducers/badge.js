import _ from 'lodash'
import * as types from '../constants/ActionTypes'

const initialBadge = {
	order: 0,
	warranty: 0,
  review: 0
}

const updateBadge = (state, type) => {
	let badge = _.cloneDeep(state)
	if(type == "order") {
		badge.order = badge.order - 1
	} else if(type == "warranty") {
		badge.warranty = badge.warranty - 1
	} 

	return badge
}

const updateReviewBadge = (state, review) => {
    let badge = _.cloneDeep(state)
    if(review.status == "0") {
        badge.review = badge.review + 1
    } else {
        badge.review = badge.review - 1
    } 

    return badge
}

const badge = (state = initialBadge, action) => {
  	switch (action.type) {
    	case types.SET_BADGE:
    		return action.badge
    	case types.UPDATE_ORDER_BADGE:
    		return updateBadge(state, 'order')
    	case types.UPDATE_WARRANTY_STATUS:
    		return updateBadge(state, 'warranty')
        case types.UPDATE_REVIEW:
            return updateReviewBadge(state, action.review)
    	default:
      		return state
  	}
}

export default badge