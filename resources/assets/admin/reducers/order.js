import { combineReducers } from 'redux';
import * as types from '../constants/ActionTypes'

const intialParams = {
    start_date: '',
    end_date: '',
    coupon: '',
    status: '',
    offset: 0
}
const params = (state = intialParams, action) => {
    switch (action.type) {
        case types.CHANGE_ORDER_PARAM:
            return action.params
        case types.CHANGE_ORDER_PAGE:
            return {
                ...state,
                offset: action.offset
            }
        default:
            return state
    }
}

const currentOrder = (state = {}, action) => {
    switch (action.type) {
        case types.SET_CURRNET_ORDER:
            return action.currentOrder
        case types.UPDATE_ORDER_STATUS:
            return Object.assign({}, state, {status: action.status} )
        default:
            return state
    }
}

const data = (state = [], action) => {
    switch (action.type) {
        case types.GET_ORDERS:
            return action.data
        default:
            return state
    }
}

const processing= (state = false, action) => {
    switch (action.type) {
        case types.START_PROCESSING:
            return true
        case types.DONE_PROCESSING:
            return false
        default:
            return state
    }
}

const pageCount = (state = 1, action) => {
    switch (action.type) {
        case types.GET_ORDERS:
            return action.page_count
        default:
            return state
    }
}

export default combineReducers({
    params,
    currentOrder,
    data,
    processing,
    pageCount
});