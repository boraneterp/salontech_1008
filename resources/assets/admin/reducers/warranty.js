import { combineReducers } from 'redux';
import _ from 'lodash'
import * as types from '../constants/ActionTypes'

const initialized = (state = false, action) => {
    switch (action.type) {
        case types.SET_WARRANTIES:
            return true
        default:
            return state
    }
}

const intialParams = {
    offset: 0
}

const params = (state = intialParams, action) => {
    switch (action.type) {
        case types.CHANGE_WARRANTY_PAGE:
            return action.params
        default:
            return state
    }
}

const updateStatus = (state, id, status) => {
    return state.map(warranty => {
        if(warranty.id == id) {
            warranty.status = status
        }
        return warranty
    })
}

const data = (state = [], action) => {
    switch (action.type) {
        case types.SET_WARRANTIES:
            return action.data
        case types.UPDATE_WARRANTY_STATUS:
            return updateStatus(state, action.warranty, action.status)
        default:
            return state
    }
}

const pageCount = (state = 1, action) => {
    switch (action.type) {
        case types.SET_WARRANTIES:
            return action.page_count
        default:
            return state
    }
}

export default combineReducers({
    initialized,
    params,
    data,
    pageCount
});