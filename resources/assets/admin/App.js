import "babel-polyfill";
import React from 'react';
import { render } from 'react-dom';
import { browserHistory } from 'react-router';
import { Provider } from 'react-redux';
import routes from './routes';
import { syncHistoryWithStore } from 'react-router-redux';
import { Router } from 'react-router';
import axios from 'axios';
import * as types from './constants/ActionTypes';
import store from './store';

const history = syncHistoryWithStore(browserHistory, store)

$.ajaxSetup( {
	beforeSend: function ( xhr, req ) {
		if(req.type === 'POST') {
			xhr.setRequestHeader( 'X-CSRF-Token', $( 'meta[name="csrf-token"]' ).attr( 'content' ) );
		}
	}
});

const dispatchRequest = (url) => {
	return url.split("/")[2] === location.pathname.split("/")[2];
}

axios.defaults.headers.common['X-CSRF-Token'] = $( 'meta[name="csrf-token"]' ).attr( 'content' );
axios.interceptors.request.use(function (config) {
	if(config.method.toLowerCase() === 'get' && dispatchRequest(config.url)) {
   		store.dispatch({ type: types.CREATE_REQUEST });
	}
    return config;
}, function (error) {
	return Promise.reject(error);
});

axios.interceptors.response.use(function (response) {
	if(dispatchRequest(response.config.url)) { 
		store.dispatch({ type: types.REQUEST_SUCCESS });
	}
	return response.data;
}, function (error) {
	store.dispatch({ type: types.REQUEST_FAILURE });
	return Promise.reject(error);
});

axios.request({
    method: 'GET',
    url: '/admin777/badges.json'
}).then((response) => {
	store.dispatch({
	  	type: types.SET_BADGE,
	  	badge: response
	})
});

render(
  	<Provider store={store}>
    	<Router history={history} routes={routes} />
  	</Provider>,
  	document.getElementById('root')
)
