import React from 'react';
import { Route, IndexRoute } from 'react-router';
import Root from './containers/Root';
import DashboardPage from './containers/DashboardPage';
import PageNotFound from './containers/PageNotFound';

import CategoryPage from './containers/categories/CategoryPage';
import CategoryDetailPage from './containers/categories/CategoryDetailPage';

import ProductListPage from './containers/products/ProductListPage';
import ProductDetailPage from './containers/products/ProductDetailPage';

import LookBookListPage from './containers/lookbooks/LookBookListPage';
import LookBookDetailPage from './containers/lookbooks/LookBookDetailPage';

import ShowcasePage from './containers/showcases/ShowcasePage';

import OrderListPage from './containers/orders/OrderListPage';
import OrderDetailPage from './containers/orders/OrderDetailPage';

import SubscriptionPage from './containers/subscriptions/SubscriptionPage';

import WarrantyPage from './containers/warranties/WarrantyPage';
import ReviewPage from './containers/reviews/ReviewPage';

import CustomerListPage from './containers/customers/CustomerListPage';
import CustomerDetailPage from './containers/customers/CustomerDetailPage';

import GeneralSettingPage from './containers/settings/GeneralSettingPage';
import HomeSettingPage from './containers/settings/HomeSettingPage';
import PromotionSettingPage from './containers/settings/PromotionSettingPage';
import CouponSettingPage from './containers/settings/CouponSettingPage';
import StoreSettingPage from './containers/settings/StoreSettingPage';
import TaxSettingPage from './containers/settings/TaxSettingPage';

export default <Route path="/" component={Root}>
  	<Route path="/admin777" >
    		<IndexRoute component={DashboardPage} />
        <Route path="category"  >
            <IndexRoute component={CategoryPage} />
            <Route path="add" component={CategoryDetailPage} />
            <Route path="detail/:id" component={CategoryDetailPage} />
        </Route>
        <Route path="orders" >
            <IndexRoute component={OrderListPage} />
            <Route path="detail/:id" component={OrderDetailPage} />
        </Route>
        <Route path="products"  >
            <IndexRoute component={ProductListPage} />
            <Route path="add" component={ProductDetailPage} />
            <Route path="detail/:id" component={ProductDetailPage} />
        </Route>
        <Route path="lookbooks"  >
            <IndexRoute component={LookBookListPage} />
            <Route path="add" component={LookBookDetailPage} />
            <Route path="detail/:id" component={LookBookDetailPage} />
        </Route>
        <Route path="showcases"  >
            <IndexRoute component={ShowcasePage} />
        </Route>
        <Route path="subscriptions" >
            <IndexRoute component={SubscriptionPage} />
        </Route>
        <Route path="reviews" >
            <IndexRoute component={ReviewPage} />
        </Route>
        <Route path="warranties" >
            <IndexRoute component={WarrantyPage} />
        </Route>
        <Route path="customers" >
            <IndexRoute component={CustomerListPage} />
            <Route path="detail/:id" component={CustomerDetailPage} />
        </Route>
        <Route path="settings" >
            <IndexRoute component={GeneralSettingPage} />
            <Route path="home" component={HomeSettingPage} />
            <Route path="promotion" component={PromotionSettingPage} />
            <Route path="coupon" component={CouponSettingPage} />
            <Route path="store" component={StoreSettingPage} />
            <Route path="tax" component={TaxSettingPage} />
        </Route>
        <Route path="*" component={PageNotFound} />
  	</Route>
</Route>