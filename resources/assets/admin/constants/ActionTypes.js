export const CREATE_REQUEST = 'CREATE_REQUEST';
export const REQUEST_SUCCESS = 'REQUEST_SUCCESS';
export const REQUEST_FAILURE = 'REQUEST_FAILURE';

export const SET_BADGE = 'SET_BADGE';
export const UPDATE_ORDER_BADGE = 'UPDATE_ORDER_BADGE';
export const GET_STATES = 'GET_STATES';
export const CHANGE_STATE_TAX = 'CHANGE_STATE_TAX';

export const SET_DASHBOARD_DATA = 'SET_DASHBOARD_DATA';

export const GET_ORDERS = 'GET_ORDERS';
export const SET_CURRNET_ORDER = 'SET_CURRNET_ORDER';
export const CHANGE_ORDER_PARAM = 'CHANGE_ORDER_PARAM';
export const CHANGE_ORDER_PAGE = 'CHANGE_ORDER_PAGE';
export const UPDATE_ORDER_STATUS = 'UPDATE_ORDER_STATUS';
export const START_PROCESSING = 'START_PROCESSING';
export const DONE_PROCESSING = 'DONE_PROCESSING';

export const GET_CATEGORIES = 'GET_CATEGORIES';
export const SET_CATEGORIES = 'SET_CATEGORIES';
export const SET_CURRNET_CATEGORY = 'SET_CURRNET_CATEGORY';
export const CHANGE_CATEGORY_PARAM = 'CHANGE_CATEGORY_PARAM';
export const SET_CATEGORIES_WITH_PRODUCTS = 'SET_CATEGORIES_WITH_PRODUCTS';
export const SET_CATEGORY_MESSAGE = 'SET_CATEGORY_MESSAGE';

export const GET_CUSTOMERS = 'GET_CUSTOMERS';
export const SET_CURRNET_CUSTOMER = 'SET_CURRNET_CUSTOMER';
export const UPDATE_CURRNET_CUSTOMER = 'UPDATE_CURRNET_CUSTOMER';
export const CHANGE_CUSTOMER_PARAM = 'CHANGE_CUSTOMER_PARAM';

export const GET_PRODUCTS = 'GET_PRODUCTS';
export const DELETE_PRODUCT = 'DELETE_PRODUCT';
export const UPDATE_PRODUCTS = 'UPDATE_PRODUCTS';
export const SET_CURRNET_PRODUCT = 'SET_CURRNET_PRODUCT';
export const SET_PRODUCT_MESSAGE = 'SET_PRODUCT_MESSAGE';
export const CHANGE_PRODUCT_PARAM = 'CHANGE_PRODUCT_PARAM';
export const CHANGE_PRODUCT_PAGE = 'CHANGE_PRODUCT_PAGE';
export const SHOW_REVIEW_MODAL = 'SHOW_REVIEW_MODAL';
export const HIDE_REVIEW_MODAL = 'HIDE_REVIEW_MODAL';
export const UPLOAD_PRODUCT_VIDEO = 'UPLOAD_PRODUCT_VIDEO';
export const REMOVE_PRODUCT_IMAGE = 'REMOVE_PRODUCT_IMAGE';
export const MOVE_PRODUCT_IMAGE = 'MOVE_PRODUCT_IMAGE';
export const UPLOAD_PRODUCT_IMAGES = 'UPLOAD_PRODUCT_IMAGES';

export const GET_LOOKBOOKS = 'GET_LOOKBOOKS';
export const DELETE_LOOKBOOK = 'DELETE_LOOKBOOK';
export const UPDATE_LOOKBOOKS = 'UPDATE_LOOKBOOKS';
export const SET_CURRNET_LOOKBOOK = 'SET_CURRNET_LOOKBOOK';
export const SET_LOOKBOOK_MESSAGE = 'SET_LOOKBOOK_MESSAGE';
export const CHANGE_LOOKBOOK_PARAM = 'CHANGE_LOOKBOOK_PARAM';
export const CHANGE_LOOKBOOK_PAGE = 'CHANGE_LOOKBOOK_PAGE';

export const SET_SHOWCASES = 'SET_SHOWCASES';
export const SET_SHOWCASE_MESSAGE = 'SET_SHOWCASE_MESSAGE';

export const CHANGE_SUBSCRIPTION_PAGE = 'CHANGE_SUBSCRIPTION_PAGE';
export const GET_SUBSCRIPTIONS = 'GET_SUBSCRIPTIONS';

export const CHANGE_WARRANTY_PAGE = 'CHANGE_WARRANTY_PAGE';
export const SET_WARRANTIES = 'SET_WARRANTIES';
export const UPDATE_WARRANTY_STATUS = 'UPDATE_WARRANTY_STATUS';

export const GET_REVIEWS = 'GET_REVIEWS';
export const UPDATE_REVIEW = 'UPDATE_REVIEW';
export const DELETE_REVIEW = 'DELETE_REVIEW';

export const SET_SETTINGS = 'SET_SETTINGS';
export const SET_SETTING_MESSAGE = 'SET_SETTING_MESSAGE';

export const GET_COUPONS = 'GET_COUPONS';
export const CHANGE_COUPON_PAGE = 'CHANGE_COUPON_PAGE';
export const SET_CURRENT_COUPON = 'SET_CURRENT_COUPON';
export const HIDE_COUPON_MODAL = 'HIDE_COUPON_MODAL';
export const DELETE_COUPON = 'DELETE_COUPON';

export const GET_STORES = 'GET_STORES';
export const SET_STORES = 'SET_STORES';
export const SET_CURRNET_STORE = 'SET_CURRNET_STORE';
export const CHANGE_STORE_PARAM = 'CHANGE_STORE_PARAM';