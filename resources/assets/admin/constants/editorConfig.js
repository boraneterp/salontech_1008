import axios from 'axios';
import objectToFormData from '../../utils/objectToFormData';

const editorConfig = {
    plugins: 'link,image,lists,paste,textcolor,code',
    toolbar: 'formatselect bullist numlist | bold italic link | alignleft aligncenter alignright alignjustify | forecolor backcolor fontsizeselect | image code',
    images_upload_handler: function (blobInfo, success, failure) {
        const config = { headers: { 'Content-Type': 'multipart/form-data' } };
        const payload = {
            file: blobInfo.blob()
        };
        axios.post('/admin777/upload', objectToFormData(payload), config).then((response) => {
            success(response);
        }).catch((error) => {
            failure("Fail to upload image!");
        })
    },
    block_formats: 'Paragraph=p;Header 2=h2;Header 3=h3;Header 4=h4',
    fontsize_formats: "8px 10px 12px 14px 18px 24px 36px",
    menubar: false,
    statusbar: false,
    content_css: "/assets/css/style.css",
    body_class: 'banner-container',
    paste_word_valid_elements: 'b,strong,i,em,h1,h2,h3,p,li,ul,ol,a',
    paste_retain_style_properties: 'none',
    paste_strip_class_attributes: 'none',
    paste_remove_styles: true,
    height:"400",
    content_css: ['//fonts.googleapis.com/css?family=Montserrat:400,700', 
            "/assets/css/style.css"]
};

export default editorConfig;