import React from 'react';
import Dropzone from 'react-dropzone';
import InputMask from 'react-input-mask';
import TinyMCE from 'react-tinymce';;
import TextareaAutosize from 'react-autosize-textarea';;
import DatePicker from "react-bootstrap-date-picker";
import { SortableContainer, SortableElement } from 'react-sortable-hoc';
import { arrayMove } from 'react-sortable-hoc';
import * as Animated from 'react-select/lib/animated';
import AsyncSelect from 'react-select/lib/Async';
import { searchProducts } from '../admin/actions/ProductAction';

export const normalizePhone = (value, previousValue) => {
    if (!value) {
        return value
    }
    const onlyNums = value.replace(/[^\d]/g, '')
    if (!previousValue || value.length > previousValue.length) {
        // typing forward
        if (onlyNums.length === 3) {
            return '(' + onlyNums + ')'
        }
        if (onlyNums.length === 6) {
            return '(' + onlyNums.slice(0, 3) + ') ' + onlyNums.slice(3) + '-'
        }
    }
    if (onlyNums.length <= 3) {
        return '(' + onlyNums
    }
    if (onlyNums.length <= 6) {
        return '(' + onlyNums.slice(0, 3) + ') ' + onlyNums.slice(3)
    }

    return '(' + onlyNums.slice(0, 3) + ') ' + onlyNums.slice(3, 6) + '-' + onlyNums.slice(6, 10)
}

export const renderField = ({ input, label, type, disabled, required, meta: { touched, error, warning } }) => (
	<div className="form-group ">
		<label className={required? 'control-label required':'control-label'}>{label}</label>
		<input {...input} placeholder={label} type={type} className="form-control" placeholder={label} disabled={disabled} />
		{touched && (error && <label className="error">{error}</label>)}
		{touched && ((!error && warning) && <label className="warning">{warning}</label>)}
	</div>
)

export const renderMaskField = ({ input, label, type, disabled, required, mask, meta: { touched, error, warning } }) => (
	<div className="form-group ">
		<label className={required? 'control-label required':'control-label'}>{label}</label>
		<InputMask {...input} placeholder={label} mask={mask} className="form-control" placeholder={label}  />
		{touched && (error && <label className="error">{error}</label>)}
		{touched && ((!error && warning) && <label className="warning">{warning}</label>)}
	</div>
)
		
export const renderInlineField = ({ input, label, type, disabled, meta: { touched, error, warning } }) => (
	<input {...input} placeholder={label} type={type} placeholder={label}  />
)

export const renderInlineDropdown = ({ input, label, options, meta: { touched, error, warning } }) => (
	<div className="styled-select-filters">
		<select {...input} >
			<option value=''>Choose {label}</option>
			{options.map((option) =>
			<option key={option.id} value={option.id}>{option.value}</option>
			)}
		</select>
	</div>
)

export const renderDatepicker = ({ input, label, type, disabled, meta: { touched, error, warning } }) => (
	<div className="form-group ">
		<label className="control-label">{label}</label>
		<DatePicker {...input} placeholder={label} className="styled-text-filters" 
			onChange={input.onChange}  />
		{touched && (error && <label className="error">{error}</label>)}
		{touched && ((!error && warning) && <label className="warning">{warning}</label>)}
	</div>
	)

export const renderTextarea = ({ input, label, meta: { touched, error, warning } }) => (
	<div className="form-group ">
		<label className="control-label">{label}</label>
		<TextareaAutosize {...input} placeholder={label} rows={3} className="form-control" placeholder={label} ></TextareaAutosize>
		{touched && (error && <label className="error">{error}</label>)}
	</div>
)

export const renderDropdown = ({ input, label, options, addDefault, required, meta: { touched, error, warning } }) => (
	<div className="form-group ">
		<label className={required? 'control-label required':'control-label'}>{label}</label>
		<select {...input} className="form-control">
			{(addDefault) &&
			<option value=''>Choose {label}</option>
			}
			{options.map((option) =>
			<option key={option.id} value={option.id} dangerouslySetInnerHTML={{__html: option.value}} />
			)}
		</select>
		{touched && (error && <label className="error">{error}</label>)}
	</div>
)

export const renderStates = ({ input, label, states, meta: { touched, error, warning } }) => (
	<div className="form-group ">
		<label className="control-label">{label}</label>
		<select {...input} className="form-control">
			<option value=''>Choose State</option>
			{states.map((state) =>
			<option key={state.code} value={state.code}>{state.name}</option>
			)}
		</select>
		{touched && (error && <label className="error">{error}</label>)}
	</div>
)

export const renderTinyMCE = (field) => {
	let props = Object.assign({}, field);
    delete props.input;
    delete props.meta;

    return (
    	<div className="form-group ">
			<label className="control-label">{field.label}</label>
	    	<TinyMCE
		        {...props}
		        content={field.input.value || ''}
		        onBlur={(event, value) => { 
		         	field.input.onChange(event.target.getContent()) 
		        }}
			/>
		</div>
	)
}

export const renderDropzoneVideo = (field) => {
  	let value = field.input.value;
  	if(Array.isArray(value)) {
  		value = value[0].preview
  	} 
  	return (
	    <div>
	    	{value &&
	    		<div className="video-preview">
	    			<a href="#" className="remove" onClick={(e) => {
    					e.preventDefault();
    					return field.input.onChange("")
    				}}><i className="fa fa-times"></i></a>
		    		<video width="320" height="240" controls id="product-video">
					  	<source src={value} type="video/mp4" />
					</video>
				</div>
	    	}
	      	<Dropzone name={field.name} className="dz-default" multiple={false} accept="video/mp4"
	        	onDrop={( filesToUpload, e ) => {
	        		return field.input.onChange(filesToUpload)
	        	}} >
	        	<div>Click or Drop video here to upload.</div>
	      	</Dropzone>
	      	{field.meta.touched && (field.meta.error && <label className="error">{field.meta.error}</label>)}
	    </div>
  	);
}

const ImageItem = SortableElement(({image, field}) => {
  	return (
  		<div>
		  	{field.multiple &&
				<a href="#" className="remove" onClick={(e) => {
					e.preventDefault();
					return field.input.onChange(field.input.value.filter(item => item.preview != image))
				}}><i className="fa fa-times"></i></a>
			}
		    <img className="img-responsive" src={image} />
	    </div>
  	)
});

const ImageList = SortableContainer(({files, field}) => {
  	return (
	    <ul>
    		{ files.map((file, i) => 
    			<li key={i} className={field.multiple? 'multi':''}>
    				<ImageItem index={i} image={file.preview} field={field} />
    			</li> 
    		)}
		</ul>
  	)
});

const onSortEnd = (sort, field) => {
	field.input.onChange(arrayMove(field.input.value, sort.oldIndex, sort.newIndex))
}

export const renderDropzoneInput = (field) => {
  	const value = field.input.value;
 	let files = []
 	if(Array.isArray(value)) {
 		value.forEach(function(img){
 			if(img.image_src) {
  				img.preview = img.image_src
 			}
		 	files.push(img)
		});
 	} else if(!Array.isArray(value) && value) {
 		files.push({"preview": value})
 	}

  	return (
	    <div>
	    	{field.multiple? 
	    		<ImageList files={files} field={field} axis="x" lockAxis="x" onSortEnd={(sort) => onSortEnd(sort, field) } />:
	    		<ul>
		    		{ files.map((file, i) => 
		    			<li key={i} >
		    				<img className="img-responsive" src={file.preview}/>
		    			</li> 
		    		)}
				</ul>
	    	}
	      	<Dropzone name={field.name} className="dz-default" multiple={field.multiple} accept="image/jpeg, image/png"
	        	onDrop={( filesToUpload, e ) => {
	        		const newFiles = field.multiple? files.concat(filesToUpload): filesToUpload
	        		return field.input.onChange(newFiles)
	        	}} >
	        	<div>Click to upload images.</div>
	      	</Dropzone>
	      	{field.meta.touched && (field.meta.error && <label className="error">{field.meta.error}</label>)}
	    </div>
  	);
}


export const renderCheckboxGroup = ({ label, required, options, input, meta }) => {
	return (
		<div className="form-group">
            {label &&
            	<label className="control-label">{label}</label>
            }
            <div>
            	{options.map((option, index) => 
	                <label className="checkbox-inline" key={index}>
	                    <input type="checkbox"
							name={`${input.name}[${index}]`}
							value={option.id}
							checked={input.value.indexOf(option.id) !== -1}
							onChange={(event) => {
							   const newValue = [...input.value];
							   if (event.target.checked) {
							       newValue.push(option.id);
							   } else {
							       newValue.splice(newValue.indexOf(option.id), 1);
							   }

							   return input.onChange(newValue);
							}}/>
	                    {option.value}
	                </label>
		        )}
            </div>
        </div>
	)
}

export const renderMultiProducts = ({ label, options, input, meta }) => {
	return (
		<div className="form-group">
            {label &&
            	<label className="control-label">{label}</label>
            }
            <div>
            	<AsyncSelect
			        name={input.name}
			        value={input.value}
			        isMulti
			        cacheOptions
			        defaultOptions
			        components={Animated}
			        onChange={(selectedOption) => {
			        	return input.onChange(selectedOption);
			        }}
			        loadOptions={searchProducts}
			      />
            </div>
        </div>
	)
}