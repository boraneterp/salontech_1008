

const isObject = (value) => {
  	return value === Object(value)
}

const isArray = (value) => {
  	return Array.isArray(value)
}

const isFile = (value) => {
  	return value instanceof File
}

const isMoment = (value) => {
  	return !_.isNull(value) && _.isFunction(value.format)
}

const makeArrayKey = (key, index) => {
  	if (key.length > 2 && key.lastIndexOf('[]') === key.length - 2) {
    	return key
  	} else {
    	return key + '[' + index + ']'
  	}
}

const objectToFormData = (obj, fd, pre) => {
  	fd = fd || new FormData()

  	Object.keys(obj).forEach(function (prop) {
    	var key = pre ? (pre + '[' + prop + ']') : prop

    	if (isObject(obj[prop]) && !isArray(obj[prop]) && !isFile(obj[prop]) && !isMoment(obj[prop])) {
      		objectToFormData(obj[prop], fd, key)
    	} else if (isMoment(obj[prop])) {
      		fd.append(key, obj[prop].format('YYYY-MM-DD'))
    	} else if (isArray(obj[prop])) {
      		obj[prop].forEach(function (value, index) {
	        	var arrayKey = makeArrayKey(key, index)

	        	if (isObject(value) && !isFile(value)) {
	          		objectToFormData(value, fd, arrayKey)
	        	} else {
	          		fd.append(arrayKey, value)
	        	}
	      	})
    	} else {
      		fd.append(key, obj[prop])
    	}
  	})

  	return fd
}

export default objectToFormData