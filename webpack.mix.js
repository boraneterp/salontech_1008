let mix = require('laravel-mix');

mix.sass('resources/assets/front/scss/style.scss', 'public/assets/css')
.scripts([
    'resources/assets/library/modernizr.min.js',
    'bower_components/jquery/dist/jquery.min.js',
    'bower_components/bootstrap/dist/js/bootstrap.min.js',
    'bower_components/vide/dist/jquery.vide.js',
    'bower_components/magnific-popup/dist/jquery.magnific-popup.min.js',
    'bower_components/wow/dist/wow.min.js',
    'bower_components/slick-carousel/slick/slick.js',
    'bower_components/jquery.scrollTo/jquery.scrollTo.js',
    'bower_components/jquery-bar-rating/jquery.barrating.js',
    'bower_components/jquery-form/src/jquery.form.js',
    'bower_components/jquery-validation/dist/jquery.validate.min.js',
    'bower_components/matchHeight/dist/jquery.matchHeight-min.js',
    'bower_components/js-cookie/src/js.cookie.js',
    'bower_components/tinymce/tinymce.min.js',
    'resources/assets/library/jquery-mask/jquery.mask.js'
], 'public/assets/js/vendor.js')
.react('resources/assets/front/js/app.js', 'public/assets/js').sourceMaps()
.react('resources/assets/admin/app.js', 'public/assets/admin/js').sourceMaps();
