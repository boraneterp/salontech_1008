<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('/.well-known/pk-validation/fileauth.txt',function(){
    return 'cw0rhmlhy4zhnt2zhqt6k5yfg5f3wb3f';
});


Route::get('/', [
    'uses' => 'PageController@index'
]);

// Route::get('/migration', [
//     'uses' => 'PageController@migration'
// ]);

Route::get('/google9fa0c1ed3cd7bc45.html', [
    'uses' => 'PageController@googleVerify'
]);

Route::post('/subscribe', [
    'uses' => 'PageController@subscribe'
]);

Route::get('/about-us', [
    'uses' => 'PageController@about'
]);

Route::get('/showcase', [
    'uses' => 'PageController@showcase'
]);

Route::get('/shipping-policy', [
    'uses' => 'PageController@shipping_policy'
]);

Route::get('/return-policy', [
    'uses' => 'PageController@return_policy'
]);

Route::get('/terms-and-conditions', [
    'uses' => 'PageController@terms_and_conditions'
]);

Route::get('/privacy-policy', [
    'uses' => 'PageController@privacy_policy'
]);

Route::get('/faqs', [
    'uses' => 'PageController@faqs'
]);

Route::get('/store-finder', [
    'uses' => 'PageController@store_finder'
]);

Route::get('/warranty', [
    'uses' => 'PageController@warranty'
]);

Route::post('/warranty', [
    'uses' => 'PageController@submitWarranty'
]);

Route::get('/contact-us', [
    'as' => '/contact-us',
    'uses' => 'PageController@contact_us'
]);

Route::post('/contact-us', [
    'uses' => 'PageController@contact'
]);

Route::get('/states.json', [
    'uses' => 'PageController@states'
]);

Route::get('/login', 'Auth\LoginController@getLogin');
Route::post('/login', 'Auth\LoginController@postLogin');
Route::get('register', 'Auth\RegisterController@getRegister');
Route::post('register', 'Auth\RegisterController@postRegister');
Route::post('check-email', 'Auth\RegisterController@checkEmail');
Route::post('/forgot_password', 'Auth\ForgotPasswordController@forgotPassword');
Route::get('/reset_password/{token}', 'Auth\ResetPasswordController@resetPassword');
Route::post('/set_password', 'Auth\ResetPasswordController@savePassword');
Route::get('logout', 'Auth\LoginController@logout');

Route::get('/search', 'ProductController@search');

Route::get('/lookbooks/{url}', [
    'uses' => 'LookbookController@detail'
])->where(['url' => '[a-z-]+']);


Route::get('/cart.json', [
    'uses' => 'CartController@getCart'
]);

Route::post('/shopping-cart/product/add', [
    'uses' => 'CartController@add'
]);

Route::post('/shopping-cart/product/update', [
    'uses' => 'CartController@update'
]);

Route::post('/shopping-cart/product/remove', [
    'uses' => 'CartController@remove'
]);

Route::get('/checkout/cart', [
    'uses' => 'CartController@index'
]);

Route::post('/address/save', [
    'uses' => 'AccountController@saveAddress'
]);

Route::get('/category/{id}/products.json', [
    'uses' => 'ProductController@getProductsByCategory'
]);

Route::get('/checkout/shipping', [
    'uses' => 'CheckoutController@shipping'
]);

Route::post('/checkout/shipping', [
    'uses' => 'CheckoutController@postShipping'
]);

Route::get('/checkout/paypal', [
    'uses' => 'CheckoutController@paypal'
]);

Route::get('/checkout/paypal/review', [
    'uses' => 'CheckoutController@paypalReview'
]);

Route::post('/checkout/billing', [
    'uses' => 'CheckoutController@postBillingAddress'
]);

Route::get('/checkout/payment', [
    'uses' => 'CheckoutController@payment'
]);

Route::post('/checkout/payment/coupon/apply', [
    'uses' => 'CheckoutController@applyCoupon'
]);

Route::post('/checkout/payment/coupon/cancel', [
    'uses' => 'CheckoutController@cancelCoupon'
]);

Route::post('/checkout/payment', [
    'uses' => 'CheckoutController@postPayment'
]);

Route::post('/checkout/', [
    'uses' => 'CheckoutController@checkout'
]);

Route::get('/checkout/quotes.json', [
    'uses' => 'CheckoutController@shippingQuote'
]);

Route::get('/checkout/review', [
    'uses' => 'CheckoutController@review'
]);

Route::post('/checkout/confirm', [
    'uses' => 'CheckoutController@confirm'
]);

Route::get('/checkout/success', [
    'uses' => 'CheckoutController@success'
]);

Route::post('/account/address/save', [
    'uses' => 'AccountController@saveAddress'
]);

Route::post('/account/payment/save', [
    'uses' => 'AccountController@savePayment'
]);

Route::group(['middleware' => 'customer'], function(){

    Route::post('/products/review', [
        'uses' => 'ProductController@postReview'
    ]);

    Route::group(['prefix' => 'account'], function(){

        Route::get('/', [
            'uses' => 'AccountController@account'
        ]);

        Route::get('/user.json', [
            'uses' => 'AccountController@getUser'
        ]);

        Route::post('/address/remove', [
            'uses' => 'AccountController@deleteAddress'
        ]);

        Route::post('/payment/remove', [
            'uses' => 'AccountController@deletePayment'
        ]);

        Route::post('/profile/save', [
            'uses' => 'AccountController@saveProfile'
        ]);

        Route::post('/password/change', [
            'uses' => 'AccountController@changePassword'
        ]);

        Route::get('{catchall}', [
            'uses' => 'AccountController@account'
        ])->where('catchall', '(.*)');
        

        // Route::get('/orders', [
        //     'uses' => 'AccountController@orders'
        // ]);

        // Route::get('/rewards', [
        //     'uses' => 'AccountController@rewards'
        // ]);

        // Route::get('/order/{id}', [
        //     'uses' => 'AccountController@orderDetail'
        // ]);

        
        
    });
});

Route::group(['prefix' => getenv('ADMIN_PATH'), 'namespace' => 'Admin'], function () {
	Route::get('login', 'AdminAuthController@getLogin');
    Route::post('login', 'AdminAuthController@postLogin');
    Route::get('logout', 'AdminAuthController@logout');
    Route::group(['middleware' => 'admin'], function(){
        Route::get('/', [
            'uses' => 'DashboardController@dashboard'
        ]);

        Route::get('/dashboard/data.json', [
            'uses' => 'DashboardController@getDashboardReport'
        ]);

        Route::get('/badges.json', [
            'uses' => 'DashboardController@getBadges'
        ]);

        Route::post('/upload', [
            'uses' => 'DashboardController@upload'
        ]);

        Route::group(['prefix' => 'orders'], function(){
            Route::get('list.json', [
                'uses' => 'OrderController@list'
            ]);

            Route::get('/detail/{id}.json', [
                'uses' => 'OrderController@detail'
            ]);

            Route::post('/{id}/status', [
                'uses' => 'OrderController@updateStatus'
            ]);
        });

        Route::group(['prefix' => 'category'], function(){
            Route::get('list.json', [
                'uses' => 'CategoryController@list'
            ]);

            Route::get('/detail/{id}.json', [
                'uses' => 'CategoryController@detail'
            ]);

            Route::get('deepList.json', [
                'uses' => 'CategoryController@deepList'
            ]);

            Route::post('/save', [
                'uses' => 'CategoryController@save'
            ]);

            Route::post('/delete', [
                'uses' => 'CategoryController@delete'
            ]);
        });

        Route::group(['prefix' => 'products'], function(){

            Route::get('list.json', [
                'uses' => 'ProductController@list'
            ]);

            Route::get('search.json', [
                'uses' => 'ProductController@search'
            ]);

            Route::get('/detail/{id}.json', [
                'uses' => 'ProductController@detail'
            ]);
            
            Route::post('/save', [
                'uses' => 'ProductController@save'
            ]);

            Route::post('/delete', [
                'uses' => 'ProductController@delete'
            ]);
        });

        Route::group(['prefix' => 'lookbooks'], function(){

            Route::get('list.json', [
                'uses' => 'LookBookController@list'
            ]);

            Route::get('/detail/{id}.json', [
                'uses' => 'LookBookController@detail'
            ]);
            
            Route::post('/save', [
                'uses' => 'LookBookController@save'
            ]);

            Route::post('/delete', [
                'uses' => 'LookBookController@delete'
            ]);
        });

        Route::get('showcases.json', [
            'uses' => 'ShowcaseController@getShowcase'
        ]);

        Route::post('showcases/save', [
            'uses' => 'ShowcaseController@save'
        ]);

        Route::get('subscriptions.json', [
            'uses' => 'SubscriptionController@list'
        ]);

        Route::get('warranties.json', [
            'uses' => 'WarrantyController@list'
        ]);

        Route::post('/warranties/{id}/status', [
            'uses' => 'WarrantyController@updateStatus'
        ]);

        Route::group(['prefix' => 'reviews'], function(){

            Route::get('list.json', [
                'uses' => 'ReviewController@list'
            ]);

            Route::post('update', [
                'uses' => 'ReviewController@changeStatus'
            ]);

            Route::post('delete', [
                'uses' => 'ReviewController@delete'
            ]);
            
        });

        Route::group(['prefix' => 'customers'], function(){

            Route::get('list.json', [
                'uses' => 'CustomerController@list'
            ]);

            Route::get('/detail/{id}.json', [
                'uses' => 'CustomerController@detail'
            ]);
            
            Route::post('/change-type', [
                'uses' => 'CustomerController@changeType'
            ]);

            Route::post('/delete', [
                'uses' => 'CustomerController@delete'
            ]);
        });

        Route::group(['prefix' => 'settings'], function(){

            Route::get('list.json', [
                'uses' => 'SettingController@getSettings'
            ]);

            Route::post('save', [
                'uses' => 'SettingController@saveSettings'
            ]);

            Route::post('tax/save', [
                'uses' => 'SettingController@saveTaxRates'
            ]);

            Route::group(['prefix' => 'coupons'], function(){

                Route::get('list.json', [
                    'uses' => 'CouponController@list'
                ]);

                Route::get('/detail/{id}.json', [
                    'uses' => 'CouponController@detail'
                ]);
                
                Route::post('/save', [
                    'uses' => 'CouponController@save'
                ]);

                Route::post('/delete', [
                    'uses' => 'CouponController@delete'
                ]);
            });

            Route::group(['prefix' => 'stores'], function(){

                Route::get('list.json', [
                    'uses' => 'StoreController@list'
                ]);

                Route::get('/detail/{id}.json', [
                    'uses' => 'StoreController@detail'
                ]);
                
                Route::post('/save', [
                    'uses' => 'StoreController@save'
                ]);

                Route::post('/delete', [
                    'uses' => 'StoreController@delete'
                ]);
            });
        });

    	Route::get('{catchall}', [
            'uses' => 'DashboardController@dashboard'
        ])->where('catchall', '(.*)');
    });
});

Route::get('/{category}/{subCategory?}', [
    'uses' => 'ProductController@category'
])->where(['category' => '[a-z-]+']);


Route::get('/detail/{category}/{product}', [
    'uses' => 'ProductController@detail'
])->where(['category' => '[a-z-]+', 'product' => '[a-z-0-9]+']);



Route::get('/testList', [
    'uses' => 'TestController@list'
]);

Route::get('captcha-form-validation',array('as'=>'google.get-recaptcha-validation-form','uses'=>'FileController@getCaptchaForm')) ;
Route::post('captcha-form-validation',array('as'=>'google.post-recaptcha-validation','uses'=>'FileController@postCaptchaForm')) ;
