<?php

namespace App\Http\Middleware;

use Session;
use Closure;
use Auth;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Routing\ResponseFactory;

class CustomerAuthMiddleware
{
    protected $auth;
    protected $response;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth, ResponseFactory $response)
    {
        $this->auth = $auth;
        $this->response = $response;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check() && Auth::user()->status == "1") {
            return $next($request);
        }

        // if(starts_with(\Request::path(), "account")) {
        //     return \Redirect::to('/');
        // } 

        return \Redirect::to('/login');
    }
}
