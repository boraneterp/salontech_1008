<?php

namespace App\Http\Controllers\Auth;

use Auth;
use Input;
use App\Http\Controllers\Controller;
use App\Repositories\UserRepository;

class ResetPasswordController extends Controller
{
    protected $user_repository;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct(UserRepository $user_repository)
    {
        $this->user_repository = $user_repository;
    }

    public function resetPassword($token) {
        $user = $this->user_repository->getUserByPasswordToken($token);
        if(!$user) {
            abort(404);
        }

        $passwordToken = $token;
        $body_class = 'reset-password';
        return view("front.auth.password", compact('passwordToken', 'body_class'));
    }

    public function savePassword() {
        $validator = \Validator::make(\Input::all(), [
            'password' => 'required|min:8',
            'confirmation' => 'required|min:8',
            'passwordToken' => 'exists:users,password_token'
        ]);
        if ($validator->fails()) {
            return \Redirect::to(\URL::previous())
                ->withInput(\Input::all())
                ->withErrors($validator);
        }

        $password = $this->user_repository->getuserByPasswordToken(Input::get("passwordToken"));
        $password->password = bcrypt(\Input::get("password"));
        $password->password_token = "";
        $password->save();
        Auth::login($password, true);
        return \Redirect::to('/account');
    }
}
