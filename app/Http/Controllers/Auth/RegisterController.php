<?php

namespace App\Http\Controllers\Auth;

use Input;
use Auth;
use App\Models\Cart;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Repositories\UserRepository;
use App\Jobs\WelcomeEmail;

class RegisterController extends Controller
{

    protected $user_repository;

    public function __construct(UserRepository $user_repository) {
        $this->user_repository = $user_repository;
    }


    protected function getRegister() {
        if(Auth::check()) {
            return \Redirect::to('/account');
        }
        $body_class = "register";
        return view("front.auth.register", compact('body_class'));
    }

    public function checkEmail() {
        $user = $this->user_repository->getByEmail(Input::get('email'));
        return response()->json(empty($user));
    }

    protected function postRegister() {
        $validator = Validator::make(\Input::all(), [
            "type" => "required",
            "company" => "required_if:type,professional",
            "cosmetology_id" => "required_if:type,professional",
            "phone_number" => "required_if:type,professional",
            "email" => "required|email|unique:users|max:255",
            "first_name" => "required|max:255",
            "last_name" => "required|max:255",
            "password" => "required|min:8|max:255",
            "confirmation" => "required|min:8|max:255",
            "address_1" => "required_if:type,professional|max:255",
            "city" => "required_if:type,professional|max:100",
            "state" => "required_if:type,professional|max:10",
            "zip" => "required_if:type,professional|max:10"
        ]);
        if ($validator->fails()) {
            return response()->json($validator->messages(), 500);
        }

        $user = $this->user_repository->save(Input::all());
        if(getenv('APP_ENV') === 'prod' || getenv('APP_ENV') === 'dev') {
            dispatch(new WelcomeEmail($user));
        }
        Auth::login($user, true);
        Cart::where('token', \Session::get('_token'))->update(['user_id' => $user->id]);
        return response()->json("success");
    }

    
}
