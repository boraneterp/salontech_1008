<?php

namespace App\Http\Controllers\Auth;

use Input;
use App\Http\Controllers\Controller;
use App\Repositories\UserRepository;
use App\Jobs\ForgotPasswordEmail;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    protected $user_repository;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct(UserRepository $user_repository)
    {
        $this->user_repository = $user_repository;
    }

    public function forgotPassword() {
        $user = $this->user_repository->getByEmail(Input::get("email"));
        if(!$user) {
            abort(404);
        }

        $user->password_token = str_random(64);
        $user->save();
        if(getenv('APP_ENV') === 'prod' || getenv('APP_ENV') === 'dev') {
            dispatch(new ForgotPasswordEmail($user));
        }
        return response()->json("success");
    }
}
