<?php

namespace App\Http\Controllers\Auth;

use Input;
use Auth;
use Util;
use Redirect;
use App\Models\Cart;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        //$this->middleware('guest', ['except' => 'logout']);
    }

    public function getLogin() {
        if(Auth::check() && Auth::user()->status == '1') {
            return \Redirect::to('/account');
        }
        $body_class = "login";
        return view("front.auth.login", compact('body_class'));
    }

    public function postLogin(Request $request) {
        $rules = array(
            'email'    => 'required|max:255',
            'password' => 'required|min:8',
        );
        $validator = \Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return back()->withInput(Input::all())
                ->withErrors($validator);
        }

        if (Auth::attempt(['email' => Input::get('email'), 'password' => Input::get('password'), 'status' => '1'], true)) {
            $this->process_login();
            $redirectUrl = "/account";
            if(!empty(Input::get('redirectUrl')) && starts_with(Input::get('redirectUrl'), getenv('APP_URL'))) {
                $redirectUrl = Input::get('redirectUrl');
            }

            return Redirect::to($redirectUrl);
        }

        return back()->withInput(Input::all())
                ->withErrors("Your credential is not vaild, please try again.");
    }

    protected function process_login() {
        $user = Auth::user();
        if($user->type != "business") {
            Cart::where('token', \Session::get('_token'))->update(['user_id' => $user->id]);
        } else {
            Cart::where('token', \Session::get('_token'))->update(['user_id' => $user->id]);
            $items = Cart::where('user_id', $user->id)->get();
            // $group = Util::getSettingGroup('general');
            // $tax_rate = Util::getSetting($group, 'tax_rate');
            foreach ($items as $item) {
                $item->price = $item->product->currentPrice();
                //$item->tax = round($item->product->currentPrice() * $tax_rate * $item->quantity / 100, 2);
                $item->total_price = round($item->product->currentPrice() * $item->quantity, 2);
                $item->save();
            }
            
        }
    } 
}
