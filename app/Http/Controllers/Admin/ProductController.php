<?php

namespace App\Http\Controllers\Admin;

use Input;
use App\Http\Controllers\Controller;
use App\Repositories\ProductRepository;

class ProductController extends Controller {

	protected $product_repository;

	public function __construct(ProductRepository $product_repository) {
		$this->product_repository = $product_repository;
	}

    public function list() {
        $products = $this->product_repository->getList(Input::all());
        return response()->json($products);
    }

    public function search() {
        $products = $this->product_repository->searchProductByName(Input::get('keyword'));
        return response()->json($products);
    }

    public function detail($id = null) {
        $product = $this->product_repository->detail($id);
        return response()->json($product);
    }

    public function save() {
    	$validator = \Validator::make(Input::all(), [
		    "category_id" => "required",
            "item_no" => "required",
		    "product_name" => "required|max:100",
            "msrp" => "required",
            "price" => "required",
            "stock" => "required|max:10",
            "order" => "required"
		]);
		if ($validator->fails()) {
			return response()->json($validator->messages(), 401);
        }

        $product = $this->product_repository->save(Input::all());
        return response()->json($product);
    }

    public function delete() {
        $this->product_repository->deleteProduct(Input::get('id'));
        return response()->json("success");
    }

}
