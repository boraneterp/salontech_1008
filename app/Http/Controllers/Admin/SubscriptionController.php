<?php
namespace App\Http\Controllers\Admin;

use Input;
use App\Http\Controllers\Controller;
use App\Repositories\SubscriptionRepository;

class SubscriptionController extends Controller {

	protected $subscription_repository;

	public function __construct(SubscriptionRepository $subscription_repository) {
		$this->subscription_repository = $subscription_repository;
	}

    public function list() {
    	$subsciptions = $this->subscription_repository->getList(Input::all());
        return response()->json($subsciptions);
    }

}
