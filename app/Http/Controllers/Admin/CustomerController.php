<?php

namespace App\Http\Controllers\Admin;

use Input;
use App\Http\Controllers\Controller;
use App\Repositories\UserRepository;

class CustomerController extends Controller {

	protected $user_repository;

	public function __construct(UserRepository $user_repository) {
		$this->user_repository = $user_repository;
	}

    public function list() {
        $customers = $this->user_repository->getList(Input::all());
        return response()->json($customers);
    }

    public function detail($id = null) {
        $user = $this->user_repository->detail($id);
        return response()->json($user);
    }

    public function changeType() {
        $validator = \Validator::make(Input::all(), [
            "id" => "required",
            "type" => "required|max:100"
        ]);
        if ($validator->fails()) {
            return response()->json($validator->messages(), 401);
        }

        $this->user_repository->changeType(Input::all());
        $user = $this->user_repository->detail(Input::get('id'));
        return response()->json($user);
    }

    public function delete() {
        $this->user_repository->deleteUser(Input::get('id'));
        $customers = $this->user_repository->getList(Input::all());
        return response()->json($customers);
    }

}
