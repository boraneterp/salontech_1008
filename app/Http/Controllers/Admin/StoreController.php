<?php

namespace App\Http\Controllers\Admin;

use Input;
use App\Http\Controllers\Controller;
use App\Repositories\StoreRepository;

class StoreController extends Controller {

	protected $store_repository;

	public function __construct(StoreRepository $store_repository) {
		$this->store_repository = $store_repository;
	}

    public function list() {
        $stores = $this->store_repository->getList(Input::all());
        return response()->json($stores);
    }

    public function save() {
    	$validator = \Validator::make(Input::all(), [
		    "name" => "required",
            "address_1" => "required",
            "city" => "required",
            "state" => "required",
            "zip" => "required"
		]);
		if ($validator->fails()) {
			return response()->json($validator->messages(), 401);
        }

        $this->store_repository->save(Input::all());
        $stores = $this->store_repository->getList();
        return response()->json($stores);
    }

    public function saveOrder() {
        $this->store_repository->saveOrder(Input::all());
        return response()->json("success");
    }

    public function delete() {
        $this->store_repository->destroy(Input::get('id'));
        $stores = $this->store_repository->getList();
        return response()->json($stores);
    }

}
