<?php

namespace App\Http\Controllers\Admin;

use Input;
use App\Http\Controllers\Controller;
use App\Repositories\ShowcaseRepository;

class ShowcaseController extends Controller {

	protected $showcase_repository;

	public function __construct(ShowcaseRepository $showcase_repository) {
		$this->showcase_repository = $showcase_repository;
	}

    public function getShowcase() {
        $showcase = $this->showcase_repository->getShowcase();
        return response()->json($showcase);
    }

    public function save() {
    	$validator = \Validator::make(Input::all(), [
		    "description" => "required",
            "main_image" => "required"
		]);
		if ($validator->fails()) {
			return response()->json($validator->messages(), 401);
        }

        $showcase = $this->showcase_repository->save(Input::all());
        if(empty($showcase)) {
            return response()->json("error", 500);
        }
        return response()->json($showcase);
    }

}
