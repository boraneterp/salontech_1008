<?php

namespace App\Http\Controllers\Admin;
use Auth;
use Input;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\ReviewRepository;

class ReviewController extends Controller {

	protected $review_repository;

	public function __construct(ReviewRepository $review_repository) {
		$this->review_repository = $review_repository;
	}

	public function list() {
		$reviews = $this->review_repository->getList(Input::all());
		return response()->json($reviews);
	}

	public function changeStatus() {
		$this->review_repository->changeStatus(Input::get('id'));
		return response()->json("success");
	}

	public function delete() {
		$this->review_repository->delete(Input::get('id'));
		return response()->json("success");
	}
}