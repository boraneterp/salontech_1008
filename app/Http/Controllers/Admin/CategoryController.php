<?php

namespace App\Http\Controllers\Admin;

use Input;
use App\Http\Controllers\Controller;
use App\Repositories\CategoryRepository;

class CategoryController extends Controller {

	protected $category_repository;

	public function __construct(CategoryRepository $category_repository) {
		$this->category_repository = $category_repository;
	}

    public function list() {
        $categories = $this->category_repository->getList(Input::all());
        return response()->json($categories);
    }

    public function detail($id = null) {
        $category = $this->category_repository->detail($id);
        return response()->json($category);
    }

    public function deepList() {
        $categories = $this->category_repository->getDeepList();
        return response()->json($categories);
    }

    public function save() {
    	$validator = \Validator::make(Input::all(), [
		    "name" => "required",
            "seo_url" => "required"
		]);
		if ($validator->fails()) {
			return response()->json($validator->messages(), 401);
        }

        $category = $this->category_repository->save(Input::all());
        return response()->json($category);
    }

    public function saveOrder() {
        $this->category_repository->saveOrder(Input::all());
        return response()->json("success");
    }

    public function delete() {
        $this->category_repository->delete(Input::all());
        $categories = $this->category_repository->getList();
        return response()->json($categories);
    }

    /*mj edit* 1011*/
    public function index()
{
    $categories = Category::with('childrenCategories')->whereNull('parent')->get();
    return view('categories', compact('categories'));
}
    /**/

}
