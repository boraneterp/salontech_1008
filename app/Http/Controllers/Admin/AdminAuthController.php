<?php

namespace App\Http\Controllers\Admin;

use Validator;
use Session;
use Input;
use App\Models\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class AdminAuthController extends Controller {

    public function postLogin() {
        $admin = Admin::where('user_name', Input::get('username'))->first();
        if(!$admin || $admin->password != Input::get('password')) {
            return \Redirect::to(getenv('ADMIN_PATH')."/login")
                ->withInput(Input::all())
                ->withErrors('User Name or Password does not match!');
        } else {
            \Auth::guard('admin')->login($admin, true);
            return \Redirect::to(getenv('ADMIN_PATH').'/');
        }
    }
    
    public function getLogin(){
        if (Auth::guard('admin')->check()) {
            return \Redirect::to(getenv('ADMIN_PATH').'/');
        }
        return view('admin.login');
    }
    
    public function logout(){
        
        \Auth::guard('admin')->logout();  
        return \Redirect::to(getenv('ADMIN_PATH').'/login');
    }
}
