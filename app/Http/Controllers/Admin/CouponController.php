<?php

namespace App\Http\Controllers\Admin;

use Input;
use App\Http\Controllers\Controller;
use App\Repositories\CouponRepository;

class CouponController extends Controller {

	protected $coupon_repository;

	public function __construct(CouponRepository $coupon_repository) {
		$this->coupon_repository = $coupon_repository;
	}

    public function list() {
        $coupons = $this->coupon_repository->getList(Input::all());
        return response()->json($coupons);
    }

    public function save() {
    	$validator = \Validator::make(Input::all(), [
		    "coupon_code" => "required",
            "type" => "required",
            "value" => "required",
            "expire_date" => "required"
		]);
		if ($validator->fails()) {
			return response()->json($validator->messages(), 401);
        }

        $coupons = $this->coupon_repository->save(Input::all());
        return response()->json($coupons);
    }

    public function delete() {
    	$coupons = $this->coupon_repository->delete(Input::all());
        return response()->json($coupons);
    }

}
