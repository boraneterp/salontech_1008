<?php

namespace App\Http\Controllers\Admin;

use Input;
use App\Http\Controllers\Controller;
use App\Repositories\LookBookRepository;

class LookBookController extends Controller {

	protected $lookbook_repository;

	public function __construct(LookBookRepository $lookbook_repository) {
		$this->lookbook_repository = $lookbook_repository;
	}

    public function list() {
        $lookbooks = $this->lookbook_repository->getList(Input::all());
        return response()->json($lookbooks);
    }

    public function detail($id = null) {
        $lookbook = $this->lookbook_repository->detail($id);
        return response()->json($lookbook);
    }

    public function save() {
    	$validator = \Validator::make(Input::all(), [
		    "name" => "required",
            "image_src" => "required",
            "steps" => "min:1",
            "steps.*.description" => "required",
            "steps.*.image" => "required"
		]);
		if ($validator->fails()) {
			return response()->json($validator->messages(), 401);
        }

        $lookbook = $this->lookbook_repository->save(Input::all());
        if(empty($lookbook)) {
            return response()->json("error", 500);
        }
        return response()->json($lookbook);
    }

    public function delete() {
        $this->lookbook_repository->deleteProduct(Input::get('id'));
        return response()->json("success");
    }

}
