<?php

namespace App\Http\Controllers\Admin;

use Input;
use App\Http\Controllers\Controller;
use App\Repositories\SettingRepository;

class SettingController extends Controller {

	protected $setting_repository;

	public function __construct(SettingRepository $setting_repository) {
		$this->setting_repository = $setting_repository;
	}

    public function getSettings() {
        $settings = $this->setting_repository->getSettings();
        return response()->json($settings);
    }

    public function saveSettings() {
    	$validator = \Validator::make(Input::all(), [
		    "group" => "required"
		]);
		if ($validator->fails()) {
			return response()->json($validator->messages(), 400);
        }

        if(Input::get('group') === 'home') {
            $this->setting_repository->saveHomeSettings(Input::all());
        } else {
            $this->setting_repository->saveSettings(Input::all());
        }

        $settings = $this->setting_repository->getSettings();
        return response()->json($settings);
    }

    public function saveTaxRates() {
        $validator = \Validator::make(Input::all(), [
            "tax_rate" => "numeric"
        ]);
        if ($validator->fails()) {
            return response()->json("Invalid tax rate.", 400);
        }

        if (Input::get("tax_rate") >= 100) {
            return response()->json("Tax rate must be less than 100.", 400);
        }
        $states = $this->setting_repository->saveStateTax(Input::all());
        return response()->json($states);
    }

}
