<?php
namespace App\Http\Controllers\Admin;

use Input;
use App\Models\Order;
use App\Http\Controllers\Controller;
use App\Repositories\OrderRepository;

class OrderController extends Controller {

	protected $order_repository;

	public function __construct(OrderRepository $order_repository) {
		$this->order_repository = $order_repository;
	}

    public function list() {
        $orders = $this->order_repository->getList(Input::all());
        return response()->json($orders);
    }

    public function getPendingOrder() {
        $orders = $this->order_repository->getPendingOrders();
        return response()->json($orders);
    }

    public function getOrder() {
        $response = $this->order_repository->getList(Input::all());
        return response()->json($response);
    }

    public function detail($id = null) {
        $order = $this->order_repository->detail($id);
        return response()->json($order);
    }

    public function updateStatus($id) {
        if(!$this->order_repository->updateStatus($id, Input::get('status'))) {
            return response()->json("fail", 400);
        }

        $order = $this->order_repository->detail($id);
        return response()->json($order);
    }

    public function refund() {
        if(!$this->order_repository->refund(Input::all())) {
            return response()->json("fail", 400);
        }
        return response()->json("success");
    }

    public function complete() {
        $this->order_repository->updateStatus(Input::get("id"), "C");
        return response()->json("success");
    }

}
