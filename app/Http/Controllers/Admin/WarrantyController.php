<?php
namespace App\Http\Controllers\Admin;

use Input;
use App\Http\Controllers\Controller;
use App\Repositories\WarrantyRepository;

class WarrantyController extends Controller {

	protected $warranty_repository;

	public function __construct(WarrantyRepository $warranty_repository) {
		$this->warranty_repository = $warranty_repository;
	}

    public function list() {
    	$warranties = $this->warranty_repository->getList(Input::all());
        return response()->json($warranties);
    }

    public function updateStatus($id) {
        if(!$this->warranty_repository->updateStatus($id, Input::get('status'))) {
            return response()->json("fail", 400);
        }
        return response()->json("success");
    }

}
