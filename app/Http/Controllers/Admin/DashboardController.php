<?php

namespace App\Http\Controllers\Admin;

use Input;
use App\Http\Controllers\Controller;
use App\Repositories\ReportRepository;

class DashboardController extends Controller {

	protected $report_repository;

	public function __construct(ReportRepository $report_repository) {
		$this->report_repository = $report_repository;
	}

    public function dashboard() {
        return view("admin.main");
    }

    public function getDashboardReport() {
    	$data = $this->report_repository->getDashboardReport();
        return response()->json($data);
    }

    public function getBadges() {
        $data = $this->report_repository->getBadges();
        return response()->json($data);
    }

    public function upload() {
        $validator = \Validator::make(Input::all(), [
            "file" => "image|mimes:jpeg,jpg,bmp,png,gif|max:2000"
        ]);
        if ($validator->fails()) {
            return response()->json($validator->messages(), 400);
        }
        $file = $this->report_repository->upload_to_S3(Input::all()['file']);
        return response()->json($file);
    }

}
