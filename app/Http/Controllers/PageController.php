<?php

namespace App\Http\Controllers;
use Auth;
use Input;
use Session;
use Util;
use Youtube;
use App\Models\State;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\ProductRepository;
use App\Repositories\LookBookRepository;
use App\Repositories\StoreRepository;
use App\Repositories\ShowcaseRepository;
use App\Repositories\WarrantyRepository;
use App\Models\Subscription;
use App\Jobs\ContactEmail;
use Illuminate\Support\Facades\URL;

class PageController extends Controller {

    protected $product_repository;

    protected $lookbook_repository;

    protected $store_repository;

    protected $showcase_repository;

    protected $warranty_repository;

    public function __construct(ProductRepository $product_repository, 
            LookBookRepository $lookbook_repository, 
            StoreRepository $store_repository, 
            ShowcaseRepository $showcase_repository,
            WarrantyRepository $warranty_repository) {
        $this->product_repository = $product_repository;
        $this->lookbook_repository = $lookbook_repository;
        $this->store_repository = $store_repository;
        $this->showcase_repository = $showcase_repository;
        $this->warranty_repository = $warranty_repository;
    }

	public function index() {
        if(getenv('APP_ENV') === 'prod') {
            $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') ? "https://" : "http://";
        
            if (substr($_SERVER['HTTP_HOST'], 0, 4) !== 'www.') {
                header('Location: '.$protocol.'www.'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
                exit;
            }
        }

        $homeSetting = json_decode(Util::getSettingGroup('home')->value);
        $instagram = Util::getInstagram();
        if(!empty($instagram)) {
            for($i = 0; $i < (sizeof($instagram['media']['nodes']) > 8? 8: sizeof($instagram['media']['nodes'])); $i++) {
                $product = null;
                if(array_key_exists('caption', $instagram['media']['nodes'][$i])) {
                    $product = $this->product_repository->getProductByHashtag($instagram['media']['nodes'][$i]['caption']);
                }
                $instagram['media']['nodes'][$i]['product'] = $product;
            }
        }
        $videos = []; //Youtube::listChannelVideos('UCmiA4wA5vt-Bx75c-kfyDRA', 2, 'date');
        $body_class = "homepage";
        $showcase = $this->showcase_repository->getShowcase();
        $lookbooks = $this->lookbook_repository->getFeaturedLookBooks();
        return view("front.home", compact('body_class', 'homeSetting', 'instagram', 'videos', 'showcase', 'lookbooks'));
	}

    //public function googleVerify() {
    //    return view("front.google");
    //}

    public function about() {
        return view("front.page.about");
    }

    public function showcase() {
        $showcase = $this->showcase_repository->getShowcase();
        return view("front.page.showcase", compact('showcase'));
    }

	public function shipping_policy() {
		return view("front.page.shipping");
	}

    public function return_policy() {
        return view("front.page.return");
    }

    public function terms_and_conditions() {
        return view("front.page.terms");
    }

    public function privacy_policy() {
        return view("front.page.privacy");
    }

    public function faqs() {
        return view("front.page.faqs");
    }

    public function subscribe() {
        $rules = array(
            "email_newsletter"   => "required|email|max:100"
        );
        $validator = \Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return response()->json($validator->messages(), 400);
        }

        $email = Input::get('email_newsletter');
        $subscription = Subscription::where("email", $email)->first();
        if(empty($subscription)) {
            $subscription = new Subscription;
            $subscription->email = $email;
            $subscription->save();
        }
        
        return response()->json("success");
    }

	public function store_finder() {
        $response = $this->store_repository->getStores(Input::get('zip'));
        $stores = $response['stores'];
        $zip = $response['zip'];
        $lat = $response['lat'];
        $lng = $response['lng'];
        $body_class = "store-finder";
		return view("front.page.store", compact('body_class', 'stores', 'zip', 'lat', 'lng'));
	}

    public function warranty() {
        $body_class = "warranty";
        return view("front.page.warranty", compact('body_class'));
    }

    public function submitWarranty() {

        $validator = \Validator::make(Input::all(), [
            "first_name"    => "required|max:45",
            "last_name"    => "required|max:45",
            "email"   => "required|email|max:100",
            "address_1"    => "required",
            "city"    => "required",
            "state"    => "required",
            "zip"    => "required",
            "category_id"    => "required",
            "product_id"    => "required",
            "proof_of_purchase"    => "required|mimes:jpeg,jpg,bmp,png,gif,pdf,docx,doc|max:2000",
            "reason"    => "required|max:500"
        ]);
        if ($validator->fails()) {
            return response()->json($validator->messages(), 401);
        }

        if(!$this->warranty_repository->submitWarranty(Input::all())) {
            return response()->json('error', 400);
        }
        return response()->json("success");
    }

    public function states() {
        $states = State::where("country", "US")->get();
        return response()->json($states);
    }

	public function contact_us() {
        $body_class = "contact";
		return view("front.page.contact", compact('body_class'));
	}

	public function contact() {
		$rules = array(
            "name"    => "required|max:45",
            "type"    => "required|max:45",
            "email"   => "required|email|max:100",
            "message"    => "required|regex:/^[\r\na-z A-Z0-9()%#@!':\/.?:,-]+$/u"
        );
        $validator = \Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return \Redirect::to("/contact-us")
                ->withInput(Input::all())
                ->withErrors($validator);
        }

        if(getenv('APP_ENV') === 'prod' || getenv('APP_ENV') === 'dev') {
            dispatch(new ContactEmail(Input::all()));
        }

		return \Redirect::to("/contact-us")->with('message', 'We\'ve receviced your message and will get back to you as soon as possible.');
	}
}