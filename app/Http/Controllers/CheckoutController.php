<?php

namespace App\Http\Controllers;
use Auth;
use Input;
use Session;
use Redirect;
use Util;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\UserRepository;
use App\Repositories\OrderRepository;
use App\Repositories\CartRepository;
use App\Repositories\ShippingService;
use App\Repositories\PaypalService;
use App\Models\Zip;
use App\Models\DeliveryLocation;

class CheckoutController extends Controller {

	protected $user_repository;

	protected $order_repository;

	protected $cart_repository;

    protected $shipping_service;

    protected $paypal_service;

	public function __construct(UserRepository $user_repository, 
			OrderRepository $order_repository,
			CartRepository $cart_repository,
            ShippingService $shipping_service,
            PaypalService $paypal_service) {
        $this->user_repository = $user_repository;
        $this->order_repository = $order_repository;
        $this->cart_repository = $cart_repository;
        $this->shipping_service = $shipping_service;
        $this->paypal_service = $paypal_service;
    }

    public function paypal() {
        $cart = $this->cart_repository->getCart();
        if(sizeof($cart['content']['items']) === 0) {
            return Redirect::to("/checkout/cart");
        }

        if(!Session::has('shipping_address')) {
            return Redirect::to("/checkout/shipping");
        }

        $shippingAddress = $this->user_repository->getUserAddress(Session::get('shipping_address'));
         if(empty($shippingAddress)) {
            return Redirect::to("/checkout/shipping");
        }

        $redirectUrl = $this->paypal_service->getRedirectUrl($cart, $shippingAddress);
        return Redirect::to( $redirectUrl );
    }

    public function shippingQuote() {
         $validator = \Validator::make(\Input::all(), [
            "address_id" => "required"
        ]);

        if ($validator->fails()) {
            return response()->json($validator->messages(), 400);
        }
        $address = $this->user_repository->getUserAddress(Input::get('address_id'));
        if(empty($address)) {
            return response()->json("Can't get quote!", 400);
        }
        $cart = $this->cart_repository->getCart();
        $quotes = $this->shipping_service->getQuotation($address, $cart['content']['weight']);
        return response()->json($quotes);
    }

	public function shipping() {

        $cart = $this->cart_repository->getCart();

        if(sizeof($cart['content']['items']) === 0) {
            return Redirect::to("/checkout/cart");
        }

        $body_class = "checkout";
        return view("front.checkout", compact('body_class'));
	}

    public function postShipping() {
        if(Auth::check() && Auth::user()->status == '1') {
            return $this->postUserShipping();
        } else {
            return $this->postGuestShipping();
        }
    }

    public function postGuestShipping() {
        $validator = \Validator::make(\Input::all(), [
            "email_address" => "required|email",
            "phone_number" => "required",
            "first_name" => "required",
            "last_name" => "required",
            "address_1" => "required",
            "city" => "required",
            "state" => "required",
            "zip" => "required",
        ]);

        if ($validator->fails()) {
            return response()->json($validator->messages(), 400);
        }

        $user = $this->user_repository->getByEmail(\Input::get('email_address'));
        if(!empty($user)) {
            return response()->json("user existed", 401);
        }

        if(!$this->user_repository->saveGuestAddress(Input::all())) {
            return response()->json("fail", 500);
        }

        $cart = $this->cart_repository->getCart();
        return response()->json($cart);
    }

    public function postUserShipping() {
        $validator = \Validator::make(\Input::all(), [
            "shipping_address" => "required"
        ]);

        if ($validator->fails()) {
            return response()->json($validator->messages(), 400);
        }

        $address = $this->user_repository->getUserAddress(Input::get('shipping_address'));
        if(empty($address)) {
            return response()->json("Address is not valid!", 400);
        }

        // $cart = $this->cart_repository->getCart();

        // $quotes = $this->shipping_service->getQuotation($address, $cart['content']['weight']);
        // if(!$this->cart_repository->setShippingMethod($quotes, Input::get('shiiping_method'))) {
        //     return response()->json("Shipping method is not valid!", 400);
        // }

        Session::put('shipping_address', Input::get('shipping_address'));

        $cart = $this->cart_repository->getCart();
        return response()->json($cart);
    }

	public function payment() {
        if(!Session::has('shipping_address')) {
            return Redirect::to("/checkout/shipping");
        }

        $cart = $this->cart_repository->getCart();

        if(sizeof($cart['content']['items']) === 0) {
            return Redirect::to("/checkout/cart");
        }

        $body_class = "checkout";
        return view("front.checkout", compact('body_class'));
	}

    public function applyCoupon() {
        $validator = \Validator::make(Input::all(), [
            "promotion_code" => "required"
        ]);

        if ($validator->fails()) {
            return response()->json($validator->messages(), 400);
        }

        if(!$this->cart_repository->applyCoupon(Input::get('promotion_code'))) {
            return response()->json("Code is invalid.", 400);
        }
        $cart = $this->cart_repository->getCart();
        return response()->json($cart);
    }

    public function cancelCoupon() {
        Session::forget('promotion_code');
        $cart = $this->cart_repository->getCart();
        return response()->json($cart);
    }

    public function postBillingAddress() {
        $validator = \Validator::make(\Input::all(), [
            "first_name" => "required",
            "last_name" => "required",
            "address_1" => "required",
            "city" => "required",
            "state" => "required",
            "zip" => "required",
        ]);

        if ($validator->fails()) {
            return response()->json($validator->messages(), 400);
        }

        $this->user_repository->saveGuestBillingAddress(Input::all());
        $cart = $this->cart_repository->getCart();
        return response()->json($cart);
    }

    public function postPayment() {
        $validator = \Validator::make(Input::all(), [
            "billing_address" => "required",
            "payment_id" => "required"
        ]);

        if ($validator->fails()) {
            return response()->json($validator->messages(), 400);
        }

        $this->cart_repository->setCardPayment(Input::all());
        $cart = $this->cart_repository->getCart();
        return response()->json($cart);
    }

    public function review(Request $request) {
        $id = $request->get('paymentId');
        if(!empty($id)) {
            $this->paypal_service->setPaypalPayment($id);
        }

        if(!Session::has('shipping_address')) {
            return Redirect::to("/checkout/shipping");
        }

        $paymentType = Session::get('payment_type');

        if($paymentType != 'card' && $paymentType != 'paypal') {
            return Redirect::to("/checkout/payment");
        }

        if($paymentType == 'card' && (!Session::has('payment_id') || !Session::has('billing_address'))) {
           return Redirect::to("/checkout/payment");
        }

        if($paymentType == 'paypal' && (!Session::has('paypal_payment_id') || !Session::has('paypal_payer'))) {
            return Redirect::to("/checkout/payment");
        }

        $cart = $this->cart_repository->getCart();

        if(sizeof($cart['content']['items']) === 0) {
            return Redirect::to("/checkout/cart");
        }

        $body_class = "checkout";
        return view("front.checkout", compact('body_class'));
    }

	public function confirm() {

        $paymentType = Session::get('payment_type');

        if(!Session::has('shipping_address')) {
            return response()->json("Please choose shipping address!", 400);
        }

        if($paymentType != 'card' && $paymentType != 'paypal') {
            return response()->json("Please choose payment type!", 400);
        }

        if($paymentType == 'card' && (!Session::has('payment_id') || !Session::has('billing_address'))) {
            return response()->json("Payment is not valid!", 400);
        }

        if($paymentType == 'paypal' && (!Session::has('paypal_payment_id') || !Session::has('paypal_payer'))) {
            return response()->json("Payment is not valid!", 400);
        }

        $cart = $this->cart_repository->getCart();

		if(sizeof($cart['content']['items']) === 0) {
			return response()->json("We can't process this transaction, please try again!", 400);
		}

        $error = $this->order_repository->checkout($cart);
        if ($error != '') {
            return response()->json($error, 400);
        }

        $cart = $this->cart_repository->getCart();
        return response()->json($cart);
    }

    public function success() {
        $body_class = "checkout";
    	return view("front.checkout", compact('body_class'));
    }
}