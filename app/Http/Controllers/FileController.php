<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FileController extends Controller
{
    //
    public function getCaptchaForm(){
        return view('files.captchaform');
    }
    public function postCaptchaForm(Request $request){
        $this->validate($request, [
            'first_name' => 'required',
            'last_name' => 'required',
            'email'=>'required|email',
            'phone'=>'required|numeric|digits:10',
            'password' => 'required',
            'confirmation' => 'required',
            'company' => 'required',
            'cosmetology_id' => 'required',
            'address_1' => 'required',
            //'address_2'
            'city' => 'required',
            'state' => 'required',
            'zip' => 'required',
            'g-recaptcha-response' => 'required',
        ]);
    }
}
