<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\CategoryRepository;
use App\Repositories\SecCategoryRepository;
use App\Repositories\ProductRepository;

class TestController extends Controller
{
   

    protected $sec_category_repository;
   
    protected $category_repository;

	protected $product_repository;


/* 
    public function __construct(SecCategoryRepository $sec_category_repository) {
        
        $this->sec_category_repository = $sec_category_repository;
	
    }

 */

   public function __construct(CategoryRepository $category_repository,ProductRepository $product_repository,SecCategoryRepository $sec_category_repository) {
       $this->category_repository = $category_repository;
       $this->product_repository = $product_repository;
       $this->sec_category_repository = $sec_category_repository;
    }


    public function list() {
        $sec_categories = $this->sec_category_repository->getList();
        return response()->json($sec_categories);
    }


}
