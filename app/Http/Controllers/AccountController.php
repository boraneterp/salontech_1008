<?php

namespace App\Http\Controllers;
use Auth;
use Input;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\UserRepository;

class AccountController extends Controller {

	protected $user_repository;

    public function __construct(UserRepository $user_repository) {
        $this->user_repository = $user_repository;
    }

    public function getUser() {
        $user = $this->user_repository->getCurrentUser();
        return response()->json($user);
    }

    public function account() {
        $body_class = "account";
    	return view("front.account", compact('body_class'));
    }

    // public function orders() {
    //     $response = $this->user_repository->getOrders(Auth::user()->id, Input::all(), false);
    //     $body_class = "account";
    //     return view("front.account", compact('body_class'));
    // }

    public function rewards() {
        $response = $this->user_repository->getRewards(Auth::user()->id, Input::all(), false);
        return response()->json($response);
    }

    // public function orderDetail($id) {
    //     $order = $this->user_repository->getOrderDetail($id);
    //     if(empty($order)) {
    //         abort(404);
    //     }
    //     return view("front.account.orderDetail", compact('order'));
    // }

    public function saveProfile() {
        $validator = \Validator::make(Input::all(), [
            "first_name" => "required",
            "last_name" => "required",
            "email" => "required"
        ]);
        if ($validator->fails()) {
            return response()->json($validator->messages(), 500);
        }
        $this->user_repository->saveProfile(Input::all());
        $user = $this->user_repository->getCurrentUser();
        return response()->json($user);
    }

    public function changePassword() {
        $validator = \Validator::make(Input::all(), [
            "current_password" => "required|min:8",
            'new_password' => 'required|min:8',
            'confirmation' => 'required|min:8'
        ]);
        if ($validator->fails()) {
            return response()->json($validator->messages(), 500);
        }

        if (!Auth::validate(array('email' => Auth::user()->email, 'password' => Input::get('current_password')))) {
            return response()->json("Your password is not correct.", 500);
        }

        $this->user_repository->changePassword(Input::all());
        return response()->json("success");
    }

    public function saveAddress() {
        $validator = \Validator::make(Input::all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'phone_number' => 'required',
            'address_1' => 'required',
            'city' => 'required',
            'state' => 'required',
            'zip' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json($validator->messages(), 500);
        }

        $address = $this->user_repository->saveAddress(Input::all());

        if(!$address) {
            return response()->json("You can't update this address.", 401);
        }

        return response()->json($address);
    }

    public function deleteAddress() {
        $this->user_repository->deleteAddress(Input::all());
        $user = $this->user_repository->getCurrentUser();
        return response()->json($user);
    }

    public function savePayment() {
        $validator = \Validator::make(Input::all(), [
            "card_holder_name" => "required|min:5",
            "card_number" => "required|min:13",
            "expire_month" => "required",
            "expire_year" => "required",
            "ccv" => "required|min:3"
        ]);
        if ($validator->fails()) {
            return response()->json($validator->messages(), 500);
        }

        if(!$this->user_repository->validateCreditcard(Input::get('card_number'))) {
            return response()->json("Credit Card Number is invalid!", 400);
        }

        $payment = $this->user_repository->savePayment(Input::all());

        if(!$payment) {
            return response()->json("You can't update this payment.", 401);
        }

        return response()->json($payment);
    }

    public function deletePayment() {
        $this->user_repository->deletePayment(Input::all());
        $user = $this->user_repository->getCurrentUser();
        return response()->json($user);
    }


}