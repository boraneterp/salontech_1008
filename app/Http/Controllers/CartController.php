<?php

namespace App\Http\Controllers;
use Auth;
use Input;
use Session;
use Redirect;
use Util;
use App\Models\DeliveryLocation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\CartRepository;

class CartController extends Controller {

	protected $cart_repository;

	public function __construct(CartRepository $cart_repository) {
        $this->cart_repository = $cart_repository;
    }

	public function index() {
        $body_class = "checkout";
        $cart = $this->cart_repository->getCart();
        return view("front.checkout", compact('body_class', 'cart'));
	}

	public function getCart() {
		$cart = $this->cart_repository->getCart();
		return response()->json($cart); 
	}

	public function add() {
		$validator = \Validator::make(\Input::all(), [
		    "id" => "required",
            "quantity" => "required"
		]);
		if ($validator->fails()) {
			return response()->json($validator->messages(), 400);
        }

        $cart = $this->cart_repository->addToCart(Input::all());
        if(!$cart) {
            return response()->json("fail", 401); 
        }

        return response()->json($cart);
	}

    public function update() {
        $validator = \Validator::make(\Input::all(), [
            "id" => "required",
            "quantity" => "required|numeric|min:1"
        ]);
        if ($validator->fails()) {
            return response()->json($validator->messages(), 400);
        }

        $cart = $this->cart_repository->updateCart(Input::all());
        if(!$cart) {
            return response()->json("fail", 401); 
        }

        return response()->json($cart);
    }

	public function remove() {
        $item = $this->cart_repository->getById(Input::get('id'));
        if(!$item->id) {
            return response()->json("fail", 400);
        }

        if (Auth::check()) {
            if($item->user_id != Auth::user()->id) {
                return response()->json("fail", 401);
            }
        } else {
            if($item->token != Session::get("_token")) {
                return response()->json("fail", 401);
            }
        }

        $cart = $this->cart_repository->removeCart(Input::get('id'));
        return response()->json($cart);
    }
}