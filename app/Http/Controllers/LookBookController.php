<?php

namespace App\Http\Controllers;
use Vinkla\Instagram\Instagram;
use Auth;
use Input;
use Session;
use Util;
use App\Models\State;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\LookBookRepository;
use App\Repositories\ProductRepository;

class LookbookController extends Controller {

    protected $lookbook_repository;

    protected $product_repository;

    public function __construct(LookBookRepository $lookbook_repository, ProductRepository $product_repository) {
        $this->lookbook_repository = $lookbook_repository;
        $this->product_repository = $product_repository;
    }

	public function detail($url = null) {
        $lookbook = $this->lookbook_repository->getDetailByUrl($url);
        if(empty($lookbook)) {
            abort(404);
        }
        $instagram = Util::getInstagram();
        if(!empty($instagram)) {
            for($i = 0; $i < (sizeof($instagram['media']['nodes']) > 8? 8: sizeof($instagram['media']['nodes'])); $i++) {
                $product = null;
                if(array_key_exists('caption', $instagram['media']['nodes'][$i])) {
                    $product = $this->product_repository->getProductByHashtag($instagram['media']['nodes'][$i]['caption']);
                }
                $instagram['media']['nodes'][$i]['product'] = $product;
            }
        }
        $others = $this->lookbook_repository->getOtherLookBooks($lookbook->id);
        $body_class = "lookbook-detail";
        return view("front.lookbook.detail", compact('body_class', 'lookbook', 'others', 'instagram'));
	}

	
}