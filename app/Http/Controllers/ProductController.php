<?php

namespace App\Http\Controllers;
use Auth;
use Input;
use Session;
use Paypal;
use Redirect;
use Util;
use Response;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\CategoryRepository;
use App\Repositories\SecCategoryRepository;
use App\Repositories\ProductRepository;

class ProductController extends Controller {

	protected $category_repository;

	protected $product_repository;

	protected $sec_category_repository;

    public function __construct(CategoryRepository $category_repository,ProductRepository $product_repository,SecCategoryRepository $sec_category_repository) {
		$this->category_repository = $category_repository;
		$this->product_repository = $product_repository;
		$this->sec_category_repository = $sec_category_repository;
	 }

    public function search() {
    	if(Input::get('keyword') == null || strlen(Input::get('keyword')) < 2) {
    		return \Redirect::to('/');
    	}
		$products = $this->product_repository->search(Input::all());
		$body_class = "product-finder";
		return Response::view("front.product.search", compact('products', 'body_class'))->header('Cache-Control', 'no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
	}

	public function category($categoryUrl = null, $subCategoryUrl = null) {
		
		$category = $this->category_repository->getCategoryByUrl($categoryUrl);
		
		$sub_category =array("id"=>"0","name"=>"No name");

		if(empty($category)) {
			abort(404);
		}

		if(empty($subCategoryUrl)){

			$products = $this->product_repository->searchCategory($category->id, Input::all());
			$body_class = "product-finder";
			return Response::view("front.product.category", compact('body_class', 'category', 'products','sub_category'))->header('Cache-Control', 'no-store, no-cache, must-revalidate, post-check=0, pre-check=0');

		}else{

			Log::info("subcategoryUrl:",[$subCategoryUrl]);

			$sec_category = $this->sec_category_repository->getSecCategoryByUrl($subCategoryUrl);
		
			if(empty($sec_category)) {
			  abort(404);
			 }
			 
			 $sub_category["id"] = $sec_category->id;
			 $sub_category["name"] = $sec_category->name;
			 $sub_category["parent_id"] = $sec_category->parent_id;

			$products = $this->product_repository->searchSecCategory($category->id,$sec_category->id,Input::all());
			
			Log::info($products);

			$body_class = "product-finder";
			return Response::view("front.product.category", compact('body_class', 'category', 'products','sub_category'))->header('Cache-Control', 'no-store, no-cache, must-revalidate, post-check=0, pre-check=0');


		}

		
	
	
	}
	/**sub cateogory */
	public function secCategory($categoryUrl = null,$subCategoryUrl = null) {

		Log::info('start secCategory');
		
		$category = $this->category_repository->getCategoryByUrl($categoryUrl);
		if(empty($category)) {
			abort(404);
		}

		$sec_category = $this->sec_category_repository->getSecCategoryByUrl($subCategoryUrl);
		if(empty($sec_category)) {
			abort(404);
		}

		Log::info('Parent Id.', ['id' => $category->id]);
		Log::info('Sub Category Id.', ['id' => $sec_category->id]);

		$products = $this->product_repository->searchSecCategory($category->id,$sec_category->id,Input::all());
		$body_class = "product-finder";
		return Response::view("front.product.category", compact('body_class', 'category', 'products'))->header('Cache-Control', 'no-store, no-cache, must-revalidate, post-check=0, pre-check=0');

	
	}
	





	public function sale() {
		$products = $this->product_repository->getSaleProducts(Input::all());
		$body_class = "product-finder";
		$white_background = true;
		$showCarousel = true;
		return Response::view("front.product.category", compact('category', 'products',  'body_class', 'white_background', 'showCarousel'))->header('Cache-Control', 'no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
	}

	public function detail($categoryUrl = null, $productUrl = null) {
		$category = $this->category_repository->getCategoryByUrl($categoryUrl, true);
		if(empty($category)) {
			abort(404);
		}
		$product = $this->product_repository->getProductByUrl($category->id, $productUrl);
		if(empty($product)) {
			abort(404);
		}

		// if($product->professional_only === "Y" && (!Auth::check() || Auth::user()->type != "business")) {
		// 	abort(404);
		// }

		$this->product_repository->addViewCount($product->id);
		$body_class = "product-detail";
		return Response::view("front.product.detail", compact('body_class', 'product'))->header('Cache-Control', 'no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
	}


	public function detail2($categoryUrl = null, $productUrl = null, $subCategoryUrl = null) {
		
		$category = $this->category_repository->getCategoryByUrl($categoryUrl, true);
		
		if(empty($category)) {
			abort(404);
		}

		$subCategory = $this->category_repository->getSubCategoryByUrl($category->id,$subCategoryUrl,true);

		if(empty($subCategory)) {
			abort(404);
		}


		$product = $this->product_repository->getProductByUrl2($category->id, $subCategory->id, $productUrl);
		
		if(empty($product)) {
			abort(404);
		}

		// if($product->professional_only === "Y" && (!Auth::check() || Auth::user()->type != "business")) {
		// 	abort(404);
		// }

		$this->product_repository->addViewCount($product->id);
		$body_class = "product-detail";
		return Response::view("front.product.detail", compact('body_class', 'product'))->header('Cache-Control', 'no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
	}





	public function getProductsByCategory($id) {
		$products = $this->product_repository->getProductsByCategory($id);
		return response()->json($products);
	}

	public function postReview() {
		$validator = \Validator::make(\Input::all(), [
			"product_id" => "required",
			"rating" => "required",
		    "review" => "required"
		]);
		if ($validator->fails()) {
			return response()->json($validator->messages(), 500);
        }
        $this->product_repository->postReview(Input::all());
		return response()->json("success");
	}
	
}