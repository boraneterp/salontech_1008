<?php

namespace App\Jobs;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ForgotPasswordEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 3;

    public $timeout = 120;

    protected $user;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $user = $this->user;
        $beautymail = app()->make(\Snowfire\Beautymail\Beautymail::class);
        try {
            $beautymail->send('emails.password', ['user' =>$user], function ($m) use ($user) {
                $m->from(getenv('MAIL_ADDRESS'), getenv('MAIL_USERNAME'))
                    ->to($user->email, $user->first_name.' '.$user->last_name)
                    ->subject(getenv('MAIL_USERNAME').' Password Reset');
            });
        } catch(\Exception $e) {
        }
    }
}
