<?php

namespace App\Jobs;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class WelcomeEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 3;

    public $timeout = 120;

    protected $user;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $user = $this->user;
        $beautymail = app()->make(\Snowfire\Beautymail\Beautymail::class);
        try {
            $beautymail->send('emails.welcome', ['user' =>$user], function ($m) use ($user) {
                $m->from(getenv('MAIL_ADDRESS'), getenv('MAIL_USERNAME'))
                    ->to($user->email, $user->first_name.' '.$user->last_name)
                    ->subject('Welcome to '.getenv('MAIL_USERNAME'));
            });
        } catch(\Exception $e) {
        }
    }
}
