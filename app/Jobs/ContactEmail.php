<?php

namespace App\Jobs;

use Auth;
use Util;
use Log;
use App\Models\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ContactEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 3;

    public $timeout = 120;

    protected $inputs;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($inputs)
    {
        $this->inputs = $inputs;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle() {

    	$group = Util::getSettingGroup('general');
    	$inputs = $this->inputs;
        $contact_email = Util::getSetting($group, 'contact_email');
        $beautymail = app()->make(\Snowfire\Beautymail\Beautymail::class);
        try {
            $beautymail->send('emails.contact', ['inputs' =>$inputs, 'contact_email' => $contact_email], function ($m) use ($inputs, $contact_email) {
                $m->from(getenv('MAIL_ADDRESS'), $inputs['name'])
                    ->to($contact_email, getenv('MAIL_USERNAME'))
                    ->subject(getenv('MAIL_USERNAME').' Contact from '.$inputs['name']);
            });
        } catch(\Exception $e) {
            \Log::error($e);
        }
    }
}
