<?php

namespace App\Jobs;

use Auth;
use App\Models\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class OrderConfirmationEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 3;

    public $timeout = 120;

    protected $order;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $order = $this->order;
        $beautymail = app()->make(\Snowfire\Beautymail\Beautymail::class);
        try {
            $beautymail->send('emails.order', ['order' =>$order], function ($m) use ($order) {
                $m->from(getenv('MAIL_ADDRESS'), getenv('MAIL_USERNAME'))
                    ->to($order->user->email, $order->shipingAddress()->first_name.' '.$order->shipingAddress()->last_name)
                    ->subject(getenv('MAIL_USERNAME').' Order Confirmation');
            });
        } catch(\Exception $e) {
            \Log::error($e);
        }
    }
}
