<?php

namespace App\Models;

use Util;
use DB;
use Illuminate\Database\Eloquent\Model;

class ProductReview extends Model {
    
    protected $table = 'product_reviews';

    protected $hidden = ['updated_at'];

    public function product() {
        return $this->belongsTo('App\Models\Product', 'product_id');
    }

    public function user() {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

}