<?php

namespace App\Models;

use Util;
use DB;
use Auth;
use Illuminate\Database\Eloquent\Model;

class Product extends Model {
    
    protected $table = 'products';

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    protected $appends = ['url', 'current_price'];

    public function category() {
        return $this->belongsTo('App\Models\Category', 'category_id');
    }

    public function sec_category() {
        return $this->belongsTo('App\Models\SecCategory', 'sub_category_id');
    }

    public function images() {
        return $this->hasMany('App\Models\ProductImage', 'product_id')->orderBy('order');
    }

    public function reviews() {
        return $this->hasMany('App\Models\ProductReview', 'product_id');
    }

    public function activeReviews() {
        return ProductReview::where("product_id", $this->id)->where("status", "1")->latest()->get();;
    }

    public function url() {
        return "/".$this->category->seo_url."/".$this->seo_url;
    }

    public function currentPrice() {
        //$promotion = $this->sale > 0? $this->sale : Util::getPromotion();
        $promotion = Util::getPromotion($this);
        if(Auth::user() && Auth::user()->status == "1" && Auth::user()->type == "business") {
            return $this->price * (100 - $promotion) / 100;
        }

        return $this->msrp * (100 - $promotion) / 100;
    }

    public function getUrlAttribute() {
        return $this->url();
    }

    public function getCurrentPriceAttribute() {
        return $this->currentPrice();
    }

    public function recommendation() {
        $bulider = Product::where('status', '1')->where('category_id', $this->category->id)->where('id', '<>', $this->id);
        if(!Auth::check() || Auth::user()->type != "business") {
            $bulider->where('professional_only', "N");
        }
        return $bulider->latest()->take(8)->get();
    }

}