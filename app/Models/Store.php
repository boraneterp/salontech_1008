<?php

namespace App\Models;

use Util;
use DB;
use Illuminate\Database\Eloquent\Model;

class Store extends Model {
    
    protected $table = 'stores';

    protected $appends = ['full_address'];

    public function getFullAddressAttribute() {
        return $this->fullAddress();
    }

    public function fullAddress() {
        return $this->address_1. (!empty($this->address_2)? ' '.$this->address_2: '').
            '<br />'.$this->city.', '.$this->state.' '.$this->zip;
    }

}