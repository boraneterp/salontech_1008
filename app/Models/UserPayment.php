<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserPayment extends Model {
    protected $table = 'user_payments';

    protected $hidden = ['created_at', 'updated_at'];
}
