<?php

namespace App\Models;

use Util;
use DB;
use Illuminate\Database\Eloquent\Model;

class Showcase extends Model {
    
    protected $table = 'showcases';

    public function linkUrl() {
    	return starts_with($this->link, 'http://')? $this->link: "http://".$this->link;
    }

}