<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderAddress extends Model {
	protected $table = 'order_addresses';

    protected $hidden = ['created_at', 'updated_at'];

    protected $appends = ['full_address'];

    public function getFullAddressAttribute() {
        return $this->fullAddress();
    }

	public function fullAddress() {
		if(empty($this->address_1)) {
			return "";
		}
        return $this->address_1. (!empty($this->address_2)? ' '.$this->address_2: '').
            '<br/>'.$this->city.', '.$this->state.' '.$this->zip;
    }

    public function getRawNumber() {
    	return "1".str_replace("(", "", str_replace(")", "", str_replace(" ", "", str_replace("-", "", $this->phone_number))));
    }
}