<?php

namespace App\Models;

use Auth;
use Illuminate\Database\Eloquent\Model;

class UserAddress extends Model {
    protected $table = 'user_addresses';

    protected $hidden = ['created_at', 'updated_at'];

    protected $appends = ['full_address'];

    public function getFullAddressAttribute() {
        return $this->fullAddress();
    }

    public function fullAddress() {
        return $this->address_1. (!empty($this->address_2)? ' '.$this->address_2: '').
            '<br />'.$this->city.', '.$this->state.' '.$this->zip;
    }
}
