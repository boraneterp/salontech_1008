<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class Admin extends Model implements AuthenticatableContract {
    use Authenticatable;

    protected $table = 'admins';
    protected $remember_token_name = 'remember_token';

    protected $fillable = ['first_name', 'last_name', 'username', 'password'];

    protected $hidden = array('password', 'remember_token');
    
}