<?php

namespace App\Models;

use Util;
use DB;
use Illuminate\Database\Eloquent\Model;

class Warranty extends Model {
    
    protected $table = 'warranties';

    protected $appends = ['full_address'];

    public function getFullAddressAttribute() {
        return $this->fullAddress();
    }

    public function product() {
        return $this->belongsTo('App\Models\Product', 'product_id');
    }

    public function fullAddress() {
		if(empty($this->address_1)) {
			return "";
		}
        return $this->address_1. (!empty($this->address_2)? ' '.$this->address_2: '').
            '<br/>'.$this->city.', '.$this->state.' '.$this->zip;
    }

}