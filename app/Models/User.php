<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'avatar', 'password', 'password_token', 'remember_token', 'updated_at'
    ];

    //protected $appends = ['user_addresses'];

    public function name() {
        return $this->first_name.' '.$this->last_name;
    }

    public function orders() {
        return $this->hasMany('App\Models\Order', 'user_id')->latest();
    }

    public function addresses() {
        return $this->hasMany('App\Models\UserAddress', 'user_id')->orderBy('updated_at', 'desc');
    }

    public function payments() {
        return $this->hasMany('App\Models\UserPayment', 'user_id')->latest();
    }

    public function recentAddress() {
        return UserAddress::where('user_id', $this->id)->orderBy('updated_at', 'desc')->first();
    }
}
