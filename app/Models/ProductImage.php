<?php

namespace App\Models;

use Util;
use DB;
use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model {
    
    protected $table = 'product_images';

    protected $hidden = ['created_at', 'updated_at'];

}