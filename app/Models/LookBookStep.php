<?php

namespace App\Models;

use Util;
use DB;
use Illuminate\Database\Eloquent\Model;

class LookBookStep extends Model {
    
    protected $table = 'look_book_steps';

    public function product() {
        return $this->belongsTo('App\Models\Product', 'product_id');
    }

}