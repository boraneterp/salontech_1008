<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderProduct extends Model {
	protected $table = 'order_products';

	public function detail() {
        return $this->belongsTo('App\Models\Product', 'product_id');
    }
}