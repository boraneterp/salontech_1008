<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SecCategory extends Model
{
    protected $table = 'sec_categories';

    protected $hidden = ['icon', 'status', 'created_at', 'updated_at'];

    public function products() {
        return $this->hasMany('App\Models\Product', 'sub_category_id')->orderBy('order', 'desc');
    }

    public function image() {
        return !empty($this->icon) ? $this->icon: '/assets/img/no_image.jpg';
    }

    public function children() {
    	return $this->hasMany('App\Models\SecCategory', 'parent_id')->orderBy('order');
    }
 
    public function getImageAttribute() {
        return $this->image();
    }


}
