<?php

namespace App\Models;

use Util;
use DB;
use Illuminate\Database\Eloquent\Model;

class Category extends Model {
    
    protected $table = 'categories';

    protected $hidden = ['icon', 'status', 'created_at', 'updated_at'];

    protected $appends = ['image'];

    public function parent() {
        if($this->parent_id === 0) {
            return $this;
        }
        return Category::where('id', $this->parent_id)->first();
    }

    public function products() {
        return $this->hasMany('App\Models\Product', 'category_id')->orderBy('order', 'desc');
    }

    public function image() {
        return !empty($this->icon) ? $this->icon: '/assets/img/no_image.jpg';
    }

    public function children() {
    	return $this->hasMany('App\Models\Category', 'parent_id')->orderBy('order');
    }

    public function subcategory() {
    	return $this->hasMany('App\Models\SecCategory', 'parent_id')->orderBy('order');
    }

    public function getImageAttribute() {
        return $this->image();
    }
 
}