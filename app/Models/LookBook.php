<?php

namespace App\Models;

use Util;
use DB;
use Illuminate\Database\Eloquent\Model;

class LookBook extends Model {
    
    protected $table = 'look_books';

    public function steps() {
        return $this->hasMany('App\Models\LookBookStep', 'look_book_id');
    }

}