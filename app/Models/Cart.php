<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model {
    protected $table = 'carts';

    protected $hidden = ['user_id', 'token', 'price', 'options', 'created_at', 'updated_at'];

    protected $appends = ['product'];

    public function user() {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function product() {
        return $this->belongsTo('App\Models\Product', 'product_id');
    }

    public function getProductAttribute() {
        return Product::where('id', $this->product_id)->first();
    }

    // public function getOptionItemsAttribute() {
    //     $options = json_decode($this->options);
    // 	$group_array = array(); 
    // 	if(sizeof($options) > 0) {
    // 		foreach ($options as $group_id => $option) {
    // 			$group = ProductOptionGroup::where('id', $group_id)->first();
    //             if(empty($group)) 
    //                 continue;
    // 			$group_items = array(
    // 				'id' => $group->id, 
    // 				'name' => $group->group_name 
    // 			); 
    // 			$item_array = array();
    // 			foreach ($option as $item) {
    // 				$item = ProductOption::where('id', $item)->first();
    // 				$item_array[] = $item;
    // 			}
    // 			$group_items['options'] = $item_array;
    // 			$group_array[] = $group_items;
	    		
	   //  	}
    // 	}
    // 	return $group_array;
    // }
}
