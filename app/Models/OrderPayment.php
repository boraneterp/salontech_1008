<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderPayment extends Model {
	protected $table = 'order_payments';

	protected $hidden = ['created_at', 'updated_at'];
}