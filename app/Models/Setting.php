<?php

namespace App\Models;

use Util;
use DB;
use Illuminate\Database\Eloquent\Model;

class Setting extends Model {

	protected $hidden = [
        'id', 'created_at', 'updated_at'
    ];
    
    protected $table = 'settings';

}