<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class State extends Model {
    protected $table = 'state_l';

    protected $fillable = ['name'];

    public static function get_name ($code) {
    	if (!empty($code)) {
    		$state = State::where('code', $code)->first();
    		return $state->name;
    	} else {
    		return '';
    	}
    	
    }
}
