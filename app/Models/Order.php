<?php

namespace App\Models;

use Util;
use DB;
use Illuminate\Database\Eloquent\Model;

class Order extends Model {
    
    protected $table = 'orders';

    public function user() {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function products() {
        return $this->hasMany('App\Models\OrderProduct', 'order_id');
    }

    public function statusHistories() {
        return $this->hasMany('App\Models\OrderStatusHistory', 'order_id')->latest();
    }

	public function addresses() {
        return $this->hasMany('App\Models\OrderAddress', 'order_id');
    }

	public function payment() {
        return $this->hasOne('App\Models\OrderPayment', 'order_id');
    }

    public function shipingAddress() {
        $shippingAddress = null;
        foreach ($this->addresses as $address) {
            if($address->type == 'S') {
                $shippingAddress = $address;
                break;
            }
        }

        return $shippingAddress;
    }

}