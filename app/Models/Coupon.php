<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model {
    protected $table = 'coupons';

    protected $hidden = ['created_at', 'updated_at'];

    public function products() {
        return $this->belongsToMany('App\Models\Product', 'coupon_product');
    }

    public function expired() {
    	return (strtotime($this->expire_date) - strtotime("now")) / 3600 <= -24;
    }
}
