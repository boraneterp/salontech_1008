<?php

namespace App\Helpers;
use Util;
use Log;
use App\Models\Zip;
use net\authorize\api\contract\v1 as AnetAPI;
use net\authorize\api\controller as AnetController;

class PaymentHelper {

	public static function chargeCreditCard($info) {
		
		// Credit Card Processing
        $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
		$merchantAuthentication->setName(getenv('AUTHORIZE_NET_API_ID'));
		$merchantAuthentication->setTransactionKey(getenv('AUTHORIZE_NET_TRANSACTION_KEY'));

		// Create the payment data for a credit card
		$creditCard = new AnetAPI\CreditCardType();
		$creditCard->setCardNumber($info['card_number']);
		$creditCard->setExpirationDate($info['expire_year']. "-".$info['expire_month']);
		$creditCard->setCardCode($info['ccv']);
		$paymentOne = new AnetAPI\PaymentType();
		$paymentOne->setCreditCard($creditCard);
		$orderInfo = new AnetAPI\OrderType();
		$orderInfo->setInvoiceNumber($info['invoice_id']);
		$customer = new AnetAPI\CustomerDataType();
		$customer->setType("individual");
		$customer->setId($info['user_id']);
		$customerAddress = new AnetAPI\CustomerAddressType();
		$customerAddress->setPhoneNumber($info['phone_number']);
		$customerAddress->setFirstName($info['first_name']);
		$customerAddress->setLastName($info['last_name']);
		$billingZip = Zip::where("zip", $info['zip'])->first();
		$customerAddress->setCity($billingZip->city);
		$customerAddress->setState($billingZip->state);
		$customerAddress->setZip($billingZip->zip);

		// Create a transaction
		$transactionRequestType = new AnetAPI\TransactionRequestType();
		$transactionRequestType->setTransactionType( "authCaptureTransaction"); 
		$transactionRequestType->setAmount($info['amount']);
		$transactionRequestType->setPayment($paymentOne);
		$transactionRequestType->setOrder($orderInfo);
		$transactionRequestType->setCustomer($customer);
		$transactionRequestType->setBillTo($customerAddress);

		$request = new AnetAPI\CreateTransactionRequest();
		$request->setMerchantAuthentication($merchantAuthentication);
		$request->setTransactionRequest( $transactionRequestType);
		$controller = new AnetController\CreateTransactionController($request);
		if(getenv('AUTHORIZE_NET_PRODUCTION') == "true") {
			$response = $controller->executeWithApiResponse( \net\authorize\api\constants\ANetEnvironment::PRODUCTION);
		} else {
			$response = $controller->executeWithApiResponse( \net\authorize\api\constants\ANetEnvironment::SANDBOX);
		}

		$transaction_id = '';
		if ($response != null) {
		    $tresponse = $response->getTransactionResponse();
		    if (($tresponse != null) && ($tresponse->getResponseCode()=="1") ) {
		    	$transaction_id = $tresponse->getTransId();
		    } 
		}

		return $transaction_id;
	}

    public static function getTransactionType($transactionId) {
        $merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
        $merchantAuthentication->setName(getenv('AUTHORIZE_NET_API_ID'));
        $merchantAuthentication->setTransactionKey(getenv('AUTHORIZE_NET_TRANSACTION_KEY'));

		$refId = 'ref' . time();

		$request = new AnetAPI\GetTransactionDetailsRequest();
		$request->setMerchantAuthentication($merchantAuthentication);
		$request->setTransId($transactionId);

		$controller = new AnetController\GetTransactionDetailsController($request);
		if(getenv('AUTHORIZE_NET_PRODUCTION') == "true") {
			$response = $controller->executeWithApiResponse( \net\authorize\api\constants\ANetEnvironment::PRODUCTION);
		} else {
			$response = $controller->executeWithApiResponse( \net\authorize\api\constants\ANetEnvironment::SANDBOX);
		}

		$status = '';
		if (($response != null) && ($response->getMessages()->getResultCode() == "Ok")) {
			$status = $response->getTransaction()->getTransactionStatus();
		} 

		return $status;
    }

    public static function voidTransaction($transactionId) {
    	Log::info("start voidTransaction: ".$transactionId);

		$merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
        $merchantAuthentication->setName(getenv('AUTHORIZE_NET_API_ID'));
        $merchantAuthentication->setTransactionKey(getenv('AUTHORIZE_NET_TRANSACTION_KEY'));
		
		$refId = 'ref' . time();
		//create a transaction
		$transactionRequestType = new AnetAPI\TransactionRequestType();
		$transactionRequestType->setTransactionType("voidTransaction"); 
		$transactionRequestType->setRefTransId($transactionId);
		$request = new AnetAPI\CreateTransactionRequest();
		$request->setMerchantAuthentication($merchantAuthentication);
		$request->setRefId($refId);
		$request->setTransactionRequest( $transactionRequestType);
		$controller = new AnetController\CreateTransactionController($request);
		if(getenv('AUTHORIZE_NET_PRODUCTION') == "true") {
			$response = $controller->executeWithApiResponse( \net\authorize\api\constants\ANetEnvironment::PRODUCTION);
		} else {
			$response = $controller->executeWithApiResponse( \net\authorize\api\constants\ANetEnvironment::SANDBOX);
		}

		$transaction_id = '';
		if ($response != null) {
      		if($response->getMessages()->getResultCode() == "Ok") {
        		$tresponse = $response->getTransactionResponse();
	      		if ($tresponse != null && $tresponse->getMessages() != null) {
	      			$transaction_id = $tresponse->getTransId();
        		}
      		}
    	}
    	
    	Log::info("end voidTransaction: ");
    	return $transaction_id;
    }

    public static function refundTransaction($order, $amount) {
    	Log::info("start refundTransaction: ".$order->id.", amount: ".$amount);
		$merchantAuthentication = new AnetAPI\MerchantAuthenticationType();
		$merchantAuthentication->setName(getenv('AUTHORIZE_NET_API_ID'));
		$merchantAuthentication->setTransactionKey(getenv('AUTHORIZE_NET_TRANSACTION_KEY'));

	  	$refId = 'ref' . time();

		// Create the payment data for a credit card
	    $creditCard = new AnetAPI\CreditCardType();
	    $creditCard->setCardNumber(substr($order->payment->card_number, -4));
	    $creditCard->setExpirationDate("XXXX");
	    $paymentOne = new AnetAPI\PaymentType();
	    $paymentOne->setCreditCard($creditCard);
    
    	//create a transaction
	    $transactionRequest = new AnetAPI\TransactionRequestType();
	    $transactionRequest->setTransactionType( "refundTransaction"); 
	    $transactionRequest->setAmount($amount);
	    $transactionRequest->setPayment($paymentOne);
	    $transactionRequest->setRefTransId($order->payment->transaction_id);
 

	    $request = new AnetAPI\CreateTransactionRequest();
	    $request->setMerchantAuthentication($merchantAuthentication);
	    $request->setRefId($refId);
	    $request->setTransactionRequest( $transactionRequest);
	    $controller = new AnetController\CreateTransactionController($request);
		if(getenv('AUTHORIZE_NET_PRODUCTION') == "true") {
			$response = $controller->executeWithApiResponse( \net\authorize\api\constants\ANetEnvironment::PRODUCTION);
		} else {
			$response = $controller->executeWithApiResponse( \net\authorize\api\constants\ANetEnvironment::SANDBOX);
		}

		$transaction_id = '';
	    if ($response != null) {
	      	if($response->getMessages()->getResultCode() == "Ok") {
	        	$tresponse = $response->getTransactionResponse();
		      	if ($tresponse != null && $tresponse->getMessages() != null) {
		      		$transaction_id = $tresponse->getTransId();
	        	} 
	      	}    
	    }

	    Log::info("end refundTransaction: ".$order->id.", amount: ".$amount);
	    return $transaction_id;
	}

}