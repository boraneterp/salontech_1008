<?php

namespace App\Helpers;
use Session;
use App\Models\State;
use App\Models\Category;
use App\Models\Cart;
use App\Models\Setting;
use App\Repositories\CartRepository;
use Illuminate\Support\Facades\Log;

class CommonHelper {

    public static function getCategories($children = false) {
        if($children) {
            
            $category = Category::where("status", '1')->with('products')->with('subcategory')->orderBy('order')->get();
           
            Log::info($category);

            return $category;
        }

        return Category::where("status", '1')->orderBy('order')->get();
    }

    public static function getStates() {
        return State::where("country", "US")->pluck("name", "code");
    }

    public static function getCategoryList($deep = false) {
        if($deep) {
            return Category::where('status', '1')->orderBy('order')->pluck("name", "id");
        } 
        return Category::where('status', '1')->where('parent_id', '0')->orderBy('order')->pluck("name", "id");
    }

    public static function getInstagram() {
        $client = new \GuzzleHttp\Client();
        $group = Self::getSettingGroup('general');
        $instagram_id = Self::getSetting($group, 'instagram_id');
        $user = null;
        try {
            $res = $client->request('GET', 'https://www.instagram.com/'.$instagram_id.'/?__a=1');
            $response = json_decode((string) $res->getBody(), true);
            $user = $response['user'];
        } catch (\Exception $e) {
        }
        return $user;
    }

    public static function getPromotion($product) {
        $productProm = Self::getProductPromotion($product);
        if($productProm > 0) {
            return $productProm;
        }
        return Self::getGlobalPromotion();
    }

    public static function getProductPromotion($product) {
        $promotion = 0;
        try {
            $now = strtotime("now");
            $start = Self::getDate($product->promotion_start);
            $end = Self::getDate($product->promotion_end, true);
            $value = $product->sale;
            if($now > $start && $now < $end) {
                $promotion = $value;
            }
        } catch(\Exception $e) {
        }
        return $promotion;
    }

    public static function getGlobalPromotion() {
        $group = Self::getSettingGroup('promotion');
        $now = strtotime("now");
        $promotion = 0;
        try {
            $start = Self::getDate(Self::getSetting($group, 'promotion_start'));
            $end = Self::getDate(Self::getSetting($group, 'promotion_end'), true);
            $value = Self::getSetting($group, 'value');
            if($now > $start && $now < $end) {
                $promotion = $value;
            }
        } catch(\Exception $e) {
        }
        
        return $promotion;
    }

    public static function getDate($str, $end = false) {
        $year = substr($str, 0, 4);
        $month = substr($str, 5, 2);
        $day = substr($str, 8, 2);
        $date = mktime(0, 0, 0, $month, $day, $year);
        if($end) {
            $date = mktime(23, 59, 59, $month, $day, $year);
        }
        return $date;
    }

    public static function getCart() {
        $cart_repository = new CartRepository(new Cart);
        return json_encode($cart_repository->getCart()); 
    }

    public static function getHeaderMessage() {
        $group = Self::getSettingGroup('general');
        $message = array('header_label' => Self::getSetting($group, 'header_label'),
            'header_message' => Self::getSetting($group, 'header_message'));
        return $message; 
    }

    public static function getShipping() {
        $group = Self::getSettingGroup('general');
        $shipping = array('free_shipping_amount' => Self::getSetting($group, 'free_shipping_amount'),
            'flat_fee' => Self::getSetting($group, 'flat_fee'));
        return json_encode($shipping); 
    }

    public static function hasPrice($type) {
        $hasPrice = false;
        if($type === 'marketplace' || $type === 'realty') {
            $hasPrice = true;
        } 
        return $hasPrice;
    }

    public static function getImageWithSuffix($imageUrl, $suffix = '') {
        if($suffix == '') {
            return $imageUrl;
        }

        $pos = strripos($imageUrl, ".");
        $fileName = substr($imageUrl, 0, $pos);
        if($suffix == '') {
            $fileName .= substr($imageUrl, $pos);
        } else {
            $fileName .= '-'.$suffix.substr($imageUrl, $pos);
        }

        return $fileName;
    }

    public static function getGreeding() {
        $greeding = 'Good Morning';
        $time = date("H");
        if ($time >= "12" && $time < "17") {
            $greeding = "Good Afternoon";
        } else if ($time >= "17" && $time < "19") {
            $greeding = "Good Evening";
        } else if ($time >= "19") {
            $greeding = "Good Night";
        }
        return $greeding;
    }

    public static function getAddress($withCountry = false) {
        if(!Session::has('zip'))
            return "";
        $address = Session::get("street") != ""? Session::get("street") : "";
        if($address != "" && Session::get("route") != "") {
             $address .= " ".Session::get("route");
        }

        if($address != "") {
            $address .= ", ";
        }
        $address .= Session::get("city").", ".Session::get("state")." ".Session::get("zip");
        if($withCountry) {
            $address .= ", United States";
        }
        return $address;
    }

    public static function formatNumber($number) {
    	if(  preg_match( '/^(\d{3})(\d{3})(\d{4})$/', $number,  $matches ) ) {
		    $result = "(".$matches[1] . ') ' .$matches[2] . '-' . $matches[3];
		    return $result;
		}
    	return $number;
    }

    public static function getYears($length) {
    	$years = array();
    	for($i = 0; $i < $length; $i++) {
    		$years[date('Y') - $i] = date('Y') - $i;
    	}
    	return $years;
    }

    public static function getMonths() {
    	$months = array();
    	for($i = 1; $i <= 12; $i++) {
    		$months[$i < 10? "0".$i: $i] = $i < 10? "0".$i: $i;
    	}
    	return $months;
    }

    public static function getDays() {
    	$days = array();
    	for($i = 1; $i <= 31; $i++) {
    		$days[$i] = $i < 10? "0".$i: $i;
    	}
    	return $days;
    }

    public static function getWeeks() {
        return array('Mon' => 'Monday', 'Tue' => 'Tuesday', 'Wed' => 'Wednesday', 'Thu' => 'Thursday', 'Fri' => 'Friday', 'Sat' => 'Saturday', 'Sun' => 'Sunday');
    }

    public static function getCreditCardYears() {
        $years = array();
        for($i = 0; $i < 10; $i++) {
            $years[date('Y') + $i] = date('Y') + $i;
        }
        return $years;
    }

    public static function stripSpecial($str) {
        return preg_replace('/[^a-zA-Z0-9 ,]/', '', $str);
    }

    public static function getRawNumber($number) {
        return "1".str_replace("(", "", str_replace(")", "", str_replace(" ", "", str_replace("-", "", $number))));
    }

    public static function getTimes() {
        return array('0' => '0:00AM', '0.25' => '0:15AM', '0.5' => '0:30AM', '0.75' => '0:45AM', '1' => '01:00AM', '1.25' => '01:15AM', '1.5' => '01:30AM', '1.75' => '01:45AM', '2' => '02:00AM', '2.25' => '02:15AM', '2.5' => '02:30AM', '2.75' => '02:45AM', '3' => '03:00AM', '3.25' => '03:15AM', '3.5' => '03:30AM', '3.75' => '03:45AM', '4' => '04:00AM', '4.25' => '04:15AM', '4.5' => '04:30AM', '4.75' => '04:45AM', '5' => '05:00AM', '5.25' => '05:15AM', '5.5' => '05:30AM', '5.75' => '05:45AM', '6' => '06:00AM', '6.25' => '06:15AM', '6.5' => '06:30AM', '6.75' => '06:45AM', '7' => '07:00AM', '7.25' => '07:15AM', '7.5' => '07:30AM', '7.75' => '07:45AM', '8' => '08:00AM', '8.25' => '08:15AM', '8.5' => '08:30AM', '8.75' => '08:45AM', '9' => '09:00AM', '9.25' => '09:15AM', '9.5' => '09:30AM', '9.75' => '09:45AM', '10' => '10:00AM', '10.25' => '10:15AM', '10.5' => '10:30AM', '10.75' => '10:45AM', '11' => '11:00AM', '11.25' => '11:15AM', '11.5' => '11:30AM', '11.75' => '11:45AM', '12' => '12:00PM', '12.25' => '12:15PM', '12.5' => '12:30PM', '12.75' => '12:45AM', '13' => '01:00PM', '13.25' => '01:15PM', '13.5' => '01:30PM', '13.75' => '01:45PM', '14' => '02:00PM', '14.25' => '02:15PM', '14.5' => '02:30PM', '14.75' => '02:45PM', '15' => '03:00PM', '15.25' => '03:15PM', '15.5' => '03:30PM', '15.75' => '03:45PM', '16' => '04:00PM', '16.25' => '04:15PM', '16.5' => '04:30PM', '16.75' => '04:45PM', '17' => '05:00PM', '17.25' => '05:15PM', '17.5' => '05:30PM', '17.75' => '05:45PM', '18' => '06:00PM', '18.25' => '06:15PM', '18.5' => '06:30PM', '18.75' => '06:45PM', '19' => '07:00PM', '19.25' => '07:15PM', '19.5' => '07:30PM', '19.75' => '07:45PM', '20' => '08:00PM', '20.25' => '08:15PM', '20.5' => '08:30PM', '20.75' => '08:45PM', '21' => '09:00PM', '21.25' => '09:15PM', '21.5' => '09:30PM', '21.75' => '09:45PM', '22' => '10:00PM', '22.25' => '10:15PM', '22.5' => '10:30PM', '22.75' => '10:45PM', '23' => '11:00PM', '23.25' => '11:15PM', '23.5' => '11:30PM', '23.75' => '11:45PM');
    }

    public static function getSetting($group = null, $code = '') {
        if(empty($group))
            return '';
        $setting = json_decode($group->value, true);
        if(!array_key_exists($code, $setting)) {
            return '';
        }

        return $setting[$code];
    }

    public static function getSettingGroup($group = null) {
        if($group == null)
            return "";
        return Setting::where('group', $group)->first();
    }

    public static function setSetting($code = '', $value = '') {
        $setting = Setting::where('code', $code)->first();
        if(!empty($setting)) {
            $setting->value = $value;
            $setting->save();
        } else {
            $setting = new Setting;
            $setting->code = $code;
            $setting->value = $value;
            $setting->save();
        }
    }

}