<?php
namespace App\Repositories;

use App\Models\Setting;
use App\Models\State;

class SettingRepository extends BaseRepository {

	public function __construct(Setting $setting) {
		$this->model = $setting;
	}

	private function queryList() {
		return $this->model->select('*');
	}

	public function getSettings() {
		$list = $this->model->get();
		$settings = array(
		);
		foreach ($list as $setting) {
			$settings[$setting->group] = json_decode($setting->value);
		}
		return $settings;
	}

	public function saveSettings($inputs) {
		$setting = null;
		$ary = array();
		foreach ($inputs as $key => $value) {
			if($key === 'group') {
				$setting = $this->model->where('group', $value)->first();
			} else {
				$ary[$key] = $value == null? '': $value;
			}
		}

		if($setting == null) {
			return;
		}

		$setting->value = json_encode($ary);
		$setting->save();
	}

	public function saveHomeSettings($inputs) {
		$setting = $this->model->where('group', 'home')->first();
		$value = json_decode($setting->value);
		$settingValue = array(
			'main_video' => isset($value->main_video)? $value->main_video: '', 
			'mobile_main_image' => isset($value->mobile_main_image)? $value->mobile_main_image: '',
			'featured_image_1' => isset($value->featured_image_1)? $value->featured_image_1: '', 
			'title_1' => array_key_exists('title_1', $inputs)? $inputs['title_1']: '',
			'link_1' => array_key_exists('link_1', $inputs)? $inputs['link_1']: '',
			'featured_image_2' => isset($value->featured_image_2)? $value->featured_image_2: '',
			'title_2' => array_key_exists('title_2', $inputs)? $inputs['title_2']: '',
			'link_2' => array_key_exists('link_2', $inputs)? $inputs['link_2']: '',
			'about_us_image' => isset($value->about_us_image)? $value->about_us_image: '',
			'about_us_link' => array_key_exists('about_us_link', $inputs)? $inputs['about_us_link']: '',
			'review_image' =>isset($value->review_image)? $value->review_image: '',
			'review_youtube_link' => array_key_exists('review_youtube_link', $inputs)? $inputs['review_youtube_link']: ''
		);

		if(array_key_exists("main_video", $inputs) && sizeof($inputs['main_video']) > 0 && is_file($inputs['main_video'][0])) {
			$main_video = $this->upload_to_S3($inputs['main_video'][0], 'settings'); 
			$settingValue['main_video'] = $main_video;
		}

		if(array_key_exists("mobile_main_image", $inputs) && sizeof($inputs['mobile_main_image']) > 0 && is_file($inputs['mobile_main_image'][0])) {
			$mobile_main_image = $this->upload_to_S3($inputs['mobile_main_image'][0], 'settings'); 
			$settingValue['mobile_main_image'] = $mobile_main_image;
		}

		if(array_key_exists("featured_image_1", $inputs) && sizeof($inputs['featured_image_1']) > 0 && is_file($inputs['featured_image_1'][0])) {
			$featured_image_1 = $this->upload_to_S3($inputs['featured_image_1'][0], 'settings'); 
			$settingValue['featured_image_1'] = $featured_image_1;
		}

		if(array_key_exists("featured_image_2", $inputs) && sizeof($inputs['featured_image_2']) > 0 && is_file($inputs['featured_image_2'][0])) {
			$featured_image_2 = $this->upload_to_S3($inputs['featured_image_2'][0], 'settings'); 
			$settingValue['featured_image_2'] = $featured_image_2;
		}

		if(array_key_exists("about_us_image", $inputs) && sizeof($inputs['about_us_image']) > 0 && is_file($inputs['about_us_image'][0])) {
			$about_us_image = $this->upload_to_S3($inputs['about_us_image'][0], 'settings'); 
			$settingValue['about_us_image'] = $about_us_image;
		}

		if(array_key_exists("review_image", $inputs) && sizeof($inputs['review_image']) > 0 && is_file($inputs['review_image'][0])) {
			$review_image = $this->upload_to_S3($inputs['review_image'][0], 'settings'); 
			$settingValue['review_image'] = $review_image;
		}

		$setting->value = json_encode($settingValue);
		$setting->save();

	}

	public function saveStateTax($input) {
		State::where('code', $input['code'])->update(['tax_rate' => $input['tax_rate']]);
		$states = State::where("country", "US")->get();
		return $states;
	}
}