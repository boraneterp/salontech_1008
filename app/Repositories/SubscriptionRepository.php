<?php
namespace App\Repositories;

use DB;
use Auth;
use GeoIP;
use App\Models\Subscription;

class SubscriptionRepository extends BaseRepository {

	public function __construct(Subscription $Subscription) {
		$this->model = $Subscription;
	}

	private function queryList() {
		return $this->model->select('*');
	}

	public function getList($inputs = null) {
		$builder = $this->queryList();
		$offset = !empty($inputs['offset'])? $inputs['offset']: 0;

		return $this->getRestResponse($builder, $offset);
	}
}