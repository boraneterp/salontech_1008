<?php

namespace App\Repositories;

use DB;
use Auth;
use Session;
use App\Models\User;
use App\Models\UserAddress;
use App\Models\UserPayment;
use App\Models\Cart;
use App\Models\Order;
use App\Models\OrderPayment;
use App\Models\OrderAddress;
use App\Models\OrderProduct;


class UserRepository extends BaseRepository
{

	/**
	 * Create a new UserRepository instance.
	 *
   	 * @param  App\Models\User $user
	 * @param  App\Models\Role $role
	 * @return void
	 */
	public function __construct(User $user)
	{
		$this->model = $user;
	}

	private function queryList() {
		return $this->model->select('*');
	}

	public function getList($inputs) {
		$builder = $this->queryList()->where('status', '1');
		if(!empty($inputs['email'])) {
			$builder->where('email', 'like', '%'.$inputs['email'].'%');
		}
		
		$withs = array('addresses', 'payments', 'orders.addresses', 'orders.payment', 'orders.products.detail');

		return $this->getRestResponse($builder, $inputs['offset'], $withs);
	}

	public function detail($id) {
		return $this->model->where('id', $id)->with('addresses', 'payments', 'orders.addresses', 'orders.payment', 'orders.products.detail')->first();
	}

	public function changeType($inputs) {
		$user = $this->model->find($inputs['id']);
		$user->type = $inputs['type'];
		$user->save();
	}

	public function getByUserName($username) {
		return $this->model->where('name', $username)->firstOrFail();
	}

	public function getByEmail($email) {
		return $this->model->where('email', $email)->where('status', '1')->first();
	}

	public function save($inputs) {
		$user = $this->model->where('email', $inputs['email'])->where('status', '1')->first();
		if(!empty($user)) {
			return null;
		}
		$user = new User;
		$user->type = $inputs['type'] == 'professional'? 'pending': 'personal';
		if($inputs['type'] === "professional") {
			$user->company = $inputs['company'];
			$user->cosmetology_id = $inputs['cosmetology_id'];
			$user->phone_number = $inputs['phone_number'];
		}
		$user->email = $inputs['email'];
		$user->first_name = $inputs['first_name'];
		$user->last_name = $inputs['last_name'];
		$user->password = bcrypt($inputs['password']);
		//$user->signup_token = str_random(64);
		$user->status = '1';
		$user->save();

		if($inputs['type'] === "professional") {
			$address = new UserAddress;
			$address->user_id = $user->id;
			$address->address_label = "Primary Address";
			$address->first_name = $inputs['first_name'];
			$address->last_name = $inputs['last_name'];
			$address->phone_number = $inputs['phone_number'];
			$address->address_1 = $inputs['address_1'];
			$address->address_2 = $inputs['address_2'];
			$address->city = $inputs['city'];
			$address->state = $inputs['state'];
			$address->zip = $inputs['zip'];
			$address->save();
		}
		return $user;
	}

	public function getCurrentUser() {
		if(!Auth::check())
			return null;
		return User::where('id', Auth::user()->id)->with('addresses', 'payments', 'orders.addresses', 'orders.payment', 'orders.products.detail')->first();
	}

	public function saveProfile($inputs) {
		$user = Auth::user();
		$user->first_name = $inputs['first_name'];
		$user->last_name = $inputs['last_name'];
		$user->email = $inputs['email'];
		$user->save();
	}

	public function changePassword($inputs) {
		$user = Auth::user();
		$user->password = bcrypt($inputs['new_password']);
		$user->save();
	}
	
	public function getUserByPasswordToken($token) {
		if(empty($token)) {
			return null;
		}
		return $this->model->where("password_token", $token)->where("status", "1")->first();
	}

	public function getAddress() {
		$address = null;
		if (Auth::check()) {
			$user = Auth::user();
			if(!empty($user->recentAddress())) {
				$address = $user->recentAddress();
			}
		}

		if($address == null) {
			$address = new UserAddress;
			if(Auth::check()) {
				$address->first_name = Auth::user()->first_name;
				$address->last_name = Auth::user()->last_name;
			}
		}

		return $address;
	}

	public function saveGuestAddress($inputs) {
		$this->authenticate($inputs);
		$address = null;
		if(array_key_exists('id', $inputs)) {
			$address = UserAddress::where("user_id", Auth::user()->id)->where("id", $inputs['id'])->first();
		}
		if(empty($address))
			$address = new UserAddress;
		$address->user_id = Auth::user()->id;
		$address->first_name = $inputs['first_name'];
		$address->last_name = $inputs['last_name'];
		$address->phone_number = $inputs['phone_number'];
		$address->address_1 = $inputs['address_1'];
		$address->address_2 = array_key_exists("address_2", $inputs) ? $inputs['address_2']: '';
		$address->city = $inputs['city'];
		$address->state = $inputs['state'];
		$address->zip = $inputs['zip'];
		$address->updated_at = \Carbon\Carbon::now()->toDateTimeString();
		$address->save();
		Session::put('shipping_address', $address->id);
		return true;
	}

	public function saveGuestBillingAddress($inputs) {
		$address = new UserAddress;
		$address->user_id = Auth::user()->id;
		$address->first_name = $inputs['first_name'];
		$address->last_name = $inputs['last_name'];
		$address->address_1 = $inputs['address_1'];
		$address->address_2 = array_key_exists("address_2", $inputs) ? $inputs['address_2']: '';
		$address->city = $inputs['city'];
		$address->state = $inputs['state'];
		$address->zip = $inputs['zip'];
		$address->updated_at = \Carbon\Carbon::now()->toDateTimeString();
		$address->save();
		Session::put('billing_address', $address->id);
		return true;
	}

	public function authenticate($inputs) {
		if (!Auth::check() || Auth::user()->status == "0") {
			$user = $this->model->where('email', $inputs['email_address'])->first();
			if(empty($user)) {
				$user = new User;
				$user->email = $inputs['email_address'];
				$user->first_name = $inputs['first_name'];
				$user->last_name = $inputs['last_name'];
				$user->save();
			} else {
				Cart::where('user_id', $user->id)->where('token', '<>', Session::get('_token'))->delete();
			}

			Cart::where('token', Session::get('_token'))->update(['user_id' => $user->id]);
			Auth::login($user, true);
		}

		return true;
	}

	public function getOrderDetail($orderId) {
		return Order::where('user_id', Auth::user()->id)->where('id', $orderId)->first();
	}

	public function getOrders($id, $inputs, $admin = true) {
		$start = 0;
		$draw = 1;
		if(!empty($inputs['start'])) {
			$start = $inputs['start'];
		}

		if(!empty($inputs['draw'])) {
			$draw = $inputs['draw'];
		}
		$orders = Order::where('user_id', $id)->with('payment')
				->offset($start)->limit(10)->latest()->get();
		$count = Order::where('user_id', $id)->count();
		$data = array();
		$orderUrl = $admin? getenv('ADMIN_PATH').'/order/detail/': '/account/order/';
		foreach ($orders as $key => $order) {
			$data[] = array($order->payment->payment_type == "reward"? 'Reward': $order->payment->transaction_id,
						"<b>$".sprintf('%01.2f', $order->charged)."</b>",
						date('Y-m-d', strtotime($order->created_at)),
						"<span class='order-status ".strtolower($order->status())."'>".$order->status()."</span>",
						'<a class="btn btn-view" href="'.$orderUrl.$order->id.'"><i class="zmdi zmdi-eye"></i></a>'
						 );
		}
		$respone = array(
			'draw' => $draw,
			'recordsTotal' => $count,
  			'recordsFiltered' => $count,
			'data' => $data
		);
		return $respone;
	}

	public function deleteUser($userId) {
		$user = $this->model->find($userId);
		DB::beginTransaction();
		$orders = Order::where('user_id', $userId)->get();
		foreach ($orders as $order) {
			OrderAddress::where('order_id', $order->id)->delete();
			OrderProduct::where('order_id', $order->id)->delete();
			OrderPayment::where('order_id', $order->id)->delete();
			$order->delete();
		}
		UserAddress::where('user_id', $user->id)->delete();
		UserPayment::where('user_id', $user->id)->delete();
        $user->delete();
		DB::commit();
	}

	public function getUserAddress($id) {
		return UserAddress::where("user_id", Auth::user()->id)->where("id", $id)->first();
	}

	public function saveAddress($inputs) {
		$address = UserAddress::where("user_id", Auth::user()->id)->where("id", $inputs['address_id'])->first();
		if(empty($address))
			$address = new UserAddress;
		$address->user_id = Auth::user()->id;
		//$address->address_label = $inputs['address_label'];
		$address->first_name = $inputs['first_name'];
		$address->last_name = $inputs['last_name'];
		$address->phone_number = $inputs['phone_number'];
		$address->address_1 = $inputs['address_1'];
		$address->address_2 = $inputs['address_2'];
		$address->city = $inputs['city'];
		$address->state = $inputs['state'];
		$address->zip = $inputs['zip'];
		$address->save();
		return $address;
	}

	public function deleteAddress($inputs) {
		$address = UserAddress::where("user_id", Auth::user()->id)->where("id", $inputs['address_id'])->first();
		if(empty($address))
			return false;
		$address->delete();
		return true;
	}

	public function savePayment($inputs) {
		$userPayment = UserPayment::where("user_id", Auth::user()->id)->where("id", $inputs['payment_id'])->first();
    	if(empty($userPayment)) {
    		$userPayment = new UserPayment;
    	}
    	$userPayment->user_id = Auth::user()->id;
    	$userPayment->card_label = "****".substr($inputs['card_number'], -4);
    	$userPayment->card_holder_name = $inputs['card_holder_name'];
        $userPayment->card_number = $inputs['card_number'];
        $userPayment->expire_month = $inputs['expire_month'];
        $userPayment->expire_year = $inputs['expire_year'];
        $userPayment->ccv = $inputs['ccv'];
        $userPayment->save();
		return $userPayment;
	}

	public function deletePayment($inputs) {
		$address = UserPayment::where("user_id", Auth::user()->id)->where("id", $inputs['payment_id'])->first();
		if(empty($address))
			return false;
		$address->delete();
		return true;
	}

}
