<?php
namespace App\Repositories;


use DB;
use Auth;
use Session;
use App\Models\SecCategory;
use App\Models\Product;


class SecCategoryRepository extends BaseRepository {

	public function __construct(SecCategory $sec_category) {
		$this->model = $sec_category;
	}
	

	private function queryList() {
		return $this->model->select('*');
	}

	public function getList($inputs = null) {
		$builder = $this->queryList();
		if(!empty($inputs['name'])) {
			$builder->where('name', 'like', '%'.$inputs['name'].'%');
		}
		return $builder
		    ->with('products')
			->orderBy('order')
			->paginate(50);
	}

	public function getSecCategoryByUrl($subCategoryUrl){
		$builder = $this->model->where('seo_url',$subCategoryUrl);
		return $builder->first();
	}




	
}