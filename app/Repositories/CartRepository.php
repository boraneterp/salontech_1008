<?php
namespace App\Repositories;

use Auth;
use DB;
use Session;
use Util;
use App\Models\User;
use App\Models\Cart;
use App\Models\State;
use App\Models\Product;
use App\Models\UserAddress;
use App\Models\DeliveryLocation;
use App\Models\ProductOption;
use App\Models\Coupon;

class CartRepository extends BaseRepository {

	public function __construct(Cart $cart) {
		$this->model = $cart;
	}

	public function addToCart($inputs) {

        $product = Product::find($inputs['id']);
        if(empty($product))
        	return false;

        if($product->professional_only === "Y" && (!Auth::check() || Auth::user()->type != "business")) {
        	return false;
        }

        $cart = $this->getCartProduct($product->id);
        $quantity = $inputs['quantity'];
        if(empty($cart)) {
        	$cart = new Cart;
        	$cart->user_id = Auth::check()? Auth::user()->id: 0;
        	$cart->token = Session::get('_token');
        	$cart->product_id = $product->id;
        	$cart->price = $product->currentPrice();
        	$cart->quantity = $quantity;
        } else {
        	$cart->quantity += $quantity;
        }

        //$cart->tax = round($product->currentPrice() * $tax_rate * $cart->quantity / 100, 2);
        $cart->weight = round($product->weight * $cart->quantity, 2);
		$cart->total_price = round($product->currentPrice() * $cart->quantity, 2);
		//$cart->total_price = round($product->currentPrice() * (1 + 7 / 100) * $cart->quantity, 2);
        $cart->save();
        $productAry = array(
			'image' => $product->image,
			'product_name' => $product->product_name,
			'price' => $product->currentPrice(),
			'quantity' => (int) $quantity,
			'total' => $product->currentPrice() * $quantity
		);

		$cartAry = $this->getCart();
		$cartAry['layerProduct'] = $productAry;
        return $cartAry;
	}

	public function updateCart($inputs) {
        $cart = $this->getCartItem($inputs['id']);
        if(empty($cart)) {
        	return false;
        }

        $group = Util::getSettingGroup('general');
        $tax_rate = Util::getSetting($group, 'tax_rate');

        $cart->quantity = $inputs['quantity'];
        //$cart->tax = round($cart->price * $tax_rate * $cart->quantity / 100, 2);
		$cart->total_price = round($cart->price * $cart->quantity, 2);
        $cart->save();
        return $this->getCart();
	}

	public function removeCart($id) {
		$this->model->destroy($id);
		Session::forget('shipping_address');
		Session::forget('billing_address');
		return $this->getCart();
	}

	public function getCart() {
		
		$items = $this->getCartItems();
		$sub_total = 0;
		$original_total = 0;
		$quantity = 0;
		$productIds = array();
		//$tax = 0;
		//$weight = 0;
		foreach ($items as $item) {
			$original_total += round(($item->product->msrp * $item->quantity), 2);
			$sub_total += round(($item->price * $item->quantity), 2);
			$quantity += $item->quantity;
			$productIds[] = $item->product_id;
			//$tax += $item->tax;
			//$weight += $item->weight;
		}

		$user = null;
		$defaultAddress = 0;
		$defaultPayment = 0;
		$guest_address = null;
		$billing_guest_address = null;
		$shipping_state = null;
		if(Auth::check()) {
			$user = User::where('id', Auth::user()->id)->with('addresses', 'payments')->first();
			if(sizeof($user->addresses) > 0) {
				if(Session::has('shipping_address')) {
					foreach ($user->addresses as $addr) {
						if($addr->id == Session::get('shipping_address')) {
							$defaultAddress = $addr->id;
							$guest_address = $addr;
							$guest_address->email_address = Auth::user()->email;
							$shipping_state = $addr->state;
						}
					}

				} else {
					$defaultAddress = $user->addresses[0]->id;
					$guest_address = $user->addresses[0];
					$guest_address->email_address = Auth::user()->email;
				}

				if(Session::has('billing_address')) {
					foreach ($user->addresses as $addr) {
						if($addr->id == Session::get('billing_address')) {
							$billing_guest_address = $addr;
						}
					}

				} else {
					$billing_guest_address = $user->addresses[0];
				}
			}

			if(sizeof($user->payments) > 0) {
				$defaultPayment = $user->payments[0]->id;
			}
		}

		$group = Util::getSettingGroup('general');
		$shipping_address = Session::has('shipping_address') ? Session::get('shipping_address') : $defaultAddress;
		//$shipping_method = Session::has('shipping_method') ? Session::get('shipping_method') : '';
		$shipping_price = $sub_total < Util::getSetting($group, 'free_shipping_amount') ? Util::getSetting($group, 'flat_fee') : 0;
		$payment_id = Session::has('payment_id') ? Session::get('payment_id') : $defaultPayment;
		$billing_address = Session::has('billing_address') ? Session::get('billing_address') : $defaultAddress;
		$shipping = array(
			'processed' => Session::has('shipping_address'),
			'shipping_address' => $shipping_address,
			'guest_address' => $guest_address
			// 'shipping_method' => $shipping_method,
			// 'shipping_price' => $shipping_price
		);

		$payment = array(
			'processed' => Session::has('payment_type'),
			'coupon' => Session::has('promotion_code') ? Session::get('promotion_code'): '',
			'type' => Session::get('payment_type'),
			'paypal_payment_id' => Session::get('paypal_payment_id'),
			'paypal_payer' => Session::get('paypal_payer'),
			'payment_id' => $payment_id,
			'billing_address' => $billing_address,
			'guest_address' => $billing_guest_address
		);

		$promotion = 0;
		$promotion_label = '';
		if(Session::has('promotion_code')) {
			$coupon = Coupon::where('coupon_code', Session::get('promotion_code'))->first();
			if(sizeof($coupon->products) > 0) {
				foreach ($items as $item) {
					foreach ($coupon->products as $product) {
						if($item->product_id == $product->id) {
							if($coupon->type == "V") {
								$promotion += $coupon->value * $item->quantity;
							} else {
								$promotion += $item->product->msrp * $item->quantity * $coupon->value / 100;
							}
						}
					}
				}

				$promotion_label = "Discount";

			} else {
				if($coupon->type == "V") {
					$promotion = $coupon->value;
					$promotion_label = "Discount $".$coupon->value." off";
				} else {
					$promotion = $original_total * $coupon->value / 100;
					$promotion_label = "Discount %".$coupon->value." off";
				}
			}
		}

		if($promotion > 0) {
			$sale_discount = $original_total - $sub_total;
			if($promotion > $sale_discount) {
				$promotion = $promotion - $sale_discount;
			} else {
				$promotion = 0;
			}
		}

		$tax = 0;
		if($shipping_state != null) {
			$state = State::where('code', $shipping_state)->first();
        	$tax_rate = $state->tax_rate;
			$tax = round(($sub_total - $promotion) * $tax_rate / 100, 2);
		}

		$totalAry = array(
			'items' => $items,
			'quantity' => $quantity,
			'sub_total' => sprintf('%01.2f', $sub_total),
			'promotion_label' => $promotion_label,
			'promotion' => sprintf('%01.2f', $promotion),
			'tax' => sprintf('%01.2f', $tax),
			'total' => sprintf('%01.2f', ($sub_total - $promotion + $tax + $shipping_price))
		);

		$cartAry = array(
			'is_guest' => (!Auth::check() || Auth()->user()->status != '1'),
			'content' => $totalAry,
			'user' => $user,
			'shipping' => $shipping,
			'payment' => $payment
		);
		
		return $cartAry;
	}

	public function setShippingMethod($quotes, $shipping_method) {
		if(sizeof($quotes) == 0) {
			return false;
		}

		$success = false;
		foreach ($quotes as $quote) {
			if($shipping_method == $quote['name']) {
				$success = true;
				Session::put('shipping_method', $quote['name']);
				Session::put('shipping_price', $quote['price']);
				break;
			}
		}

		return $success;
		
	}

	public function applyCoupon($coupon_code) {
		$coupon = Coupon::where("coupon_code", $coupon_code)->whereRaw("use_count < count")->first();
		if(empty($coupon) || $coupon->expired()) {
			return false;
		}

		Session::put('promotion_code', $coupon->coupon_code);
		return true;
	}

	public function setCardPayment($inputs) {
        if(Auth::user()->status == "1" || !Session::has('billing_address')) {
        	Session::put('billing_address', $inputs['billing_address']);
        }
        Session::put('payment_type', 'card');
        Session::put('payment_id', $inputs['payment_id']);
	}

	public function getCartItems() {
		if (Auth::check()) {
			$carts = $this->model->where("user_id", Auth::user()->id)->get();
		} else {
			$carts = $this->model->where("token", Session::get('_token'))->get();
		}

		return $carts;
	}

	public function getCartProduct($product_id) {
		if (Auth::check()) {
			$cart = $this->model->where("user_id", Auth::user()->id)->where("product_id", $product_id)->first();
		} else {
			$cart = $this->model->where("token", Session::get('_token'))->where("product_id", $product_id)->first();
		}
		return $cart;
	}

	public function getCartItem($item_id) {
		if (Auth::check()) {
			$cart = $this->model->where("user_id", Auth::user()->id)->where("id", $item_id)->first();
		} else {
			$cart = $this->model->where("token", Session::get('_token'))->where("id", $item_id)->first();
		}
		return $cart;
	}

	public function clear() {
		if (Auth::check()) {
			$carts = $this->model->where("user_id", Auth::user()->id)->delete();
		} else {
			$carts = $this->model->where("token", Session::get('_token'))->delete();
		}
	}

	public function getUserAddresses() {
		if(Auth::check()) {
			return Auth::user()->addresses;
		}
		return array();
	}

	public function getAddress() {
		$address = null;
		if (Auth::check()) {
			$user = Auth::user();
			if(!empty($user->recentAddress())) {
				$address = $user->recentAddress();
			}
		}

		if($address == null) {
			$address = new UserAddress;
			if(Auth::check()) {
				$address->first_name = Auth::user()->first_name;
				$address->last_name = Auth::user()->last_name;
			}
			$address->zip = Session::get('user_zip');
		}

		return $address;
	}
}