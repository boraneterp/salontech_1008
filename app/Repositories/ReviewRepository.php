<?php
namespace App\Repositories;

use DB;
use Auth;
use Session;
use App\Models\ProductReview;
use App\Models\Product;

class ReviewRepository extends BaseRepository {

	public function __construct(ProductReview $review) {
		$this->model = $review;
	}

	private function queryList() {
		return $this->model->select('*');
	}

	public function getList($inputs) {

		$builder = $this->queryList();
		if(!empty($inputs['status']) || $inputs['status'] == '0') {
			$builder->where('status', $inputs['status']);
		} 
		
		$withs = array('product', 'user');
		return $this->getRestResponse($builder, $inputs['offset'], $withs);
	}

	public function changeStatus($id) {
		$review = $this->getById($id);
		if($review->status == 0) {
			$review->status = 1;
		} else {
			$review->status = 0;
		}
		$review->save();
		$this->calculateProductReview($review->product->id);
	}

	public function delete($id) {
		$review = $this->getById($id);
		$product_id = $review->product->id;
		$this->destroy($id);
		$this->calculateProductReview($product_id);
	}

	public function calculateProductReview($product_id) {
		$product = Product::find($product_id);
		$results = DB::select('select IFNULL(AVG(rating), 0) as rating, COUNT(rating) as count from product_reviews 
		where product_id = :product_id 
		and status = 1', 
		['product_id' => $product->id]);
		$product->review_count = $results[0]->count;
		$product->review_rate = $results[0]->rating;
		$product->save();
	}
}