<?php
namespace App\Repositories;

use DB;
use Auth;
use Util;
use App\Models\Showcase;
use App\Models\Setting;

class ShowcaseRepository extends BaseRepository {

	public function __construct(Showcase $showcase) {
		$this->model = $showcase;
	}

	public function getShowcase() {
		$showcase = Util::getSettingGroup('showcase');
		$response = json_decode($showcase->value);
		$response->items = $this->model->orderBy('order', 'asc')->get();
		return $response;
	}

	public function save($inputs) {
		DB::beginTransaction();
		$setting = Setting::where('group', 'showcase')->first();
		$value = json_decode($setting->value);
		$settingValue = array(
			'main_image' => $value->main_image, 
			'link' => $inputs['link'], 
			'description' => $inputs['description']
		);

		if(array_key_exists("main_image", $inputs) && sizeof($inputs['main_image']) > 0 && is_file($inputs['main_image'][0])) {
			$main_image = $this->upload_to_S3($inputs['main_image'][0], 'showcases', 'main'); 
			$settingValue['main_image'] = $main_image;
		}

		$setting->value = json_encode($settingValue);
		if (!$setting->save()) {
			DB::rollback();
        	return null;
        }

        Showcase::truncate();
        if(array_key_exists("items", $inputs) && sizeof($inputs['items']) > 0) {
			foreach($inputs['items'] as $index => $item) {
				$showcase = new Showcase;
				$showcase->name = $item['name'];
				$showcase->link = array_key_exists('link', $item)? $item['link']: '';
				if(is_array($item['image'])) {
					$image = $this->upload_to_S3($item['image'][0], 'showcase'); 
					$showcase->image = $image;
				} else {
					$showcase->image = $item['image'];
				}
				$showcase->order = $index;
				if (!$showcase->save()) {
					DB::rollback();
		        	return null;
		        }
			}
		}

		DB::commit();
		return self::getShowcase();
	}

	public function getDetailByUrl($url) {
		return $this->model->where('seo_url', $url)->with('steps')->first();
	}
}