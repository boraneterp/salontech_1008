<?php
namespace App\Repositories;

use App\Jobs\ReviewRequestEmail;
use DB;
use Session;
use Auth;
use Payment;
use Util;
use Log;
use App\Repositories\PaypalService;
use App\Models\Cart;
use App\Models\Product;
use App\Models\Coupon;
use App\Models\Order;
use App\Models\OrderProduct;
use App\Models\UserAddress;
use App\Models\OrderAddress;
use App\Models\OrderPayment;
use App\Models\UserPayment;
use App\Models\OrderStatusHistory;
use App\Jobs\OrderConfirmationEmail;
use App\Jobs\OrderCompleteEmail;
//use App\Jobs\ReviewRequestEmail;
use App\Jobs\OrderRefundEmail;

class OrderRepository extends BaseRepository {

	protected $paypal_service;

	public function __construct(Order $order, PaypalService $paypal_service) {
		$this->model = $order;
		$this->paypal_service = $paypal_service;
	}

	private function queryList() {
		return $this->model->select('*');
	}

	public function getList($inputs) {
		$builder = $this->queryList();

		if(!empty($inputs['start_date'])) {
			$builder->whereDate('created_at', '>=', substr($inputs['start_date'], 0, 10));
		}

		if(!empty($inputs['end_date'])) {
			$builder->whereDate('created_at', '<=', substr($inputs['end_date'], 0, 10));
		}

		if(!empty($inputs['coupon'])) {
			$builder->where('coupon_code', 'like', '%'.$inputs['coupon'].'%');
		}

		if(!empty($inputs['status'])) {
			$builder->where('status', $inputs['status']);
		}

		$offset = $inputs['offset'];
		$withs = array('user', 'addresses', 'payment', 'statusHistories', 'products.detail');

		return $this->getRestResponse($builder, $offset, $withs);
	}

	public function detail($id) {
		return $this->model->where('id', $id)->with('user', 'addresses', 'payment', 'statusHistories', 'products.detail')->first();
	}

	public function checkout($cart) {

		DB::beginTransaction();

		$error = "We can't process this transaction, please try again!";

		$address = UserAddress::where('id', $cart['shipping']['shipping_address'])->where('user_id', Auth::user()->id)->first();
		if(empty($address)) {
			return "Shipping Address is invalid!";
		}

		if(Session::get('payment_type') == "card") {
			$payment = UserPayment::where('id', $cart['payment']['payment_id'])->where('user_id', Auth::user()->id)->first();
			if(empty($payment)) {
				return 'Payment is not valid!';
			}
		}

		$group = Util::getSettingGroup('general');
		$shipping_method = $cart['content']['sub_total'] < Util::getSetting($group, 'free_shipping_amount') ? 'Flat Shipping' : 'Free Shipping';
		$shipping_price = $cart['content']['sub_total'] < Util::getSetting($group, 'free_shipping_amount') ? Util::getSetting($group, 'flat_fee') : 0;

		$order = new Order;
		$order->user_id = Auth::user()->id;
		$order->is_professional = Auth::user()->type == "business"? 'Y': 'N';
		$order->sub_total = $cart['content']['sub_total'];
		$order->tax = $cart['content']['tax'];

		$order->shipping_method = $shipping_method;
		$order->shipping_price = $shipping_price;
		$charged = $cart['content']['total'];
		$order->order_total = $cart['content']['total'];
		if(Session::has('promotion_code')) {
			$coupon = Coupon::where('coupon_code', Session::get('promotion_code'))->first();
			if(!empty($coupon)) {
				$order->coupon_code = $coupon->coupon_code;
				$order->promotion = $cart['content']['promotion'];
				$coupon->use_count = $coupon->use_count + 1;
				$coupon->save();
			}
			
		}
		// $order->comments = $inputs['comments'];
		// if(Auth::user()->points >= 1000 && $inputs['used_points'] != "" 
		// 	&& $inputs['used_points'] <= Auth::user()->points 
		// 	&& $inputs['used_points'] % 10 == 0) {
		// 	$order->used_points = $inputs['used_points'];
		// 	$charged = $charged - $inputs['used_points'] / 100;
		// } 

		if($charged <= 0) {
			$charged = 0;
		}

		$order->charged = $charged;
		$order->content = json_encode($cart);;

		if (!$order->save()) {
			DB::rollback();
        	return $error;
        }

        // Order Items
        foreach($cart['content']['items'] as $item) {
        	$orderItem = new OrderProduct;
        	$orderItem->order_id = $order->id;
        	$orderItem->product_id = $item->product_id;
        	$orderItem->quantity = $item->quantity;
        	$orderItem->price = $item['product']['current_price'];
        	//$orderItem->tax = $item->tax;
        	//$orderItem->option_price = $item->option_price;
        	$orderItem->total_price = $item->total_price;
        	//$orderItem->options = $item->options;
        	//$orderItem->comments = $item->comments;
        	if (!$orderItem->save()) {
				DB::rollback();
	        	return $error;
	        }

	        $product = Product::find($item->product_id);
	        $product->sold = $product->sold + $item->quantity;
	        $product->stock = $product->stock - $item->quantity;
	        if (!$product->save()) {
				DB::rollback();
	        	return $error;
	        }
        }

		// Order Address
        $orderAddress = new OrderAddress;
        $orderAddress->order_id = $order->id;
        $orderAddress->type = 'S';
        $orderAddress->first_name = $address->first_name;
        $orderAddress->last_name = $address->last_name;
        $orderAddress->phone_number = $address->phone_number;
        $orderAddress->address_1 = $address->address_1;
        $orderAddress->address_2 = $address->address_2;
        $orderAddress->city = $address->city;
        $orderAddress->state = $address->state;
        $orderAddress->zip = $address->zip;

        if (!$orderAddress->save()) {
			DB::rollback();
        	return $error;
        }

        // Order Payment
        $orderPayment = new OrderPayment;
        $orderPayment->order_id = $order->id;
        $orderPayment->payment_type = Session::get('payment_type');
        if(Session::get('payment_type') == "card") {

        	$billingAddress = UserAddress::where('id', $cart['payment']['billing_address'])->where('user_id', Auth::user()->id)->first();

	        $orderAddress = new OrderAddress;
	        $orderAddress->order_id = $order->id;
	        $orderAddress->type = 'B';
	        $orderAddress->first_name = $billingAddress->first_name;
	        $orderAddress->last_name = $billingAddress->last_name;
	        $orderAddress->phone_number = $billingAddress->phone_number;
	        $orderAddress->address_1 = $billingAddress->address_1;
	        $orderAddress->address_2 = $billingAddress->address_2;
	        $orderAddress->city = $billingAddress->city;
	        $orderAddress->state = $billingAddress->state;
	        $orderAddress->zip = $billingAddress->zip;

	        if (!$orderAddress->save()) {
				DB::rollback();
	        	return $error;
	        }


	        $orderPayment->card_holder_name = $payment->card_holder_name;
	        $orderPayment->card_number = $payment->card_number;
	        $orderPayment->expire_month = $payment->expire_month;
	        $orderPayment->expire_year = $payment->expire_year;
	        $orderPayment->ccv = $payment->ccv;

        	$info = array(
	    		'card_number' => $payment->card_number, 
	    		'expire_year' => $payment->expire_year, 
	    		'expire_month' => $payment->expire_month, 
	    		'ccv' => $payment->ccv, 
	    		'invoice_id' => $order->id, 
	    		'user_id' => Auth::user()->id, 
	    		'phone_number' => $billingAddress->phone_number, 
	    		'first_name' => $billingAddress->first_name, 
	    		'last_name' => $billingAddress->last_name, 
	    		'zip' => $billingAddress->zip, 
	    		'amount' => $order->charged
	    	);

	    	$transaction_id = Payment::chargeCreditCard($info);
			if($transaction_id == "") {
				DB::rollback();
	        	return $error;
			}
	    	$orderPayment->transaction_id = $transaction_id;
	        
	    } else if(Session::get('payment_type') == "paypal") {
	    	$transaction_id = $this->paypal_service->executePayment();
	    	if($transaction_id == "") {
	    		DB::rollback();
	        	return $error;
	    	}

	    	$orderPayment->transaction_id = $transaction_id;
	    	$orderPayment->payer_email = Session::get('paypal_payer');
	    }


        $orderPayment->save();

        $statusHistory = new OrderStatusHistory;
        $statusHistory->order_id = $order->id;
        $statusHistory->status = 'P';
        $statusHistory->comments = 'Order Created';
        $statusHistory->save();

        Cart::where('user_id', Auth::user()->id)->delete();

        if(Auth::user()->status == '0') {
        	UserAddress::where('user_id', Auth::user()->id)->delete();
        	UserPayment::where('user_id', Auth::user()->id)->delete();
        }

		DB::commit();

		Session::forget('shipping_address');
		//Session::forget('shipping_method');
		Session::forget('shipping_price');
		Session::forget('billing_address');
		Session::forget('payment_type');
		Session::forget('paypal_payment_id');
		Session::forget('paypal_payer');
		Session::forget('payment_id');
		Session::forget('promotion_code');

		if(getenv('APP_ENV') === 'prod' || getenv('APP_ENV') === 'dev') {
			// Send Email only email is confirmed or guest checkout
			dispatch(new OrderConfirmationEmail($order));
		}

		return '';
	}


	public function updateStatus($id, $status) {
		$order = Order::find($id);
		if(empty($order)) {
			return false;
		}

		DB::beginTransaction();

		$statusHistory = new OrderStatusHistory;
        $statusHistory->order_id = $id;
        $statusHistory->status = $status;
		$comments = 'Order Completed';
		$transaction_id = '';
        if($status == 'I') {
        	$comments = 'Order Processing';
        } else if($status == 'R') {
			Log::info("start refund order: ".$order->id. ", payment type: ".$order->payment->payment_type);
        	if($order->payment->payment_type === 'card') {
        		$payment_status = Payment::getTransactionType($order->payment->transaction_id);
        		Log::info("payment status: ".$payment_status);
        		if($payment_status === 'settledSuccessfully') {
					$transaction_id = Payment::refundTransaction($order, $order->charged);
					Log::info("transaction_id: ".$transaction_id);
					if($transaction_id === '') {
						DB::rollback();
			    		return false;
					}

				} else if ($payment_status === 'capturedPendingSettlement') {
					$transaction_id = Payment::voidTransaction($order->payment->transaction_id);
					Log::info("transaction_id: ".$transaction_id);
					if($transaction_id === '') {
						DB::rollback();
			    		return false;
					}
					
				} else {
					DB::rollback();
				    return false;
				}
				
        	} else {
        		$transaction_id = $this->paypal_service->refundSale($order->payment->transaction_id, $order->charged);
        		if($transaction_id === '') {
					DB::rollback();
		    		return false;
				}
        	}

        	$statusHistory->transaction_id = $transaction_id;
			$comments = 'Order Refunded with transaction id: '.$transaction_id;
			$order->refund_total = $order->charged;
			$order->charged = 0;
        }

		
        $statusHistory->comments = $comments;
        $statusHistory->save();

		$order->status = $status;
		$order->save();

		if(getenv('APP_ENV') === 'prod' || getenv('APP_ENV') === 'dev') {
			if($status == 'C') {
				dispatch(new OrderCompleteEmail($order));
                //dispatch(new ReviewRequestEmail($order))->delay(now()->addMinutes(5));
			} else if($status == 'R') {
				dispatch(new OrderRefundEmail($order));
			}
		}

		DB::commit();

		return $order;
	}
}