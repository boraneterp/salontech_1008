<?php
namespace App\Repositories;

use DB;
use Auth;
use App\Models\LookBook;
use App\Models\LookBookStep;

class LookBookRepository extends BaseRepository {

	public function __construct(LookBook $lookBook) {
		$this->model = $lookBook;
	}

	private function queryList() {
		return $this->model->select('*');
	}

	public function getList($inputs = null) {
		$builder = $this->queryList();
		
		if(!empty($inputs['name'])) {
			$builder->where('name', 'like', '%'.$inputs['name'].'%');
		}

		$offset = $inputs['offset'];

		return $this->getRestResponse($builder, $offset);
	}

	public function getFeaturedLookBooks() {
		return $this->model->take(3)->latest()->get();
	}

	public function getOtherLookBooks($id) {
		return $this->model->where('id', '!=', $id)->take(3)->latest()->get();
	}

	public function detail($id = null) {
		return $this->model->where('id', $id)
			->with('steps')->first();
	}

	public function save($inputs) {
		if(array_key_exists("id", $inputs)) {
			$lookbook = $this->model->where('id', $inputs['id'])->first();
		} else {
			$lookbook = new LookBook;
		}

		if(empty($lookbook)) {
			return null;
		}

		DB::beginTransaction();
		$lookbook->name = $inputs['name'];
		$lookbook->seo_url = $this->toSeoUrl($inputs['name']);
		$lookbook->description = $inputs['description'];
		if(array_key_exists("image_src", $inputs) && sizeof($inputs['image_src']) > 0 && is_file($inputs['image_src'][0])) {
			$imageCuts = array(
				'' => 1900, 
				'sm' => 700
			);
			$image_src = $this->upload_to_S3_with_thumbnails($inputs['image_src'][0], 'lookbooks/'.$lookbook->seo_url, null, $imageCuts); 
			$lookbook->image_src = $image_src;
		}
		if (!$lookbook->save()) {
			DB::rollback();
        	return null;
        }

        $keepedSteps = array();
        if(sizeof($inputs['steps']) > 0) {
			foreach($inputs['steps'] as $step) {
				$lookbookStep = null;
				if(array_key_exists("id", $step)) {
					$lookbookStep = LookBookStep::where("id", $step['id'])->where("look_book_id", $lookbook->id)->first();
				}
				$new = false;
				if(empty($lookbookStep)) {
					$lookbookStep = new LookBookStep;
					$new = true;
				}

				$lookbookStep->look_book_id = $lookbook->id;
				if(is_array($step['image'])) {
					$image = $this->upload_to_S3($step['image'][0], 'lookbooks/'.$lookbook->seo_url); 
					$lookbookStep->image = $image;
				}
				$lookbookStep->step_name = $step['step_name'];
				$lookbookStep->description = $step['description'];
				$lookbookStep->category_id = (array_key_exists('category_id', $step) && $step['category_id'] != 'null')? $step['category_id']: 0;
				$lookbookStep->product_id = (array_key_exists('product_id', $step) && $step['product_id'] != 'null')? $step['product_id']: 0;
				if (!$lookbookStep->save()) {
					DB::rollback();
		        	return null;
		        }

		        $keepedSteps[] = $lookbookStep->id;
			}
		}

		LookBookStep::where('look_book_id', $lookbook->id)->whereNotIn('id', $keepedSteps)->delete();

		DB::commit();
		return self::detail($lookbook->id);

	}

	public function getDetailByUrl($url) {
		return $this->model->where('seo_url', $url)->with('steps')->first();
	}
}