<?php
namespace App\Repositories;

use UPSRate;
use Session;
use Util;
use App\Models\Setting;

class ShippingService {

	public function getQuotation($address, $weight) {
        $group = Util::getSettingGroup('general');
		$shipment = new \Ups\Entity\Shipment();

        $shipperAddress = $shipment->getShipper()->getAddress();
        $shipperAddress->setAttentionName('Salon Tech');
        $shipperAddress->setAddressLine1(Util::getSetting($group, 'address_1'));
        $shipperAddress->setAddressLine2(Util::getSetting($group, 'address_2'));
        $shipperAddress->setStateProvinceCode(Util::getSetting($group, 'state'));
        $shipperAddress->setCity(Util::getSetting($group, 'city'));
        $shipperAddress->setCountryCode('US');
        $shipperAddress->setPostalCode(Util::getSetting($group, 'zip_code'));

        $shipTo = $shipment->getShipTo();
        $shipToAddress = $shipTo->getAddress();
        $shipToAddress->setAddressLine1($address->address_1);
        $shipToAddress->setAddressLine2($address->address_2);
        $shipToAddress->setStateProvinceCode($address->state);
        $shipToAddress->setCity($address->city);
        $shipToAddress->setCountryCode('US');
        $shipToAddress->setPostalCode($address->zip);

        $package = new \Ups\Entity\Package();
        $package->getPackagingType()->setCode(\Ups\Entity\PackagingType::PT_PACKAGE);
        $package->getPackageWeight()->setWeight($weight);

        $dimensions = new \Ups\Entity\Dimensions();
        $dimensions->setHeight(10);
        $dimensions->setWidth(10);
        $dimensions->setLength(10);

        $unit = new \Ups\Entity\UnitOfMeasurement;
        $unit->setCode(\Ups\Entity\UnitOfMeasurement::UOM_IN);

        $dimensions->setUnitOfMeasurement($unit);
        $package->setDimensions($dimensions);

        $shipment->addPackage($package);
        $response = array();
        // for($i = 1; $i < 5; $i++) {
        //     if($i === 1) {
        //         $name = "UPS Ground";
        //     } else {
        //         $name = "UPS ".$i;
        //     }
        //     $response[] = array('name' => $name, 
        //             'price' => round($i * $address->id * 5 / 22, 2), 
        //             "label" => $name." - $".round($i * $address->id * 5 / 22, 2));
        // }
 
        try {
            $rates = \UPSRate::shopRates($shipment)->RatedShipment;
            foreach ($rates as $rate) {
                $response[] = array('name' => $rate->getServiceName(), 
                    'price' => $rate->TotalCharges->MonetaryValue, 
                    "label" => $rate->getServiceName()." - $".$rate->TotalCharges->MonetaryValue);
            }
        } catch (Exception $e) {
        }

        return $response;
	}

}