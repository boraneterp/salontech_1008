<?php
namespace App\Repositories;

use DB;
use Auth;
use Session;
use App\Models\Category;
use App\Models\Product;
use App\Models\ProductImage;
use App\Models\ProductOptionGroup;
use App\Models\ProductOption;
use App\Models\ProductReview;

class ProductRepository extends BaseRepository {

	public function __construct(Product $product) {
		$this->model = $product;
	}

	private function queryList() {
		return $this->model->select('*');
	}

	public function getList($inputs) {
		$builder = $this->queryList();
		if(!empty($inputs['category'])) {
			$builder->where('category_id', $inputs['category']);
		}

		if(!empty($inputs['name'])) {
			$builder->where('product_name', 'like', '%'.$inputs['name'].'%');
		}

		if(!empty($inputs['item_no'])) {
			$builder->where('item_no', 'like', '%'.$inputs['item_no'].'%');
		}

		if(!empty($inputs['status'])) {
			$builder->where('status', $inputs['status']);
		}

		$offset = $inputs['offset'];
		$withs = array("category", "images", "reviews.user");

		$orderyBy = array();

		$orderyBy[] = array(
			'orderBy' => 'category_id',
			'dir' => 'desc'
		);

		$orderyBy[] = array(
			'orderBy' => 'order',
			'dir' => 'desc'
		);

		return $this->getRestResponse($builder, $offset, $withs, $orderyBy);
	}

	public function searchProductByName($name) {
		$products = $this->model->where('product_name', 'like', '%'.$name.'%')->get();
		$results = array();
		foreach ($products as $product) {
			$results[] = array(
				'value' => $product->id, 
				'label' => $product->product_name
			);
		}

		return $results;
	}

	public function detail($id) {
		return $this->model->where('id', $id)->with('category', 'images')->first();
	}

	public function save($inputs) {

		DB::beginTransaction();
		if(array_key_exists("id", $inputs)) {
			$product = $this->model->where('id', $inputs['id'])->first();
		} else {
			$product = new Product;
		}
		$product->product_name = $inputs['product_name'];
		$product->item_no = $inputs['item_no'];
		$product->seo_url = array_key_exists("seo_url", $inputs)? $inputs['seo_url'] : $this->toSeoUrl($inputs['product_name']);
		$product->hashtag = (array_key_exists("hashtag", $inputs) && $inputs['hashtag'] != 'null') ? $inputs['hashtag']: '';
		$product->category_id = $inputs['category_id'];
		$product->msrp = $inputs['msrp'];
		$product->price = $inputs['price'];
		$product->professional_only = array_key_exists("professional_only", $inputs) ? $inputs['professional_only']: 'N';
		$product->sale = array_key_exists("sale", $inputs)? $inputs['sale']: 0;
		$product->promotion_start = (array_key_exists("promotion_start", $inputs) && $inputs['promotion_start'] != 'null'  && $inputs['promotion_start'] != '') ? date_create_from_format('Y-m-d', substr($inputs['promotion_start'], 0, 10)): null;
		$product->promotion_end = (array_key_exists("promotion_end", $inputs) && $inputs['promotion_end'] != 'null'  && $inputs['promotion_end'] != '') ? date_create_from_format('Y-m-d', substr($inputs['promotion_end'], 0, 10)): null;
		$product->stock = $inputs['stock'];
		$product->order = $inputs['order'];
		$product->image = '';
		$product->description = array_key_exists("description", $inputs) ? $inputs['description']: '';

		if(array_key_exists("video", $inputs) && sizeof($inputs['video']) > 0 && is_file($inputs['video'][0])) {
			$video = $this->upload_to_S3($inputs['video'][0], 'product/'.$product->seo_url); 
			$product->video = $video;
		} else {
			$product->video = (array_key_exists("video", $inputs) && $inputs['video'] != 'null') ? $inputs['video']: '';
		}

		$product->save();

		// Product Images
		$keepedImages = array();
		if(array_key_exists("images", $inputs) && sizeof($inputs['images']) > 0) {
			$index = 1;
			foreach($inputs['images'] as $image) {
				if(is_array($image)) {
					$keepedImages[] = $image['id'];
					ProductImage::where("id", $image['id'])->update(['order' => $index]);
					$index++;
					continue;
				}

				$productImage = new ProductImage;
				$productImage->product_id = $product->id;
				$imageCuts = array(
					'' => 800, 
					'md' => 500, 
					'sm' => 320, 
					'xs' => 160
				);
				$image = $this->upload_to_S3_with_thumbnails($image, 'product/'.$product->seo_url, null, $imageCuts); 
				$productImage->image_src = $image;
        		$productImage->order = $index;
        		$productImage->save();
        		$keepedImages[] = $productImage->id;
        		$index++;
			}
		}

		ProductImage::where('product_id', $product->id)->whereNotIn('id', $keepedImages)->delete();

		if(sizeof($product->images) > 0) {
			$product->image = $product->images[0]->image_src;
			$product->save();
		}

		DB::commit();
		return $this->detail($product->id);
	}

	public function getProductsByCategory($id) {
		return $this->model->where('category_id', $id)->get();
	}

	public function getGroup($id) {
		return ProductOptionGroup::where('id', $id)->with('options')->first();
	}

	public function saveOption($inputs) {
		DB::beginTransaction();
		$product = $this->model->find($inputs['product_id']);

		$group = ProductOptionGroup::find($inputs['group_id']);
		if(empty($group))
			$group = new ProductOptionGroup;

		$group->product_id = $product->id;
		$group->group_name = $inputs['group_name'];
		$group->is_multi = array_key_exists("is_multi", $inputs)? 'Y': 'N';
		$group->max_option = $inputs['max_option'];
		$group->extra_price = $inputs['extra_price'];
		$group->description = $inputs['description'];
		if (!$group->save()) {
			DB::rollback();
        	return \Redirect::to(\URL::previous())
                ->withInput(\Input::all())
                ->withErrors("Error, Please try again later!");
        }

        $keepedOptions = array();
		if(array_key_exists("option_id", $inputs)) {
			for($n = 0; $n < sizeof($inputs['option_id']); $n++) {
				if(!empty($inputs['option_id'][$n])) {
					$keepedOptions[] = $inputs['option_id'][$n];
				} 
			}
		}

		ProductOption::where('group_id', $group->id)->whereNotIn('id', $keepedOptions)->delete();

		for($j = 0; $j < sizeof($inputs['option_id']); $j++) {
			$option = ProductOption::find($inputs['option_id'][$j]);
			if(empty($option))
				$option = new ProductOption;
			$option->group_id = $group->id;
			$option->option_name = $inputs['option_name'][$j];
			$option->price = $inputs['option_price'][$j];
			$option->order = $j + 1;
			if (!$option->save()) {
				DB::rollback();
	        	return \Redirect::to(\URL::previous())
	                ->withInput(\Input::all())
	                ->withErrors("Error, Please try again later!");
	        }
		}

		DB::commit();
	}

	public function deleteOption($inputs) {
		ProductOption::where('group_id', $inputs['id'])->delete();
		ProductOptionGroup::where('id', $inputs['id'])->delete();
	}

	public function getProductByHashtag($title) {
		$words = explode(" ", $title);
		$hasTags = array();
		foreach ($words as $word) {
			if(starts_with($word, "#") && strlen($word) > 1) {
				$hasTags[] = $word;
			}
		}
		$product = array(
			'name' => '', 
			'image' => '', 
			'url' => ''
		);
		if(sizeof($hasTags) > 0) {
			$p = $this->model->where("status", '1')->where("professional_only", 'N')->whereIn("hashtag", $hasTags)->first();
			if(!empty($p)) {
				$product['name'] = $p->product_name;
				$product['image'] = $p->image;
				$product['url'] = $p->url();
			}
		}
		return $product;
	}

	public function getFeaturedProducts() {
		return $this->model->with('category')->take(8)->latest()->get();
	}

	public function searchCategory($categoryId = null, $inputs) {
		$builder = $this->queryList();
		$builder->where('status', 1)->where('category_id', $categoryId);
		// if(!Auth::check() || Auth::user()->type != "business") {
		// 	$builder->where('professional_only', 'N');
		// }
        
        $order = 'order';
        $direction = 'desc';
        if(!empty($inputs['sort'])) {
        	if($inputs['sort'] === 'best-selling') {
        		$order = 'sold';
        	} else if($inputs['sort'] === 'price-ascending') {
        		$order = 'price';
        		$direction = 'asc';
        	} else if($inputs['sort'] === 'price-descending') {
        		$order = 'price';
        		$direction = 'desc';
        	} else if($inputs['sort'] === 'created-ascending') {
        		$direction = 'asc';
        	} 
        }

		return $builder
			->with('category')
			->orderBy($order, $direction)
			->paginate(24);
	}


	public function searchSecCategory($categoryId = null, $sec_categoryId,$inputs) {
		$builder = $this->queryList();
		$builder->where('status', 1)->where('category_id', $categoryId)->where('sub_category_id', $sec_categoryId);
		
        $order = 'order';
        $direction = 'desc';
        if(!empty($inputs['sort'])) {
        	if($inputs['sort'] === 'best-selling') {
        		$order = 'sold';
        	} else if($inputs['sort'] === 'price-ascending') {
        		$order = 'price';
        		$direction = 'asc';
        	} else if($inputs['sort'] === 'price-descending') {
        		$order = 'price';
        		$direction = 'desc';
        	} else if($inputs['sort'] === 'created-ascending') {
        		$direction = 'asc';
        	} 
        }

		return $builder
			->with('category')
			->with('sec_category')
			->orderBy($order, $direction)
			->paginate(24);
	}





	public function search($inputs) {
		$builder = $this->queryList();
		$builder->where('status', 1);

		if(!empty($inputs['keyword'])) {
			$builder->where('product_name', 'like', '%'.$inputs['keyword'].'%');
		}

		// if(!Auth::check() || Auth::user()->type != "business") {
		// 	$builder->where('professional_only', 'N');
		// }

		$order = 'order';
        $direction = 'desc';
        if(!empty($inputs['sort_by'])) {
        	if($inputs['sort_by'] === 'best-selling') {
        		$order = 'sold';
        	} else if($inputs['sort_by'] === 'price-ascending') {
        		$order = 'price';
        		$direction = 'asc';
        	} else if($inputs['sort_by'] === 'price-descending') {
        		$order = 'price';
        		$direction = 'desc';
        	} else if($inputs['sort_by'] === 'created-ascending') {
        		$direction = 'asc';
        	} 
        }

		return $builder
			->with('category')
			->orderBy($order, $direction)
			->paginate(24);
	}

	public function getSaleProducts($inputs) {
		$builder = $this->queryList();
		$builder->where('status', 1)->where('sale', 'Y');

		$order = 'created_at';
        $direction = 'desc';
        if(!empty($inputs['sort_by'])) {
        	if($inputs['sort_by'] === 'best-selling') {
        		$order = 'sold';
        	} else if($inputs['sort_by'] === 'price-ascending') {
        		$order = 'price';
        		$direction = 'asc';
        	} else if($inputs['sort_by'] === 'price-descending') {
        		$order = 'price';
        		$direction = 'desc';
        	} else if($inputs['sort_by'] === 'created-ascending') {
        		$direction = 'asc';
        	} 
        }

		return $builder
			->with('category', 'groups')
			->orderBy($order, $direction)
			->paginate(100);
	}

	public function getProductByUrl($categoryId, $url) {
		return $this->model->where('seo_url', $url)->where('status', 1)->first();
	}



	public function addViewCount($id) {
		$product = $this->model->find($id);
		$product->view = $product->view + 1;
		$product->save();
	}

	public function deleteProduct($id) {
		$product = $this->getById($id);
		$product->delete();
	}

	public function postReview($inputs) {
		$product = Product::find($inputs['product_id']);
		if(empty($product)) {
			return false;
		}
		$review = new ProductReview;
		$review->user_id = Auth::user()->id;
		$review->product_id = $inputs['product_id'];
		$review->rating = $inputs['rating'];
		$review->content = $inputs['review'];
		$review->save();

		// $results = DB::select('select AVG(rating) as rating, COUNT(rating) as count from product_reviews
		// where product_id = :product_id 
		// and status = 1', 
		// ['product_id' => $product->id]);
		// $product->review_count = $results[0]->count;
		// $product->review_rate = $results[0]->rating;
		//$product->save();
	} 

}