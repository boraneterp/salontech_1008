<?php
namespace App\Repositories;

use DB;
use Auth;
use GeoIP;
use App\Models\Zip;
use App\Models\Store;

class StoreRepository extends BaseRepository {

	public function __construct(Store $store) {
		$this->model = $store;
	}

	private function queryList() {
		return $this->model->select('*');
	}

	public function getList($inputs = null) {
		$builder = $this->queryList();
		if(!empty($inputs['name'])) {
			$builder->where('name', 'like', '%'.$inputs['name'].'%');
		}

		return $builder
			->latest()
			->paginate(50);
	}

	public function save($inputs) {
		$store = $this->getById($inputs['id']);
		$store->name = $inputs['name'];
		$store->distributor = array_key_exists("distributor", $inputs)? 'Y': 'N';
		$store->phone_number = $inputs['phone_number'];
		$store->address_1 = $inputs['address_1'];
		$store->address_2 = $inputs['address_2'];
		$store->city = $inputs['city'];
		$store->state = $inputs['state'];
		$store->zip = $inputs['zip'];
		$store->lat = $inputs['lat'];
		$store->lng = $inputs['lng'];
		$store->save();
	}

	public function saveOrder($inputs) {
		if(array_key_exists("store", $inputs)) {
			for($i = 0; $i < sizeof($inputs['store']); $i++) {
				$store = store::find($inputs['store'][$i]);
				$store->order = ($i + 1);
				$store->save();
			}
		} else if(array_key_exists("sub-store", $inputs)) {
			for($i = 0; $i < sizeof($inputs['sub-store']); $i++) {
				$store = store::find($inputs['sub-store'][$i]);
				$store->order = ($i + 1);
				$store->save();
			}
		} 
	}

	public function getStoreByUrl($url, $first = false) {
		$builder = $this->model->where('seo_url', $url)->where('status', 1);
		if($first) {
			$builder->where('parent_id', 0);
		}
		return $builder->with('children')->first();
	}

	public function getChildren($parentId) {
		return $this->model->where('parent_id', $parentId)->where('status', 1)->orderBy('order')->get();
	}

	public function getStores($requestZip) {
		$lat = 0;
		$lng = 0;
		$zipCode = '';
		if(!empty($requestZip)) {
			$zip = Zip::where('zip', $requestZip)->first();
			if(!empty($zip)) {
				$lat = $zip->lat;
				$lng = $zip->lng;
				$zipCode = $zip->zip;
			}
		} 

		if($lat == 0) {
			$location = GeoIP::getLocation();
			$lat = $location['lat'];
			$lng = $location['lon'];
			$zipCode = $location['postal_code'];
		}

		$salonOnly = ' WHERE distributor = "N" ';
		if(Auth::check() && Auth::user()->status == '1' && Auth::user()->type == 'business') {
			$salonOnly = '';
		}

		$results = DB::select('
			SELECT name, distributor, phone_number, lat, lng, CONCAT(address_1, IF(address_2 IS NOT NULL, CONCAT(" ", address_2), ""), "<br />", city ,", ", state, " ",  zip) as full_address ,ROUND((((acos(sin(('.$lat.'*pi()/180)) * 
			    sin((lat*pi()/180))+cos(('.$lat.'*pi()/180)) * 
			    cos((lat*pi()/180)) * cos((('.$lng.' - lng)* 
			    pi()/180))))*180/pi())*60*1.1515
			), 2) as distance 
			FROM stores'.$salonOnly.'
			ORDER BY distance
			LIMIT 0, 10'
		);

		$response = array(
			'stores' => $results,
			'lat' => $lat,
			'lng' => $lng,
			'zip' => $zipCode
		);

		return $response;
	}
}