<?php
namespace App\Repositories;

use App\Models\Coupon;
use App\Models\CouponProduct;

class CouponRepository extends BaseRepository {

	public function __construct(Coupon $coupon) {
		$this->model = $coupon;
	}

	private function queryList() {
		return $this->model->select('*');
	}

	public function getList($inputs = null) {
		$builder = $this->queryList();
		
		if(!empty($inputs['coupon_code'])) {
			$builder->where('coupon_code', 'like', '%'.$inputs['coupon_code'].'%');
		}

		$offset = $inputs['offset'];

		return $this->getRestResponse($builder, $offset, "products");
	}

	public function save($inputs) {
		if(array_key_exists("id", $inputs)) {
			$coupon = $this->model->where('id', $inputs['id'])->first();
		} else {
			$coupon = new Coupon;
		}
		$coupon->coupon_code = $inputs['coupon_code'];
		$coupon->type = $inputs['type'];
		$coupon->value = $inputs['value'];
		$coupon->count = $inputs['count'];
		$expire_date = date_create_from_format('Y-m-d', substr($inputs['expire_date'], 0, 10));
		$coupon->expire_date = $expire_date;
		$coupon->save();
		if(array_key_exists("id", $inputs)) {
			CouponProduct::where('coupon_id', $coupon->id)->delete();
		}
		if(array_key_exists("products", $inputs)) {
			foreach($inputs['products'] as $product) {
				$couponProduct = new CouponProduct;
				$couponProduct->coupon_id = $coupon->id;
				$couponProduct->product_id = $product['value'];
				$couponProduct->save();
			}
		}
		$builder = $this->model->select('*');
		return $this->getRestResponse($builder, 0, "products");
	}

	public function delete($inputs) {
		$this->destroy($inputs['id']);
		$builder = $this->model->select('*');
		return $this->getRestResponse($builder, 0);
	}
}