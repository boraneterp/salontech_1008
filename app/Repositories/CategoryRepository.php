<?php
namespace App\Repositories;

use App\Models\Category;
use App\Models\Product;
use App\Models\SecCategory;

class CategoryRepository extends BaseRepository {

	public function __construct(Category $category) {
		$this->model = $category;
	}

	private function queryList() {
		return $this->model->select('*');
	}

	public function getList($inputs = null) {
		$builder = $this->queryList();
		if(!empty($inputs['name'])) {
			$builder->where('name', 'like', '%'.$inputs['name'].'%');
		}

		return $builder
			->with('children')
			->orderBy('order')
			->paginate(50);
	}

	public function getDeepList() {
		$builder = $this->queryList();
		return $builder
			->with('products')
			->orderBy('order')
			->paginate(50);
	}

	public function detail($id) {
		return $this->model->where('id', $id)->first();
	}

	public function save($inputs) {
		if(array_key_exists("id", $inputs)) {
			$category = $this->model->where('id', $inputs['id'])->first();
		} else {
			$category = new Category;
		}
		$category->name = $inputs['name'];
		$category->seo_url = $inputs['seo_url'];
		$category->order = $inputs['order'];
		$category->banner_content = array_key_exists('banner_content', $inputs) ? $inputs['banner_content'] : '';
		$category->save();
		return $this->detail($category->id);
	}

	public function saveOrder($inputs) {
		if(array_key_exists("category", $inputs)) {
			for($i = 0; $i < sizeof($inputs['category']); $i++) {
				$category = Category::find($inputs['category'][$i]);
				$category->order = ($i + 1);
				$category->save();
			}
		} else if(array_key_exists("sub-category", $inputs)) {
			for($i = 0; $i < sizeof($inputs['sub-category']); $i++) {
				$category = Category::find($inputs['sub-category'][$i]);
				$category->order = ($i + 1);
				$category->save();
			}
		} 
	}

	public function delete($inputs) {
		$product = Product::where('category_id', $inputs['id'])->first();
		if(!empty($product)){
			return false;
		}
		$this->destroy($inputs['id']);
		return true;
	}

	public function getCategoryByUrl($url, $first = false) {
		$builder = $this->model->where('seo_url', $url)->where('status', 1);
	
		if($first) {
			$builder->where('parent_id', 0);
		}
	
		return $builder->with('children')->first();
	}


	public function getSubCategoryByUr($parentId ,$url, $first = false) {
		
		$builder = SecCategory::where('seo_url', $url)->where('status', 1);
		
		if($first) {
			
			$builder->where('parent_id', $parentId);
		}
		
		return $builder->first();
	}



	public function getChildren($parentId) {
		return $this->model->where('parent_id', $parentId)->where('status', 1)->orderBy('order')->get();
	}
}