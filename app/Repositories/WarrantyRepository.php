<?php
namespace App\Repositories;

use DB;
use Auth;
use App\Models\Warranty;

class WarrantyRepository extends BaseRepository {

	public function __construct(Warranty $warranty) {
		$this->model = $warranty;
	}

	private function queryList() {
		return $this->model->select('*');
	}

	public function getList($inputs = null) {
		$builder = $this->queryList();
		$offset = !empty($inputs['offset'])? $inputs['offset']: 0;

		return $this->getRestResponse($builder, $offset, array("product"));
	}

	public function submitWarranty($inputs) {
		$warranty = new Warranty;
		$warranty->first_name = $inputs['first_name'];
		$warranty->last_name = $inputs['last_name'];
		$warranty->email = $inputs['email'];
		$warranty->salon_name = $inputs['salon_name'];
		$warranty->phone_number = $inputs['phone_number'];
		$warranty->address_1 = $inputs['address_1'];
		$warranty->address_2 = $inputs['address_2'];
		$warranty->city = $inputs['city'];
		$warranty->state = $inputs['state'];
		$warranty->zip = $inputs['zip'];
		$warranty->category_id = $inputs['category_id'];
		$warranty->product_id = $inputs['product_id'];
		if(array_key_exists("proof_of_purchase", $inputs)) {
        	$proof_of_purchase = $this->upload_to_S3($inputs['proof_of_purchase'], 'warranty'); 
        	$warranty->proof_of_purchase = $proof_of_purchase;
        }
		$warranty->date_code = $inputs['date_code'];
		$warranty->reason = $inputs['reason'];
		$warranty->save();
		return true;
	}

	public function updateStatus($id, $status) {
		$warranty = $this->model->find($id);
		if(empty($warranty)) {
			return false;
		}
		
		$warranty->status = $status;
		$warranty->save();
		return true;
	}
	
}