<?php
namespace App\Repositories;

use DB;
use App\Models\Product;
use App\Models\Order;

class ReportRepository extends BaseRepository {

	public function getDashboardReport() {
		$month = DB::table('orders')->where('status', '!=', 'R')->whereMonth('created_at', date('m'))->whereYear('created_at', date('Y'))->sum('charged');
		$total = DB::table('orders')->where('status', '!=', 'R')->sum('charged');
		$count = DB::table('orders')->where('status', '!=', 'R')->count();
		$average = $count > 0? $total / $count: 0;
		$best_selling_products = DB::table('products')->join('categories', 'products.category_id', '=', 'categories.id')->take(5)->select('products.*', 'categories.name as category_name')->orderBy('sold', 'desc')->get();
		$most_viewed_products = DB::table('products')->join('categories', 'products.category_id', '=', 'categories.id')->take(5)->select('products.*', 'categories.name  as category_name')->orderBy('view', 'desc')->get();
		$data = array(
			'lifetime_sales' => $total, 
			'month_sales' => $month, 
			'average_order' => $average, 
			'best_selling_products' => $best_selling_products,
			'most_viewed_products' => $most_viewed_products
		);
		return $data;
	}

	public function getBadges() {
		$order = DB::table('orders')->where('status', 'P')->count();
		$warranty = DB::table('warranties')->where('status', 'P')->count();
		$review = DB::table('product_reviews')->where('status', '0')->count();
		$customer = DB::table('users')->where('type', 'pending')->count();
		$data = array(
			'order' => $order, 
			'warranty' => $warranty,
			'review' => $review,
			'customer' => $customer
		);
		return $data;
	}
	
}