<?php 
namespace App\Repositories;
use DB;
use Image;
use App\CustomCollection;
use \Eventviva\ImageResize;

abstract class BaseRepository {
	/**
	 * The Model instance.
	 *
	 * @var Illuminate\Database\Eloquent\Model
	 */
	protected $model;
	/**
	 * Get number of records.
	 *
	 * @return array
	 */
	public function getNumber()
	{
		$total = $this->model->count();
		$new = $this->model->whereSeen(0)->count();
		return compact('total', 'new');
	}
	/**
	 * Destroy a model.
	 *
	 * @param  int $id
	 * @return void
	 */
	public function destroy($id)
	{
		$this->getById($id)->delete();
	}
	/**
	 * Get Model by id.
	 *
	 * @param  int  $id
	 * @return App\Models\Model
	 */
	public function getById($id) {
		$m = $this->model->find($id);
		if(empty($m))
			return new $this->model;
		return $m;
	}

	public function getRestResponse($builder, $os, $withs = array(), $orderBy = array()) {
		$offset = 0;
		if(!empty($os)) {
			$offset = $os;
		}
		
		$itemPerPage = 30;

		$count = $builder->count();

		if(sizeof($withs) > 0) {
			$builder->with($withs)->offset($offset)->limit($itemPerPage);
		} else {
			$builder->offset($offset)->limit($itemPerPage);
		}

		if(sizeof($orderBy) > 0) {
			foreach ($orderBy as $order) {
				$builder->orderBy($order['orderBy'], $order['dir']);
			}
		} else {
			$builder->latest();
		}

		$data = $builder->get();

		$page_count = 1;
		if($count != 0) {
			$page_count = (int) ($count / $itemPerPage);
			if($count % $itemPerPage != 0) {
				$page_count++;
			}
		}

		$respone = array(
			'page_count' => $page_count,
			'data' => $data
		);

		return $respone;
	}

	public function upload_to_S3($file, $fileFolder = null, $name = null) {
    	$folder = $fileFolder === null? strtolower(str_random(2)): $fileFolder;
    	$fileName = ($name === null? str_random(10): $name).'.'.$file->getClientOriginalExtension();
		$s3 = \Storage::disk('s3');
		$filePath = $folder.'/'.$fileName;
		$s3->put($filePath, file_get_contents($file), 'public');
		return $s3->getDriver()->getAdapter()->getClient()->getObjectUrl(\Config::get('filesystems.disks.s3.bucket'), $filePath);
    }

    public function upload_to_S3_with_thumbnails($file, $fileFolder = null, $name = null, $imageCuts = null) {
    	$folder = $fileFolder === null? strtolower(str_random(2)): $fileFolder;
    	$fileName = ($name === null? str_random(10): $name);
		$filePath = $folder.'/'.$fileName.'.'.$file->getClientOriginalExtension();
		$s3 = \Storage::disk('s3');
		if($imageCuts == null) {
			$s3->put($filePath, file_get_contents($file), 'public');
		} else {
			$img = Image::make($file->getRealPath());
			foreach ($imageCuts as $suffix => $width) {
				if($suffix != '') {
					$uploadPath = $folder.'/'.$fileName.'-'.$suffix.'.'.$file->getClientOriginalExtension();
				} else {
					$uploadPath = $folder.'/'.$fileName.'.'.$file->getClientOriginalExtension();
				}
				
    			if($width > $img->width()) {
					$s3->put($uploadPath, $img->stream()->__toString(), 'public');
				} else {
					$s3->put($uploadPath, (string) ($img->resize($width, null, function ($constraint) {
					    $constraint->aspectRatio();
					    $constraint->upsize();
					})->encode('jpg', 100)), 'public');
				}
			}
		}
		return $s3->getDriver()->getAdapter()->getClient()->getObjectUrl(\Config::get('filesystems.disks.s3.bucket'), $filePath);
    }

    public function toSeoUrl($string) {
    	$accents = array('Š' => 'S', 'š' => 's', 'Ð' => 'Dj','Ž' => 'Z', 'ž' => 'z', 'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'A', 'Å' => 'A', 'Æ' => 'A', 'Ç' => 'C', 'È' => 'E', 'É' => 'E', 'Ê' => 'E', 'Ë' => 'E', 'Ì' => 'I', 'Í' => 'I', 'Î' => 'I', 'Ï' => 'I', 'Ñ' => 'N', 'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O', 'Õ' => 'O', 'Ö' => 'O', 'Ø' => 'O', 'Ù' => 'U', 'Ú' => 'U', 'Û' => 'U', 'Ü' => 'U', 'Ý' => 'Y', 'Þ' => 'B', 'ß' => 'Ss','à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a', 'ä' => 'a', 'å' => 'a', 'æ' => 'a', 'ç' => 'c', 'è' => 'e', 'é' => 'e', 'ê' => 'e', 'ë' => 'e', 'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i', 'ð' => 'o', 'ñ' => 'n', 'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o', 'ö' => 'o', 'ø' => 'o', 'ù' => 'u', 'ú' => 'u', 'û' => 'u', 'ý' => 'y', 'ý' => 'y', 'þ' => 'b', 'ÿ' => 'y', 'ƒ' => 'f');
		$string = strtr($string, $accents);
		$string = strtolower($string);
		$string = preg_replace('/[^a-zA-Z0-9\s]/', '', $string);
		$string = preg_replace('{ +}', ' ', $string);
		$string = trim($string);
		$string = str_replace(' ', '-', $string);
		return $string;
    }

    function distance($lat1, $lon1, $lat2, $lon2) {
		$theta = $lon1 - $lon2;
		$dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
		$dist = acos($dist);
		$dist = rad2deg($dist);
		$miles = $dist * 60 * 1.1515;
		return $miles;
	}

	function getZipsByDistance($zip, $distance = 3) {
		$results = DB::select('SELECT zip
		FROM (
		SELECT
		  *, (
		    3959 * acos (
		      cos ( radians('.$zip->lat.') )
		      * cos( radians( lat ) )
		      * cos( radians( lng ) - radians('.$zip->lng.') )
		      + sin ( radians('.$zip->lat.') )
		      * sin( radians( lat ) )
		    )
		  ) AS distance
		FROM zips
		) A
		where distance < :distance
		order by distance', 
		['distance' => $distance]);
		$resultAry = array();
		foreach ($results as $result) {
			$resultAry[] = $result->zip;
		}

		return $resultAry;
	}

	public function getWeekList() {
		return \App\Models\Week::orderBy('order')->lists("name", "code");
	}

	function validateCreditcard($credit_card_number) {
	    // Get the first digit
	    $firstnumber = substr($credit_card_number, 0, 1);
	    // Make sure it is the correct amount of digits. Account for dashes being present.
	    switch ($firstnumber)
	    {
	        case 3:
	            if (!preg_match('/^3\d{3}[ \-]?\d{6}[ \-]?\d{5}$/', $credit_card_number)) {
	                return false;
	            }
	            break;
	        case 4:
	            if (!preg_match('/^4\d{3}[ \-]?\d{4}[ \-]?\d{4}[ \-]?\d{4}$/', $credit_card_number))
	            {
	                return false;
	            }
	            break;
	        case 5:
	            if (!preg_match('/^5\d{3}[ \-]?\d{4}[ \-]?\d{4}[ \-]?\d{4}$/', $credit_card_number))
	            {
	                return false;
	            }
	            break;
	        case 6:
	            if (!preg_match('/^6011[ \-]?\d{4}[ \-]?\d{4}[ \-]?\d{4}$/', $credit_card_number))
	            {
	                return false;
	            }
	            break;
	        default:
	            return false;
	    }
	    // Here's where we use the Luhn Algorithm
	    $credit_card_number = str_replace('-', '', $credit_card_number);
	    $map = array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9,
	                0, 2, 4, 6, 8, 1, 3, 5, 7, 9);
	    $sum = 0;
	    $last = strlen($credit_card_number) - 1;
	    for ($i = 0; $i <= $last; $i++) {
	        $sum += $map[$credit_card_number[$last - $i] + ($i & 1) * 10];
	    }
	    if ($sum % 10 != 0) {
	        return false;
	    }
	    // If we made it this far the credit card number is in a valid format
	    return true;
	}

 //    public function resizeImage($imageUrl, $imageCuts) {
	// 	$img = Image::make($imageUrl);
	// 	$pos = strripos($imageUrl, ".");
	// 	$ratio = $img->height() / $img->width();
	// 	echo "Start processing ".$imageUrl."\n";
	// 	$s3 = \Storage::disk('s3');
	// 	foreach ($imageCuts as $suffix => $width) {
	// 		$fileName = str_replace("https://salontech.s3.amazonaws.com/", "", substr($imageUrl, 0, $pos));
	// 		if($suffix == '') {
	// 			$fileName .= substr($imageUrl, $pos);
	// 		} else {
	// 			$fileName .= '-'.$suffix.substr($imageUrl, $pos);
	// 		}

	// 		echo "Resize to file name: ".$fileName." with size: ".$width." x ". (int)($width * $ratio)."\n"; 
			
	// 		if($width > $img->width()) {
	// 			$s3->put($fileName, $img->stream()->__toString(), 'public');
	// 		} else {
	// 			$s3->put($fileName, (string) ($img->resize($width, null, function ($constraint) {
	// 			    $constraint->aspectRatio();
	// 			    $constraint->upsize();
	// 			})->encode()), 'public');
	// 		}
	// 	}
	// }

	// public function migration() {
	// 	$imageCuts = array(
	// 		'' => 800, 
	// 		'md' => 500, 
	// 		'sm' => 320, 
	// 		'xs' => 160
	// 	);
	// 	$products = \App\Models\ProductImage::get();
	// 	foreach ($products as $product) {
	// 		$this->resizeImage(str_replace("salontech-dev", "salontech", $product->image_src), $imageCuts);
	// 	}
	// }
}

