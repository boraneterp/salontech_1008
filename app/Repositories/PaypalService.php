<?php
namespace App\Repositories;

use Paypal;
use Session;
use Util;
use Log;
use App\Models\Order;

class PaypalService {

	private $_apiContext;

	public function __construct() {
		$this->_apiContext = Paypal::ApiContext(
            config('services.paypal.client_id'),
            config('services.paypal.secret'));

        $this->_apiContext->setConfig(array(
            'mode' => getenv('PAYPAL_MODE'),
            'service.EndPoint' => getenv('PAYPAL_ENDPOINT'),
            'http.ConnectionTimeOut' => 30,
            'log.LogEnabled' => true,
            'log.FileName' => storage_path('logs/paypal.log'),
            'log.LogLevel' => 'FINE'
        ));
	}

	public function getRedirectUrl($cart, $address) {
		$payer = Paypal::Payer();
        $payer->setPaymentMethod('paypal');
        $group = Util::getSettingGroup('general');
        $shipping_price = $cart['content']['sub_total'] < Util::getSetting($group, 'free_shipping_amount') ? Util::getSetting($group, 'flat_fee') : 0;
        $details = Paypal::Details();
        $details->setSubtotal($cart['content']['sub_total'] - $cart['content']['promotion']);
        $details->setTax($cart['content']['tax']);
        $details->setShipping($shipping_price);
        $amount = Paypal::Amount();
        $amount->setCurrency('USD');
        $amount->setTotal($cart['content']['total']); 
        $amount->setDetails($details); 

        $itemList = Paypal::ItemList();
        foreach($cart['content']['items'] as $itm) {
        	$item = Paypal::Item();
        	$item->setName($itm['product']['product_name']);
        	$item->setQuantity($itm['quantity']);
        	$item->setPrice($itm['product']['current_price']);
        	$item->setTax($itm['tax']);
        	$item->setCurrency('USD');
        	$itemList->addItem($item);
        }

        if($cart['content']['promotion'] > 0) {
            $item = Paypal::Item();
            $item->setName('Discount');
            $item->setQuantity(1);
            $item->setPrice($cart['content']['promotion'] * (-1));
            $item->setCurrency('USD');
            $itemList->addItem($item);
        }

        $shippingAddress = Paypal::ShippingAddress();
        $shippingAddress->setRecipientName($address->first_name." ".$address->last_name);
        $shippingAddress->setLine1($address->address_1);
        $shippingAddress->setLine2($address->address_2);
        $shippingAddress->setCity($address->city);
        $shippingAddress->setCountryCode('US');
        $shippingAddress->setPostalCode($address->zip);
        $shippingAddress->setPhone($address->phone_number);
        $shippingAddress->setState($address->state);
        
        $itemList->setShippingAddress($shippingAddress);

        $transaction = Paypal::Transaction();
        $transaction->setAmount($amount);
        $transaction->setDescription('Salon Tech Purchase');
        $transaction->setItemList($itemList);

        $redirectUrls = Paypal::RedirectUrls();
        $redirectUrls->setReturnUrl(getenv('APP_URL').'/checkout/review');
        $redirectUrls->setCancelUrl(getenv('APP_URL').'/checkout/cart');

        $payment = Paypal::Payment();
        $payment->setIntent('sale');
        $payment->setPayer($payer);
        $payment->setRedirectUrls($redirectUrls);
        $payment->setTransactions(array($transaction));

        try {
            $response = $payment->create($this->_apiContext);
        } catch (Exception $ex) {
            Log::error($ex);
            return $transaction_id;
        }

        
        $redirectUrl = $response->links[1]->href;
        return $redirectUrl;
	}


	public function setPaypalPayment($id) {
        $payment = PayPal::getById($id, $this->_apiContext);
        Session::put('payment_type', 'paypal');
        Session::put('paypal_payment_id', $id);
        Session::put('paypal_payer', $payment->getPayer()->getPayerInfo()->getEmail());
	}

	public function executePayment() {
        Log::info("Execute payment start");
        $transaction_id = "";
		$payment = PayPal::getById(Session::get('paypal_payment_id'), $this->_apiContext);
		$payer_id = $payment->getPayer()->getPayerInfo()->getPayerId();
		$paymentExecution = PayPal::PaymentExecution();
		$paymentExecution->setPayerId($payer_id);
		try {
		    $executePayment = $payment->execute($paymentExecution, $this->_apiContext);
		} catch (PayPal\Exception\PayPalConnectionException $ex) {
		    return $transaction_id;
		} 

        \Log::info($executePayment);
        if("approved" == $executePayment->getState()) {
            $transactions = $executePayment->getTransactions();
            $transaction = $transactions[0];
            $relatedResources = $transaction->getRelatedResources();
            $relatedResource = $relatedResources[0];
            $transaction_id = $relatedResource->getSale()->getId();
        }

        Log::info("Execute payment end");
		return $transaction_id;
	}

    public function refundSale($sale_id) {
        Log::info("refundSale start");
        $transaction_id = "";

        $sale = Paypal::Sale();
        $sale->setId($sale_id);
        try {
            $refund = $sale->refund(Paypal::Refund(), $this->_apiContext);
        } catch (Exception $ex) {
            Log::error($ex);
            return $transaction_id;
        }

        \Log::info($refund);
        $transaction_id = $refund->getId();
        Log::info("Execute payment end");
        return $transaction_id;
    }
}