<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ReviewRequestEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'review:email';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Request for review about 2 weeks from the date an order was completed';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //

        /*
        $targetUsers = DB::table('orders')
            ->whereColumn([
                ['status','=','C'],
                ['updated_at','=','DATE_SUB(CURDATE(), INTERVAL 1 DAY)']
            ])
            ->get();
        */

        $targetUsers = DB::select("SELECT * FROM orders WHERE updated_at BETWEEN DATE_SUB(CURRENT_TIMESTAMP,INTERVAL 1 DAY) AND CURRENT_TIMESTAMP");

        Mail::send(['html'=>'emails.reviewRequest'],array('$users' => $targetUsers),function ($message)
        {
            $message->from('salontech@salontech.com', 'Salontech Team');
            $message->to('$user')->subject('What do you think of the latest purchase?');
        });
        var_dump( Mail:: failures());
        exit;

    }
}
